package ch.ethz.matsim.projects.astra_2018_002.analysis.flow;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.Scenario;
import org.matsim.api.core.v01.network.Link;
import org.matsim.core.api.experimental.events.EventsManager;
import org.matsim.core.config.Config;
import org.matsim.core.config.ConfigUtils;
import org.matsim.core.events.EventsUtils;
import org.matsim.core.events.MatsimEventsReader;
import org.matsim.core.network.io.MatsimNetworkReader;
import org.matsim.core.scenario.ScenarioUtils;
import org.matsim.core.utils.geometry.geotools.MGC;
import org.matsim.core.utils.gis.PolylineFeatureFactory;
import org.matsim.core.utils.gis.ShapeFileWriter;
import org.matsim.core.utils.misc.Time;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.referencing.crs.CoordinateReferenceSystem;

import com.vividsolutions.jts.geom.Coordinate;

import ch.ethz.matsim.baseline_scenario.config.CommandLine;
import ch.ethz.matsim.baseline_scenario.config.CommandLine.ConfigurationException;
import ch.ethz.matsim.projects.astra_2018_002.analysis.flow.FlowListener.CountItem;
import ch.ethz.matsim.projects.astra_2018_002.flow_efficiency.AvFlowEfficiencyCalculator;
import ch.ethz.matsim.projects.astra_2018_002.flow_efficiency.FlowEfficiencyCalculator;
import ch.ethz.matsim.projects.astra_2018_002.flow_efficiency.FlowEfficiencyConfigurator;
import ch.ethz.matsim.projects.astra_2018_002.flow_efficiency.FlowEfficiencyDefinition;

public class RunFlowAnalysis {
	static public void main(String[] args) throws ConfigurationException, IOException {
		CommandLine cmd = new CommandLine.Builder(args) //
				.requireOptions("network-path", "events-path", "output-path", "year") //
				.allowOptions("osm-types", "link-ids", "start-time", "end-time", "interval", "flow-efficiency-path").build();

		Config config = ConfigUtils.createConfig();
		Scenario scenario = ScenarioUtils.createScenario(config);

		new MatsimNetworkReader(scenario.getNetwork()).readFile(cmd.getOptionStrict("network-path"));
		FlowEfficiencyConfigurator.defineVehicleTypes(scenario, Integer.parseInt(cmd.getOptionStrict("year")));

		FlowEfficiencyDefinition definition = new FlowEfficiencyDefinition();
		
		if (cmd.hasOption("flow-efficiency-path")) {
			definition = FlowEfficiencyDefinition.fromFile(new File(cmd.getOptionStrict("flow-efficiency-path")));
		}
		
		FlowEfficiencyCalculator flowEfficiencyCalculator = new AvFlowEfficiencyCalculator(definition);

		double startTime = cmd.getOption("start-time").map(Time::parseTime).orElse(5.0 * 3600);
		double endTime = cmd.getOption("end-time").map(Time::parseTime).orElse(22.0 * 3600);
		double interval = cmd.getOption("interval").map(Time::parseTime).orElse(3600.0);

		List<Link> links = new LinkedList<>();

		Set<String> osmTypes = new HashSet<>();

		if (cmd.hasOption("osm-types")) {
			String[] rawTypes = cmd.getOption("osm-types").get().split(",");

			for (String rawType : rawTypes) {
				osmTypes.add(rawType.trim());
			}
		}

		Set<Id<Link>> linkIds = new HashSet<>();

		if (cmd.hasOption("link-ids")) {
			String[] rawIds = cmd.getOption("link-ids").get().split(",");

			for (String rawId : rawIds) {
				linkIds.add(Id.createLinkId(rawId));
			}
		}

		for (Link link : scenario.getNetwork().getLinks().values()) {
			String osmType = (String) link.getAttributes().getAttribute("osm:way:highway");

			if ((osmType != null && osmTypes.contains(osmType)) || linkIds.contains(link.getId())) {
				links.add(link);
			}
		}
		
		if (links.size() == 0) {
			links.addAll(scenario.getNetwork().getLinks().values());
		}

		FlowListener flowListener = new FlowListener(links, startTime, endTime, interval, flowEfficiencyCalculator,
				scenario.getVehicles());

		EventsManager eventsManager = EventsUtils.createEventsManager();
		eventsManager.addHandler(flowListener);
		new MatsimEventsReader(eventsManager).readFile(cmd.getOptionStrict("events-path"));

		CoordinateReferenceSystem crs = MGC.getCRS("EPSG:2056");

		int morningPeakIndex = 7 - 5;
		int eveningPeakIndex = 17 - 5;
		
		Collection<SimpleFeature> features = new ArrayList<>(links.size());
		PolylineFeatureFactory featureFactory = new PolylineFeatureFactory.Builder() //
				.setCrs(crs) //
				.setName("links") //
				.addAttribute("link_id", String.class) //
				.addAttribute("capacity", Double.class) //
				.addAttribute("freespeed", Double.class) //
				.addAttribute("lanes", Double.class) //
				.addAttribute("osm_type", String.class) //
				.addAttribute("av_op", Integer.class) //
				//
				.addAttribute("day_priv", Integer.class) //
				.addAttribute("day_pool", Integer.class) //
				.addAttribute("day_taxi", Integer.class) //
				.addAttribute("day_conv", Integer.class) //
				.addAttribute("day_truck", Integer.class) //
				.addAttribute("day_pass5", Integer.class) //
				.addAttribute("day_pass4", Integer.class) //
				.addAttribute("day_pass3", Integer.class) //
				.addAttribute("day_truck5", Integer.class) //
				.addAttribute("day_pcu", Double.class) //
				.addAttribute("day_pcut", Double.class) //
				.addAttribute("day_pcup", Double.class) //
				//
				.addAttribute("am_priv", Integer.class) //
				.addAttribute("am_pool", Integer.class) //
				.addAttribute("am_taxi", Integer.class) //
				.addAttribute("am_conv", Integer.class) //
				.addAttribute("am_truck", Integer.class) //
				.addAttribute("am_pass5", Integer.class) //
				.addAttribute("am_pass4", Integer.class) //
				.addAttribute("am_pass3", Integer.class) //
				.addAttribute("am_truck5", Integer.class) //
				.addAttribute("am_pcu", Double.class) //
				.addAttribute("am_pcut", Double.class) //
				.addAttribute("am_pcup", Double.class) //
				//
				.addAttribute("pm_priv", Integer.class) //
				.addAttribute("pm_pool", Integer.class) //
				.addAttribute("pm_taxi", Integer.class) //
				.addAttribute("pm_conv", Integer.class) //
				.addAttribute("pm_truck", Integer.class) //
				.addAttribute("pm_pass5", Integer.class) //
				.addAttribute("pm_pass4", Integer.class) //
				.addAttribute("pm_pass3", Integer.class) //
				.addAttribute("pm_truck5", Integer.class) //
				.addAttribute("pm_pcu", Double.class) //
				.addAttribute("pm_pcut", Double.class) //
				.addAttribute("pm_pcup", Double.class) //
				//
				.create();

		for (Link link : links) {
			CountItem totalItem = flowListener.getTotalItem(link.getId());
			CountItem morningItem = flowListener.getTimeItems(link.getId())[morningPeakIndex];
			CountItem eveningItem = flowListener.getTimeItems(link.getId())[eveningPeakIndex];

			Coordinate fromCoord = new Coordinate(link.getFromNode().getCoord().getX(),
					link.getFromNode().getCoord().getY());
			Coordinate toCoord = new Coordinate(link.getToNode().getCoord().getX(), link.getToNode().getCoord().getY());

			String osmType = (String) link.getAttributes().getAttribute("osm:way:highway");

			if (osmType == null) {
				osmType = "unknown";
			}
			
			Boolean avOperation = (Boolean) link.getAttributes().getAttribute("avOperation");
			
			SimpleFeature feature = featureFactory.createPolyline(new Coordinate[] { fromCoord, toCoord },
					new Object[] { //
							link.getId().toString(), //
							link.getCapacity(), //
							link.getFreespeed(), //
							link.getNumberOfLanes(), //
							osmType, //
							(avOperation != null && avOperation) ? 1 : 0, //
							//
							totalItem.privateVehicles, //
							totalItem.pooledVehicles, //
							totalItem.taxiVehicles, //
							totalItem.conventionalVehicles, //
							totalItem.truckVehicles, //
							totalItem.level5Vehicles, //
							totalItem.level4Vehicles, //
							totalItem.level3Vehicles, //
							totalItem.automatedTrucks, //
							totalItem.pcusAll, //
							totalItem.pcusTrucks, //
							totalItem.pcusVehicles, //
							//
							morningItem.privateVehicles, //
							morningItem.pooledVehicles, //
							morningItem.taxiVehicles, //
							morningItem.conventionalVehicles, //
							morningItem.truckVehicles, //
							morningItem.level5Vehicles, //
							morningItem.level4Vehicles, //
							morningItem.level3Vehicles, //
							morningItem.automatedTrucks, //
							morningItem.pcusAll, //
							morningItem.pcusTrucks, //
							morningItem.pcusVehicles, //
							//
							eveningItem.privateVehicles, //
							eveningItem.pooledVehicles, //
							eveningItem.taxiVehicles, //
							eveningItem.conventionalVehicles, //
							eveningItem.truckVehicles, //
							eveningItem.level5Vehicles, //
							eveningItem.level4Vehicles, //
							eveningItem.level3Vehicles, //
							eveningItem.automatedTrucks, //
							eveningItem.pcusAll, //
							eveningItem.pcusTrucks, //
							eveningItem.pcusVehicles, //
					//
					}, null);
			features.add(feature);
		}

		ShapeFileWriter.writeGeometries(features, cmd.getOptionStrict("output-path"));
	}
}

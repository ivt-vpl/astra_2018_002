package ch.ethz.matsim.projects.astra_2018_002.link_assignment;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Function;

import org.apache.log4j.Logger;
import org.geotools.data.DataStore;
import org.geotools.data.DataStoreFinder;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureIterator;
import org.geotools.data.simple.SimpleFeatureSource;
import org.matsim.api.core.v01.Coord;
import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.network.Link;
import org.matsim.api.core.v01.network.Network;
import org.matsim.api.core.v01.population.Person;
import org.matsim.core.network.NetworkUtils;
import org.matsim.core.network.algorithms.NetworkCleaner;
import org.matsim.core.network.algorithms.TransportModeNetworkFilter;
import org.matsim.core.network.io.MatsimNetworkReader;
import org.matsim.core.network.io.NetworkWriter;
import org.matsim.core.router.DijkstraFactory;
import org.matsim.core.router.util.LeastCostPathCalculator;
import org.matsim.core.router.util.LeastCostPathCalculator.Path;
import org.matsim.core.router.util.TravelDisutility;
import org.matsim.core.router.util.TravelTime;
import org.matsim.core.trafficmonitoring.FreeSpeedTravelTime;
import org.matsim.core.utils.collections.QuadTree;
import org.matsim.vehicles.Vehicle;
import org.opengis.feature.simple.SimpleFeature;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.MultiPolygon;
import com.vividsolutions.jts.geom.Point;

import ch.ethz.matsim.baseline_scenario.config.CommandLine;
import ch.ethz.matsim.baseline_scenario.config.CommandLine.ConfigurationException;

public class AVLinkAssignment {
	private final Logger logger = Logger.getLogger(AVLinkAssignment.class);

	private final GeometryFactory geometryFactory = new GeometryFactory();

	private final Network network;

	private final Collection<Link> allowedLinks = new HashSet<>();
	private final QuadTree<Link> index;
	private final List<Coord> centroids = new LinkedList<>();

	private Collection<Id<Link>> linkIdsCache = null;

	public AVLinkAssignment(Network network) {
		this.network = NetworkUtils.createNetwork();

		new TransportModeNetworkFilter(network).filter(this.network, Collections.singleton("car"));
		new NetworkCleaner().run(this.network);

		double[] dimensions = NetworkUtils.getBoundingBox(network.getNodes().values());
		this.index = new QuadTree<>(dimensions[0], dimensions[1], dimensions[2], dimensions[3]);
	}

	public void allowLink(Link link) {
		linkIdsCache = null;

		if (!allowedLinks.contains(link)) {
			index.put(link.getCoord().getX(), link.getCoord().getY(), link);
		}

		allowedLinks.add(link);
	}

	public void allowOsmType(String osmType) {
		for (Link link : network.getLinks().values()) {
			String linkOsmType = (String) link.getAttributes().getAttribute("osm:way:highway");

			if (linkOsmType != null && linkOsmType.equals(osmType)) {
				allowLink(link);
			}
		}
	}

	public void allowAll() {
		for (Link link : network.getLinks().values()) {
			allowLink(link);
		}
	}

	public void allowIf(Function<Link, Boolean> filter) {
		for (Link link : network.getLinks().values()) {
			if (filter.apply(link)) {
				allowLink(link);
			}
		}
	}

	public void allowArea(URL url) {
		try {
			DataStore dataStore = DataStoreFinder.getDataStore(Collections.singletonMap("url", url));

			SimpleFeatureSource featureSource = dataStore.getFeatureSource(dataStore.getTypeNames()[0]);
			SimpleFeatureCollection featureCollection = featureSource.getFeatures();
			SimpleFeatureIterator featureIterator = featureCollection.features();

			while (featureIterator.hasNext()) {
				SimpleFeature feature = featureIterator.next();
				Geometry geometry = (Geometry) feature.getDefaultGeometry();

				if (geometry instanceof MultiPolygon) {
					for (Link link : network.getLinks().values()) {
						Coord linkCoord = link.getCoord();
						Coordinate linkCoordinate = new Coordinate(linkCoord.getX(), linkCoord.getY());
						Point linkPoint = geometryFactory.createPoint(linkCoordinate);

						if (geometry.contains(linkPoint)) {
							allowLink(link);
						}
					}
				} else {
					throw new IllegalStateException("Expected a shape file with polygons.");
				}

				centroids.add(new Coord(geometry.getCentroid().getX(), geometry.getCentroid().getY()));
			}

			featureIterator.close();
			dataStore.dispose();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	private boolean hasAllowedUpstream(Link link) {
		for (Link inLink : link.getFromNode().getInLinks().values()) {
			if (inLink != link && allowedLinks.contains(inLink)) {
				return true;
			}
		}

		return false;
	}

	private boolean hasAllowedDownstream(Link link) {
		for (Link outLink : link.getToNode().getOutLinks().values()) {
			if (outLink != link && allowedLinks.contains(outLink)) {
				return true;
			}
		}

		return false;
	}

	public Collection<Id<Link>> getLinkIds() throws InterruptedException {
		TravelTime travelTime = new FreeSpeedTravelTime();

		TravelDisutility travelDisutility = new TravelDisutility() {
			@Override
			public double getLinkTravelDisutility(Link link, double time, Person person, Vehicle vehicle) {
				return getLinkMinimumTravelDisutility(link) + (allowedLinks.contains(link) ? 1.0 : 1000.0);
			}

			@Override
			public double getLinkMinimumTravelDisutility(Link link) {
				return link.getLength() / link.getFreespeed();
			}
		};

		Set<Link> result = new HashSet<>();
		result.addAll(allowedLinks);

		// Make sure there is a connection between all centroids
		LeastCostPathCalculator router = new DijkstraFactory().createPathCalculator(this.network, travelDisutility,
				travelTime);

		if (centroids.size() > 0) {
			for (Coord a : centroids) {
				for (Coord b : centroids) {
					Link linkA = index.getClosest(a.getX(), a.getY());
					Link linkB = index.getClosest(b.getX(), b.getY());

					Path path = router.calcLeastCostPath(linkA.getToNode(), linkB.getFromNode(), 0.0, null, null);

					for (Link link : path.links) {
						result.add(link);
					}
				}
			}
		}

		// Find islands and connect them
		logger.info("Finding islands ...");

		List<Link> islands = new LinkedList<>();
		Set<Link> remaining = new HashSet<>(result);

		while (remaining.size() > 0) {
			List<Link> pending = new LinkedList<>();

			Link islandLink = remaining.iterator().next();
			pending.add(islandLink);

			while (pending.size() > 0) {
				Link link = pending.remove(0);
				remaining.remove(link);

				for (Link downstreamLink : link.getToNode().getOutLinks().values()) {
					if (remaining.contains(downstreamLink)) {
						pending.add(downstreamLink);
					}
				}
			}

			pending.add(islandLink);

			while (pending.size() > 0) {
				Link link = pending.remove(0);
				remaining.remove(link);

				for (Link upstreamLink : link.getFromNode().getInLinks().values()) {
					if (remaining.contains(upstreamLink)) {
						pending.add(upstreamLink);
					}
				}
			}

			islands.add(islandLink);
		}

		logger.info(String.format("Found %d islands!", islands.size()));
		logger.info(String.format("Routing islands ..."));

		// Make sure there is a connection from every island to some reference link
		Link referenceLink = null;

		for (Link link : allowedLinks) {
			if (hasAllowedDownstream(link) && hasAllowedUpstream(link)) {
				referenceLink = link;
				break;
			}
		}

		long numberOfIslands = islands.size();
		AtomicLong numberOfProcessedIslands = new AtomicLong(0);

		Iterator<Link> iterator = islands.iterator();
		List<Thread> threads = new LinkedList<>();

		final Link finalReferenceLink = referenceLink;

		int batchSize = 40;

		for (int i = 0; i < Runtime.getRuntime().availableProcessors(); i++) {
			Thread thread = new Thread(() -> {
				LeastCostPathCalculator localRouter = new DijkstraFactory().createPathCalculator(this.network,
						travelDisutility, travelTime);
				Set<Link> localResult = new HashSet<>();

				while (true) {
					List<Link> localLinks = new LinkedList<>();

					synchronized (iterator) {
						while (iterator.hasNext() && localLinks.size() < batchSize) {
							localLinks.add(iterator.next());
						}
					}

					for (Link link : localLinks) {
						Path path1 = localRouter.calcLeastCostPath(link.getToNode(), finalReferenceLink.getFromNode(),
								0.0, null, null);
						Path path2 = localRouter.calcLeastCostPath(finalReferenceLink.getToNode(), link.getFromNode(),
								0.0, null, null);

						localResult.addAll(path1.links);
						localResult.addAll(path2.links);
					}

					long current = numberOfProcessedIslands.addAndGet(localLinks.size());
					logger.info(String.format("Processed island %d/%d ...", current, numberOfIslands));

					if (localLinks.size() == 0) {
						break;
					}
				}

				synchronized (result) {
					result.addAll(localResult);
				}
			});

			thread.start();
			threads.add(thread);
		}

		for (Thread thread : threads) {
			thread.join();
		}

		logger.info(String.format("Routing done! Added %d new links.", result.size()));

		// Add tags
		logger.info(String.format("Creating cleaned network ..."));

		for (Link link : result) {
			Set<String> allowedModes = new HashSet<>(link.getAllowedModes());
			allowedModes.add("___newmode___");
			link.setAllowedModes(allowedModes);
		}

		Network newNetwork = NetworkUtils.createNetwork();
		new TransportModeNetworkFilter(network).filter(newNetwork, Collections.singleton("___newmode___"));
		new NetworkCleaner().run(newNetwork);

		Set<Id<Link>> remainingIds = new HashSet<>();

		for (Link link : newNetwork.getLinks().values()) {
			if (link.getAllowedModes().contains("___newmode___")) {
				remainingIds.add(link.getId());
			}
		}

		for (Link link : result) {
			Set<String> allowedModes = new HashSet<>(link.getAllowedModes());
			allowedModes.remove("___newmode___");
			link.setAllowedModes(allowedModes);
		}

		logger.info(String.format("Cleaning network done!"));

		return remainingIds;
	}

	public void apply(Network network) throws InterruptedException {
		if (linkIdsCache == null) {
			linkIdsCache = getLinkIds();
		}

		logger.info(String.format("Setting attribute ..."));
		for (Id<Link> linkId : linkIdsCache) {
			Link link = network.getLinks().get(linkId);
			link.getAttributes().putAttribute("avOperation", true);
		}
		logger.info(String.format("Setting attribute done!"));
	}

	static public void main(String[] args) throws MalformedURLException, ConfigurationException, InterruptedException {
		CommandLine cmd = new CommandLine.Builder(args) //
				.requireOptions("input-path", "output-path", "cities-shapefile", "year") //
				.build();

		Network network = NetworkUtils.createNetwork();
		new MatsimNetworkReader(network).readFile(cmd.getOptionStrict("input-path"));

		int year = Integer.parseInt(cmd.getOptionStrict("year"));
		AVLinkAssignment assignment = new AVLinkAssignment(network);

		if (year >= 2030) {
			assignment.allowArea(new File(cmd.getOptionStrict("cities-shapefile")).toURI().toURL());

			assignment.allowOsmType("motorway");
			assignment.allowOsmType("motorway_link");
			assignment.allowOsmType("trunk");
			assignment.allowOsmType("trunk_link");
		}

		if (year >= 2040) {
			assignment.allowOsmType("primary");
			assignment.allowOsmType("primary_link");

			assignment.allowIf(link -> link.getFreespeed() <= 13.8);
		}

		if (year >= 2050) {
			assignment.allowAll();
		}

		// Add attribute
		assignment.apply(network);

		// Add prav3, car_passenger and truck to every link
		for (Link link : network.getLinks().values()) {
			if (link.getAllowedModes().contains("car")) {
				Set<String> allowedModes = new HashSet<>(link.getAllowedModes());
				allowedModes.addAll(Arrays.asList("prav3", "prav4", "prav5", "car_passenger", "truck"));

				Boolean avOperation = (Boolean) link.getAttributes().getAttribute("avOperation");
				if (avOperation != null && avOperation) {
					allowedModes.add("av");
				}

				link.setAllowedModes(allowedModes);
			}
		}

		new NetworkWriter(network).write(cmd.getOptionStrict("output-path"));
	}
}

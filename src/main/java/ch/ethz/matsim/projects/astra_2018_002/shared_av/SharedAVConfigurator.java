package ch.ethz.matsim.projects.astra_2018_002.shared_av;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.matsim.contrib.dvrp.run.DvrpConfigGroup;
import org.matsim.contrib.dvrp.trafficmonitoring.DvrpTravelTimeModule;
import org.matsim.core.config.Config;
import org.matsim.core.config.groups.PlanCalcScoreConfigGroup.ModeParams;
import org.matsim.core.controler.Controler;

import ch.ethz.matsim.av.framework.AVConfigGroup;
import ch.ethz.matsim.av.framework.AVModule;
import ch.ethz.matsim.discrete_mode_choice.modules.config.DiscreteModeChoiceConfigGroup;
import ch.ethz.matsim.projects.astra_2018_002.ASTRAQSimModule;
import ch.ethz.matsim.projects.astra_2018_002.shared_av.pricing.AVPricingModule;

public class SharedAVConfigurator {
	static public void configure(Config config) {
		DvrpConfigGroup dvrpConfig = (DvrpConfigGroup) config.getModules().get(DvrpConfigGroup.GROUP_NAME);

		if (dvrpConfig == null) {
			dvrpConfig = new DvrpConfigGroup();
			config.addModule(dvrpConfig);
		}

		AVConfigGroup avConfig = (AVConfigGroup) config.getModules().get("av");

		if (avConfig == null) {
			avConfig = new AVConfigGroup();
			config.addModule(avConfig);
			avConfig.setParallelRouters(config.global().getNumberOfThreads());
		}

		SharedAVConfigGroup sharedConfig = (SharedAVConfigGroup) config.getModules()
				.get(SharedAVConfigGroup.GROUP_NAME);

		if (sharedConfig == null) {
			sharedConfig = new SharedAVConfigGroup();
			config.addModule(sharedConfig);
		}

		if (sharedConfig.getUseSharedAVs()) {
			ModeParams avParams = new ModeParams(AVModule.AV_MODE);
			config.planCalcScore().addModeParams(avParams);

			DiscreteModeChoiceConfigGroup dmcConfig = (DiscreteModeChoiceConfigGroup) config.getModules()
					.get(DiscreteModeChoiceConfigGroup.GROUP_NAME);

			Set<String> availableModes = new HashSet<>(dmcConfig.getCarModeAvailabilityConfig().getAvailableModes());
			Set<String> cachedModes = new HashSet<>(dmcConfig.getCachedModes());

			if (sharedConfig.getTaxiFleetSize() > 0) {
				availableModes.add(SharedAVModule.AV_TAXI);
				cachedModes.add(SharedAVModule.AV_TAXI);
			}

			if (sharedConfig.getPooledFleetSize() > 0) {
				availableModes.add(SharedAVModule.AV_POOL);
				cachedModes.add(SharedAVModule.AV_POOL);
			}

			Collection<String> tripConstraints = new HashSet<>(dmcConfig.getTripConstraints());
			tripConstraints.add(SharedAVModule.CONSTRAINT_NAME);
			dmcConfig.setTripConstraints(tripConstraints);

			dmcConfig.getCarModeAvailabilityConfig().setAvailableModes(availableModes);
			dmcConfig.setCachedModes(cachedModes);
		}
	}

	static public void configureController(Controler controller) {
		SharedAVConfigGroup sharedConfig = (SharedAVConfigGroup) controller.getConfig().getModules()
				.get(SharedAVConfigGroup.GROUP_NAME);

		if (sharedConfig.getUseSharedAVs()) {
			controller.addOverridingModule(new DvrpTravelTimeModule());
			controller.addOverridingModule(new AVModule());
			controller.addOverridingModule(new SharedAVModule());
			controller.addOverridingModule(new ASTRAQSimModule());
		}
	}

	static public void configurePricing(Controler controller) {
		SharedAVConfigGroup sharedConfig = (SharedAVConfigGroup) controller.getConfig().getModules()
				.get(SharedAVConfigGroup.GROUP_NAME);

		if (sharedConfig.getUseSharedAVs()) {
			if (sharedConfig.getUseCostCalculator()) {
				double scalingFactor = sharedConfig.getCostCalculatorScalingFactor();
				int horizon = sharedConfig.getCostCalculatorHorzion();

				controller.addOverridingModule(new AVPricingModule(scalingFactor, horizon));
			}
		}
	}
}

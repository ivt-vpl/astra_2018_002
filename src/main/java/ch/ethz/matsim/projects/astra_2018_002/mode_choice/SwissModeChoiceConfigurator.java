package ch.ethz.matsim.projects.astra_2018_002.mode_choice;

import java.util.Arrays;

import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.Scenario;
import org.matsim.api.core.v01.TransportMode;
import org.matsim.api.core.v01.population.Person;
import org.matsim.core.config.Config;
import org.matsim.core.config.groups.ControlerConfigGroup.RoutingAlgorithmType;
import org.matsim.core.config.groups.PlanCalcScoreConfigGroup;
import org.matsim.core.config.groups.PlanCalcScoreConfigGroup.ModeParams;
import org.matsim.core.config.groups.PlansCalcRouteConfigGroup.ModeRoutingParams;
import org.matsim.households.Household;

import ch.ethz.matsim.discrete_mode_choice.modules.ConstraintModule;
import ch.ethz.matsim.discrete_mode_choice.modules.DiscreteModeChoiceConfigurator;
import ch.ethz.matsim.discrete_mode_choice.modules.EstimatorModule;
import ch.ethz.matsim.discrete_mode_choice.modules.ModelModule.ModelType;
import ch.ethz.matsim.discrete_mode_choice.modules.SelectorModule;
import ch.ethz.matsim.discrete_mode_choice.modules.config.DiscreteModeChoiceConfigGroup;
import ch.ethz.matsim.discrete_mode_choice.modules.config.VehicleTourConstraintConfigGroup.HomeType;

public class SwissModeChoiceConfigurator {
	static public void configure(Config config) {
		// Set up discrete choice
		DiscreteModeChoiceConfigurator.configureAsModeChoiceInTheLoop(config);
		DiscreteModeChoiceConfigGroup dmcConfig = (DiscreteModeChoiceConfigGroup) config.getModules()
				.get(DiscreteModeChoiceConfigGroup.GROUP_NAME);

		dmcConfig.setModelType(ModelType.Tour);
		dmcConfig.setPerformReroute(false);

		dmcConfig.setSelector(SelectorModule.MULTINOMIAL_LOGIT);

		dmcConfig.setTripEstimator(SwissDiscreteModeChoiceModule.SWISS_ESTIMATOR_NAME);
		dmcConfig.setTourEstimator(EstimatorModule.CUMULATIVE);
		dmcConfig.setCachedModes(Arrays.asList("car", "bike", "pt", "walk", "car_passenger", "truck", "truckAv"));

		dmcConfig.setTourFinder(SwissDiscreteModeChoiceModule.SWISS_TOUR_FINDER_NAME);
		dmcConfig.getActivityTourFinderConfigGroup().setActivityType("home");

		dmcConfig.setModeAvailability(SwissDiscreteModeChoiceModule.SWISS_MODE_AVAILABILITY_NAME);
		dmcConfig.getCarModeAvailabilityConfig().setAvailableModes(Arrays.asList("car", "bike", "pt", "walk"));

		dmcConfig.setTourConstraints(
				Arrays.asList(ConstraintModule.VEHICLE_CONTINUITY, ConstraintModule.FROM_TRIP_BASED));
		dmcConfig.setTripConstraints(
				Arrays.asList(ConstraintModule.TRANSIT_WALK, SwissDiscreteModeChoiceModule.OUTSIDE_TRIP_CONSTRAINT_NAME,
						SwissDiscreteModeChoiceModule.PASSENGER_CONSTRAINT_NAME));

		dmcConfig.getVehicleTourConstraintConfig().setHomeType(HomeType.USE_ACTIVITY_TYPE);
		dmcConfig.getVehicleTourConstraintConfig().setRestrictedModes(Arrays.asList("car", "bike", "prav3", "prav4", "prav5"));

		dmcConfig.setTourFilters(Arrays.asList(SwissDiscreteModeChoiceModule.OUTSIDE_FILTER));

		// These parameters are only used by SwissRailRaptor. We configure the
		// parameters here in a way that SRR searches for the route with the shortest
		// travel time.
		PlanCalcScoreConfigGroup scoringConfig = config.planCalcScore();

		ModeParams ptParams = scoringConfig.getModes().get(TransportMode.pt);
		ptParams.setConstant(0.0);
		ptParams.setMarginalUtilityOfDistance(0.0);
		ptParams.setMarginalUtilityOfTraveling(-1.0);
		ptParams.setMonetaryDistanceRate(0.0);

		scoringConfig.setMarginalUtilityOfMoney(0.0);
		scoringConfig.setMarginalUtlOfWaitingPt_utils_hr(0.0);

		scoringConfig.getOrCreateModeParams("car_passenger");
		scoringConfig.getOrCreateModeParams("truck");
		scoringConfig.getOrCreateModeParams("truckAv");

		// Some additional settings
		config.controler().setRoutingAlgorithmType(RoutingAlgorithmType.FastAStarLandmarks);

		ModeRoutingParams bikeParams = config.plansCalcRoute().getModeRoutingParams().get(TransportMode.bike);
		bikeParams.setBeelineDistanceFactor(1.4);
		bikeParams.setTeleportedModeSpeed(3.1); // 11.6 km/h

		ModeRoutingParams walkParams = config.plansCalcRoute().getModeRoutingParams().get(TransportMode.walk);
		walkParams.setBeelineDistanceFactor(1.3);
		walkParams.setTeleportedModeSpeed(1.2); // 4.32 km/h
	}

	static public void copyHousholdAttributes(Scenario scenario) {
		for (Household household : scenario.getHouseholds().getHouseholds().values()) {
			for (Id<Person> memberId : household.getMemberIds()) {
				Person person = scenario.getPopulation().getPersons().get(memberId);

				if (person != null) {
					// In long distance scenario some persons may be missing
					person.getAttributes().putAttribute("bikeAvailability",
							household.getAttributes().getAttribute("bikeAvailability"));
					person.getAttributes().putAttribute("householdIncome", household.getIncome().getIncome());
					person.getAttributes().putAttribute("spRegion", household.getAttributes().getAttribute("spRegion"));
					person.getAttributes().putAttribute("municipalityType",
							household.getAttributes().getAttribute("municipalityType"));
				}
			}
		}
	}
}

package ch.ethz.matsim.projects.astra_2018_002.private_av;

import org.matsim.core.config.ReflectiveConfigGroup;

public class PrivateAVConfigGroup extends ReflectiveConfigGroup {
	public final static String GROUP_NAME = "private_avs";

	private final static String USE_PRIVATE_AVS = "usePrivateAVs";
	private final static String ASSIGN_PRIVATE_AVS = "assignPrivateAVs";

	private boolean usePrivateAVs = false;
	private boolean assignPrivateAVs = false;

	public PrivateAVConfigGroup() {
		super(GROUP_NAME);
	}

	@StringGetter(USE_PRIVATE_AVS)
	public boolean getUsePrivateAVs() {
		return usePrivateAVs;
	}

	@StringSetter(USE_PRIVATE_AVS)
	public void setUsePrivateAVs(boolean usePrivateAVs) {
		this.usePrivateAVs = usePrivateAVs;
	}

	@StringGetter(ASSIGN_PRIVATE_AVS)
	public boolean getAssignPrivateAVs() {
		return assignPrivateAVs;
	}

	@StringSetter(ASSIGN_PRIVATE_AVS)
	public void setAssignPrivateAVs(boolean assignPrivateAVs) {
		this.assignPrivateAVs = assignPrivateAVs;
	}
}

package ch.ethz.matsim.projects.astra_2018_002.shared_av.waiting_time.zonal;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.events.PersonDepartureEvent;
import org.matsim.api.core.v01.events.PersonEntersVehicleEvent;
import org.matsim.api.core.v01.events.handler.PersonDepartureEventHandler;
import org.matsim.api.core.v01.events.handler.PersonEntersVehicleEventHandler;
import org.matsim.api.core.v01.network.Link;
import org.matsim.api.core.v01.network.Network;
import org.matsim.api.core.v01.population.Person;
import org.matsim.core.controler.events.AfterMobsimEvent;
import org.matsim.core.controler.listener.AfterMobsimListener;
import org.matsim.core.router.LinkWrapperFacility;
import org.matsim.facilities.Facility;

import ch.ethz.matsim.av.framework.AVModule;
import ch.ethz.matsim.projects.astra_2018_002.shared_av.SharedAVModule;
import ch.ethz.matsim.projects.astra_2018_002.shared_av.waiting_time.AVWaitingTime;
import ch.ethz.matsim.projects.astra_2018_002.shared_av.waiting_time.index.IndexEstimator;
import ch.ethz.matsim.projects.astra_2018_002.shared_av.waiting_time.index.MovingAverageIndexEstimator;

public class ZonalWaitingTime
		implements AVWaitingTime, PersonDepartureEventHandler, PersonEntersVehicleEventHandler, AfterMobsimListener {
	private final Logger logger = Logger.getLogger(AVWaitingTime.class);

	private final double estimationStartTime;
	private final double estimationEndTime;
	private final double estimationInterval;
	private final int numberOfEstimationBins;

	private final Map<Id<Link>, Integer> groupIndices;
	private final IndexEstimator taxiEstimator;
	private final IndexEstimator pooledEstimator;

	public ZonalWaitingTime(double estimationStartTime, double estimationEndTime, double estimationInterval,
			int horizon, double defaultWaitingTime, Network network, List<WaitingTimeZone> waitingTimeZones) {
		this.estimationStartTime = estimationStartTime;
		this.estimationEndTime = estimationEndTime;
		this.estimationInterval = estimationInterval;
		this.numberOfEstimationBins = (int) Math.ceil((estimationEndTime - estimationStartTime) / estimationInterval);
		this.groupIndices = createGroupIndices(network, waitingTimeZones);

		this.taxiEstimator = new MovingAverageIndexEstimator(waitingTimeZones.size(), numberOfEstimationBins, horizon,
				defaultWaitingTime);
		this.pooledEstimator = new MovingAverageIndexEstimator(waitingTimeZones.size(), numberOfEstimationBins, horizon,
				defaultWaitingTime);
	}

	private Map<Id<Link>, Integer> createGroupIndices(Network network, List<WaitingTimeZone> waitingTimeZones) {
		List<String> names = new LinkedList<>();
		List<Integer> counts = new LinkedList<>();
		Map<Id<Link>, Integer> indices = new HashMap<>();

		for (int index = 0; index < waitingTimeZones.size(); index++) {
			WaitingTimeZone zone = waitingTimeZones.get(index);

			names.add(zone.getIdentifier());
			counts.add(zone.getCoveredLinks().size());

			for (Link link : zone.getCoveredLinks()) {
				link.getAttributes().putAttribute("group_index", index);
				indices.put(link.getId(), index);
			}
		}

		logger.info(String.format("Found %d estimation groups:", names.size()));
		for (int k = 0; k < names.size(); k++) {
			logger.info(String.format("%s: %d obs", names.get(k), counts.get(k)));
		}

		return indices;
	}

	private IndexEstimator getEstimator(boolean isPooled) {
		if (isPooled) {
			return pooledEstimator;
		} else {
			return taxiEstimator;
		}
	}

	@Override
	public double getWaitingTime(Link link, double time, boolean isPooled) {
		Integer groupIndex = (Integer) link.getAttributes().getAttribute("group_index");

		if (groupIndex == null) {
			return getWaitingTime(new LinkWrapperFacility(link), time, isPooled);
		} else {
			return getEstimator(isPooled).estimate(groupIndex, getTimeIndex(time));
		}
	}

	@Override
	public double getWaitingTime(Facility<?> facility, double time, boolean isPooled) {
		Integer groupIndex = groupIndices.get(facility.getLinkId());

		if (groupIndex != null) {
			return getEstimator(isPooled).estimate(groupIndex, getTimeIndex(time));
		} else {
			throw new IllegalStateException(
					"Requested waiting time for link that is not in index: " + facility.getLinkId().toString());
		}
	}

	private int getTimeIndex(double time) {
		if (time < estimationStartTime) {
			return 0;
		} else if (time >= estimationEndTime) {
			return numberOfEstimationBins - 1;
		} else {
			return (int) Math.floor((time - estimationStartTime) / estimationInterval);
		}
	}

	private final Map<Id<Person>, PersonDepartureEvent> departureEvents = new HashMap<>();

	private void recordPickupTime(double pickupTime, double time, Id<Link> linkId, boolean isPooled) {
		Integer groupIndex = groupIndices.get(linkId);

		if (groupIndex != null) {
			getEstimator(isPooled).record(groupIndex, getTimeIndex(time), pickupTime);
		}
	}

	@Override
	public void handleEvent(PersonDepartureEvent event) {
		if (event.getLegMode().equals(AVModule.AV_MODE)) {
			departureEvents.put(event.getPersonId(), event);
		}
	}

	@Override
	public void handleEvent(PersonEntersVehicleEvent event) {
		PersonDepartureEvent departureEvent = departureEvents.remove(event.getPersonId());

		if (departureEvent != null) {
			boolean isPooled = event.getVehicleId().toString().contains(SharedAVModule.AV_POOL);

			recordPickupTime(event.getTime() - departureEvent.getTime(), departureEvent.getTime(),
					departureEvent.getLinkId(), isPooled);
		}
	}

	@Override
	public void notifyAfterMobsim(AfterMobsimEvent event) {
		departureEvents.clear();
		taxiEstimator.finish();
		pooledEstimator.finish();
	}

	public int getNumberOfObservations(Facility<?> facility, double time, boolean isPooled) {
		Integer groupIndex = groupIndices.get(facility.getLinkId());

		if (groupIndex != null) {
			return getEstimator(isPooled).getNumberOfObservations(groupIndex, getTimeIndex(time));
		} else {
			throw new IllegalStateException(
					"Requested waiting time for link that is not in index: " + facility.getLinkId().toString());
		}
	}
}

package ch.ethz.matsim.projects.astra_2018_002.mode_choice.base;

public class TripVariablesPrivateAv {
	public enum AutomationLevel {
		LEVEL3, LEVEL4, LEVEL5
	}
	
	public AutomationLevel automationLevel;
	
	public double travelTime_min = 0.0;
	public double travelCost_CHF = 0.0;
	public double travelDistance_km = 0.0;
	public double crowflyDistance_km = 0.0;
	
	public double delay_min = 0.0;
	
	public double automatedShare = 0.0;
}

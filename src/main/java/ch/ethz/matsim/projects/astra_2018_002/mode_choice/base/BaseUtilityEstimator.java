package ch.ethz.matsim.projects.astra_2018_002.mode_choice.base;

import java.util.List;

import org.matsim.api.core.v01.Coord;
import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.TransportMode;
import org.matsim.api.core.v01.network.Link;
import org.matsim.api.core.v01.network.Network;
import org.matsim.api.core.v01.population.Activity;
import org.matsim.api.core.v01.population.Leg;
import org.matsim.api.core.v01.population.Person;
import org.matsim.api.core.v01.population.PlanElement;
import org.matsim.core.population.routes.NetworkRoute;
import org.matsim.core.router.TripRouter;
import org.matsim.core.router.util.TravelTime;
import org.matsim.core.utils.geometry.CoordUtils;
import org.matsim.core.utils.misc.Time;
import org.matsim.facilities.ActivityFacilities;
import org.matsim.pt.transitSchedule.api.TransitSchedule;

import ch.ethz.matsim.av.framework.AVModule;
import ch.ethz.matsim.baseline_scenario.transit.routing.EnrichedTransitRoute;
import ch.ethz.matsim.discrete_mode_choice.components.estimators.AbstractTripRouterEstimator;
import ch.ethz.matsim.discrete_mode_choice.model.DiscreteModeChoiceTrip;
import ch.ethz.matsim.discrete_mode_choice.model.trip_based.candidates.TripCandidate;
import ch.ethz.matsim.projects.astra_2018_002.mode_choice.base.TripVariablesPrivateAv.AutomationLevel;
import ch.ethz.matsim.projects.astra_2018_002.mode_choice.cost.CostModel;
import ch.ethz.matsim.projects.astra_2018_002.shared_av.SharedAVModule;
import ch.ethz.matsim.projects.astra_2018_002.shared_av.routing.ExtendedAVRoute;

/**
 * Estimates trip utilities based on
 * 
 * Bundesamt für Raumentwicklung (2017) Analyse der SP-Befragung 2015 zur
 * Verkehrsmodus- und Routenwahl
 * 
 * Derived classes implement specific model formulations from Chapter 3.
 */
public abstract class BaseUtilityEstimator extends AbstractTripRouterEstimator {
	private final TransitSchedule transitSchedule;
	private final CostModel costModel;
	private final Network network;
	private final TravelTime travelTime;

	public BaseUtilityEstimator(TripRouter tripRouter, Network network, ActivityFacilities facilities,
			CostModel costModel, TransitSchedule transitSchedule, TravelTime travelTime) {
		super(tripRouter, facilities);

		this.costModel = costModel;
		this.transitSchedule = transitSchedule;
		this.network = network;
		this.travelTime = travelTime;
	}

	protected double estimateTrip(Person person, String mode, DiscreteModeChoiceTrip trip,
			List<TripCandidate> previousTrips, List<? extends PlanElement> routedTrip) {
		Boolean rawIsFreight = (Boolean) person.getAttributes().getAttribute("isFreight");
		if (rawIsFreight != null && rawIsFreight) {
			return 0.0;
		}
		
		PersonVariables personVariables = new PersonVariables();

		personVariables.age_over18a = Math.max(0.0,
				((double) (Integer) person.getAttributes().getAttribute("age")) - 18);
		personVariables.age_10a = ((double) (Integer) person.getAttributes().getAttribute("age")) / 10.0;
		personVariables.income_1000CHF = (Double) person.getAttributes().getAttribute("householdIncome") * 1e-3;
		personVariables.isMale = ((String) person.getAttributes().getAttribute("sex")).equals("m");
		personVariables.carAlwaysAvailable = ((String) person.getAttributes().getAttribute("carAvail"))
				.equals("always");
		personVariables.regionIndex = (Integer) person.getAttributes().getAttribute("spRegion");

		personVariables.hasGeneralSubscription = (Boolean) person.getAttributes().getAttribute("ptHasGA");
		personVariables.hasHalbtaxSubscription = (Boolean) person.getAttributes().getAttribute("ptHasHalbtax");
		personVariables.hasRegionalSubscription = (Boolean) person.getAttributes().getAttribute("ptHasVerbund");

		switch ((String) person.getAttributes().getAttribute("municipalityType")) {
		case "rural":
			personVariables.municipalityType = PersonVariables.MunicipalityType.Rural;
			break;
		case "suburban":
			personVariables.municipalityType = PersonVariables.MunicipalityType.SubUrban;
			break;
		case "urban":
			personVariables.municipalityType = PersonVariables.MunicipalityType.Urban;
			break;
		default:
			throw new IllegalStateException();
		}

		Double homeX = (Double) person.getAttributes().getAttribute("home_x");
		Double homeY = (Double) person.getAttributes().getAttribute("home_y");

		if (homeX == null || homeY == null) {
			homeX = 0.0;
			homeY = 0.0;

			for (PlanElement element : person.getSelectedPlan().getPlanElements()) {
				if (element instanceof Activity) {
					Activity activity = (Activity) element;

					if (activity.getType().equals("home")) {
						homeX = activity.getCoord().getX();
						homeY = activity.getCoord().getY();
					}
				}
			}

			person.getAttributes().putAttribute("home_x", homeX);
			person.getAttributes().putAttribute("home_y", homeY);
		}

		personVariables.homeLocation = new Coord(homeX, homeY);

		double outsideOffset = 0.0;

		if (trip.getOriginActivity().getType().equals("outside")
				|| trip.getDestinationActivity().getType().equals("outside")) {
			if (mode.equals(trip.getInitialMode())) {
				outsideOffset = 50.0;
			}
		}

		switch (mode) {
		case TransportMode.walk:
			return estimateWalkTrip(personVariables, trip, routedTrip) + outsideOffset;
		case TransportMode.bike:
			return estimateBikeTrip(personVariables, trip, routedTrip) + outsideOffset;
		case "prav3":
		case "prav4":
		case "prav5":
			return estimatePrivateAVTrip(personVariables, trip, routedTrip) + outsideOffset;
		case TransportMode.car:
			return estimateCarTrip(personVariables, trip, routedTrip) + outsideOffset;
		case "car_passenger":
		case "truck":
			return 0.0;
		case TransportMode.pt:
			return estimatePtTrip(personVariables, trip, routedTrip) + outsideOffset;
		case SharedAVModule.AV_POOL:
		case SharedAVModule.AV_TAXI:
			return estimateSharedAvTrip(personVariables, trip, routedTrip) + outsideOffset;
		case "outside":
			return 0.0;
		default:
			throw new IllegalStateException("Unknown mode: " + mode);
		}
	}

	abstract protected double estimateSharedAvTrip(DiscreteModeChoiceTrip trip, PersonVariables personVariables,
			TripVariablesSharedAv tripVariables);

	private double estimateSharedAvTrip(PersonVariables personVariables, DiscreteModeChoiceTrip trip,
			List<? extends PlanElement> elements) {
		TripVariablesSharedAv tripVariables = new TripVariablesSharedAv();

		double travelDistance_km = 0.0;
		double totalTravelTime = 0.0;
		double serviceDistance_km = 0.0;

		boolean foundTaxi = false;
		boolean foundPooled = false;

		for (PlanElement element : elements) {
			if (element instanceof Leg) {
				Leg leg = (Leg) element;
				totalTravelTime += leg.getTravelTime();

				switch (leg.getMode()) {
				case TransportMode.access_walk:
				case TransportMode.egress_walk:
					tripVariables.accessEgressTime_min += leg.getTravelTime() / 60.0;
					travelDistance_km += leg.getRoute().getDistance() / 1000.0;
					break;
				case AVModule.AV_MODE:
					ExtendedAVRoute route = (ExtendedAVRoute) leg.getRoute();
					tripVariables.inVehicleTime_min += route.getTravelTime() / 60.0;
					tripVariables.waitingTime_min += route.getWaitingTime() / 60.0;
					travelDistance_km += route.getDistance() / 1000.0;
					serviceDistance_km += route.getDistance() / 1000.0;

					String operatorId = route.getOperatorId().toString();
					foundTaxi |= operatorId.equals(SharedAVModule.AV_TAXI);
					foundPooled |= operatorId.equals(SharedAVModule.AV_POOL);

					break;
				default:
					throw new IllegalStateException("Unknown mode on AV trip: " + leg.getMode());
				}
			}
		}

		tripVariables.travelCost_CHF = costModel.calculateSharedAvTravelCost(serviceDistance_km, foundPooled);

		tripVariables.delay_min = calculateDelay(trip, totalTravelTime) / 60.0;
		tripVariables.travelDistance_km = travelDistance_km;

		tripVariables.crowflyDistance_km = CoordUtils.calcEuclideanDistance(trip.getOriginActivity().getCoord(),
				trip.getDestinationActivity().getCoord()) * 1e-3;

		if (foundTaxi ^ foundPooled) {
			tripVariables.isPooled = foundPooled;
		} else {
			throw new IllegalStateException("Did not find taxi or pool operator.");
		}

		return estimateSharedAvTrip(trip, personVariables, tripVariables);
	}

	abstract protected double estimatePtTrip(DiscreteModeChoiceTrip trip, PersonVariables personVariables,
			TripVariablesPt tripVariables);

	private TripVariablesPt.MainMode updatePtMainMode(String transportMode, TripVariablesPt.MainMode current) {
		if (current.equals(TripVariablesPt.MainMode.Rail) || transportMode.equals("rail")) {
			return TripVariablesPt.MainMode.Rail;
		} else if (current.equals(TripVariablesPt.MainMode.Tram) || transportMode.equals("tram")) {
			return TripVariablesPt.MainMode.Tram;
		} else {
			return TripVariablesPt.MainMode.Bus;
		}
	}

	private double estimatePtTrip(PersonVariables personVariables, DiscreteModeChoiceTrip trip,
			List<? extends PlanElement> elements) {
		TripVariablesPt tripVariables = new TripVariablesPt();

		/*
		 * TODO: Not sure how occupancy is defined. Can we replicate it?
		 * 
		 * TODO: We don't include headway right now. The plan is to have it as an
		 * attribute of the preceeding activity.
		 * 
		 * TODO: We should put complex cost model in. Right now using distance-based
		 * cost.
		 */

		/*
		 * Note 1: Waiting time includes "transfer walk time" between two stops acc. to
		 * Basil Schmid.
		 * 
		 * Note 2: For mode choice, we do not consider any waiting time *before* the
		 * first vehicular stage. This implies that agents are able to minimize their
		 * waiting time by departing at the proper time.
		 */

		int numberOfVehicularTrips = 0;
		boolean isFirstWaitingTime = true;
		double inVehicleDistance_km = 0.0;
		double inRailDistance_km = 0.0;

		double totalTravelTime = 0.0;
		double totalTravelDistance = 0.0;

		for (PlanElement element : elements) {
			if (element instanceof Leg) {
				Leg leg = (Leg) element;
				totalTravelDistance += leg.getRoute().getDistance();

				totalTravelTime += leg.getTravelTime();

				switch (leg.getMode()) {
				case TransportMode.access_walk:
				case TransportMode.egress_walk:
					tripVariables.accessEgressTime_min += leg.getTravelTime() / 60.0;
					break;
				case TransportMode.transit_walk:
					tripVariables.waitingTime_min += leg.getTravelTime() / 60.0;
					break;
				case TransportMode.pt:
					EnrichedTransitRoute route = (EnrichedTransitRoute) leg.getRoute();
					String transportMode = transitSchedule.getTransitLines().get(route.getTransitLineId()).getRoutes()
							.get(route.getTransitRouteId()).getTransportMode();

					tripVariables.inVehicleTime_min += route.getInVehicleTime() / 60.0;

					if (!isFirstWaitingTime) {
						tripVariables.waitingTime_min += route.getWaitingTime() / 60.0;
					} else {
						isFirstWaitingTime = false;
					}

					tripVariables.mainMode = updatePtMainMode(transportMode, tripVariables.mainMode);

					inVehicleDistance_km += route.getDistance() / 1000.0;
					numberOfVehicularTrips++;
					
					if (transportMode.equals("rail")) {
						inRailDistance_km += route.getDistance() / 1000.0;
					}
					
					break;
				default:
					throw new IllegalStateException("Unknown mode in PT trip: " + leg.getMode());
				}
			}
		}

		tripVariables.lineSwitches = Math.max(0, numberOfVehicularTrips - 1);
		tripVariables.inVehicleDistance_km = inVehicleDistance_km;

		tripVariables.delay_min = calculateDelay(trip, totalTravelTime) / 60.0;
		tripVariables.travelDistance_km = totalTravelDistance / 1000.0;

		tripVariables.crowflyDistance_km = CoordUtils.calcEuclideanDistance(trip.getOriginActivity().getCoord(),
				trip.getDestinationActivity().getCoord()) * 1e-3;

		tripVariables.originHomeDistance_km = CoordUtils.calcEuclideanDistance(personVariables.homeLocation,
				trip.getOriginActivity().getCoord()) * 1e-3;
		tripVariables.destinationHomeDistance_km = CoordUtils.calcEuclideanDistance(personVariables.homeLocation,
				trip.getDestinationActivity().getCoord()) * 1e-3;

		tripVariables.railShare = inVehicleDistance_km > 0.0 ? inRailDistance_km / inVehicleDistance_km : 0.0;
		tripVariables.travelCost_CHF = costModel.calculatePtTravelCost(personVariables, tripVariables);

		return estimatePtTrip(trip, personVariables, tripVariables);
	}

	abstract protected double estimatePrivateAVTrip(DiscreteModeChoiceTrip trip, PersonVariables personVariables,
			TripVariablesPrivateAv tripVariables);

	private double estimatePrivateAVTrip(PersonVariables personVariables, DiscreteModeChoiceTrip trip,
			List<? extends PlanElement> elements) {
		TripVariablesPrivateAv tripVariables = new TripVariablesPrivateAv();
		Leg leg = (Leg) elements.get(0);

		double travelDistance_km = leg.getRoute().getDistance() / 1000.0;

		tripVariables.travelTime_min = leg.getTravelTime() / 60.0;
		tripVariables.travelCost_CHF = costModel.calculatePrivateAvTravelCost(travelDistance_km);

		tripVariables.crowflyDistance_km = CoordUtils.calcEuclideanDistance(trip.getOriginActivity().getCoord(),
				trip.getDestinationActivity().getCoord());

		double totalTravelTime = leg.getTravelTime();
		double departureTime = trip.getOriginActivity().getEndTime();

		switch (((Leg) elements.get(0)).getMode()) {
		case "prav3":
			tripVariables.automationLevel = AutomationLevel.LEVEL3;
			tripVariables.automatedShare = 0.0;
			break;
		case "prav4":
			tripVariables.automationLevel = AutomationLevel.LEVEL4;
			tripVariables.automatedShare = calculateAutomatedShare(departureTime, (NetworkRoute) leg.getRoute());
			break;
		case "prav5":
			tripVariables.automationLevel = AutomationLevel.LEVEL5;
			tripVariables.automatedShare = calculateAutomatedShare(departureTime, (NetworkRoute) leg.getRoute());
			break;
		default:
			new IllegalStateException("Unknown private AV mode given.");
		}

		tripVariables.delay_min = calculateDelay(trip, totalTravelTime) / 60.0;

		return estimatePrivateAVTrip(trip, personVariables, tripVariables);
	}

	private double calculateAutomatedShare(double departureTime, NetworkRoute route) {
		double automatedTravelTime = 0.0;
		double totalTravelTime = 0.0;

		double time = departureTime;

		Link firstLink = network.getLinks().get(route.getStartLinkId());
		double firstLinkTravelTime = travelTime.getLinkTravelTime(firstLink, time, null, null);

		totalTravelTime += firstLinkTravelTime;

		if (isAutomatedLink(firstLink)) {
			automatedTravelTime += firstLinkTravelTime;
		}

		for (Id<Link> linkId : route.getLinkIds()) {
			Link link = network.getLinks().get(linkId);

			double linkTravelTime = travelTime.getLinkTravelTime(link, time, null, null);
			totalTravelTime += linkTravelTime;

			if (isAutomatedLink(link)) {
				automatedTravelTime += linkTravelTime;
			}
		}

		return automatedTravelTime / totalTravelTime;
	}

	private boolean isAutomatedLink(Link link) {
		Boolean avOperation = (Boolean) link.getAttributes().getAttribute("avOperation");
		return avOperation != null && avOperation;
	}

	abstract protected double estimateCarTrip(DiscreteModeChoiceTrip trip, PersonVariables personVariables,
			TripVariablesCar tripVariables);

	private double estimateCarTrip(PersonVariables personVariables, DiscreteModeChoiceTrip trip,
			List<? extends PlanElement> elements) {
		TripVariablesCar tripVariables = new TripVariablesCar();
		Leg leg = (Leg) elements.get(0);

		double travelDistance_km = leg.getRoute().getDistance() / 1000.0;

		tripVariables.travelTime_min = leg.getTravelTime() / 60.0;
		tripVariables.travelCost_CHF = costModel.calculateCarTravelCost(travelDistance_km);

		tripVariables.parkingSearchTime_min = 0.0;
		tripVariables.parkingCost_CHF = 0.0;

		tripVariables.delay_min = calculateDelay(trip, leg.getTravelTime()) / 60.0;
		tripVariables.travelDistance_km = travelDistance_km;

		tripVariables.crowflyDistance_km = CoordUtils.calcEuclideanDistance(trip.getOriginActivity().getCoord(),
				trip.getDestinationActivity().getCoord()) * 1e-3;

		return estimateCarTrip(trip, personVariables, tripVariables);
	}

	abstract protected double estimateWalkTrip(DiscreteModeChoiceTrip trip, PersonVariables personVariables,
			TripVariablesWalk tripVariables);

	private double estimateWalkTrip(PersonVariables personVariables, DiscreteModeChoiceTrip trip,
			List<? extends PlanElement> elements) {
		TripVariablesWalk tripVariables = new TripVariablesWalk();
		Leg leg = (Leg) elements.get(0);

		tripVariables.travelTime_min = leg.getTravelTime() / 60.0;
		tripVariables.travelDistance_km = leg.getRoute().getDistance() / 1000.0;
		tripVariables.delay_min = calculateDelay(trip, leg.getTravelTime()) / 60.0;

		return estimateWalkTrip(trip, personVariables, tripVariables);
	}

	abstract protected double estimateBikeTrip(DiscreteModeChoiceTrip trip, PersonVariables personVariables,
			TripVariablesBike tripVariables);

	private double estimateBikeTrip(PersonVariables personVariables, DiscreteModeChoiceTrip trip,
			List<? extends PlanElement> elements) {
		TripVariablesBike tripVariables = new TripVariablesBike();
		Leg leg = (Leg) elements.get(0);

		tripVariables.travelTime_min = leg.getTravelTime() / 60.0;
		tripVariables.travelDistance_km = leg.getRoute().getDistance() / 1000.0;
		tripVariables.delay_min = calculateDelay(trip, leg.getTravelTime()) / 60.0;

		return estimateBikeTrip(trip, personVariables, tripVariables);
	}

	private double calculateDelay(DiscreteModeChoiceTrip trip, double travelTime) {
		if (!Time.isUndefinedTime(trip.getOriginActivity().getEndTime())) {
			double arrivalTime = trip.getOriginActivity().getEndTime() + travelTime;

			if (!Time.isUndefinedTime(trip.getDestinationActivity().getStartTime())) {
				return Math.max(0.0, arrivalTime - trip.getDestinationActivity().getStartTime());
			}

			return 0.0;
		} else {
			throw new IllegalStateException();
		}
	}
}

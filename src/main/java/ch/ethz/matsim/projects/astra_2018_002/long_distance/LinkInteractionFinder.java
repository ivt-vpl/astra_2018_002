package ch.ethz.matsim.projects.astra_2018_002.long_distance;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Function;

import org.apache.log4j.Logger;
import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.network.Link;
import org.matsim.api.core.v01.network.Network;
import org.matsim.api.core.v01.population.Person;
import org.matsim.api.core.v01.population.Population;
import org.matsim.api.core.v01.population.PopulationFactory;
import org.matsim.core.router.StageActivityTypes;
import org.matsim.core.router.TripStructureUtils;
import org.matsim.core.router.costcalculators.OnlyTimeDependentTravelDisutility;
import org.matsim.core.router.util.LeastCostPathCalculator;
import org.matsim.core.router.util.LeastCostPathCalculator.Path;
import org.matsim.core.router.util.LeastCostPathCalculatorFactory;
import org.matsim.core.router.util.TravelDisutility;
import org.matsim.core.router.util.TravelTime;
import org.matsim.core.trafficmonitoring.FreeSpeedTravelTime;

public class LinkInteractionFinder {
	private final Logger logger = Logger.getLogger(LinkInteractionFinder.class);

	private StageActivityTypes stageActivityTypes;
	private LeastCostPathCalculatorFactory routerFactory;

	private int numberOfThreads;
	private int batchSize = 100;

	public LinkInteractionFinder(int numberOfThreads, int batchSize, StageActivityTypes stageActivityTypes,
			PopulationFactory populationFactory, LeastCostPathCalculatorFactory routerFactory) {
		this.numberOfThreads = numberOfThreads;
		this.batchSize = batchSize;
		this.stageActivityTypes = stageActivityTypes;
		this.routerFactory = routerFactory;
	}

	public Collection<Id<Person>> findInteractingPersons(Population population, Network network,
			Function<Link, Boolean> filter) throws InterruptedException {
		Collection<Id<Person>> interactionIds = new ArrayList<>(population.getPersons().size());
		Iterator<? extends Person> personIterator = population.getPersons().values().iterator();

		TravelTime travelTime = new FreeSpeedTravelTime();
		TravelDisutility travelDisutility = new OnlyTimeDependentTravelDisutility(travelTime);

		List<Thread> threads = new ArrayList<>(numberOfThreads);

		long totalNumberOfPersons = population.getPersons().size();
		AtomicLong processedNumberOfPersons = new AtomicLong(0);

		for (int index = 0; index < numberOfThreads; index++) {
			Thread thread = new Thread(() -> {
				Collection<Id<Person>> batchInteractionIds = new ArrayList<>(batchSize);
				Collection<Person> batch = new ArrayList<>(batchSize);
				LeastCostPathCalculator router = routerFactory.createPathCalculator(network, travelDisutility,
						travelTime);

				do {
					batch.clear();
					batchInteractionIds.clear();

					synchronized (personIterator) {
						while (personIterator.hasNext() && batch.size() < batchSize) {
							batch.add(personIterator.next());
						}
					}

					for (Person person : batch) {
						boolean hasInteraction = false;

						for (TripStructureUtils.Trip trip : TripStructureUtils.getTrips(person.getSelectedPlan(),
								stageActivityTypes)) {
							Link originLink = network.getLinks().get(trip.getOriginActivity().getLinkId());
							Link destinationLink = network.getLinks().get(trip.getDestinationActivity().getLinkId());

							Path path = router.calcLeastCostPath(originLink.getToNode(), destinationLink.getFromNode(),
									0.0, null, null);

							for (Link link : path.links) {
								if (filter.apply(link)) {
									hasInteraction = true;
									break;
								}
							}
						}

						if (hasInteraction) {
							batchInteractionIds.add(person.getId());
						}
					}

					processedNumberOfPersons.addAndGet(batch.size());

					synchronized (interactionIds) {
						interactionIds.addAll(batchInteractionIds);
					}
				} while (batch.size() > 0);
			});

			threads.add(thread);
			thread.start();
		}

		Thread progressThread = new Thread(() -> {
			try {
				long previousNumberOfPersons = 0;

				do {
					long currentNumberOfPersons = processedNumberOfPersons.get();

					if (currentNumberOfPersons > previousNumberOfPersons) {
						logger.info(String.format("Finding link interactions... %d/%d (%.2f%%)", currentNumberOfPersons,
								totalNumberOfPersons, 100.0 * currentNumberOfPersons / totalNumberOfPersons));
					}

					previousNumberOfPersons = currentNumberOfPersons;
					Thread.sleep(100);
				} while (previousNumberOfPersons < totalNumberOfPersons);
			} catch (InterruptedException e) {
			}
		});
		progressThread.start();

		for (Thread thread : threads) {
			thread.join();
		}

		progressThread.join();

		return interactionIds;
	}
}

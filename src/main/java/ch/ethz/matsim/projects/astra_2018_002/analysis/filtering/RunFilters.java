package ch.ethz.matsim.projects.astra_2018_002.analysis.filtering;

import org.matsim.api.core.v01.Scenario;
import org.matsim.api.core.v01.population.Person;
import org.matsim.api.core.v01.population.Population;
import org.matsim.api.core.v01.population.PopulationWriter;
import org.matsim.core.config.Config;
import org.matsim.core.config.ConfigUtils;

import ch.ethz.matsim.baseline_scenario.config.CommandLine;
import ch.ethz.matsim.baseline_scenario.config.CommandLine.ConfigurationException;

import org.matsim.core.population.PopulationUtils;
import org.matsim.core.population.io.PopulationReader;
import org.matsim.core.scenario.ScenarioUtils;

import ch.ethz.matsim.baseline_scenario.config.CommandLine;
import ch.ethz.matsim.projects.astra_2018_002.mode_choice.SwissDiscreteModeChoiceModule;

public class RunFilters {
	static public void main(String[] args) throws ConfigurationException {
	
	CommandLine cmd = new CommandLine.Builder(args) //
			.requireOptions("plans-file-path", "output-file-path") //
			.build();
		
    // create population data container
    Config config = ConfigUtils.createConfig();
    Scenario scenario = ScenarioUtils.createScenario(config);
    // read original population file
    new PopulationReader(scenario).readFile(cmd.getOptionStrict("plans-file-path"));
    //new PopulationReader(scenario).readFile("C:\\Users\\clivings\\Documents\\ASTRA_2018_002_AFs_Schweiz\\Output_From_MATSim\\from_nama\\prav_2050A_1pm_AvLowPCUs_test\\output_plans.xml.gz");
    Population originalPopulation = scenario.getPopulation();
    // filter females and add to new filtered population object
    Population filteredPopulation = PopulationUtils.createPopulation(config);
    for (Person person : originalPopulation.getPersons().values()) {
        if (person.getAttributes().getAttribute("isFreight") == null) {
            filteredPopulation.addPerson(person);
        }
    }
    // write out new population
    //new PopulationWriter(filteredPopulation).write("C:\\Users\\clivings\\Documents\\ASTRA_2018_002_AFs_Schweiz\\ProcessingResultsForOthers\\filtered_population.xml");
    new PopulationWriter(filteredPopulation).write(cmd.getOptionStrict("output-file-path"));
		
	}
}


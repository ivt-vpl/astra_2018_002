package ch.ethz.matsim.projects.astra_2018_002;

import org.matsim.core.controler.AbstractModule;
import org.matsim.core.controler.corelisteners.PlansScoring;

public class ASTRAModule extends AbstractModule {
	@Override
	public void install() {
		bind(PlansScoring.class).toInstance(new PlansScoring() {
		});
	}
}

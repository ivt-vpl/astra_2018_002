package ch.ethz.matsim.projects.astra_2018_002.shared_av.routing;

import java.util.LinkedList;
import java.util.List;

import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.TransportMode;
import org.matsim.api.core.v01.network.Link;
import org.matsim.api.core.v01.network.Network;
import org.matsim.api.core.v01.population.Activity;
import org.matsim.api.core.v01.population.Leg;
import org.matsim.api.core.v01.population.Person;
import org.matsim.api.core.v01.population.PlanElement;
import org.matsim.api.core.v01.population.PopulationFactory;
import org.matsim.contrib.dvrp.path.VrpPathWithTravelData;
import org.matsim.contrib.dvrp.path.VrpPaths;
import org.matsim.core.network.NetworkUtils;
import org.matsim.core.router.LinkWrapperFacility;
import org.matsim.core.router.RoutingModule;
import org.matsim.core.router.StageActivityTypes;
import org.matsim.core.router.StageActivityTypesImpl;
import org.matsim.core.router.util.LeastCostPathCalculator;
import org.matsim.core.router.util.TravelTime;
import org.matsim.core.utils.collections.QuadTree;
import org.matsim.facilities.Facility;

import ch.ethz.matsim.av.data.AVOperator;
import ch.ethz.matsim.av.framework.AVModule;
import ch.ethz.matsim.av.routing.AVRoute;
import ch.ethz.matsim.av.routing.AVRouteFactory;
import ch.ethz.matsim.projects.astra_2018_002.shared_av.waiting_time.AVWaitingTime;

public class ASTRAAVRoutingModule implements RoutingModule {
	public static final String AV_INTERACTION_ACTIVITY_TYPE = "av interaction";

	private final PopulationFactory populationFactory;
	private final AVRouteFactory routeFactory;
	private final RoutingModule walkRoutingModule;
	private final LeastCostPathCalculator router;
	private final Network network;

	private final TravelTime travelTime;
	private final AVWaitingTime waitingTime;

	private final String mode;
	private final boolean isPooled;
	
	private final QuadTree<Link> serviceAreaIndex;

	public ASTRAAVRoutingModule(String mode, PopulationFactory populationFactory, AVRouteFactory routeFactory,
			RoutingModule walkRoutingModule, LeastCostPathCalculator router, Network network, TravelTime travelTime,
			AVWaitingTime waitingTime, boolean isPooled, QuadTree<Link> serviceAreaIndex) {
		this.mode = mode;
		this.populationFactory = populationFactory;
		this.routeFactory = routeFactory;
		this.walkRoutingModule = walkRoutingModule;
		this.router = router;
		this.network = network;
		this.travelTime = travelTime;
		this.waitingTime = waitingTime;
		this.isPooled = isPooled;
		this.serviceAreaIndex = serviceAreaIndex;
	}

	@Override
	public List<? extends PlanElement> calcRoute(Facility<?> originFacility, Facility<?> destinationFacility,
			double departureTime, Person person) {
		List<PlanElement> elements = new LinkedList<>();

		Link pickupLink = network.getLinks().get(originFacility.getLinkId());
		Link dropoffLink = network.getLinks().get(destinationFacility.getLinkId());

		Facility<?> pickupFacility = originFacility;
		Facility<?> dropoffFacility = destinationFacility;

		if (pickupLink == null) {
			//pickupLink = NetworkUtils.getNearestLink(network, originFacility.getCoord());
			pickupLink = serviceAreaIndex.getClosest(originFacility.getCoord().getX(), destinationFacility.getCoord().getY());
			pickupFacility = new LinkWrapperFacility(pickupLink);
		}

		if (dropoffLink == null) {
			//dropoffLink = NetworkUtils.getNearestLink(network, destinationFacility.getCoord());
			dropoffLink = serviceAreaIndex.getClosest(destinationFacility.getCoord().getX(), destinationFacility.getCoord().getY());
			dropoffFacility = new LinkWrapperFacility(dropoffLink);
		}

		if (originFacility != pickupFacility) {
			List<? extends PlanElement> accessElements = walkRoutingModule.calcRoute(originFacility, pickupFacility,
					departureTime, person);
			Leg accessLeg = (Leg) accessElements.get(0);

			accessLeg.setMode(TransportMode.access_walk);
			elements.add(accessLeg);
			departureTime += accessLeg.getTravelTime();

			Activity interactionActivity = populationFactory.createActivityFromLinkId(AV_INTERACTION_ACTIVITY_TYPE,
					pickupLink.getId());
			interactionActivity.setMaximumDuration(0.0);
			elements.add(interactionActivity);
		}

		double pickupWaitingTime = waitingTime.getWaitingTime(pickupLink, departureTime, isPooled);

		VrpPathWithTravelData path = VrpPaths.calcAndCreatePath(pickupLink, dropoffLink,
				departureTime + pickupWaitingTime, router, travelTime);
		AVRoute avRoute = routeFactory.createRoute(pickupLink.getId(), dropoffLink.getId(),
				Id.create(mode, AVOperator.class));

		avRoute.setDistance(VrpPaths.calcDistance(path));
		avRoute.setTravelTime(path.getTravelTime());
		
		ExtendedAVRoute extendedAvRoute = new ExtendedAVRoute(avRoute);
		extendedAvRoute.setWaitingTime(pickupWaitingTime);

		Leg avLeg = populationFactory.createLeg(AVModule.AV_MODE);
		avLeg.setRoute(extendedAvRoute);
		avLeg.setTravelTime(extendedAvRoute.getJourneyTime());
		avLeg.setDepartureTime(departureTime);
		elements.add(avLeg);

		departureTime += extendedAvRoute.getJourneyTime();

		if (destinationFacility != dropoffFacility) {
			List<? extends PlanElement> egressElements = walkRoutingModule.calcRoute(dropoffFacility,
					destinationFacility, departureTime, person);
			Leg egressLeg = (Leg) egressElements.get(0);

			Activity interactionActivity = populationFactory.createActivityFromLinkId(AV_INTERACTION_ACTIVITY_TYPE,
					dropoffLink.getId());
			interactionActivity.setMaximumDuration(0.0);
			elements.add(interactionActivity);

			egressLeg.setMode(TransportMode.egress_walk);
			elements.add(egressLeg);
		}

		return elements;
	}

	@Override
	public StageActivityTypes getStageActivityTypes() {
		return new StageActivityTypesImpl(AV_INTERACTION_ACTIVITY_TYPE);
	}
}

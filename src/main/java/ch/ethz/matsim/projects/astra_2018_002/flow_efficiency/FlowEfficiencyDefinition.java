package ch.ethz.matsim.projects.astra_2018_002.flow_efficiency;

import java.io.File;
import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class FlowEfficiencyDefinition {
	public Double pcu_car = 1.00;

	// It was decided that for now, prav3s be clustered with cars. But this
	// allows in future for a differentiation to be made.
	public Double pcu_prav3_motorway = 1.00;
	public Double pcu_prav3_trunk = 1.00;
	public Double pcu_prav3_secondaryfast = 1.00;
	public Double pcu_prav3_secondaryslow = 1.00;

	// It was decided that for now, prav4s be clustered with prav5s. But
	// this allows in the future for a differentiation to be made.
	public Double pcu_prav4_motorway = 0.83;
	public Double pcu_prav4_trunk = 0.77;
	public Double pcu_prav4_secondaryfast = 0.83;
	public Double pcu_prav4_secondaryslow = 0.80;

	public Double pcu_prav5_motorway = 0.83;
	public Double pcu_prav5_trunk = 0.77;
	public Double pcu_prav5_secondaryfast = 0.83;
	public Double pcu_prav5_secondaryslow = 0.80;
	
	public Double pcu_sharedAv_motorway = 0.83;
	public Double pcu_sharedAv_trunk = 0.77;
	public Double pcu_sharedAv_secondaryfast = 0.83;
	public Double pcu_sharedAv_secondaryslow = 0.80;
	
	public Double pcu_truck = 4.00;
	
	public Double pcu_truckAv_motorway = 3.08;
	public Double pcu_truckAv_trunk = 3.08;
	public Double pcu_truckAv_secondaryfast = 3.33;
	public Double pcu_truckAv_secondaryslow = 3.20;
	
	static public FlowEfficiencyDefinition fromFile(File path) throws JsonParseException, JsonMappingException, IOException {
		return new ObjectMapper().readValue(path, FlowEfficiencyDefinition.class);
	}
}

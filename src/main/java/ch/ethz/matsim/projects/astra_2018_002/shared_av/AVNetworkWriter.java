package ch.ethz.matsim.projects.astra_2018_002.shared_av;

import org.matsim.api.core.v01.network.Network;
import org.matsim.api.core.v01.network.NetworkWriter;
import org.matsim.core.controler.OutputDirectoryHierarchy;
import org.matsim.core.controler.events.StartupEvent;
import org.matsim.core.controler.listener.StartupListener;

import com.google.inject.Inject;
import com.google.inject.name.Named;

import ch.ethz.matsim.av.framework.AVModule;

public class AVNetworkWriter implements StartupListener {
	private final OutputDirectoryHierarchy hierarchy;
	private final Network network;

	@Inject
	public AVNetworkWriter(OutputDirectoryHierarchy hierarchy, @Named(AVModule.AV_MODE) Network network) {
		this.hierarchy = hierarchy;
		this.network = network;
	}

	@Override
	public void notifyStartup(StartupEvent event) {
		new NetworkWriter(network).write(hierarchy.getOutputFilename("av_network.xml.gz"));
	}
}

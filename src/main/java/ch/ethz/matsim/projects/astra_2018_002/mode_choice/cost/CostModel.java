package ch.ethz.matsim.projects.astra_2018_002.mode_choice.cost;

import ch.ethz.matsim.projects.astra_2018_002.mode_choice.base.PersonVariables;
import ch.ethz.matsim.projects.astra_2018_002.mode_choice.base.TripVariablesPt;
import ch.ethz.matsim.projects.astra_2018_002.shared_av.pricing.SharedAVTravelCost;

public class CostModel {
	private final CostParameters parameters;
	private final SharedAVTravelCost shared;
	private final int year;
	private final boolean usePtCostReduction;

	public CostModel(CostParameters parameters, SharedAVTravelCost shared, int year, boolean usePtCostReduction) {
		this.parameters = parameters;
		this.shared = shared;
		this.year = year;
		this.usePtCostReduction = usePtCostReduction;
	}

	public double calculateCarTravelCost(double distance_km) {
		return parameters.carCostPerKm_CHF * distance_km;
	}

	public double calculatePrivateAvTravelCost(double distance_km) {
		return parameters.privateAvCostPerKm_CHF * distance_km;
	}

	public double calculateSharedAvTravelCost(double distance_km, boolean isPooled) {
		return shared.getTravelCost(distance_km, isPooled);
	}

	public double calculatePtTravelCost(PersonVariables personVariables, TripVariablesPt tripVariables) {
		/*
		 * TODO: Later on, we should have a more complex cost model here.
		 */

		if (personVariables.hasGeneralSubscription) {
			return 0.0;
		}

		if (personVariables.hasRegionalSubscription) {
			if (tripVariables.originHomeDistance_km <= parameters.ptRegionalRadius_km
					&& tripVariables.destinationHomeDistance_km <= parameters.ptRegionalRadius_km) {
				return 0.0;
			}
		}

		double fullCost = Math.max(parameters.ptCostMinimum_CHF,
				parameters.ptCostPerKm_CHF * tripVariables.inVehicleDistance_km);
		//removing "year >= 2050 &&" from the if statement below, so that when usePtCostReduction's value is true, the cost reduction to be applied regardless of year. - cvl Nov 8th 2019
		if (usePtCostReduction) { // Scaling for automation
			// Uncomment the "System.out." command so that the use of this function is printed to the command line, which lets you see if the reduction was applied - cvl Nov 8th 2019
			//System.out.println("HEY PT COST REDUCTION IS BEING APPLIED! OR NOT!!! LET'S SEE!!!!!!!!!!!!");
			double reduction = parameters.ptRailReduction * tripVariables.railShare
					+ parameters.ptNonRailReducation * (1.0 - tripVariables.railShare);
			fullCost *= (1.0 - reduction);
			//System.out.println("HEY THE PT REDUCTION IS" + reduction);
			//System.out.println("HEY THE FULL COST OF PT IS" + fullCost);
		}
		// Uncomment the "System.out." command so that the use of this function is printed to the command line, which lets you see if the reduction was applied - cvl Nov 8th 2019
		//System.out.println("YEAR AND PTCOSTREDUCTION WERE EVALUATED!!! AND THE FULL COST IS" + fullCost + "AND YEAR IS" + year + "AND USEPTCOSTREDUCITON IS" + usePtCostReduction);

		if (personVariables.hasHalbtaxSubscription) {
			return fullCost * 0.5;
		} else {
			return fullCost;
		}
	}
}

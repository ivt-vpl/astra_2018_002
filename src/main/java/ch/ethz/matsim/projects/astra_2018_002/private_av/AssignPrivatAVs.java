package ch.ethz.matsim.projects.astra_2018_002.private_av;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import org.matsim.api.core.v01.Scenario;
import org.matsim.api.core.v01.population.Leg;
import org.matsim.api.core.v01.population.Person;
import org.matsim.api.core.v01.population.Plan;
import org.matsim.api.core.v01.population.PlanElement;
import org.matsim.api.core.v01.population.Population;
import org.matsim.core.config.Config;
import org.matsim.core.config.ConfigUtils;
import org.matsim.core.population.PopulationUtils;
import org.matsim.core.scenario.ScenarioUtils;


public class AssignPrivatAVs {
	static public final String AV_AVAILABILITY_ATTRIBUTE = "prAvAvail";
	static public final String FREIGHT_AV = "freight_av";
	static public final Boolean freight_av = false;
	
	static public void main(Scenario scenario, int year, String ASTRA_Scenario) {
		Random random = new Random(1);

		for (Person person : scenario.getPopulation().getPersons().values()) {	
			Boolean isFreight = (Boolean) person.getAttributes().getAttribute("isFreight");
			if (isFreight != null && isFreight) {
				continue;
			}
			
			String carAvailability = (String) person.getAttributes().getAttribute("carAvail");
			//System.out.println(carAvailability);//to check that the program is actually retrieving the value of carAvail
			String string1 = "always";
			String string2 = "sometimes";
			
			// if carAvail == always OR sometimes, then randomly assign an AV according to percentages from TP5, to produce
			// market share of AVs. A similar procedure is used to assign car "ownership", aka if an agent has "always"/"sometimes"
			// access or "never" access to a car. 

			if( ASTRA_Scenario.equals("A")&& year >= 2050){
				if(carAvailability.equals(string1)|| carAvailability.equals(string2)){
					double r = random.nextDouble();
					
						if (r <= 0.6868) {//produces "true" 68.68% of the time.
							//Value is specified by the Excel sheet "Calculation_PRAV_FleetSize_ForMATSim"
							//Values used in the Excel sheet come from Teilprojekt 5: Mischverkehr
							//Specifically, for the scenarios "A" and "B", the numbers are from the document 
							//"Ergebnisse aus dem Flottenmodell des TP5 als Input f�r MatSim (TP2)" Fassung 31.05.19
							//which is a document provided direclty to us (we are TP2) from TP5 via e-mail. 
							//For scenario "C", the numbers are from the document
							//"TP 5: Migrationsszenarien>Prognose Fahryeugbestand: Szenario Pro sharing (Extremszenario)"
							//which is a slide from TP5's presentation at the BK4 on 18.06.2019
						
							person.getAttributes().putAttribute(AV_AVAILABILITY_ATTRIBUTE,"car");
							person.getAttributes().putAttribute("carAvail", carAvailability);
						//currently only differentiating between conventional vehicles and "highly automated, highly networked" vehicles
							
						//if more types of vehicles are required in the future, you can uncomment the code below. 
						// BE SURE TO CHANGE THE VALUES OF r, THOUGH, TO BE REASONABLE ASSUMPTIONS WITHIN THE CONTEXT OF YOUR PROJECT!
						/*} else if (r <= 0.66) {//produces "true" 65.91% of the time: because the first step took 42.05% of the agents already,
							// then this step will only consider the next 23.86% of agents
						
							person.getAttributes().putAttribute(AV_AVAILABILITY_ATTRIBUTE,"prav3");
							person.getAttributes().putAttribute("carAvail", "never");
						
						} else if (r <= 0.82){
							person.getAttributes().putAttribute(AV_AVAILABILITY_ATTRIBUTE,"prav4");
							person.getAttributes().putAttribute("carAvail", "never");
						*/
							
						} else {
							person.getAttributes().putAttribute(AV_AVAILABILITY_ATTRIBUTE,"prav5");
							person.getAttributes().putAttribute("carAvail", "never");
						}
					
					} else {  // make sure that those agents who "never" have a car available also do not have an private av available,and to 
						// prevent "null" exceptions from Java, which occur when a variable, such as the attribute "prAvAvail" don't have a value
						person.getAttributes().putAttribute(AV_AVAILABILITY_ATTRIBUTE,"noPrav");
						person.getAttributes().putAttribute("carAvail", "never");
					}
				
			} else if (ASTRA_Scenario.equals("A")&& year >= 2040){
				if(carAvailability.equals(string1)|| carAvailability.equals(string2)){
					double r = random.nextDouble();
					
						if (r <= 0.9435) {//produces "true" 94.35% of the time
							//Value is specified by the Excel sheet "Calculation_PRAV_FleetSize_ForMATSim"
							//Values used in the Excel sheet come from Teilprojekt 5: Mischverkehr
							//Specifically, for the scenarios "A" and "B", the numbers are from the document 
							//"Ergebnisse aus dem Flottenmodell des TP5 als Input f�r MatSim (TP2)" Fassung 31.05.19
							//which is a document provided direclty to us (we are TP2) from TP5 via e-mail. 
							//For scenario "C", the numbers are from the document
							//"TP 5: Migrationsszenarien>Prognose Fahryeugbestand: Szenario Pro sharing (Extremszenario)"
							//which is a slide from TP5's presentation at the BK4 on 18.06.2019
						
							person.getAttributes().putAttribute(AV_AVAILABILITY_ATTRIBUTE,"car");
							person.getAttributes().putAttribute("carAvail", carAvailability);
						//currently only differentiating between conventional vehicles and "highly automated, highly networked" vehicles			//currently only differentiating between conventional vehicles and "highly automated, highly networked" vehicles
							
						//if more types of vehicles are required in the future, you can uncomment the code below. 
						// BE SURE TO CHANGE THE VALUES OF r, THOUGH, TO BE REASONABLE ASSUMPTIONS WITHIN THE CONTEXT OF YOUR PROJECT!
						/*} else if (r <= 0.66) {//produces "true" 65.91% of the time: because the first step took 42.05% of the agents already,
							// then this step will only consider the next 23.86% of agents
						
							person.getAttributes().putAttribute(AV_AVAILABILITY_ATTRIBUTE,"prav3");
							person.getAttributes().putAttribute("carAvail", "never");
						
						} else if (r <= 0.82){
							person.getAttributes().putAttribute(AV_AVAILABILITY_ATTRIBUTE,"prav4");
							person.getAttributes().putAttribute("carAvail", "never");
						*/
							
						} else {
							person.getAttributes().putAttribute(AV_AVAILABILITY_ATTRIBUTE,"prav5");
							person.getAttributes().putAttribute("carAvail", "never");
						}
					
					} else {  // make sure that those agents who "never" have a car available also do not have an private av available,and to 
						// prevent "null" exceptions from Java, which occur when a variable, such as the attribute "prAvAvail" don't have a value
						person.getAttributes().putAttribute(AV_AVAILABILITY_ATTRIBUTE,"noPrav");
						person.getAttributes().putAttribute("carAvail", "never");
					}
				
			} else if (ASTRA_Scenario.equals("A")&& year >= 2030){
				if(carAvailability.equals(string1)|| carAvailability.equals(string2)){
					double r = random.nextDouble();
					
						if (r <= 0.9954) {//produces "true" 99.54% of the time
							//Value is specified by the Excel sheet "Calculation_PRAV_FleetSize_ForMATSim"
							//Values used in the Excel sheet come from Teilprojekt 5: Mischverkehr
							//Specifically, for the scenarios "A" and "B", the numbers are from the document 
							//"Ergebnisse aus dem Flottenmodell des TP5 als Input f�r MatSim (TP2)" Fassung 31.05.19
							//which is a document provided direclty to us (we are TP2) from TP5 via e-mail. 
							//For scenario "C", the numbers are from the document
							//"TP 5: Migrationsszenarien>Prognose Fahryeugbestand: Szenario Pro sharing (Extremszenario)"
							//which is a slide from TP5's presentation at the BK4 on 18.06.2019
						
							person.getAttributes().putAttribute(AV_AVAILABILITY_ATTRIBUTE,"car");
							person.getAttributes().putAttribute("carAvail", carAvailability);
						//currently only differentiating between conventional vehicles and "highly automated, highly networked" vehicles
							
						//if more types of vehicles are required in the future, you can uncomment the code below. 
						// BE SURE TO CHANGE THE VALUES OF r, THOUGH, TO BE REASONABLE ASSUMPTIONS WITHIN THE CONTEXT OF YOUR PROJECT!
						/*} else if (r <= 0.66) {//produces "true" 65.91% of the time: because the first step took 42.05% of the agents already,
							// then this step will only consider the next 23.86% of agents
						
							person.getAttributes().putAttribute(AV_AVAILABILITY_ATTRIBUTE,"prav3");
							person.getAttributes().putAttribute("carAvail", "never");
						
						} else if (r <= 0.82){
							person.getAttributes().putAttribute(AV_AVAILABILITY_ATTRIBUTE,"prav4");
							person.getAttributes().putAttribute("carAvail", "never");
						*/
							
						} else {
							person.getAttributes().putAttribute(AV_AVAILABILITY_ATTRIBUTE,"prav5");
							person.getAttributes().putAttribute("carAvail", "never");
						}
					
					} else {  // make sure that those agents who "never" have a car available also do not have an private av available,and to 
						// prevent "null" exceptions from Java, which occur when a variable, such as the attribute "prAvAvail" don't have a value
						person.getAttributes().putAttribute(AV_AVAILABILITY_ATTRIBUTE,"noPrav");
						person.getAttributes().putAttribute("carAvail", "never");
					}
				
			} else if (ASTRA_Scenario.equals("A")&& year < 2030){
				if(carAvailability.equals(string1)|| carAvailability.equals(string2)){
						
							person.getAttributes().putAttribute(AV_AVAILABILITY_ATTRIBUTE,"car");
							person.getAttributes().putAttribute("carAvail", carAvailability);
						
					
					} else {  // make sure that those agents who "never" have a car available also do not have an private av available,and to 
						// prevent "null" exceptions from Java, which occur when a variable, such as the attribute "prAvAvail" don't have a value
						person.getAttributes().putAttribute(AV_AVAILABILITY_ATTRIBUTE,"noPrav");
						person.getAttributes().putAttribute("carAvail", "never");
					}
				
			} else if (ASTRA_Scenario.equals("B")&& year >= 2050){
				
				if(carAvailability.equals(string1)|| carAvailability.equals(string2)){
					double r = random.nextDouble();
					if (r <= 0.8191) {//produces "true" 81.50% of the time
						//if they are part of this lucky percentage, they have "decided" to keep their/purchase a car, else...
						//Where does the value come from?
						//Value is specified by the Excel sheet "Calculation_PRAV_FleetSize_ForMATSim"
						//Values used in the Excel sheet come from Teilprojekt 5: Mischverkehr
						//Specifically, for the scenarios "A" and "B", the numbers are from the document 
						//"Ergebnisse aus dem Flottenmodell des TP5 als Input f�r MatSim (TP2)" Fassung 31.05.19
						//which is a document provided direclty to us (we are TP2) from TP5 via e-mail. 
						//For scenario "C", the numbers are from the document
						//"TP 5: Migrationsszenarien>Prognose Fahryeugbestand: Szenario Pro sharing (Extremszenario)"
						//which is a slide from TP5's presentation at the BK4 on 18.06.2019
						person.getAttributes().putAttribute("carAvail", carAvailability);
					} else { //...else they have "decided" to give up their/not purchase a car! They've converted to the sharing economy! 
						person.getAttributes().putAttribute("carAvail", "never");
					}
				}
				
				carAvailability = (String) person.getAttributes().getAttribute("carAvail");
				
				//now, for the agents that still have cars, aka whose attribute "carAvail" still equals "always" or "sometimes"...
				//let us assign which one of them has a car capable of autonomous operation...
				if(carAvailability.equals(string1)|| carAvailability.equals(string2)){
					double r = random.nextDouble();
					 
					// these agents still have good ol' conventional cars....
						if (r <= 0.6939) {//produces "true" 69.39% of the time
							//Value is specified by the Excel sheet "Calculation_PRAV_FleetSize_ForMATSim"
							//Values used in the Excel sheet come from Teilprojekt 5: Mischverkehr
							//Specifically, for the scenarios "A" and "B", the numbers are from the document 
							//"Ergebnisse aus dem Flottenmodell des TP5 als Input f�r MatSim (TP2)" Fassung 31.05.19
							//which is a document provided direclty to us (we are TP2) from TP5 via e-mail. 
							//For scenario "C", the numbers are from the document
							//"TP 5: Migrationsszenarien>Prognose Fahryeugbestand: Szenario Pro sharing (Extremszenario)"
							//which is a slide from TP5's presentation at the BK4 on 18.06.2019
						
							person.getAttributes().putAttribute(AV_AVAILABILITY_ATTRIBUTE,"car");
							person.getAttributes().putAttribute("carAvail", carAvailability);
						//currently only differentiating between conventional vehicles and "highly automated, highly networked" vehicles
							
						//if more types of vehicles are required in the future, you can uncomment the code below. 
						// BE SURE TO CHANGE THE VALUES OF r, THOUGH, TO BE REASONABLE ASSUMPTIONS WITHIN THE CONTEXT OF YOUR PROJECT!
						/*} else if (r <= 0.66) {//produces "true" 65.91% of the time: because the first step took 42.05% of the agents already,
							// then this step will only consider the next 23.86% of agents
						
							person.getAttributes().putAttribute(AV_AVAILABILITY_ATTRIBUTE,"prav3");
							person.getAttributes().putAttribute("carAvail", "never");
						
						} else if (r <= 0.82){
							person.getAttributes().putAttribute(AV_AVAILABILITY_ATTRIBUTE,"prav4");
							person.getAttributes().putAttribute("carAvail", "never");
						*/
							
						//now, for the other lucky few, they now will have an autonomous vehicle!
						//we also specify that agents never have BOTH a conventional car and an autonomous vehicle
						} else {
							person.getAttributes().putAttribute(AV_AVAILABILITY_ATTRIBUTE,"prav5");
							person.getAttributes().putAttribute("carAvail", "never");
						}
					
					} else {  // make sure that those agents who "never" have a car available also do not have an private av available,and to 
						// prevent "null" exceptions from Java, which occur when a variable, such as the attribute "prAvAvail" don't have a value
						person.getAttributes().putAttribute(AV_AVAILABILITY_ATTRIBUTE,"noPrav");
						person.getAttributes().putAttribute("carAvail", "never");
					}
				
			} else if (ASTRA_Scenario.equals("B")&& year >= 2040){
				
				if(carAvailability.equals(string1)|| carAvailability.equals(string2)){
					double r = random.nextDouble();
					if (r <= 0.88770) {//produces "true" 84.70% of the time
						//if they are part of this lucky percentage, they have "decided" to keep their/purchase a car, else...
						//Where does the value come from? 
						//Value is specified by the Excel sheet "Calculation_PRAV_FleetSize_ForMATSim"
						//Values used in the Excel sheet come from Teilprojekt 5: Mischverkehr
						//Specifically, for the scenarios "A" and "B", the numbers are from the document 
						//"Ergebnisse aus dem Flottenmodell des TP5 als Input f�r MatSim (TP2)" Fassung 31.05.19
						//which is a document provided direclty to us (we are TP2) from TP5 via e-mail. 
						//For scenario "C", the numbers are from the document
						//"TP 5: Migrationsszenarien>Prognose Fahryeugbestand: Szenario Pro sharing (Extremszenario)"
						//which is a slide from TP5's presentation at the BK4 on 18.06.2019
						person.getAttributes().putAttribute("carAvail", carAvailability);
					} else { //...else they have "decided" to give up their/not purchase a car! They've converted to the sharing economy! 
						person.getAttributes().putAttribute("carAvail", "never");
					}
				}
				
				carAvailability = (String) person.getAttributes().getAttribute("carAvail");
				
				//now, for the agents that still have cars, aka whose attribute "carAvail" still equals "always" or "sometimes"...
				//let us assign which one of them has a car capable of autonomous operation...
				if(carAvailability.equals(string1)|| carAvailability.equals(string2)){
					double r = random.nextDouble();
					 
					// these agents still have good ol' conventional cars....
						if (r <= 0.9485) {//produces "true" 94.85% of the time
							//Value is specified by the Excel sheet "Calculation_PRAV_FleetSize_ForMATSim"
							//Values used in the Excel sheet come from Teilprojekt 5: Mischverkehr
							//Specifically, for the scenarios "A" and "B", the numbers are from the document 
							//"Ergebnisse aus dem Flottenmodell des TP5 als Input f�r MatSim (TP2)" Fassung 31.05.19
							//which is a document provided direclty to us (we are TP2) from TP5 via e-mail. 
							//For scenario "C", the numbers are from the document
							//"TP 5: Migrationsszenarien>Prognose Fahryeugbestand: Szenario Pro sharing (Extremszenario)"
							//which is a slide from TP5's presentation at the BK4 on 18.06.2019
						
							person.getAttributes().putAttribute(AV_AVAILABILITY_ATTRIBUTE,"car");
							person.getAttributes().putAttribute("carAvail", carAvailability);
						//currently only differentiating between conventional vehicles and "highly automated, highly networked" vehicles
							
						//if more types of vehicles are required in the future, you can uncomment the code below. 
						// BE SURE TO CHANGE THE VALUES OF r, THOUGH, TO BE REASONABLE ASSUMPTIONS WITHIN THE CONTEXT OF YOUR PROJECT!
						/*} else if (r <= 0.66) {//produces "true" 65.91% of the time: because the first step took 42.05% of the agents already,
							// then this step will only consider the next 23.86% of agents
						
							person.getAttributes().putAttribute(AV_AVAILABILITY_ATTRIBUTE,"prav3");
							person.getAttributes().putAttribute("carAvail", "never");
						
						} else if (r <= 0.82){
							person.getAttributes().putAttribute(AV_AVAILABILITY_ATTRIBUTE,"prav4");
							person.getAttributes().putAttribute("carAvail", "never");
						*/
							
						//now, for the other lucky few, they now will have an autonomous vehicle
						//we also specify that agents never have BOTH a conventional car and an autonomous vehicle
						} else {
							person.getAttributes().putAttribute(AV_AVAILABILITY_ATTRIBUTE,"prav5");
							person.getAttributes().putAttribute("carAvail", "never");
						}
					
					} else {  // make sure that those agents who "never" have a car available also do not have an private av available,and to 
						// prevent "null" exceptions from Java, which occur when a variable, such as the attribute "prAvAvail" don't have a value
						person.getAttributes().putAttribute(AV_AVAILABILITY_ATTRIBUTE,"noPrav");
						person.getAttributes().putAttribute("carAvail", "never");
					}
				
		} else if (ASTRA_Scenario.equals("B")&& year >= 2030){
			
			if(carAvailability.equals(string1)|| carAvailability.equals(string2)){
				double r = random.nextDouble();
				if (r <= 0.9886) {//produces "true" 94.70% of the time
					//if they are part of this lucky percentage, they have "decided" to keep their/purchase a car, else...
					//Where does the value come from?
					//Value is specified by the Excel sheet "Calculation_PRAV_FleetSize_ForMATSim"
					//Values used in the Excel sheet come from Teilprojekt 5: Mischverkehr
					//Specifically, for the scenarios "A" and "B", the numbers are from the document 
					//"Ergebnisse aus dem Flottenmodell des TP5 als Input f�r MatSim (TP2)" Fassung 31.05.19
					//which is a document provided direclty to us (we are TP2) from TP5 via e-mail. 
					//For scenario "C", the numbers are from the document
					//"TP 5: Migrationsszenarien>Prognose Fahryeugbestand: Szenario Pro sharing (Extremszenario)"
					//which is a slide from TP5's presentation at the BK4 on 18.06.2019
					person.getAttributes().putAttribute("carAvail", carAvailability);
				} else { //...else they have "decided" to give up their/not purchase a car! They've converted to the sharing economy! 
					person.getAttributes().putAttribute("carAvail", "never");
				}
			}
			
			carAvailability = (String) person.getAttributes().getAttribute("carAvail");
			
			//now, for the agents that still have cars, aka whose attribute "carAvail" still equals "always" or "sometimes"...
			//let us assign which one of them has a car capable of autonomous operation...
			if(carAvailability.equals(string1)|| carAvailability.equals(string2)){
				double r = random.nextDouble();
				 
				// these agents still have good ol' conventional cars....
					if (r <= 0.9957) {//produces "true" 99.50% of the time
						//Value is specified by the Excel sheet "Calculation_PRAV_FleetSize_ForMATSim"
						//Values used in the Excel sheet come from Teilprojekt 5: Mischverkehr
						//Specifically, for the scenarios "A" and "B", the numbers are from the document 
						//"Ergebnisse aus dem Flottenmodell des TP5 als Input f�r MatSim (TP2)" Fassung 31.05.19
						//which is a document provided direclty to us (we are TP2) from TP5 via e-mail. 
						//For scenario "C", the numbers are from the document
						//"TP 5: Migrationsszenarien>Prognose Fahryeugbestand: Szenario Pro sharing (Extremszenario)"
						//which is a slide from TP5's presentation at the BK4 on 18.06.2019
					
						person.getAttributes().putAttribute(AV_AVAILABILITY_ATTRIBUTE,"car");
						person.getAttributes().putAttribute("carAvail", carAvailability);
					//currently only differentiating between conventional vehicles and "highly automated, highly networked" vehicles
						
					//if more types of vehicles are required in the future, you can uncomment the code below. 
					// BE SURE TO CHANGE THE VALUES OF r, THOUGH, TO BE REASONABLE ASSUMPTIONS WITHIN THE CONTEXT OF YOUR PROJECT!
					/*} else if (r <= 0.66) {//produces "true" 65.91% of the time: because the first step took 42.05% of the agents already,
						// then this step will only consider the next 23.86% of agents
					
						person.getAttributes().putAttribute(AV_AVAILABILITY_ATTRIBUTE,"prav3");
						person.getAttributes().putAttribute("carAvail", "never");
					
					} else if (r <= 0.82){
						person.getAttributes().putAttribute(AV_AVAILABILITY_ATTRIBUTE,"prav4");
						person.getAttributes().putAttribute("carAvail", "never");
					*/
						
					//now, for the other lucky few, they now will have an autonomous vehicle
						//we also specify that agents never have BOTH a conventional car and an autonomous vehicle
					} else {
						person.getAttributes().putAttribute(AV_AVAILABILITY_ATTRIBUTE,"prav5");
						person.getAttributes().putAttribute("carAvail", "never");
					}
				
				} else {  // make sure that those agents who "never" have a car available also do not have an private av available,and to 
					// prevent "null" exceptions from Java, which occur when a variable, such as the attribute "prAvAvail" don't have a value
					person.getAttributes().putAttribute(AV_AVAILABILITY_ATTRIBUTE,"noPrav");
					person.getAttributes().putAttribute("carAvail", "never");
				}
		} else if (ASTRA_Scenario.equals("B")&& year <= 2030){
			
			if(carAvailability.equals(string1)|| carAvailability.equals(string2)){
				
				person.getAttributes().putAttribute(AV_AVAILABILITY_ATTRIBUTE,"car");
				person.getAttributes().putAttribute("carAvail", carAvailability);
			
		
			} else {  // make sure that those agents who "never" have a car available also do not have an private av available,and to 
				// prevent "null" exceptions from Java, which occur when a variable, such as the attribute "prAvAvail" don't have a value
				person.getAttributes().putAttribute(AV_AVAILABILITY_ATTRIBUTE,"noPrav");
				person.getAttributes().putAttribute("carAvail", "never");
			
			}
		} else if (ASTRA_Scenario.equals("C")&& year >= 2050){
			
			
			if(carAvailability.equals(string1)|| carAvailability.equals(string2)){
				double r = random.nextDouble();
				if (r <= 0.4419) {//produces "true" 44.19% of the time
					//if they are part of this lucky percentage, they have "decided" to keep their/purchase a car, else...
					//Where does the value come from?
					//Value is specified by the Excel sheet "Calculation_PRAV_FleetSize_ForMATSim"
					//Values used in the Excel sheet come from Teilprojekt 5: Mischverkehr
					//Specifically, for the scenarios "A" and "B", the numbers are from the document 
					//"Ergebnisse aus dem Flottenmodell des TP5 als Input f�r MatSim (TP2)" Fassung 31.05.19
					//which is a document provided direclty to us (we are TP2) from TP5 via e-mail. 
					//For scenario "C", the numbers are from the document
					//"TP 5: Migrationsszenarien>Prognose Fahryeugbestand: Szenario Pro sharing (Extremszenario)"
					//which is a slide from TP5's presentation at the BK4 on 18.06.2019
					person.getAttributes().putAttribute("carAvail", carAvailability);
				} else { //...else they have "decided" to give up their/not purchase a car! They've converted to the sharing economy! 
					person.getAttributes().putAttribute("carAvail", "never");
				}
			}
			
			carAvailability = (String) person.getAttributes().getAttribute("carAvail");
			
			//now, for the agents that still have cars, aka whose attribute "carAvail" still equals "always" or "sometimes"...
			//let us assign which one of them has a car capable of autonomous operation...
			if(carAvailability.equals(string1)|| carAvailability.equals(string2)){
				double r = random.nextDouble();
				 
				// these agents still have good ol' conventional cars....
					if (r <= 0.4426) {//produces "true" 44.26% of the time
						//Value is specified by the Excel sheet "Calculation_PRAV_FleetSize_ForMATSim"
						//Values used in the Excel sheet come from Teilprojekt 5: Mischverkehr
						//Specifically, for the scenarios "A" and "B", the numbers are from the document 
						//"Ergebnisse aus dem Flottenmodell des TP5 als Input f�r MatSim (TP2)" Fassung 31.05.19
						//which is a document provided direclty to us (we are TP2) from TP5 via e-mail. 
						//For scenario "C", the numbers are from the document
						//"TP 5: Migrationsszenarien>Prognose Fahryeugbestand: Szenario Pro sharing (Extremszenario)"
						//which is a slide from TP5's presentation at the BK4 on 18.06.2019
					
						person.getAttributes().putAttribute(AV_AVAILABILITY_ATTRIBUTE,"car");
						person.getAttributes().putAttribute("carAvail", carAvailability);
					//currently only differentiating between conventional vehicles and "highly automated, highly networked" vehicles
						
					//if more types of vehicles are required in the future, you can uncomment the code below. 
					// BE SURE TO CHANGE THE VALUES OF r, THOUGH, TO BE REASONABLE ASSUMPTIONS WITHIN THE CONTEXT OF YOUR PROJECT!
					/*} else if (r <= 0.66) {//produces "true" 65.91% of the time: because the first step took 42.05% of the agents already,
						// then this step will only consider the next 23.86% of agents
					
						person.getAttributes().putAttribute(AV_AVAILABILITY_ATTRIBUTE,"prav3");
						person.getAttributes().putAttribute("carAvail", "never");
					
					} else if (r <= 0.82){
						person.getAttributes().putAttribute(AV_AVAILABILITY_ATTRIBUTE,"prav4");
						person.getAttributes().putAttribute("carAvail", "never");
					*/
						
					//now, for the other lucky few, they now will have an autonomous vehicle
						//we also specify that agents never have BOTH a conventional car and an autonomous vehicle
					} else {
						person.getAttributes().putAttribute(AV_AVAILABILITY_ATTRIBUTE,"prav5");
						person.getAttributes().putAttribute("carAvail", "never");
					}
				
				} else {  // make sure that those agents who "never" have a car available also do not have an private av available,and to 
					// prevent "null" exceptions from Java, which occur when a variable, such as the attribute "prAvAvail" don't have a value
					person.getAttributes().putAttribute(AV_AVAILABILITY_ATTRIBUTE,"noPrav");
					person.getAttributes().putAttribute("carAvail", "never");
				}
		} else if (ASTRA_Scenario.equals("C")&& year >= 2040){
			 
			if(carAvailability.equals(string1)|| carAvailability.equals(string2)){
				double r = random.nextDouble();
				if (r <= 0.8752) {//produces "true" 87.52% of the time
					//if they are part of this lucky percentage, they have "decided" to keep their/purchase a car, else...
					//Where does the value come from?
					//Value is specified by the Excel sheet "Calculation_PRAV_FleetSize_ForMATSim"
					//Values used in the Excel sheet come from Teilprojekt 5: Mischverkehr
					//Specifically, for the scenarios "A" and "B", the numbers are from the document 
					//"Ergebnisse aus dem Flottenmodell des TP5 als Input f�r MatSim (TP2)" Fassung 31.05.19
					//which is a document provided direclty to us (we are TP2) from TP5 via e-mail. 
					//For scenario "C", the numbers are from the document
					//"TP 5: Migrationsszenarien>Prognose Fahryeugbestand: Szenario Pro sharing (Extremszenario)"
					//which is a slide from TP5's presentation at the BK4 on 18.06.2019
					person.getAttributes().putAttribute("carAvail", carAvailability);
				} else { //...else they have "decided" to give up their/not purchase a car! They've converted to the sharing economy! 
					person.getAttributes().putAttribute("carAvail", "never");
				}
			}
			
			carAvailability = (String) person.getAttributes().getAttribute("carAvail");
			
			//now, for the agents that still have cars, aka whose attribute "carAvail" still equals "always" or "sometimes"...
			//let us assign which one of them has a car capable of autonomous operation...
			if(carAvailability.equals(string1)|| carAvailability.equals(string2)){
				double r = random.nextDouble();
				 
				// these agents still have good ol' conventional cars....
					if (r <= 0.8626) {//produces "true" 86.26% of the time
						//Value is specified by the Excel sheet "Calculation_PRAV_FleetSize_ForMATSim"
						//Values used in the Excel sheet come from Teilprojekt 5: Mischverkehr
						//Specifically, for the scenarios "A" and "B", the numbers are from the document 
						//"Ergebnisse aus dem Flottenmodell des TP5 als Input f�r MatSim (TP2)" Fassung 31.05.19
						//which is a document provided direclty to us (we are TP2) from TP5 via e-mail. 
						//For scenario "C", the numbers are from the document
						//"TP 5: Migrationsszenarien>Prognose Fahryeugbestand: Szenario Pro sharing (Extremszenario)"
						//which is a slide from TP5's presentation at the BK4 on 18.06.2019
					
						person.getAttributes().putAttribute(AV_AVAILABILITY_ATTRIBUTE,"car");
						person.getAttributes().putAttribute("carAvail", carAvailability);
					//currently only differentiating between conventional vehicles and "highly automated, highly networked" vehicles
						
					//if more types of vehicles are required in the future, you can uncomment the code below. 
					// BE SURE TO CHANGE THE VALUES OF r, THOUGH, TO BE REASONABLE ASSUMPTIONS WITHIN THE CONTEXT OF YOUR PROJECT!
					/*} else if (r <= 0.66) {//produces "true" 65.91% of the time: because the first step took 42.05% of the agents already,
						// then this step will only consider the next 23.86% of agents
					
						person.getAttributes().putAttribute(AV_AVAILABILITY_ATTRIBUTE,"prav3");
						person.getAttributes().putAttribute("carAvail", "never");
					
					} else if (r <= 0.82){
						person.getAttributes().putAttribute(AV_AVAILABILITY_ATTRIBUTE,"prav4");
						person.getAttributes().putAttribute("carAvail", "never");
					*/
						
					//now, for the other lucky few, they now will have an autonomous vehicle
						//we also specify that agents never have BOTH a conventional car and an autonomous vehicle
					} else {
						person.getAttributes().putAttribute(AV_AVAILABILITY_ATTRIBUTE,"prav5");
						person.getAttributes().putAttribute("carAvail", "never");
					}
				
				} else {  // make sure that those agents who "never" have a car available also do not have an private av available,and to 
					// prevent "null" exceptions from Java, which occur when a variable, such as the attribute "prAvAvail" don't have a value
					person.getAttributes().putAttribute(AV_AVAILABILITY_ATTRIBUTE,"noPrav");
					person.getAttributes().putAttribute("carAvail", "never");
				}
		} else if (ASTRA_Scenario.equals("C")&& year >= 2030){ 
			
			//According to the projections of TP5 (TUM and Gruner), private car ownership in 2030 in this "extreme" scenario will be
			// 100.000569% of what it is in 2030 in the A or "business as usual scenario". So the same, really. 
			//Thus, no current car "owners" will "give up" their cars. 
			
			/* if(carAvailability.equals(string1)|| carAvailability.equals(string2)){
				double r = random.nextDouble();
				if (r <= 0.9886) {//produces "true" 94.70% of the time
					//if they are part of this lucky percentage, they have "decided" to keep their/purchase a car, else...
					person.getAttributes().putAttribute("carAvail", carAvailability);
				} else { //...else they have "decided" to give up their/not purchase a car! They've converted to the sharing economy! 
					person.getAttributes().putAttribute("carAvail", "never");
				}
			}
			
			carAvailability = (String) person.getAttributes().getAttribute("carAvail");*/
			
			//now, for the agents that still have cars, aka whose attribute "carAvail" still equals "always" or "sometimes"...
			//let us assign which one of them has a car capable of autonomous operation...
			if(carAvailability.equals(string1)|| carAvailability.equals(string2)){
				double r = random.nextDouble();
				 
				// these agents still have good ol' conventional cars....
					if (r <= 0.9847) {//produces "true" 98.47% of the time
						//Value is specified by the Excel sheet "Calculation_PRAV_FleetSize_ForMATSim"
						//Values used in the Excel sheet come from Teilprojekt 5: Mischverkehr
						//Specifically, for the scenarios "A" and "B", the numbers are from the document 
						//"Ergebnisse aus dem Flottenmodell des TP5 als Input f�r MatSim (TP2)" Fassung 31.05.19
						//which is a document provided direclty to us (we are TP2) from TP5 via e-mail. 
						//For scenario "C", the numbers are from the document
						//"TP 5: Migrationsszenarien>Prognose Fahryeugbestand: Szenario Pro sharing (Extremszenario)"
						//which is a slide from TP5's presentation at the BK4 on 18.06.2019
					
						person.getAttributes().putAttribute(AV_AVAILABILITY_ATTRIBUTE,"car");
						person.getAttributes().putAttribute("carAvail", carAvailability);
					//currently only differentiating between conventional vehicles and "highly automated, highly networked" vehicles
						
					//if more types of vehicles are required in the future, you can uncomment the code below. 
					// BE SURE TO CHANGE THE VALUES OF r, THOUGH, TO BE REASONABLE ASSUMPTIONS WITHIN THE CONTEXT OF YOUR PROJECT!
					/*} else if (r <= 0.66) {//produces "true" 65.91% of the time: because the first step took 42.05% of the agents already,
						// then this step will only consider the next 23.86% of agents
					
						person.getAttributes().putAttribute(AV_AVAILABILITY_ATTRIBUTE,"prav3");
						person.getAttributes().putAttribute("carAvail", "never");
					
					} else if (r <= 0.82){
						person.getAttributes().putAttribute(AV_AVAILABILITY_ATTRIBUTE,"prav4");
						person.getAttributes().putAttribute("carAvail", "never");
					*/
						
					//now, for the other lucky few, they now will have an autonomous vehicle
						//we also specify that agents never have BOTH a conventional car and an autonomous vehicle
					} else {
						person.getAttributes().putAttribute(AV_AVAILABILITY_ATTRIBUTE,"prav5");
						person.getAttributes().putAttribute("carAvail", "never");
					}
				
				} else {  // make sure that those agents who "never" have a car available also do not have an private av available,and to 
					// prevent "null" exceptions from Java, which occur when a variable, such as the attribute "prAvAvail" don't have a value
					person.getAttributes().putAttribute(AV_AVAILABILITY_ATTRIBUTE,"noPrav");
					person.getAttributes().putAttribute("carAvail", "never");
				}	
		} else if (ASTRA_Scenario.equals("C")&& year <= 2030){
			
			if(carAvailability.equals(string1)|| carAvailability.equals(string2)){
				
				person.getAttributes().putAttribute(AV_AVAILABILITY_ATTRIBUTE,"car");
				person.getAttributes().putAttribute("carAvail", carAvailability);
			
		
			} else {  // make sure that those agents who "never" have a car available also do not have an private av available,and to 
				// prevent "null" exceptions from Java, which occur when a variable, such as the attribute "prAvAvail" don't have a value
				person.getAttributes().putAttribute(AV_AVAILABILITY_ATTRIBUTE,"noPrav");
				person.getAttributes().putAttribute("carAvail", "never");
			
			}			
		} else if (ASTRA_Scenario.equals("D")){
			if(carAvailability.equals(string1)|| carAvailability.equals(string2)){
				//everyone who owns a car has an automated one. 
						person.getAttributes().putAttribute(AV_AVAILABILITY_ATTRIBUTE,"prav5");
						person.getAttributes().putAttribute("carAvail", "never");
				
				} else {  // make sure that those agents who "never" have a car available also do not have an private av available,and to 
					// prevent "null" exceptions from Java, which occur when a variable, such as the attribute "prAvAvail" don't have a value
					person.getAttributes().putAttribute(AV_AVAILABILITY_ATTRIBUTE,"noPrav");
					person.getAttributes().putAttribute("carAvail", "never");
				}
		} else if (ASTRA_Scenario.equals("E")&& year >= 2050){
			 
			
				if(carAvailability.equals(string1)|| carAvailability.equals(string2)){
					double r = random.nextDouble();
					if (r <= 0.4419) {//produces "true" 44.19% of the time
						//if they are part of this lucky percentage, they have "decided" to keep their/purchase a car, else...
						//Value is the same as for scenario "C", because scenario "E" is the same as "C" except all cars are AVs
						//Value is specified by the Excel sheet "Calculation_PRAV_FleetSize_ForMATSim.
						//Values used in the Excel sheet come from Teilprojekt 5: Mischverkehr
						//Specifically, for the scenarios "A" and "B", the numbers are from the document 
						//"Ergebnisse aus dem Flottenmodell des TP5 als Input f�r MatSim (TP2)" Fassung 31.05.19
						//which is a document provided direclty to us (we are TP2) from TP5 via e-mail. 
						//For scenario "C", the numbers are from the document
						//"TP 5: Migrationsszenarien>Prognose Fahryeugbestand: Szenario Pro sharing (Extremszenario)"
						//which is a slide from TP5's presentation at the BK4 on 18.06.2019
						person.getAttributes().putAttribute("carAvail", carAvailability);
					} else { //...else they have "decided" to give up their/not purchase a car! They've converted to the sharing economy! 
						person.getAttributes().putAttribute("carAvail", "never");
					}
				}
			
				carAvailability = (String) person.getAttributes().getAttribute("carAvail");
			
				//now, for the agents that still have cars, aka whose attribute "carAvail" still equals "always" or "sometimes"...
				//let us assign which one of them has a car capable of autonomous operation...namely all of them
				if(carAvailability.equals(string1)|| carAvailability.equals(string2)){
					
					person.getAttributes().putAttribute(AV_AVAILABILITY_ATTRIBUTE,"prav5");
					person.getAttributes().putAttribute("carAvail", "never");
			
				} else {  // make sure that those agents who "never" have a car available also do not have an private av available,and to 
					// prevent "null" exceptions from Java, which occur when a variable, such as the attribute "prAvAvail" don't have a value
					person.getAttributes().putAttribute(AV_AVAILABILITY_ATTRIBUTE,"noPrav");
					person.getAttributes().putAttribute("carAvail", "never");
				}
		} else if (ASTRA_Scenario.equals("E")&& year >= 2040){
			
		
				if(carAvailability.equals(string1)|| carAvailability.equals(string2)){
					double r = random.nextDouble();
					if (r <= 0.8752) {//produces "true" 87.52% of the time
						//if they are part of this lucky percentage, they have "decided" to keep their/purchase a car, else...
						//Value is the same as for scenario "C", because scenario "E" is the same as "C" except all cars are AVs
						//Value is specified by the Excel sheet "Calculation_PRAV_FleetSize_ForMATSim.
						//Values used in the Excel sheet come from Teilprojekt 5: Mischverkehr
						//Specifically, for the scenarios "A" and "B", the numbers are from the document 
						//"Ergebnisse aus dem Flottenmodell des TP5 als Input f�r MatSim (TP2)" Fassung 31.05.19
						//which is a document provided direclty to us (we are TP2) from TP5 via e-mail. 
						//For scenario "C", the numbers are from the document
						//"TP 5: Migrationsszenarien>Prognose Fahryeugbestand: Szenario Pro sharing (Extremszenario)"
						//which is a slide from TP5's presentation at the BK4 on 18.06.2019
						person.getAttributes().putAttribute("carAvail", carAvailability);
					} else { //...else they have "decided" to give up their/not purchase a car! They've converted to the sharing economy! 
						person.getAttributes().putAttribute("carAvail", "never");
					}
				}
		
				carAvailability = (String) person.getAttributes().getAttribute("carAvail");
		
				//now, for the agents that still have cars, aka whose attribute "carAvail" still equals "always" or "sometimes"...
				//let us assign which one of them has a car capable of autonomous operation...namely all of them
				if(carAvailability.equals(string1)|| carAvailability.equals(string2)){
				
					person.getAttributes().putAttribute(AV_AVAILABILITY_ATTRIBUTE,"prav5");
					person.getAttributes().putAttribute("carAvail", "never");
		
				} else {  // make sure that those agents who "never" have a car available also do not have an private av available,and to 
					// prevent "null" exceptions from Java, which occur when a variable, such as the attribute "prAvAvail" don't have a value
					person.getAttributes().putAttribute(AV_AVAILABILITY_ATTRIBUTE,"noPrav");
					person.getAttributes().putAttribute("carAvail", "never");
				}
		} else if (ASTRA_Scenario.equals("E")&& year >= 2030){ 
				//Value is the same as for scenario "C", because scenario "E" is the same as "C" except all cars are AVs
				//According to the projections of TP5 (TUM and Gruner), private car ownership in 2030 in this "extreme" scenario will be
				// 100.000569% of what it is in 2030 in the A or "business as usual scenario". So the same, really. 
				//Thus, no current car "owners" will "give up" their cars. 
				//if(carAvailability.equals(string1)|| carAvailability.equals(string2)){
					//double r = random.nextDouble();
					//if (r <= 0.9847) {//produces "true" 98.47% of the time
						//if they are part of this lucky percentage, they have "decided" to keep their/purchase a car, else...
						//person.getAttributes().putAttribute("carAvail", carAvailability);
					//} else { //...else they have "decided" to give up their/not purchase a car! They've converted to the sharing economy! 
						//person.getAttributes().putAttribute("carAvail", "never");
					//}
				//}
	
				carAvailability = (String) person.getAttributes().getAttribute("carAvail");
	
				//now, for the agents that still have cars, aka whose attribute "carAvail" still equals "always" or "sometimes"...
				//let us assign which one of them has a car capable of autonomous operation...namely all of them
				if(carAvailability.equals(string1)|| carAvailability.equals(string2)){
			
					person.getAttributes().putAttribute(AV_AVAILABILITY_ATTRIBUTE,"prav5");
					person.getAttributes().putAttribute("carAvail", "never");
	
				} else {  // make sure that those agents who "never" have a car available also do not have an private av available,and to 
					// prevent "null" exceptions from Java, which occur when a variable, such as the attribute "prAvAvail" don't have a value
					person.getAttributes().putAttribute(AV_AVAILABILITY_ATTRIBUTE,"noPrav");
					person.getAttributes().putAttribute("carAvail", "never");
				}
		} else if (ASTRA_Scenario.equals("E")&& year <= 2030){
			
				if(carAvailability.equals(string1)|| carAvailability.equals(string2)){
				
					person.getAttributes().putAttribute(AV_AVAILABILITY_ATTRIBUTE,"car");
					person.getAttributes().putAttribute("carAvail", carAvailability);
			
		
				} else {  // make sure that those agents who "never" have a car available also do not have an private av available,and to 
					// prevent "null" exceptions from Java, which occur when a variable, such as the attribute "prAvAvail" don't have a value
					person.getAttributes().putAttribute(AV_AVAILABILITY_ATTRIBUTE,"noPrav");
					person.getAttributes().putAttribute("carAvail", "never");
			
				}
		} else {
			System.out.println("Error: Scenario must be A, B, C, D, or E");
		}
	}
			
			
		// should also set selected plan to choose AV in order to start the 
		// scenario with the percentages given by TP5 as a starting point....		
		for (Person person : scenario.getPopulation().getPersons().values()) {
			for (Plan plan : person.getPlans()){
				for (PlanElement element : plan.getPlanElements()) {
					if (element instanceof Leg) {
						Leg leg = (Leg) element;
						
						if (leg.getMode().equals("car")) {
							String pravAvailability = (String) person.getAttributes().getAttribute(AV_AVAILABILITY_ATTRIBUTE);
							
							if (pravAvailability.equals("noPrav")) {
								leg.setMode("walk"); 
							} else {
								leg.setMode(pravAvailability);
							}
						}
					}
				}
			}
		}
		//assign freight vehicles av property or not. 

		for (Person person : scenario.getPopulation().getPersons().values()) {	
			Boolean isFreight = (Boolean) person.getAttributes().getAttribute("isFreight");
			if (isFreight != null && isFreight) {
				if( ASTRA_Scenario.equals("A") || ASTRA_Scenario.equals("B") || ASTRA_Scenario.equals("C") && year >= 2050){

						double r = random.nextDouble();
						
							if (r <= 0.6092) {
								//Value is specified by the Excel sheet "Calculation_PRAV_FleetSize_ForMATSim.
								//Values used in the Excel sheet come from Teilprojekt 5: Mischverkehr
								//Specifically, for the scenarios "A" and "B", the numbers are from the document 
								//"Ergebnisse aus dem Flottenmodell des TP5 als Input f�r MatSim (TP2)" Fassung 31.05.19
								//which is a document provided direclty to us (we are TP2) from TP5 via e-mail. 
								//For scenario "C", the numbers are from the document
								//"TP 5: Migrationsszenarien>Prognose Fahryeugbestand: Szenario Pro sharing (Extremszenario)"
								//which is a slide from TP5's presentation at the BK4 on 18.06.2019
								person.getAttributes().putAttribute(FREIGHT_AV,false);
							
							} else {
								person.getAttributes().putAttribute(FREIGHT_AV,true);
							}	
				} else if(ASTRA_Scenario.equals("A") || ASTRA_Scenario.equals("B") || ASTRA_Scenario.equals("C") && year >= 2040){

					double r = random.nextDouble();
					
					if (r <= 0.9268) {
						//Value is specified by the Excel sheet "Calculation_PRAV_FleetSize_ForMATSim.
						//Values used in the Excel sheet come from Teilprojekt 5: Mischverkehr
						//Specifically, for the scenarios "A" and "B", the numbers are from the document 
						//"Ergebnisse aus dem Flottenmodell des TP5 als Input f�r MatSim (TP2)" Fassung 31.05.19
						//which is a document provided direclty to us (we are TP2) from TP5 via e-mail. 
						//For scenario "C", the numbers are from the document
						//"TP 5: Migrationsszenarien>Prognose Fahryeugbestand: Szenario Pro sharing (Extremszenario)"
						//which is a slide from TP5's presentation at the BK4 on 18.06.2019
					
						person.getAttributes().putAttribute(FREIGHT_AV,false);
					
					} else {
						person.getAttributes().putAttribute(FREIGHT_AV,true);
						
					}
				} else if( ASTRA_Scenario.equals("A") || ASTRA_Scenario.equals("B") || ASTRA_Scenario.equals("C") && year >= 2030){

					double r = random.nextDouble();
					
					if (r <= 0.9940) {
						//Value is specified by the Excel sheet "Calculation_PRAV_FleetSize_ForMATSim.
						//Values used in the Excel sheet come from Teilprojekt 5: Mischverkehr
						//Specifically, for the scenarios "A" and "B", the numbers are from the document 
						//"Ergebnisse aus dem Flottenmodell des TP5 als Input f�r MatSim (TP2)" Fassung 31.05.19
						//which is a document provided direclty to us (we are TP2) from TP5 via e-mail. 
						//For scenario "C", the numbers are from the document
						//"TP 5: Migrationsszenarien>Prognose Fahryeugbestand: Szenario Pro sharing (Extremszenario)"
						//which is a slide from TP5's presentation at the BK4 on 18.06.2019
					
						person.getAttributes().putAttribute(FREIGHT_AV,false);
					
					} else {
						person.getAttributes().putAttribute(FREIGHT_AV,true);
					}
					
				} else if( ASTRA_Scenario.equals("D")){
					//Scenario "D" is one of the scenarios in which all motorized vehicles are AVs
						person.getAttributes().putAttribute(FREIGHT_AV,true);
						//It turned out that 20 iterations are not enough for all frieght agents to go through
						//the "replanning" aka the choice model of choosing a new route and thus also their mode.
						//Thus, not all trucks were driving as AVs. 
						//So, the following code was implemented to make sure all trucks also have plans that use truckAv.
						//TODO: 2020-06-29 clivings Post-Project improvement: Make sure the plans reflect the vehicle type right at the begining for trucks in A, B, and C
						for(PlanElement planElement : person.getSelectedPlan().getPlanElements()) {
							if (planElement instanceof Leg) {
								if (((Leg) planElement).getMode() == "truck") {
									((Leg) planElement).setMode("truckAv");
								}
							}
						}
				} else if( ASTRA_Scenario.equals("E")){
					//Scenario "E" is one of the scenarios in which all motorized vehicles are AVs
						person.getAttributes().putAttribute(FREIGHT_AV,true);
						//It turned out that 20 iterations are not enough for all frieght agents to go through
						//the "replanning" aka the choice model of choosing a new route and thus also their mode.
						//Thus, not all trucks were driving as AVs. 
						//So, the following code was implemented to make sure all trucks also have plans that use truckAv.
						for(PlanElement planElement : person.getSelectedPlan().getPlanElements()) {
							if (planElement instanceof Leg) {
								if (((Leg) planElement).getMode() == "truck") {
									((Leg) planElement).setMode("truckAv");
								}
							}
						}	
				} else {
					System.out.println("Error: Scenario must be A, B, C, D, or E");
				}
			}
		}
			
		//The following code checks for cases in which an agent has both a conventional car and a prav5. 
		//The code above SHOULD take care of this and prevent such a thing, but if you are having 
		//wierd results and suspect this problem, uncomment this code. 
		/*int countTotal = 0;
		int countCarAvail = 0;
		int countPravAvail = 0;
		
		for (Person person : scenario.getPopulation().getPersons().values()) {
			if (!person.getId().toString().contains("freight")) {
				String carAvailability = (String) person.getAttributes().getAttribute("carAvail");
				String pravAvailability = (String) person.getAttributes().getAttribute(AV_AVAILABILITY_ATTRIBUTE);
				
				if (!carAvailability.equals("never")) {
					countCarAvail++;
				}
				
				if (pravAvailability.equals("prav5")) {
					countPravAvail++;
				}
				
				if (!carAvailability.equals("never") && pravAvailability.equals("prav5")) {
					throw new IllegalStateException();
				}
				
				countTotal++;
			}
		}
		
		System.out.println(String.format("Car availability: %.2f%%", 100.0 * (double) countCarAvail / countTotal));
		System.out.println(String.format("Prav availability: %.2f%%", 100.0 * (double) countPravAvail / countTotal));
		System.out.println(String.format("Prav share: %.2f%%", 100.0 * (double) countPravAvail / (countPravAvail + countCarAvail)));*/
	}
}
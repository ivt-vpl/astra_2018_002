package ch.ethz.matsim.projects.astra_2018_002.mode_choice;

import org.matsim.api.core.v01.network.Network;
import org.matsim.core.router.TripRouter;
import org.matsim.core.router.util.TravelTime;
import org.matsim.facilities.ActivityFacilities;
import org.matsim.pt.transitSchedule.api.TransitSchedule;

import ch.ethz.matsim.discrete_mode_choice.model.DiscreteModeChoiceTrip;
import ch.ethz.matsim.projects.astra_2018_002.mode_choice.SwissUtilityParameters.SharedAVParameters;
import ch.ethz.matsim.projects.astra_2018_002.mode_choice.base.BaseUtilityEstimator;
import ch.ethz.matsim.projects.astra_2018_002.mode_choice.base.PersonVariables;
import ch.ethz.matsim.projects.astra_2018_002.mode_choice.base.TripVariablesBike;
import ch.ethz.matsim.projects.astra_2018_002.mode_choice.base.TripVariablesCar;
import ch.ethz.matsim.projects.astra_2018_002.mode_choice.base.TripVariablesPrivateAv;
import ch.ethz.matsim.projects.astra_2018_002.mode_choice.base.TripVariablesPt;
import ch.ethz.matsim.projects.astra_2018_002.mode_choice.base.TripVariablesSharedAv;
import ch.ethz.matsim.projects.astra_2018_002.mode_choice.base.TripVariablesWalk;
import ch.ethz.matsim.projects.astra_2018_002.mode_choice.cost.CostModel;

/**
 * Estimates trip utilities based on
 * 
 * Bundesamt für Raumentwicklung (2017) Analyse der SP-Befragung 2015 zur
 * Verkehrsmodus- und Routenwahl
 */
public class SwissUtilityEstimator extends BaseUtilityEstimator {
	private final SwissUtilityParameters parameters;

	public SwissUtilityEstimator(TripRouter tripRouter, Network network, ActivityFacilities facilities,
			CostModel costModel, TransitSchedule transitSchedule, SwissUtilityParameters parameters,
			TravelTime travelTime) {
		super(tripRouter, network, facilities, costModel, transitSchedule, travelTime);
		this.parameters = parameters;
	}

	private double interaction(double value, double reference, double lambda) {
		return Math.pow(Math.max(0.001, value) / reference, lambda);
	}

	private double estimateSociodemographics(SwissUtilityParameters.SociodemographicsParameters socioParameters,
			PersonVariables personVariables) {
		double utility = 0.0;

		utility += socioParameters.betaAge * personVariables.age_10a;
		utility += socioParameters.betaAgeSquared * Math.pow(personVariables.age_10a, 2.0);
		utility += socioParameters.betaIncome * personVariables.income_1000CHF;
		utility += socioParameters.betaIncomeSquared * Math.pow(personVariables.income_1000CHF, 2.0);

		if (personVariables.isMale) {
			utility += socioParameters.betaMale;
		}

		return utility;
	}

	private double estimatePurpose(SwissUtilityParameters.PurposeParameters purposeParameters,
			DiscreteModeChoiceTrip trip) {
		/*
		 * TODO: How is "purpose" defined in the choice model? What if the destination
		 * purpose is "home"? For now, we use the destination purpose if it is not home,
		 * otherwise origin purpose.
		 */

		String purpose = trip.getDestinationActivity().getType();

		if (purpose.equals("home")) {
			purpose = trip.getOriginActivity().getType();
		}

		switch (purpose) {
		case "work":
			return purposeParameters.betaWork;
		case "education":
			return purposeParameters.betaEducation;
		case "leisure":
			return purposeParameters.betaLeisure;
		case "service":
			return purposeParameters.betaErrand;
		case "shop":
			return purposeParameters.betaShop;
		default:
			return 0.0;
		}
	}

	private double estimateRegion(SwissUtilityParameters.RegionParameters regionParameters,
			PersonVariables personVariables) {
		/*
		 * TODO: How is "region" defined in the choice model? Is it the home location of
		 * the respondent, the origin or destination of the trip?
		 */

		switch (personVariables.regionIndex) {
		case 1:
			return regionParameters.betaRegion1;
		case 2:
			return regionParameters.betaRegion2;
		case 3:
			return regionParameters.betaRegion3;
		default:
			// TODO: Theoretically, this should not happen. But it needs to be fixed in the
			// scenario pipeline!
			return 0.0;
		}
	}

	private double estimateMunicipalityType(
			SwissUtilityParameters.MunicipalityTypeParameters municipalityTypeParameters,
			PersonVariables personVariables) {
		/*
		 * TODO: How is "munipality type" defined in the choice model? Is it the home
		 * location of the respondent, the origin or destination of the trip?
		 */

		switch (personVariables.municipalityType) {
		case Rural:
			return municipalityTypeParameters.betaRural;
		case SubUrban:
			return municipalityTypeParameters.betaSubUrban;
		case Urban:
			return municipalityTypeParameters.betaUrban;
		default:
			throw new IllegalStateException();
		}
	}

	@Override
	public double estimatePtTrip(DiscreteModeChoiceTrip trip, PersonVariables personVariables,
			TripVariablesPt tripVariables) {
		double utility = 0.0;

		utility += parameters.pt.alpha;

		utility += parameters.pt.betaAccessEgressTime * tripVariables.accessEgressTime_min;

		utility += parameters.pt.betaTravelTime
				* interaction(tripVariables.travelDistance_km, parameters.referenceDistance_km,
						parameters.pt.lambdaTravelTimeDistance) //
				* tripVariables.inVehicleTime_min;

		utility += parameters.pt.betaWaitingTime * tripVariables.waitingTime_min;

		utility += parameters.betaCost
				* interaction(tripVariables.travelDistance_km, parameters.referenceDistance_km,
						parameters.pt.lambdaCostDistance) //
				* interaction(personVariables.income_1000CHF, parameters.referenceIncome_1000CHF,
						parameters.lambdaCostIncome) //
				* interaction(tripVariables.crowflyDistance_km, parameters.referenceCrowflyDistance_km,
						parameters.lambdaCostCrowflyDistance) //
				* tripVariables.travelCost_CHF;

		utility += parameters.pt.betaLineSwitch
				* interaction(tripVariables.travelDistance_km, parameters.referenceDistance_km,
						parameters.pt.lambdaLineSwitchesDistance)//
				* tripVariables.lineSwitches;

		utility += parameters.pt.betaHeadway
				* interaction(tripVariables.travelDistance_km, parameters.referenceDistance_km,
						parameters.pt.lambdaHeadwayDistance)//
				* tripVariables.headway_min;

		utility += parameters.pt.betaOccupancy * tripVariables.occupancy;

		switch (tripVariables.mainMode) {
		case Bus:
			utility += parameters.pt.betaMainTransportModeBus;
			break;
		case Rail:
			utility += parameters.pt.betaMainTransportModeRail;
			break;
		case Tram:
			utility += parameters.pt.betaMainTransportModeTram;
			break;
		default:
			throw new IllegalStateException();
		}

		utility += estimateSociodemographics(parameters.pt.sociodemographics, personVariables);
		utility += estimatePurpose(parameters.pt.purpose, trip);
		utility += estimateRegion(parameters.pt.region, personVariables);
		utility += estimateMunicipalityType(parameters.pt.municipalityType, personVariables);

		if (personVariables.hasGeneralSubscription) {
			utility += parameters.pt.betaSubscriptionGA;
		} else if (personVariables.hasHalbtaxSubscription) {
			utility += parameters.pt.betaSubscriptionHalbtax;
		} else if (personVariables.hasRegionalSubscription) {
			utility += parameters.pt.betaSubscriptionRegional;
		} else {
			utility += parameters.pt.betaSubscriptionNone;
		}

		utility += parameters.betaDelay
				* interaction(tripVariables.travelDistance_km, parameters.referenceDistance_km,
						parameters.lambdaDelayDistance) //
				* tripVariables.delay_min;

		return utility;
	}

	@Override
	public double estimateSharedAvTrip(DiscreteModeChoiceTrip trip, PersonVariables personVariables,
			TripVariablesSharedAv tripVariables) {
		double utility = 0.0;

		SharedAVParameters avParameters = tripVariables.isPooled ? parameters.pav : parameters.tav;

		utility += avParameters.alpha;

		utility += avParameters.betaTravelTime //
				* tripVariables.inVehicleTime_min;

		utility += avParameters.betaWaitingTime //
				* tripVariables.waitingTime_min;

		utility += avParameters.betaAccessEgressTime //
				* tripVariables.accessEgressTime_min;

		utility += parameters.betaCost //
				* interaction(tripVariables.crowflyDistance_km, parameters.referenceCrowflyDistance_km,
						parameters.lambdaCostCrowflyDistance) //
				* tripVariables.travelCost_CHF;

		utility += avParameters.betaDelay * tripVariables.delay_min;

		return utility;
	}

	@Override
	public double estimatePrivateAVTrip(DiscreteModeChoiceTrip trip, PersonVariables personVariables,
			TripVariablesPrivateAv tripVariables) {
		double utility = 0.0;
		double betaTravelTime = 0.0;

		switch (tripVariables.automationLevel) {
		case LEVEL3:
			betaTravelTime = parameters.prav.betaTravelTimeConventional;
			break;
		case LEVEL4:
		case LEVEL5:
			betaTravelTime = tripVariables.automatedShare * parameters.prav.betaTravelTimeAutomated
					+ (1.0 - tripVariables.automatedShare) * parameters.prav.betaTravelTimeConventional;
			break;
		default:
			throw new IllegalStateException();
		}

		utility += parameters.prav.alpha;
		utility += betaTravelTime * tripVariables.travelTime_min;

		utility += parameters.betaCost //
				* interaction(tripVariables.crowflyDistance_km, parameters.referenceCrowflyDistance_km,
						parameters.lambdaCostCrowflyDistance) //
				* tripVariables.travelCost_CHF;

		utility += parameters.walk.betaTravelTime * parameters.prav.constantAccessEgressWalkTime_min;
		utility += parameters.prav.betaDelay * tripVariables.delay_min;

		return utility;
	}

	@Override
	public double estimateCarTrip(DiscreteModeChoiceTrip trip, PersonVariables personVariables,
			TripVariablesCar tripVariables) {
		double utility = 0.0;

		utility += parameters.car.alpha;

		utility += parameters.car.betaTravelTime //
				* interaction(tripVariables.travelDistance_km, parameters.referenceDistance_km,
						parameters.car.lambdaTravelTimeDistance) //
				* tripVariables.travelTime_min;

		double parkingSearchTime_min = tripVariables.parkingSearchTime_min
				+ parameters.car.constantParkingSearchPenalty_min;
		utility += parameters.car.betaParkingSearchTime * parkingSearchTime_min;

		utility += parameters.betaCost //
				* interaction(tripVariables.travelDistance_km, parameters.referenceDistance_km,
						parameters.car.lambdaCostDistance) //
				* interaction(personVariables.income_1000CHF, parameters.referenceIncome_1000CHF,
						parameters.lambdaCostIncome) //
				* interaction(tripVariables.crowflyDistance_km, parameters.referenceCrowflyDistance_km,
						parameters.lambdaCostCrowflyDistance) //
				* tripVariables.travelCost_CHF;

		utility += parameters.car.betaParkingCost
				* interaction(personVariables.income_1000CHF, parameters.referenceIncome_1000CHF,
						parameters.lambdaCostIncome) //
				* tripVariables.parkingCost_CHF;

		if (personVariables.carAlwaysAvailable) {
			utility += parameters.car.betaAlwaysAvailable;
		}

		utility += estimateSociodemographics(parameters.car.sociodemographics, personVariables);
		utility += estimatePurpose(parameters.car.purpose, trip);
		utility += estimateRegion(parameters.car.region, personVariables);
		utility += estimateMunicipalityType(parameters.car.municipalityType, personVariables);

		utility += parameters.betaDelay
				* interaction(tripVariables.travelDistance_km, parameters.referenceDistance_km,
						parameters.lambdaDelayDistance) //
				* tripVariables.delay_min;

		utility += parameters.walk.betaTravelTime * parameters.car.constantAccessEgressWalkTime_min;

		return utility;
	}

	@Override
	public double estimateWalkTrip(DiscreteModeChoiceTrip trip, PersonVariables personVariables,
			TripVariablesWalk tripVariables) {
		double utility = 0.0;

		utility += parameters.walk.alpha;
		utility += parameters.walk.betaTravelTime
				* interaction(tripVariables.travelDistance_km, parameters.referenceDistance_km,
						parameters.walk.lambdaTravelTimeDistance) //
				* tripVariables.travelTime_min;

		utility += estimateSociodemographics(parameters.walk.sociodemographics, personVariables);
		utility += estimatePurpose(parameters.walk.purpose, trip);
		utility += estimateRegion(parameters.walk.region, personVariables);
		utility += estimateMunicipalityType(parameters.walk.municipalityType, personVariables);

		utility += parameters.betaDelay
				* interaction(tripVariables.travelDistance_km, parameters.referenceDistance_km,
						parameters.lambdaDelayDistance) //
				* tripVariables.delay_min;

		return utility;
	}

	@Override
	public double estimateBikeTrip(DiscreteModeChoiceTrip trip, PersonVariables personVariables,
			TripVariablesBike tripVariables) {
		double utility = 0.0;

		utility += parameters.bike.alpha;
		utility += parameters.bike.betaTravelTime
				* interaction(tripVariables.travelDistance_km, parameters.referenceDistance_km,
						parameters.bike.lambdaTravelTimeDistance) //
				* tripVariables.travelTime_min;

		utility += estimateSociodemographics(parameters.bike.sociodemographics, personVariables);
		utility += estimatePurpose(parameters.bike.purpose, trip);
		utility += estimateRegion(parameters.bike.region, personVariables);
		utility += estimateMunicipalityType(parameters.bike.municipalityType, personVariables);

		// From Zurich model
		utility += parameters.bike.betaAgeOver18 * personVariables.age_over18a;

		utility += parameters.betaDelay
				* interaction(tripVariables.travelDistance_km, parameters.referenceDistance_km,
						parameters.lambdaDelayDistance) //
				* tripVariables.delay_min;

		return utility;
	}
}

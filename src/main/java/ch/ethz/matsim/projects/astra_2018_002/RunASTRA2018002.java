package ch.ethz.matsim.projects.astra_2018_002;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.matsim.api.core.v01.Scenario;
import org.matsim.core.config.Config;
import org.matsim.core.config.ConfigUtils;
import org.matsim.core.controler.Controler;
import org.matsim.core.scenario.ScenarioUtils;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import ch.ethz.matsim.av.routing.AVRoute;
import ch.ethz.matsim.av.routing.AVRouteFactory;
import ch.ethz.matsim.baseline_scenario.config.CommandLine;
import ch.ethz.matsim.baseline_scenario.config.CommandLine.ConfigurationException;
import ch.ethz.matsim.baseline_scenario.traffic.BaselineTrafficModule;
import ch.ethz.matsim.baseline_scenario.transit.BaselineTransitModule;
import ch.ethz.matsim.baseline_scenario.transit.routing.DefaultEnrichedTransitRoute;
import ch.ethz.matsim.baseline_scenario.transit.routing.DefaultEnrichedTransitRouteFactory;
import ch.ethz.matsim.discrete_mode_choice.modules.DiscreteModeChoiceModule;
import ch.ethz.matsim.projects.astra_2018_002.flow_efficiency.FlowEfficiencyConfigurator;
import ch.ethz.matsim.projects.astra_2018_002.mode_choice.SwissDiscreteModeChoiceModule;
import ch.ethz.matsim.projects.astra_2018_002.mode_choice.SwissModeChoiceConfigurator;
import ch.ethz.matsim.projects.astra_2018_002.mode_choice.SwissUtilityParameters;
import ch.ethz.matsim.projects.astra_2018_002.mode_choice.SwissUtilityParameters.UtilitySet;
import ch.ethz.matsim.projects.astra_2018_002.private_av.PrivateAVConfigGroup;
import ch.ethz.matsim.projects.astra_2018_002.private_av.PrivateAVConfigurator;
import ch.ethz.matsim.projects.astra_2018_002.shared_av.SharedAVConfigGroup;
import ch.ethz.matsim.projects.astra_2018_002.shared_av.SharedAVConfigurator;
import ch.sbb.matsim.routing.pt.raptor.SwissRailRaptorModule;

public class RunASTRA2018002 {
	private final static Logger logger = Logger.getLogger(RunASTRA2018002.class);
	static public void main(String[] args) throws ConfigurationException, JsonParseException, JsonMappingException, IOException {
		CommandLine cmd = new CommandLine.Builder(args) //
				.requireOptions("config-path", "year", "ASTRA_Scenario") //
				.allowOptions("use-route-choice", "use-only-significant", "crossing-penalty",
						"model", "use-pt-cost-reduction", "flow-efficiency-path") //
				.allowPrefixes(SwissDiscreteModeChoiceModule.COMMAND_LINE_PREFIX, "utility-parameters") //
				.build();
		
		logger.info(String.format("config-path = %s", cmd.getOptionStrict("config-path")));
		logger.info(String.format("year = %s", cmd.getOptionStrict("year")));
		logger.info(String.format("ASTRA_Scenario = %s", cmd.getOptionStrict("ASTRA_Scenario")));
		logger.info(String.format("use-route-choice = %s", cmd.getOption("use-route-choice")));
		logger.info(String.format("use-only-significant = %s", cmd.getOption("use-only-significant")));
		logger.info(String.format("crossing-penalty = %s", cmd.getOption("crossing-penalty")));
		logger.info(String.format("model = %s", cmd.getOption("model")));
		logger.info(String.format("use-pt-cost-reduction = %s", cmd.getOption("use-pt-cost-reduction")));
		logger.info(String.format("flow-efficiency-path = %s", cmd.getOption("flow-efficiency-path")));
		logger.info(String.format("utility-parameters = %s", SwissDiscreteModeChoiceModule.COMMAND_LINE_PREFIX));
		
		int year = Integer.parseInt(cmd.getOptionStrict("year"));
		String ASTRA_Scenario = cmd.getOptionStrict("ASTRA_Scenario");
		
		// Load config
		Config config = ConfigUtils.loadConfig(cmd.getOptionStrict("config-path"), new PrivateAVConfigGroup(),
				new SharedAVConfigGroup());
		cmd.applyConfiguration(config);

		// Adjust config (they need to stay in the correct order!)
		SwissModeChoiceConfigurator.configure(config);
		SharedAVConfigurator.configure(config);
		PrivateAVConfigurator.configure(config, year);
		FlowEfficiencyConfigurator.configure(config, cmd);
		ASTRAConfigurator.configure(config);

		// Load scenario
		Scenario scenario = ScenarioUtils.createScenario(config);
		scenario.getPopulation().getFactory().getRouteFactories().setRouteFactory(DefaultEnrichedTransitRoute.class,
				new DefaultEnrichedTransitRouteFactory());
		scenario.getPopulation().getFactory().getRouteFactories().setRouteFactory(AVRoute.class, new AVRouteFactory());
		ScenarioUtils.loadScenario(scenario);

		// Adjust scenario
		SwissModeChoiceConfigurator.copyHousholdAttributes(scenario);
		FlowEfficiencyConfigurator.defineVehicleTypes(scenario, year);
		PrivateAVConfigurator.assignPrivateAVs(scenario, year, ASTRA_Scenario);
		ASTRAConfigurator.updateNetwork(scenario, year);

		// Set up controller
		Controler controller = new Controler(scenario);
		controller.addOverridingModule(new SwissRailRaptorModule());
		controller.addOverridingModule(new BaselineTransitModule());
		controller.addOverridingModule(new ASTRAModule());
		controller.addOverridingModule(
				new BaselineTrafficModule(cmd.getOption("crossing-penalty").map(Double::parseDouble).orElse(3.0)));
		SharedAVConfigurator.configureController(controller);

		UtilitySet utilitySet = Enum.valueOf(UtilitySet.class, cmd.getOption("model").orElse("ASTRA_2018_002"));
		boolean useRouteChoice = cmd.getOption("use-route-choice").map(Boolean::parseBoolean).orElse(true);
		boolean useOnlySignificant = cmd.getOption("use-only-significant").map(Boolean::parseBoolean).orElse(false);
		controller.addOverridingModule(new DiscreteModeChoiceModule());
		controller.addOverridingModule(
				new SwissDiscreteModeChoiceModule(utilitySet, useRouteChoice, useOnlySignificant, cmd, year));
		SharedAVConfigurator.configurePricing(controller);

		// Run
		controller.run();
	}
}

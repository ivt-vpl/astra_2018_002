package ch.ethz.matsim.projects.astra_2018_002.shared_av.service_area;

import java.io.IOException;
import java.net.URL;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;

import org.apache.log4j.Logger;
import org.geotools.data.DataStore;
import org.geotools.data.DataStoreFinder;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureIterator;
import org.geotools.data.simple.SimpleFeatureSource;
import org.matsim.api.core.v01.network.Link;
import org.matsim.api.core.v01.network.Network;
import org.opengis.feature.simple.SimpleFeature;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Point;

public class ServiceAreaReader {
	private final static Logger logger = Logger.getLogger(ServiceAreaReader.class);

	private final Network network;
	private final String operatingAreaAttribute;

	private final static GeometryFactory geometryFactory = new GeometryFactory();

	public ServiceAreaReader(String operatingAreaAttribute, Network network) {
		this.network = network;
		this.operatingAreaAttribute = operatingAreaAttribute;
	}

	public ServiceArea read(String activeServiceArea, URL url) {
		Collection<Geometry> shapes = new LinkedList<>();

		try {
			DataStore dataStore = DataStoreFinder.getDataStore(Collections.singletonMap("url", url));

			SimpleFeatureSource featureSource = dataStore.getFeatureSource(dataStore.getTypeNames()[0]);
			SimpleFeatureCollection featureCollection = featureSource.getFeatures();
			SimpleFeatureIterator featureIterator = featureCollection.features();

			while (featureIterator.hasNext()) {
				SimpleFeature feature = featureIterator.next();
				String featureName = (String) feature.getAttribute(operatingAreaAttribute);

				if (activeServiceArea.equals(featureName)) {
					shapes.add((Geometry) feature.getDefaultGeometry());
				}
			}

			featureIterator.close();
			dataStore.dispose();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

		Collection<Link> links = new HashSet<>();

		for (Link link : network.getLinks().values()) {
			Coordinate coordinate = new Coordinate(link.getCoord().getX(), link.getCoord().getY());
			Point point = geometryFactory.createPoint(coordinate);

			for (Geometry shape : shapes) {
				if (shape.contains(point)) {
					links.add(link);
				}
			}
		}

		logger.info(String.format("Found %d links in active service area '%s'", links.size(), activeServiceArea));

		if (links.size() == 0) {
			throw new IllegalStateException("Found no links in service area " + activeServiceArea);
		}

		return new ServiceArea(links);
	}
}

package ch.ethz.matsim.projects.astra_2018_002.analysis;

import java.io.File;
import java.io.IOException;

import org.matsim.api.core.v01.Scenario;
import org.matsim.api.core.v01.population.Person;
import org.matsim.core.config.Config;
import org.matsim.core.config.ConfigUtils;
import org.matsim.core.network.io.MatsimNetworkReader;
import org.matsim.core.population.io.PopulationReader;
import org.matsim.core.scenario.ScenarioUtils;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.ethz.matsim.baseline_scenario.config.CommandLine;
import ch.ethz.matsim.baseline_scenario.config.CommandLine.ConfigurationException;
import ch.ethz.matsim.projects.astra_2018_002.private_av.AssignPrivatAVs;

public class WriteScenarioInformation {
	static public void main(String[] args)
			throws ConfigurationException, JsonGenerationException, JsonMappingException, IOException {
		CommandLine cmd = new CommandLine.Builder(args) //
				.requireOptions("population-path", "network-path", "output-path") //
				.build();

		Config config = ConfigUtils.createConfig();
		Scenario scenario = ScenarioUtils.createScenario(config);

		new MatsimNetworkReader(scenario.getNetwork()).readFile(cmd.getOptionStrict("network-path"));
		new PopulationReader(scenario).readFile(cmd.getOptionStrict("population-path"));

		Result result = new Result();

		for (Person person : scenario.getPopulation().getPersons().values()) {
			Boolean isOutside = (Boolean) person.getAttributes().getAttribute("outside");
			Boolean isFreight = (Boolean) person.getAttributes().getAttribute("isFreight");

			if (isOutside == null || !isOutside) {
				if (isFreight == null || !isFreight) {
					int age = (Integer) person.getAttributes().getAttribute("age");
					boolean carAvailability = !"never".equals(person.getAttributes().getAttribute("carAvail"));
					
					String pravAttribute = (String) person.getAttributes().getAttribute(AssignPrivatAVs.AV_AVAILABILITY_ATTRIBUTE);
					carAvailability |= "prav3".equals(pravAttribute);
					carAvailability |= "prav4".equals(pravAttribute);
					carAvailability |= "prav5".equals(pravAttribute);

					result.numberOfPersons++;
					result.averageAge += age;

					if (age >= 18) {
						result.numberOfPersonsOver18 += 1.0;

						if (carAvailability) {
							result.carAvailability += 1.0;
						}
					}
				}
			}
		}

		result.averageAge /= result.numberOfPersons;
		result.carAvailability /= result.numberOfPersonsOver18;

		ObjectMapper mapper = new ObjectMapper();
		mapper.writeValue(new File(cmd.getOptionStrict("output-path")), result);
	}

	static public class Result {
		public int numberOfPersons;
		public int numberOfPersonsOver18;
		public double carAvailability;
		public double averageAge;
	}
}

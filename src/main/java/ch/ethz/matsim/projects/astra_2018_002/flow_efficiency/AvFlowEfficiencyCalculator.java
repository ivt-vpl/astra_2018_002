package ch.ethz.matsim.projects.astra_2018_002.flow_efficiency;

import org.matsim.api.core.v01.network.Link;
import org.matsim.vehicles.Vehicle;
import org.matsim.vehicles.VehicleType;

import org.apache.log4j.Logger;

public class AvFlowEfficiencyCalculator implements FlowEfficiencyCalculator {
	private final FlowEfficiencyDefinition definition;
	static private boolean logWritten = false;
	private final static Logger logger = Logger.getLogger(AvFlowEfficiencyCalculator.class);

	public AvFlowEfficiencyCalculator(FlowEfficiencyDefinition definition) {
		this.definition = definition;

	}

	@Override
	public double calculateFlowEfficiency(Vehicle vehicle, Link link) {
		double flowEfficiency = Double.NaN;
		if (!logWritten) {
			logger.info(String.format("AvFlowEfficiencyCalculator Sets  pcu_car = %s", definition.pcu_car));
			logger.info(String.format("AvFlowEfficiencyCalculator Sets  pcu_prav3_motorway = %s",
					definition.pcu_prav3_motorway));
			logger.info(
					String.format("AvFlowEfficiencyCalculator Sets  pcu_prav3_trunk = %s", definition.pcu_prav3_trunk));
			logger.info(String.format("AvFlowEfficiencyCalculator Sets  pcu_prav3_secondaryfast = %s",
					definition.pcu_prav3_secondaryfast));
			logger.info(String.format("AvFlowEfficiencyCalculator Sets  pcu_prav3_secondaryslow = %s",
					definition.pcu_prav3_secondaryslow));
			logger.info(String.format("AvFlowEfficiencyCalculator Sets  pcu_prav4_motorway = %s",
					definition.pcu_prav4_motorway));
			logger.info(
					String.format("AvFlowEfficiencyCalculator Sets  pcu_prav4_trunk = %s", definition.pcu_prav4_trunk));
			logger.info(String.format("AvFlowEfficiencyCalculator Sets  pcu_prav4_secondaryfast = %s",
					definition.pcu_prav4_secondaryfast));
			logger.info(String.format("AvFlowEfficiencyCalculator Sets  pcu_prav4_secondaryslow = %s",
					definition.pcu_prav4_secondaryslow));
			logger.info(String.format("AvFlowEfficiencyCalculator Sets  pcu_prav5_motorway = %s",
					definition.pcu_prav5_motorway));
			logger.info(
					String.format("AvFlowEfficiencyCalculator Sets  pcu_prav5_trunk = %s", definition.pcu_prav5_trunk));
			logger.info(String.format("AvFlowEfficiencyCalculator Sets  pcu_prav5_secondaryfast = %s",
					definition.pcu_prav5_secondaryfast));
			logger.info(String.format("AvFlowEfficiencyCalculator Sets  pcu_prav5_secondaryslow = %s",
					definition.pcu_prav5_secondaryslow));
			logger.info(String.format("AvFlowEfficiencyCalculator Sets  pcu_sharedAv_motorway = %s",
					definition.pcu_sharedAv_motorway));
			logger.info(String.format("AvFlowEfficiencyCalculator Sets  pcu_sharedAv_trunk = %s",
					definition.pcu_sharedAv_trunk));
			logger.info(String.format("AvFlowEfficiencyCalculator Sets  pcu_sharedAv_secondaryfast = %s",
					definition.pcu_sharedAv_secondaryfast));
			logger.info(String.format("AvFlowEfficiencyCalculator Sets  pcu_sharedAv_secondaryslow = %s",
					definition.pcu_sharedAv_secondaryslow));
			logger.info(String.format("AvFlowEfficiencyCalculator Sets  pcu_truck = %s", definition.pcu_truck));
			logger.info(String.format("AvFlowEfficiencyCalculator Sets  pcu_truckAv_motorway = %s",
					definition.pcu_truckAv_motorway));
			logger.info(String.format("AvFlowEfficiencyCalculator Sets  pcu_truckAv_trunk = %s",
					definition.pcu_truckAv_trunk));
			logger.info(String.format("AvFlowEfficiencyCalculator Sets  pcu_truckAv_secondaryfast = %s",
					definition.pcu_truckAv_secondaryfast));
			logger.info(String.format("AvFlowEfficiencyCalculator Sets  pcu_truckAv_secondaryslow = %s",
					definition.pcu_truckAv_secondaryslow));
			logWritten = true;
		}

		// define the pcu for each vehicle type and link type and if automated operation
		// is allowed.

		// first get vehicle type and link type and AV Operation permissions.
		VehicleType vehicletype = vehicle.getType();
		String linktype = (String) link.getAttributes().getAttribute("osm.way.highway");

		Boolean rawAvOperation = (Boolean) link.getAttributes().getAttribute("avOperation");
		boolean avOperation = rawAvOperation != null && rawAvOperation;

		// we discovered through an error message when testing this code that
		// the Switzerland scenario has links that have no osm.way.highway
		// classification.
		// Thus we need to give them one to avoid a null error.
		if (linktype == null) {
			linktype = "unclassified";
		}

		// Why these PCUs?
		//
		// Currently four link categories are being used for determining the PCU
		// of an autonomous vehicle (av).
		// In the PCU-Faktorenmodell delivered by EBP on March 29th, 2019 per
		// e-mail:
		// HLS, 120km/h
		// HLS, 80-100km/h
		// untergeordnet, 60-80km/h
		// untergeordnet, 30-50km/h
		//
		// Currently these four categories will be assigned the following OSM
		// categories
		// motorway
		// trunk
		// primary, secondary
		// tertiary, unclassified, residential
		//
		// But the speed limits used by EBP actually correspond to the following
		// Swiss road categories:
		// Autobahn
		// Autostrasse
		// Strasse Ausserorts
		// Strasse Innerorts
		//
		// Autobahn and Autostrasse comfortably parallel the OSM categories
		// motorway and trunk.
		// But the two subordinate road categories are defined by the geographic
		// area they are located within, not by a road category per se.
		// Thus using the OSM categories probably does not reflect the reality
		// on the ground very well.

		// TODO: create a "within settlement boundaries" and
		// "outside settlement boundaries" method
		// to differentiate between non-motorway, non-trunk links instead of
		// using OSM categories
		// since this would more closely resemble the Swiss traffic regulations
		// regarding speed limits
		// and very likely how licensure for autonomous vehicles will be granted
		// (either inside or outside settlement limits, rather than by
		// speed limit or OSM road class).

		// TODO: make the code look for these pcus in the config file, to
		// improve usability

		// the PCU for car is our reference.
		if (vehicletype.getId().equals(FlowEfficiencyConfigurator.CONVENTIONAL_VEHICLE_TYPE_ID)) {
			flowEfficiency = definition.pcu_car;
		}
		// prav3 vehicles where autonomous operation IS allowed
		else if (vehicletype.getId().equals(FlowEfficiencyConfigurator.PRAV3_ID) && avOperation == true) {

			if (linktype.equals("motorway")) {
				flowEfficiency = 1.0 / definition.pcu_prav3_motorway;

			} else if (linktype.equals("trunk")) {
				flowEfficiency = 1.0 / definition.pcu_prav3_trunk;

			} else if (linktype.equals("primary") || linktype.equals("secondary")) {
				flowEfficiency = 1.0 / definition.pcu_prav3_secondaryfast;

			} else { // thus this PCU should be assigned on OSM
						// highway categories "tertiary", "unclassified", and
						// "residential"
				flowEfficiency = 1.0 / definition.pcu_prav3_secondaryslow;
			}
			// prav3 vehicles where autonomous operation IS NOT allowed
		} else if (vehicletype.getId().equals(FlowEfficiencyConfigurator.PRAV3_ID) && avOperation == false) {

			flowEfficiency = 1.0 / definition.pcu_car;

			// prav4 vehicles where autonomous operation IS allowed
		} else if (vehicletype.getId().equals(FlowEfficiencyConfigurator.PRAV4_ID) && avOperation == true) {

			if (linktype.equals("motorway")) {
				flowEfficiency = 1.0 / definition.pcu_prav4_motorway;

			} else if (linktype.equals("trunk")) {
				flowEfficiency = 1.0 / definition.pcu_prav4_trunk;

			} else if (linktype.equals("primary") || linktype.equals("secondary")) {
				flowEfficiency = 1.0 / definition.pcu_prav4_secondaryfast;

			} else { // thus this PCU should be assigned on OSM
						// highway categories "tertiary", "unclassified", and
						// "residential"
				flowEfficiency = 1.0 / definition.pcu_prav4_secondaryslow;
			}
			// prav4 vehicles where autonomous operation IS NOT allowed
		} else if (vehicletype.getId().equals(FlowEfficiencyConfigurator.PRAV4_ID) && avOperation == false) {

			flowEfficiency = 1.0 / definition.pcu_car;

			// prav5 vehicles where autonomous operation IS allowed
		} else if (vehicletype.getId().equals(FlowEfficiencyConfigurator.PRAV5_ID) && avOperation == true) {

			if (linktype.equals("motorway")) {
				flowEfficiency = 1.0 / definition.pcu_prav5_motorway;

			} else if (linktype.equals("trunk")) {
				flowEfficiency = 1.0 / definition.pcu_prav5_trunk;

			} else if (linktype.equals("primary") || linktype.equals("secondary")) {
				flowEfficiency = 1.0 / definition.pcu_prav5_secondaryfast;

			} else { // thus this PCU should be assigned on OSM
						// highway categories "tertiary", "unclassified", and
						// "residential"
				flowEfficiency = 1.0 / definition.pcu_prav5_secondaryslow;
			}
			// prav5 vehicles where autonomous operation IS NOT allowed
		} else if (vehicletype.getId().equals(FlowEfficiencyConfigurator.PRAV5_ID) && avOperation == false) {

			flowEfficiency = 1.0 / definition.pcu_car;

			// sharedAV vehicles are only allowed to operate where autonomous operation IS
			// allowed
		} else if (vehicletype.getId().equals(FlowEfficiencyConfigurator.SHARED_AV_VEHICLE_TYPE_ID)
				&& avOperation == true) {

			if (linktype.equals("motorway")) {
				flowEfficiency = 1.0 / definition.pcu_sharedAv_motorway;

			} else if (linktype.equals("trunk")) {
				flowEfficiency = 1.0 / definition.pcu_sharedAv_trunk;

			} else if (linktype.equals("primary") || linktype.equals("secondary")) {
				flowEfficiency = 1.0 / definition.pcu_sharedAv_secondaryfast;

			} else { // thus this PCU should be assigned on OSM
						// highway categories "tertiary", "unclassified", and
						// "residential"
				flowEfficiency = 1.0 / definition.pcu_sharedAv_secondaryslow;
			}
			// conventional trucks
		} else if (vehicletype.getId().equals(FlowEfficiencyConfigurator.FREIGHT_TRUCK_ID)) {

			flowEfficiency = 1.0 / definition.pcu_truck;

			// Now AV-Trucks on roads where autonomous operation IS allowed
		} else if (vehicletype.getId().equals(FlowEfficiencyConfigurator.FREIGHT_TRUCK_AV_ID) && avOperation == true) {

			if (linktype.equals("motorway")) {
				flowEfficiency = 1.0 / definition.pcu_truckAv_motorway;

			} else if (linktype.equals("trunk")) {
				flowEfficiency = 1.0 / definition.pcu_truckAv_trunk;

			} else if (linktype.equals("primary") || linktype.equals("secondary")) {
				flowEfficiency = 1.0 / definition.pcu_truckAv_secondaryfast;

			} else { // thus this PCU should be assigned on OSM
						// highway categories "tertiary", "unclassified", and
						// "residential"
				flowEfficiency = 1.0 / definition.pcu_truckAv_secondaryslow;
			}
			// Now AV-Trucks on roads where autonomous operation IS NOT allowed
		} else if (vehicletype.getId().equals(FlowEfficiencyConfigurator.FREIGHT_TRUCK_AV_ID) && avOperation == false) {

			flowEfficiency = 1.0 / definition.pcu_truck;

		} else {
			flowEfficiency = definition.pcu_car;
		}

		// TODO: eventually include freight and small busses and taxis (since we
		// are defining vehicle type by mode, correct?)
		// Will AV-taxis at least just refer to the vehicle type "prav5"?

		if (Double.isNaN(flowEfficiency)) {
			throw new RuntimeException("Something is wrong with the flow efficiency.");
		}

		return flowEfficiency;
	}
}

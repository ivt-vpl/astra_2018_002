package ch.ethz.matsim.projects.astra_2018_002.flow_efficiency;

import org.matsim.api.core.v01.network.Link;
import org.matsim.vehicles.Vehicle;

public interface FlowEfficiencyCalculator {
	double calculateFlowEfficiency(Vehicle vehicle, Link link);

	class SingletonCalculator implements FlowEfficiencyCalculator {
		private FlowEfficiencyCalculator delegate;

		public void set(FlowEfficiencyCalculator delegate) {
			this.delegate = delegate;
		}

		@Override
		public double calculateFlowEfficiency(Vehicle vehicle, Link link) {
			if (delegate == null) {
				return 1.0;
			} else {
				return delegate.calculateFlowEfficiency(vehicle, link);
			}
		}
	}

	final static SingletonCalculator INSTANCE = new SingletonCalculator();
}

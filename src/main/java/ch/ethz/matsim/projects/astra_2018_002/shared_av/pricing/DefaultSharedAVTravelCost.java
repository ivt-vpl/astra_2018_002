package ch.ethz.matsim.projects.astra_2018_002.shared_av.pricing;

import ch.ethz.matsim.projects.astra_2018_002.mode_choice.cost.CostParameters;

public class DefaultSharedAVTravelCost implements SharedAVTravelCost {
	private final CostParameters costParameters;

	public DefaultSharedAVTravelCost(CostParameters costParameters) {
		this.costParameters = costParameters;
	}

	@Override
	public double getTravelCost(double distance_km, boolean isPooled) {
		if (isPooled) {
			return costParameters.pooledAvCostPerKm_CHF * distance_km;
		} else {
			return costParameters.taxiAvCostPerKm_CHF * distance_km;
		}
	}
}

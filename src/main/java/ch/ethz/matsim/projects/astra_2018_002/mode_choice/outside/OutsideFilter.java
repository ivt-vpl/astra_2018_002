package ch.ethz.matsim.projects.astra_2018_002.mode_choice.outside;

import java.util.List;

import org.matsim.api.core.v01.population.Person;

import ch.ethz.matsim.discrete_mode_choice.model.DiscreteModeChoiceTrip;
import ch.ethz.matsim.discrete_mode_choice.model.tour_based.TourFilter;

public class OutsideFilter implements TourFilter {
	// private static final long[] histogram = new long[9];
	// private static long totalNumberOfTours = 0;

	@Override
	public boolean filter(Person person, List<DiscreteModeChoiceTrip> tour) {
		DiscreteModeChoiceTrip start = tour.get(0);
		DiscreteModeChoiceTrip end = tour.get(tour.size() - 1);

		if (start.getOriginActivity().getType().equals("outside")) {
			return false;
		}

		if (end.getDestinationActivity().getType().equals("outside")) {
			return false;
		}

		/*
		 * synchronized (histogram) { histogram[Math.min(8, tour.size())]++;
		 * totalNumberOfTours++;
		 * 
		 * if (totalNumberOfTours % 1000 == 0) { for (int i = 0; i < 9; i++) {
		 * System.err.print(String.format("%s: %.2f%%, ", i == 8 ? "8+" :
		 * String.valueOf(i), 100.0 * histogram[i] / totalNumberOfTours)); }
		 * 
		 * System.err.println(""); } }
		 */

		if (tour.size() > 6) {
			return false;
		}

		return true;
	}
}

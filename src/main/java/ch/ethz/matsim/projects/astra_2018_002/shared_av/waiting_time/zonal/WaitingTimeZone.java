package ch.ethz.matsim.projects.astra_2018_002.shared_av.waiting_time.zonal;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.network.Link;

public class WaitingTimeZone {
	private final String identifier;
	private final Collection<Link> coveredLinks;
	private final Collection<Id<Link>> coveredLinkIds;

	public WaitingTimeZone(String identifier, Collection<Link> coveredLinks) {
		this.identifier = identifier;
		this.coveredLinks = coveredLinks;
		this.coveredLinkIds = coveredLinks.stream().map(Link::getId).collect(Collectors.toSet());
	}

	public boolean isCovered(Id<Link> linkId) {
		return coveredLinkIds.contains(linkId);
	}

	public Collection<Link> getCoveredLinks() {
		return coveredLinks;
	}

	public Optional<Link> getRepresentativeLink() {
		if (coveredLinks.size() > 0) {
			return Optional.ofNullable(coveredLinks.iterator().next());
		} else {
			return Optional.empty();
		}
	}

	public String getIdentifier() {
		return identifier;
	}
}

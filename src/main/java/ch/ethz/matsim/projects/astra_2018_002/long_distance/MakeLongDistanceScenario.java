package ch.ethz.matsim.projects.astra_2018_002.long_distance;

import java.io.File;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.Scenario;
import org.matsim.api.core.v01.TransportMode;
import org.matsim.api.core.v01.network.Link;
import org.matsim.api.core.v01.network.Network;
import org.matsim.api.core.v01.network.NetworkWriter;
import org.matsim.api.core.v01.population.Person;
import org.matsim.api.core.v01.population.PopulationFactory;
import org.matsim.api.core.v01.population.PopulationWriter;
import org.matsim.core.config.Config;
import org.matsim.core.config.ConfigUtils;
import org.matsim.core.config.ConfigWriter;
import org.matsim.core.network.NetworkUtils;
import org.matsim.core.network.algorithms.TransportModeNetworkFilter;
import org.matsim.core.router.DijkstraFactory;
import org.matsim.core.router.StageActivityTypes;
import org.matsim.core.router.StageActivityTypesImpl;
import org.matsim.core.router.util.LeastCostPathCalculatorFactory;
import org.matsim.core.scenario.ScenarioUtils;
import org.matsim.facilities.FacilitiesWriter;
import org.matsim.households.HouseholdsWriterV10;
import org.matsim.pt.PtConstants;
import org.matsim.pt.transitSchedule.api.TransitScheduleWriter;
import org.matsim.vehicles.VehicleWriterV1;

import ch.ethz.matsim.baseline_scenario.config.CommandLine;
import ch.ethz.matsim.baseline_scenario.config.CommandLine.ConfigurationException;
import ch.ethz.matsim.baseline_scenario.transit.routing.DefaultEnrichedTransitRoute;
import ch.ethz.matsim.baseline_scenario.transit.routing.DefaultEnrichedTransitRouteFactory;

public class MakeLongDistanceScenario {
	private final static Logger logger = Logger.getLogger(MakeLongDistanceScenario.class);

	static public void main(String[] args) throws ConfigurationException, InterruptedException {
		CommandLine cmd = new CommandLine.Builder(args) //
				.requireOptions("config-path", "output-path") //
				.allowOptions("threads", "batch-size", "osm-types", "population-path", "network-path") //
				.build();

		Config config = ConfigUtils.loadConfig(cmd.getOptionStrict("config-path"));

		if (cmd.hasOption("population-path")) {
			config.plans().setInputFile(cmd.getOptionStrict("population-path"));
		}
		
		if (cmd.hasOption("network-path")) {
			config.network().setInputFile(cmd.getOptionStrict("network-path"));
		}

		Scenario scenario = ScenarioUtils.createScenario(config);
		scenario.getPopulation().getFactory().getRouteFactories().setRouteFactory(DefaultEnrichedTransitRoute.class,
				new DefaultEnrichedTransitRouteFactory());
		ScenarioUtils.loadScenario(scenario);

		for (Link link : scenario.getNetwork().getLinks().values()) {
			link.setLength(Math.max(1.0, link.getLength()));
		}

		Network roadNetwork = NetworkUtils.createNetwork();
		new TransportModeNetworkFilter(scenario.getNetwork()).filter(roadNetwork,
				Collections.singleton(TransportMode.car));

		int numberOfThreads = cmd.getOption("threads").map(Integer::parseInt)
				.orElse(Runtime.getRuntime().availableProcessors());
		int batchSize = cmd.getOption("batch-size").map(Integer::parseInt).orElse(20);

		StageActivityTypes stageActivityTypes = new StageActivityTypesImpl(PtConstants.TRANSIT_ACTIVITY_TYPE);
		PopulationFactory populationFactory = scenario.getPopulation().getFactory();

		LeastCostPathCalculatorFactory routerFactory = new DijkstraFactory();

		LinkInteractionFinder interactionFinder = new LinkInteractionFinder(numberOfThreads, batchSize,
				stageActivityTypes, populationFactory, routerFactory);

		Set<String> osmTypes = new HashSet<>(Arrays.asList("motorway", "trunk"));

		if (cmd.hasOption("osm-types")) {
			osmTypes = new HashSet<>(Arrays.asList(cmd.getOptionStrict("osm-types").split(",")).stream()
					.map(String::trim).collect(Collectors.toList()));
		}

		final Set<String> finalOsmTypes = new HashSet<>(osmTypes);

		Function<Link, Boolean> highwayFilter = (link) -> {
			String attribute = (String) link.getAttributes().getAttribute("osm:way:highway");
			return attribute != null && finalOsmTypes.contains(attribute);
		};

		Collection<Id<Person>> interactionIds = interactionFinder.findInteractingPersons(scenario.getPopulation(),
				roadNetwork, highwayFilter);

		Set<Id<Person>> removalIds = new HashSet<>(scenario.getPopulation().getPersons().keySet());
		removalIds.removeAll(interactionIds);

		logger.info(String.format("Found %d/%d persons which do have highway interactions (removing %d persons)",
				interactionIds.size(), scenario.getPopulation().getPersons().size(), removalIds.size()));
		removalIds.forEach(scenario.getPopulation()::removePerson);

		File outputDirectory = new File(cmd.getOptionStrict("output-path"));
		File parentDirectory = outputDirectory.getAbsoluteFile().getParentFile();

		if (!parentDirectory.exists()) {
			throw new IllegalStateException("Directory does not exist: " + parentDirectory);
		}

		if (!outputDirectory.exists()) {
			outputDirectory.mkdir();
		}

		new PopulationWriter(scenario.getPopulation())
				.write(new File(outputDirectory, "lod_population.xml.gz").toString());
		new NetworkWriter(scenario.getNetwork()).write(new File(outputDirectory, "lod_network.xml.gz").toString());
		new TransitScheduleWriter(scenario.getTransitSchedule())
				.writeFile(new File(outputDirectory, "lod_transit_schedule.xml.gz").toString());
		new VehicleWriterV1(scenario.getTransitVehicles())
				.writeFile(new File(outputDirectory, "lod_transit_vehicles.xml.gz").toString());
		new FacilitiesWriter(scenario.getActivityFacilities())
				.write(new File(outputDirectory, "lod_facilities.xml.gz").toString());
		new HouseholdsWriterV10(scenario.getHouseholds())
				.writeFile(new File(outputDirectory, "lod_households.xml.gz").toString());

		// Prepare config
		config.plans().setInputFile("lod_population.xml.gz");
		config.network().setInputFile("lod_network.xml.gz");
		config.transit().setTransitScheduleFile("lod_transit_schedule.xml.gz");
		config.transit().setVehiclesFile("lod_transit_vehicles.xml.gz");
		config.facilities().setInputFile("lod_facilities.xml.gz");
		config.households().setInputFile("lod_households.xml.gz");

		new ConfigWriter(config).write(new File(outputDirectory, "lod_config.xml").toString());
	}
}

package ch.ethz.matsim.projects.astra_2018_002.flow_efficiency;

import org.matsim.api.core.v01.network.Link;
import org.matsim.vehicles.Vehicle;

public class StaticFlowEfficiencyCalculator implements FlowEfficiencyCalculator {
	private final double flowEfficiency;

	public StaticFlowEfficiencyCalculator(double flowEfficiency) {
		this.flowEfficiency = flowEfficiency;
	}

	public StaticFlowEfficiencyCalculator() {
		this(1.0);
	}

	@Override
	public double calculateFlowEfficiency(Vehicle vehicle, Link link) {
		return flowEfficiency;
	}
}

package ch.ethz.matsim.projects.astra_2018_002;

import java.util.ArrayList;
import java.util.Collection;

import org.matsim.contrib.dynagent.run.DynActivityEnginePlugin;
import org.matsim.core.config.Config;
import org.matsim.core.controler.AbstractModule;
import org.matsim.core.mobsim.qsim.AbstractQSimPlugin;
import org.matsim.core.mobsim.qsim.PopulationPlugin;
import org.matsim.core.mobsim.qsim.TeleportationPlugin;
import org.matsim.core.mobsim.qsim.changeeventsengine.NetworkChangeEventsPlugin;
import org.matsim.core.mobsim.qsim.messagequeueengine.MessageQueuePlugin;
import org.matsim.core.mobsim.qsim.qnetsimengine.QNetsimEnginePlugin;

import com.google.inject.Provides;
import com.google.inject.Singleton;

import ch.ethz.matsim.av.framework.AVQSimPlugin;
import ch.ethz.matsim.baseline_scenario.transit.simulation.BaselineTransitPlugin;

public class ASTRAQSimModule extends AbstractModule {
	@Override
	public void install() {
	}

	@Provides
	@Singleton
	public Collection<AbstractQSimPlugin> provideAbstractQSimPlugins(Config config) {
		final Collection<AbstractQSimPlugin> plugins = new ArrayList<>();

		plugins.add(new MessageQueuePlugin(config));
		plugins.add(new DynActivityEnginePlugin(config));
		plugins.add(new QNetsimEnginePlugin(config));

		if (config.network().isTimeVariantNetwork()) {
			plugins.add(new NetworkChangeEventsPlugin(config));
		}

		if (config.transit().isUseTransit()) {
			plugins.add(new BaselineTransitPlugin(config));
		}

		plugins.add(new TeleportationPlugin(config));
		plugins.add(new PopulationPlugin(config));
		plugins.add(new AVQSimPlugin(config));

		return plugins;

	}
}

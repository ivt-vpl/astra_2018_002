package ch.ethz.matsim.projects.astra_2018_002.analysis;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.Scenario;
import org.matsim.api.core.v01.network.Link;
import org.matsim.api.core.v01.network.Network;
import org.matsim.core.config.Config;
import org.matsim.core.config.ConfigUtils;
import org.matsim.core.network.io.MatsimNetworkReader;
import org.matsim.core.population.routes.NetworkRoute;
import org.matsim.core.scenario.ScenarioUtils;
import org.matsim.pt.transitSchedule.api.TransitLine;
import org.matsim.pt.transitSchedule.api.TransitRoute;
import org.matsim.pt.transitSchedule.api.TransitScheduleReader;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.ethz.matsim.baseline_scenario.config.CommandLine;
import ch.ethz.matsim.baseline_scenario.config.CommandLine.ConfigurationException;

public class CalculatePTVehicleDistance {
	static public void main(String[] args) throws ConfigurationException, IOException {
		CommandLine cmd = new CommandLine.Builder(args) //
				.requireOptions("schedule-path", "network-path", "output-path") //
				.build();

		Config config = ConfigUtils.createConfig();
		Scenario scenario = ScenarioUtils.createScenario(config);

		new MatsimNetworkReader(scenario.getNetwork()).readFile(cmd.getOptionStrict("network-path"));
		new TransitScheduleReader(scenario).readFile(cmd.getOptionStrict("schedule-path"));

		Network network = scenario.getNetwork();

		Result result = new Result();

		for (TransitLine transitLine : scenario.getTransitSchedule().getTransitLines().values()) {
			for (TransitRoute transitRoute : transitLine.getRoutes().values()) {
				NetworkRoute networkRoute = transitRoute.getRoute();
				double routeDistance = 0.0;
				int departures = transitRoute.getDepartures().size();

				List<Id<Link>> linkIds = new ArrayList<>(networkRoute.getLinkIds());
				linkIds.add(networkRoute.getEndLinkId());

				for (Id<Link> linkId : linkIds) {
					routeDistance += network.getLinks().get(linkId).getLength();
				}

				switch (transitRoute.getTransportMode()) {
				case "tram":
					result.tramDistance += routeDistance * departures;
					break;
				case "rail":
					result.railDistance += routeDistance * departures;
					break;
				case "bus":
					result.busDistance += routeDistance * departures;
					break;
				}
			}
		}
		
		System.out.println("Bus: " + result.busDistance);
		System.out.println("Tram: " + result.tramDistance);
		System.out.println("Rail: " + result.railDistance);
		
		System.out.println("Bus: " + (result.busDistance * 1e-6));
		System.out.println("Tram: " + (result.tramDistance * 1e-6));
		System.out.println("Rail: " + (result.railDistance * 1e-6));
		System.out.println("Total: " + ((result.railDistance + result.tramDistance + result.busDistance) * 1e-6));
		
		ObjectMapper mapper = new ObjectMapper();
		mapper.writeValue(new File(cmd.getOptionStrict("output-path")), result);
	}

	static public class Result {
		public double busDistance = 0.0;
		public double tramDistance = 0.0;
		public double railDistance = 0.0;
	}
}

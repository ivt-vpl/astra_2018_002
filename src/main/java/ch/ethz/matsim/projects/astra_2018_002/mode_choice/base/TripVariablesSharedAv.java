package ch.ethz.matsim.projects.astra_2018_002.mode_choice.base;

public class TripVariablesSharedAv {
	public double inVehicleTime_min = 0.0;
	public double travelCost_CHF = 0.0;
	
	public double accessEgressTime_min = 0.0;
	public double waitingTime_min = 0.0;
	
	public double delay_min = 0.0;
	public double travelDistance_km = 0.0;
	
	public boolean isPooled = false;
	public double crowflyDistance_km = 0.0;
}

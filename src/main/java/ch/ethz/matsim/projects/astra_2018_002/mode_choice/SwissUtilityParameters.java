package ch.ethz.matsim.projects.astra_2018_002.mode_choice;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import ch.ethz.matsim.baseline_scenario.config.CommandLine;
import ch.ethz.matsim.baseline_scenario.config.CommandLine.ConfigurationException;
import ch.ethz.matsim.projects.astra_2018_002.mode_choice.cost.CostParameters;

/**
 * Gives utility parameters based on
 * 
 * <pre>
 * Bundesamt für Raumentwicklung (2017) Analyse der SP-Befragung 2015 zur
 * Verkehrsmodus- und Routenwahl
 * </pre>
 * 
 * Ommitted choice dimensions:
 * <ul>
 * <li>Delay probability + Intreaction</li>
 * <li>Road toll [CHF] + Interacton</li>
 * </ul>
 */
public class SwissUtilityParameters {
	private final static Logger logger = Logger.getLogger(SwissUtilityParameters.class);

	private SwissUtilityParameters() {
	}

	// Constants
	public double referenceDistance_km = 20.0;
	public double referenceIncome_1000CHF = 7.0;
	public double referenceCrowflyDistance_km = 40.0;

	// Costs
	public CostParameters costs = new CostParameters();

	// I) General
	public double betaCost = 0.0;
	public double betaDelay = 0.0;

	public double lambdaDelayDistance = 0.0;
	public double lambdaCostIncome = 0.0;
	public double lambdaCostCrowflyDistance = 0.0;

	public class SociodemographicsParameters {
		public double betaMale = 0.0;
		public double betaAge = 0.0;
		public double betaAgeSquared = 0.0;
		public double betaIncome = 0.0;
		public double betaIncomeSquared = 0.0;
	}

	public class PurposeParameters {
		public double betaWork = 0.0;
		public double betaEducation = 0.0;
		public double betaShop = 0.0;
		public double betaErrand = 0.0;
		public double betaLeisure = 0.0;
	}

	public class RegionParameters {
		public double betaRegion1 = 0.0;
		public double betaRegion2 = 0.0;
		public double betaRegion3 = 0.0;
	}

	public class MunicipalityTypeParameters {
		public double betaUrban = 0.0;
		public double betaSubUrban = 0.0;
		public double betaRural = 0.0;
	}

	// II) Walk
	public class WalkParameters {
		public double alpha = 0.0;
		public double betaTravelTime = 0.0;

		public SociodemographicsParameters sociodemographics = new SociodemographicsParameters();
		public PurposeParameters purpose = new PurposeParameters();
		public RegionParameters region = new RegionParameters();
		public MunicipalityTypeParameters municipalityType = new MunicipalityTypeParameters();

		public double lambdaTravelTimeDistance = 0.0;
	}

	public WalkParameters walk = new WalkParameters();

	// III) Bike
	public class BikeParameters {
		public double alpha = 0.0;
		public double betaTravelTime = 0.0;
		public double betaAgeOver18 = 0.0;

		public SociodemographicsParameters sociodemographics = new SociodemographicsParameters();
		public PurposeParameters purpose = new PurposeParameters();
		public RegionParameters region = new RegionParameters();
		public MunicipalityTypeParameters municipalityType = new MunicipalityTypeParameters();

		public double lambdaTravelTimeDistance = 0.0;
	}

	public BikeParameters bike = new BikeParameters();

	// IV) Car
	public class CarParameters {
		public double alpha = 0.0;
		public double betaTravelTime = 0.0;
		public double betaParkingSearchTime = 0.0;
		public double betaParkingCost = 0.0;

		public double betaAlwaysAvailable = 0.0;

		public SociodemographicsParameters sociodemographics = new SociodemographicsParameters();
		public PurposeParameters purpose = new PurposeParameters();
		public RegionParameters region = new RegionParameters();
		public MunicipalityTypeParameters municipalityType = new MunicipalityTypeParameters();

		public double lambdaTravelTimeDistance = 0.0;
		public double lambdaCostDistance = 0.0;

		public double constantAccessEgressWalkTime_min = 0.0;
		public double constantParkingSearchPenalty_min = 0.0;
	}

	public CarParameters car = new CarParameters();

	// V) Public Transit
	public class PtParameters {
		public double alpha = 0.0;
		public double betaTravelTime = 0.0;
		public double betaAccessEgressTime = 0.0;
		public double betaWaitingTime = 0.0;
		public double betaLineSwitch = 0.0;
		public double betaHeadway = 0.0;
		public double betaOccupancy = 0.0;

		public double betaMainTransportModeRail = 0.0;
		public double betaMainTransportModeTram = 0.0;
		public double betaMainTransportModeBus = 0.0;

		public SociodemographicsParameters sociodemographics = new SociodemographicsParameters();
		public PurposeParameters purpose = new PurposeParameters();
		public RegionParameters region = new RegionParameters();
		public MunicipalityTypeParameters municipalityType = new MunicipalityTypeParameters();

		public double betaSubscriptionNone = 0.0;
		public double betaSubscriptionHalbtax = 0.0;
		public double betaSubscriptionRegional = 0.0;
		public double betaSubscriptionGA = 0.0;

		public double lambdaTravelTimeDistance = 0.0;
		public double lambdaLineSwitchesDistance = 0.0;
		public double lambdaHeadwayDistance = 0.0;
		public double lambdaCostDistance = 0.0;
	}

	public PtParameters pt = new PtParameters();

	// VI) Private AV
	public class PrivateAVParameters {
		public double alpha = 0.0;

		public double betaTravelTimeConventional = 0.0;
		public double betaTravelTimeAutomated = 0.0;

		public double betaDelay = 0.0;

		public double constantAccessEgressWalkTime_min = 0.0;
	}

	public PrivateAVParameters prav = new PrivateAVParameters();

	// VII) Shared AV
	public class SharedAVParameters {
		public double alpha = 0.0;

		public double betaTravelTime = 0.0;
		public double betaWaitingTime = 0.0;

		public double betaAccessEgressTime = 0.0;
		public double betaDelay = 0.0;
	}

	public SharedAVParameters tav = new SharedAVParameters();
	public SharedAVParameters pav = new SharedAVParameters();

	/**
	 * Chapter 3.2.1: Modell with linear utility function WITHOUT sociodemographics
	 */
	static private SwissUtilityParameters buildLinearWithoutSociodemographics(boolean useRouteChoice,
			boolean onlySignificant) {
		SwissUtilityParameters parameters = new SwissUtilityParameters();
		double s = onlySignificant ? 0.0 : 1.0;

		// General
		parameters.betaCost = -0.164;

		if (useRouteChoice) {
			parameters.betaDelay = -0.033 * s;
		}

		// Walk
		parameters.walk.alpha = 0.791;
		parameters.walk.betaTravelTime = -0.085;

		// Bike
		parameters.bike.alpha = -0.464;
		parameters.bike.betaTravelTime = -0.077;

		// Car
		parameters.car.alpha = 0.0;
		parameters.car.betaTravelTime = -0.057;
		parameters.car.betaParkingSearchTime = -0.047;
		parameters.car.betaParkingCost = -0.135;

		// PT
		parameters.pt.alpha = -0.522;
		parameters.pt.betaTravelTime = -0.038;
		parameters.pt.betaAccessEgressTime = -0.050;
		parameters.pt.betaWaitingTime = -0.030;
		parameters.pt.betaLineSwitch = -0.227;
		parameters.pt.betaHeadway = -0.014;
		parameters.pt.betaOccupancy = -0.032;

		if (useRouteChoice) {
			parameters.pt.betaMainTransportModeRail = 0.0;
			parameters.pt.betaMainTransportModeTram = -0.279;
			parameters.pt.betaMainTransportModeBus = -0.261;
		}

		return parameters;
	}

	/**
	 * Chapter 3.2.2: Modell with linear utility function WITH sociodemographics
	 */
	static private SwissUtilityParameters buildLinearWithSociodemographics(boolean useRouteChoice,
			boolean onlySignificant) {
		SwissUtilityParameters parameters = new SwissUtilityParameters();
		double s = onlySignificant ? 0.0 : 1.0;

		// General
		parameters.betaCost = -0.107;

		if (useRouteChoice) {
			parameters.betaDelay = -0.062;
		}

		// Walk
		parameters.walk.alpha = 0.879;
		parameters.walk.betaTravelTime = -0.079;

		parameters.walk.sociodemographics.betaMale = -0.052 * s;
		parameters.walk.sociodemographics.betaAge = 0.430;
		parameters.walk.sociodemographics.betaAgeSquared = -0.044;
		parameters.walk.sociodemographics.betaIncome = -0.015 * s;
		parameters.walk.sociodemographics.betaIncomeSquared = -0.002 * s;

		parameters.walk.purpose.betaWork = -0.112;
		parameters.walk.purpose.betaEducation = 0.677;
		parameters.walk.purpose.betaShop = -0.284;
		parameters.walk.purpose.betaErrand = -1.120;

		parameters.walk.region.betaRegion1 = 0.352;
		parameters.walk.region.betaRegion3 = -0.246;

		parameters.walk.municipalityType.betaSubUrban = -0.190;
		parameters.walk.municipalityType.betaRural = -0.044 * s;

		// Bike
		parameters.bike.alpha = -1.510;
		parameters.bike.betaTravelTime = -0.071;

		parameters.bike.sociodemographics.betaMale = 0.411;
		parameters.bike.sociodemographics.betaAge = 0.59;
		parameters.bike.sociodemographics.betaAgeSquared = -0.073;
		parameters.bike.sociodemographics.betaIncome = 0.121;
		parameters.bike.sociodemographics.betaIncomeSquared = -0.006;

		parameters.bike.purpose.betaWork = 0.392;
		parameters.bike.purpose.betaEducation = 0.979;
		parameters.bike.purpose.betaShop = -0.753;
		parameters.bike.purpose.betaErrand = -0.684;

		parameters.bike.region.betaRegion1 = 0.593;
		parameters.bike.region.betaRegion3 = 0.609;

		parameters.bike.municipalityType.betaSubUrban = -0.344;
		parameters.bike.municipalityType.betaRural = -0.320;

		// Car
		parameters.car.alpha = 0.0;
		parameters.car.betaTravelTime = -0.040;
		parameters.car.betaParkingSearchTime = -0.064;
		parameters.car.betaParkingCost = -0.158;

		parameters.car.betaAlwaysAvailable = 0.853;

		// PT
		parameters.pt.alpha = -1.080;
		parameters.pt.betaTravelTime = -0.028;
		parameters.pt.betaAccessEgressTime = -0.037;
		parameters.pt.betaWaitingTime = -0.025;
		parameters.pt.betaLineSwitch = -0.186;
		parameters.pt.betaHeadway = -0.009;
		parameters.pt.betaOccupancy = -0.030;

		if (useRouteChoice) {
			parameters.pt.betaMainTransportModeRail = 0.0;
			parameters.pt.betaMainTransportModeTram = -0.193;
			parameters.pt.betaMainTransportModeBus = -0.186;
		}

		parameters.pt.betaSubscriptionHalbtax = 0.494;
		parameters.pt.betaSubscriptionRegional = 1.580;
		parameters.pt.betaSubscriptionGA = 1.790;

		parameters.pt.sociodemographics.betaMale = -0.052 * s;
		parameters.pt.sociodemographics.betaAge = 0.111 * s;
		parameters.pt.sociodemographics.betaAgeSquared = -0.007 * s;
		parameters.pt.sociodemographics.betaIncome = -0.027 * s;
		parameters.pt.sociodemographics.betaIncomeSquared = 0.000 * s;

		parameters.pt.purpose.betaWork = 0.321;
		parameters.pt.purpose.betaEducation = 0.891;
		parameters.pt.purpose.betaShop = -0.354;
		parameters.pt.purpose.betaErrand = -0.357;

		parameters.pt.region.betaRegion1 = 0.378;
		parameters.pt.region.betaRegion3 = 0.060 * s;

		parameters.pt.municipalityType.betaSubUrban = -0.191;
		parameters.pt.municipalityType.betaRural = -0.242;

		return parameters;
	}

	/**
	 * Chapter 3.2.3: Modell with non-linear utility function
	 */
	static private SwissUtilityParameters buildNonLinear(boolean useRouteChoice, boolean onlySignificant) {
		SwissUtilityParameters parameters = new SwissUtilityParameters();
		double s = onlySignificant ? 0.0 : 1.0;

		// General
		parameters.betaCost = -0.172;
		parameters.lambdaCostIncome = -0.208;

		if (useRouteChoice) {
			parameters.betaDelay = -0.09;
			parameters.lambdaDelayDistance = -0.406;
		}

		// Walk
		parameters.walk.alpha = 0.001 * s;
		parameters.walk.betaTravelTime = -0.048;
		parameters.walk.lambdaTravelTimeDistance = -0.360;

		parameters.walk.sociodemographics.betaMale = -0.059 * s;
		parameters.walk.sociodemographics.betaAge = 0.411;
		parameters.walk.sociodemographics.betaAgeSquared = -0.044;
		parameters.walk.sociodemographics.betaIncome = 0.030 * s;
		parameters.walk.sociodemographics.betaIncomeSquared = -0.003 * s;

		parameters.walk.purpose.betaWork = -0.085;
		parameters.walk.purpose.betaEducation = 0.573;
		parameters.walk.purpose.betaShop = -0.327;
		parameters.walk.purpose.betaErrand = -1.210;

		parameters.walk.region.betaRegion1 = 0.333;
		parameters.walk.region.betaRegion3 = -0.238;

		parameters.walk.municipalityType.betaSubUrban = -0.157;
		parameters.walk.municipalityType.betaRural = -0.017 * s;

		// Bike
		parameters.bike.alpha = -2.750;
		parameters.bike.betaTravelTime = -0.052;
		parameters.bike.lambdaTravelTimeDistance = -0.367;

		parameters.bike.sociodemographics.betaMale = 0.399;
		parameters.bike.sociodemographics.betaAge = 0.623;
		parameters.bike.sociodemographics.betaAgeSquared = -0.078;
		parameters.bike.sociodemographics.betaIncome = 0.192;
		parameters.bike.sociodemographics.betaIncomeSquared = -0.008;

		parameters.bike.purpose.betaWork = 0.383;
		parameters.bike.purpose.betaEducation = 0.869;
		parameters.bike.purpose.betaShop = -0.806;
		parameters.bike.purpose.betaErrand = -0.726;

		parameters.bike.region.betaRegion1 = 0.601;
		parameters.bike.region.betaRegion3 = 0.618;

		parameters.bike.municipalityType.betaSubUrban = -0.263;
		parameters.bike.municipalityType.betaRural = -0.205;

		// Car
		parameters.car.alpha = 0.0;
		parameters.car.betaTravelTime = -0.047;
		parameters.car.betaParkingSearchTime = -0.070;
		parameters.car.betaParkingCost = -0.160;

		parameters.car.lambdaTravelTimeDistance = -0.305;
		parameters.car.lambdaCostDistance = -0.686;

		parameters.car.betaAlwaysAvailable = 0.832;

		// PT
		parameters.pt.alpha = -0.761;
		parameters.pt.betaTravelTime = -0.035;
		parameters.pt.betaAccessEgressTime = -0.039;
		parameters.pt.betaWaitingTime = -0.020;
		parameters.pt.betaLineSwitch = -0.215;
		parameters.pt.betaHeadway = -0.009;

		parameters.pt.lambdaTravelTimeDistance = -0.362;
		parameters.pt.lambdaLineSwitchesDistance = -0.285;
		parameters.pt.lambdaHeadwayDistance = -0.583;
		parameters.pt.lambdaCostDistance = -0.480;

		parameters.pt.betaOccupancy = -0.038;

		if (useRouteChoice) {
			parameters.pt.betaMainTransportModeRail = 0.0;
			parameters.pt.betaMainTransportModeTram = -0.200;
			parameters.pt.betaMainTransportModeBus = -0.212;
		}

		parameters.pt.betaSubscriptionHalbtax = 0.315;
		parameters.pt.betaSubscriptionRegional = 1.480;
		parameters.pt.betaSubscriptionGA = 1.610;

		parameters.pt.sociodemographics.betaMale = -0.059 * s;
		parameters.pt.sociodemographics.betaAge = 0.132;
		parameters.pt.sociodemographics.betaAgeSquared = -0.010 * s;
		parameters.pt.sociodemographics.betaIncome = -0.034 * s;
		parameters.pt.sociodemographics.betaIncomeSquared = 0.001 * s;

		parameters.pt.purpose.betaWork = 0.315;
		parameters.pt.purpose.betaEducation = 0.896;
		parameters.pt.purpose.betaShop = -0.403;
		parameters.pt.purpose.betaErrand = -0.307;

		parameters.pt.region.betaRegion1 = 0.390;
		parameters.pt.region.betaRegion3 = 0.083;

		parameters.pt.municipalityType.betaSubUrban = -0.208;
		parameters.pt.municipalityType.betaRural = -0.247;

		return parameters;
	}

	/**
	 * Appendix A5: Table 32
	 */
	static private SwissUtilityParameters buildAppendix(boolean useRouteChoice, boolean onlySignificant) {
		SwissUtilityParameters parameters = new SwissUtilityParameters();
		double s = onlySignificant ? 0.0 : 1.0;

		// General
		parameters.betaCost = -0.182;
		parameters.lambdaCostIncome = -0.254;

		if (useRouteChoice) {
			parameters.betaDelay = -0.072;
			parameters.lambdaDelayDistance = -0.257;
		}

		// Walk
		parameters.walk.alpha = 0.140 * s;
		parameters.walk.betaTravelTime = -0.054;
		parameters.walk.lambdaTravelTimeDistance = -0.350;

		parameters.walk.sociodemographics.betaMale = -0.0 * s;
		parameters.walk.sociodemographics.betaAge = 0.433 * s;
		parameters.walk.sociodemographics.betaAgeSquared = -0.047 * s;
		parameters.walk.sociodemographics.betaIncome = 0.056 * s;
		parameters.walk.sociodemographics.betaIncomeSquared = -0.004 * s;

		parameters.walk.purpose.betaWork = -0.088 * s;
		parameters.walk.purpose.betaEducation = 0.655 * s;
		parameters.walk.purpose.betaShop = -0.355 * s;
		parameters.walk.purpose.betaErrand = -1.281;

		parameters.walk.region.betaRegion1 = 0.403;
		parameters.walk.region.betaRegion3 = -0.239 * s;

		parameters.walk.municipalityType.betaSubUrban = -0.170 * s;
		parameters.walk.municipalityType.betaRural = -0.003 * s;

		// Bike
		parameters.bike.alpha = -2.517;
		parameters.bike.betaTravelTime = -0.059;
		parameters.bike.lambdaTravelTimeDistance = -0.456;

		parameters.bike.sociodemographics.betaMale = 0.434;
		parameters.bike.sociodemographics.betaAge = 0.645;
		parameters.bike.sociodemographics.betaAgeSquared = -0.080;
		parameters.bike.sociodemographics.betaIncome = 0.218;
		parameters.bike.sociodemographics.betaIncomeSquared = -0.009;

		parameters.bike.purpose.betaWork = 0.405;
		parameters.bike.purpose.betaEducation = 0.953;
		parameters.bike.purpose.betaShop = -0.859;
		parameters.bike.purpose.betaErrand = -0.795;

		parameters.bike.region.betaRegion1 = 0.663;
		parameters.bike.region.betaRegion3 = 0.667;

		parameters.bike.municipalityType.betaSubUrban = -0.266 * s;
		parameters.bike.municipalityType.betaRural = -0.201 * s;

		// Car
		parameters.car.alpha = 0.0;
		parameters.car.betaTravelTime = -0.05;
		parameters.car.betaParkingSearchTime = -0.067;
		parameters.car.betaParkingCost = -0.176;

		parameters.car.lambdaTravelTimeDistance = -0.302;
		parameters.car.lambdaCostDistance = -0.675;

		parameters.car.betaAlwaysAvailable = 0.890;

		// PT
		parameters.pt.alpha = -1.023;
		parameters.pt.betaTravelTime = -0.035;
		parameters.pt.betaAccessEgressTime = -0.042;
		parameters.pt.betaWaitingTime = -0.034;
		parameters.pt.betaLineSwitch = -0.179;
		parameters.pt.betaHeadway = -0.009;

		parameters.pt.lambdaTravelTimeDistance = -0.316;
		parameters.pt.lambdaLineSwitchesDistance = -0.467;
		parameters.pt.lambdaHeadwayDistance = -0.668;
		parameters.pt.lambdaCostDistance = -0.433;

		parameters.pt.betaOccupancy = -0.039;

		if (useRouteChoice) {
			parameters.pt.betaMainTransportModeRail = 0.0;
			parameters.pt.betaMainTransportModeTram = -0.240;
			parameters.pt.betaMainTransportModeBus = -0.233;
		}

		parameters.pt.betaSubscriptionHalbtax = 0.360;
		parameters.pt.betaSubscriptionRegional = 1.571;
		parameters.pt.betaSubscriptionGA = 1.712;

		parameters.pt.sociodemographics.betaMale = -0.047 * s;
		parameters.pt.sociodemographics.betaAge = 0.137 * s;
		parameters.pt.sociodemographics.betaAgeSquared = -0.010 * s;
		parameters.pt.sociodemographics.betaIncome = -0.026 * s;
		parameters.pt.sociodemographics.betaIncomeSquared = 0.005 * s;

		parameters.pt.purpose.betaWork = 0.326;
		parameters.pt.purpose.betaEducation = 0.911;
		parameters.pt.purpose.betaShop = -0.408;
		parameters.pt.purpose.betaErrand = -0.347 * s;

		parameters.pt.region.betaRegion1 = 0.415;
		parameters.pt.region.betaRegion3 = 0.107 * s;

		parameters.pt.municipalityType.betaSubUrban = -0.178;
		parameters.pt.municipalityType.betaRural = -0.213;

		return parameters;
	}

	/**
	 * Custom: Keep interactions and sociodemographics, but remove constants for
	 * region, municipality type and purpose
	 */
	static private SwissUtilityParameters buildCustom(boolean useRouteChoice, boolean onlySignificant) {
		SwissUtilityParameters parameters = new SwissUtilityParameters();
		double s = onlySignificant ? 0.0 : 1.0;

		// General
		parameters.betaCost = -0.182;
		parameters.lambdaCostIncome = -0.254;

		if (useRouteChoice) {
			parameters.betaDelay = -0.072;
			parameters.lambdaDelayDistance = -0.257;
		}

		// Walk
		parameters.walk.alpha = 0.140 * s;
		parameters.walk.betaTravelTime = -0.054;
		parameters.walk.lambdaTravelTimeDistance = -0.350;

		parameters.walk.sociodemographics.betaMale = -0.0 * s;
		parameters.walk.sociodemographics.betaAge = 0.433 * s;
		parameters.walk.sociodemographics.betaAgeSquared = -0.047 * s;
		parameters.walk.sociodemographics.betaIncome = 0.056 * s;
		parameters.walk.sociodemographics.betaIncomeSquared = -0.004 * s;

		/*
		 * parameters.walk.purpose.betaWork = -0.088 * s;
		 * parameters.walk.purpose.betaEducation = 0.655 * s;
		 * parameters.walk.purpose.betaShop = -0.355 * s;
		 * parameters.walk.purpose.betaErrand = -1.281;
		 * 
		 * parameters.walk.region.betaRegion1 = 0.403;
		 * parameters.walk.region.betaRegion3 = -0.239 * s;
		 * 
		 * parameters.walk.municipalityType.betaSubUrban = -0.170 * s;
		 * parameters.walk.municipalityType.betaRural = -0.003 * s;
		 */

		// Bike
		parameters.bike.alpha = -2.517;
		parameters.bike.betaTravelTime = -0.059;
		parameters.bike.lambdaTravelTimeDistance = -0.456;

		parameters.bike.sociodemographics.betaMale = 0.434;
		parameters.bike.sociodemographics.betaAge = 0.645;
		parameters.bike.sociodemographics.betaAgeSquared = -0.080;
		parameters.bike.sociodemographics.betaIncome = 0.218;
		parameters.bike.sociodemographics.betaIncomeSquared = -0.009;

		/*
		 * parameters.bike.purpose.betaWork = 0.405;
		 * parameters.bike.purpose.betaEducation = 0.953;
		 * parameters.bike.purpose.betaShop = -0.859; parameters.bike.purpose.betaErrand
		 * = -0.795;
		 * 
		 * parameters.bike.region.betaRegion1 = 0.663;
		 * parameters.bike.region.betaRegion3 = 0.667;
		 * 
		 * parameters.bike.municipalityType.betaSubUrban = -0.266 * s;
		 * parameters.bike.municipalityType.betaRural = -0.201 * s;
		 */

		// Car
		parameters.car.alpha = 0.0;
		parameters.car.betaTravelTime = -0.05;
		parameters.car.betaParkingSearchTime = -0.067;
		parameters.car.betaParkingCost = -0.176;

		parameters.car.lambdaTravelTimeDistance = -0.302;
		parameters.car.lambdaCostDistance = -0.675;

		parameters.car.betaAlwaysAvailable = 0.890;

		// PT
		parameters.pt.alpha = -1.023;
		parameters.pt.betaTravelTime = -0.035;
		parameters.pt.betaAccessEgressTime = -0.042;
		parameters.pt.betaWaitingTime = -0.034;
		parameters.pt.betaLineSwitch = -0.179;
		parameters.pt.betaHeadway = -0.009;

		parameters.pt.lambdaTravelTimeDistance = -0.316;
		parameters.pt.lambdaLineSwitchesDistance = -0.467;
		parameters.pt.lambdaHeadwayDistance = -0.668;
		parameters.pt.lambdaCostDistance = -0.433;

		parameters.pt.betaOccupancy = -0.039;

		if (useRouteChoice) {
			parameters.pt.betaMainTransportModeRail = 0.0;
			parameters.pt.betaMainTransportModeTram = -0.240;
			parameters.pt.betaMainTransportModeBus = -0.233;
		}

		parameters.pt.betaSubscriptionHalbtax = 0.360;
		parameters.pt.betaSubscriptionRegional = 1.571;
		parameters.pt.betaSubscriptionGA = 1.712;

		parameters.pt.sociodemographics.betaMale = -0.047 * s;
		parameters.pt.sociodemographics.betaAge = 0.137 * s;
		parameters.pt.sociodemographics.betaAgeSquared = -0.010 * s;
		parameters.pt.sociodemographics.betaIncome = -0.026 * s;
		parameters.pt.sociodemographics.betaIncomeSquared = 0.005 * s;

		/*
		 * parameters.pt.purpose.betaWork = 0.326; parameters.pt.purpose.betaEducation =
		 * 0.911; parameters.pt.purpose.betaShop = -0.408;
		 * parameters.pt.purpose.betaErrand = -0.347 * s;
		 * 
		 * parameters.pt.region.betaRegion1 = 0.415; parameters.pt.region.betaRegion3 =
		 * 0.107 * s;
		 * 
		 * parameters.pt.municipalityType.betaSubUrban = -0.178;
		 * parameters.pt.municipalityType.betaRural = -0.213;
		 */

		return parameters;
	}

	/**
	 * Preliminary ASTRA 2018/002 model
	 */
	static private SwissUtilityParameters buildASTRA() {
		SwissUtilityParameters parameters = new SwissUtilityParameters();

		// Cost
		parameters.betaCost = -0.126;
		parameters.lambdaCostCrowflyDistance = -0.4;
		parameters.referenceCrowflyDistance_km = 40.0;

		// Car
		parameters.car.alpha = 0.827;
		parameters.car.betaTravelTime = -0.067;
		parameters.car.betaParkingSearchTime = -0.067;

		parameters.car.constantAccessEgressWalkTime_min = 4.0;
		parameters.car.constantParkingSearchPenalty_min = 4.0;

		parameters.car.region.betaRegion1 = -0.4;
		parameters.car.region.betaRegion3 = 0.4;

		// PT
		parameters.pt.alpha = 0.0;
		parameters.pt.betaLineSwitch = -0.17;
		parameters.pt.betaTravelTime = -0.019;
		parameters.pt.betaWaitingTime = -0.038;
		parameters.pt.betaAccessEgressTime = -0.08;

		// Bike
		parameters.bike.alpha = 0.344;
		parameters.bike.betaTravelTime = -0.09;
		parameters.bike.betaAgeOver18 = -0.049;

		parameters.bike.region.betaRegion3 = -0.366;

		// Walk
		parameters.walk.alpha = 1.3;
		parameters.walk.betaTravelTime = -0.141;

		// Taxi AV
		parameters.tav.alpha = -0.533;
		parameters.tav.betaTravelTime = -0.045;
		parameters.tav.betaWaitingTime = -0.0605;
		parameters.tav.betaAccessEgressTime = -0.0605;

		// Pooled AV
		parameters.pav.alpha = -0.85;
		parameters.pav.betaTravelTime = -0.0379;
		parameters.pav.betaWaitingTime = -0.0605;
		parameters.pav.betaAccessEgressTime = -0.0605;

		// Private AV
		parameters.prav.alpha = 0.762;
		parameters.prav.betaTravelTimeConventional = parameters.car.betaTravelTime;
		parameters.prav.betaTravelTimeAutomated = -0.036;
		parameters.prav.constantAccessEgressWalkTime_min = 4;

		return parameters;
	}

	public enum UtilitySet {
		CHAPTER_3_2_1, CHAPTER_3_2_2, CHAPTER_3_2_3, APPENDIX_A5, CUSTOM, ZERO, ASTRA_2018_002
	}

	static public SwissUtilityParameters build(UtilitySet set, boolean useRouteChoice, boolean onlySignificant) {
		logger.info("Preparing utility parameters: " + set.toString());

		switch (set) {
		case CHAPTER_3_2_1:
			return buildLinearWithoutSociodemographics(useRouteChoice, onlySignificant);
		case CHAPTER_3_2_2:
			return buildLinearWithSociodemographics(useRouteChoice, onlySignificant);
		case CHAPTER_3_2_3:
			return buildNonLinear(useRouteChoice, onlySignificant);
		case APPENDIX_A5:
			return buildAppendix(useRouteChoice, onlySignificant);
		case CUSTOM:
			return buildCustom(useRouteChoice, onlySignificant);
		case ASTRA_2018_002:
			return buildASTRA();
		case ZERO:
			return new SwissUtilityParameters();
		default:
			throw new IllegalStateException();
		}
	}

	static public void adaptFromMap(SwissUtilityParameters parameters, Map<String, Double> values) {
		for (Map.Entry<String, Double> entry : values.entrySet()) {
			String option = entry.getKey();
			double value = entry.getValue();

			try {
				String[] parts = option.split("\\.");

				Object activeObject = parameters;

				for (int i = 0; i < parts.length - 1; i++) {
					Field field = activeObject.getClass().getField(parts[i]);
					activeObject = field.get(activeObject);
				}

				Field field = activeObject.getClass().getField(parts[parts.length - 1]);

				if (field.getType().equals(double.class)) {
					logger.info(String.format("adaptFromMap Sets %s = %f", option, value));
					field.set(activeObject, value);
				} else {
					throw new IllegalStateException(
							String.format("Option %s does not refer to a double field", option));
				}
			} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
				throw new IllegalStateException("Error while processing option " + option);
			}
		}
	}

	static public void adaptFromCommandLine(SwissUtilityParameters parameters, CommandLine cmd) {
		Map<String, Double> values = new HashMap<>();

		for (String option : cmd.getAvailableOptions()) {
			if (option.startsWith(SwissDiscreteModeChoiceModule.COMMAND_LINE_PREFIX + ":")) {
				try {
					values.put(option.split(":")[1], Double.parseDouble(cmd.getOptionStrict(option)));
				} catch (ConfigurationException e) {
					// Should not happen
				}
			}
		}

		adaptFromMap(parameters, values);
	}

	static public void adaptFromFile(SwissUtilityParameters parameters, File path) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(path)));
		Map<String, Double> values = new HashMap<>();

		String line = null;
		while ((line = reader.readLine()) != null) {
			if (!(line.trim().length() == 0 || line.trim().startsWith("#"))) {
				String[] parts = line.split("=");

				if (parts.length != 2) {
					reader.close();
					throw new RuntimeException("Error while parsing line: " + line);
				} else {
					String option = parts[0].trim();
					String value = parts[1].trim();
					System.out.println(option + " = " + value);
					logger.info(String.format("adaptFromFile Sets %s = %s", option, value));
					//System.exit(0);
					values.put(option, Double.parseDouble(value));
				}
			}
		}

		reader.close();
		adaptFromMap(parameters, values);
	}
}

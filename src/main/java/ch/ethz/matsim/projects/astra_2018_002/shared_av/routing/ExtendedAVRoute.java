package ch.ethz.matsim.projects.astra_2018_002.shared_av.routing;

import org.matsim.core.utils.misc.Time;

import ch.ethz.matsim.av.routing.AVRoute;

public class ExtendedAVRoute extends AVRoute {
	private double waitingTime = Time.getUndefinedTime();

	public ExtendedAVRoute(AVRoute route) {
		super(route);
	}

	public double getWaitingTime() {
		return waitingTime;
	}

	public void setWaitingTime(double waitingTime) {
		this.waitingTime = waitingTime;
	}

	public double getJourneyTime() {
		return getTravelTime() + waitingTime;
	}
}

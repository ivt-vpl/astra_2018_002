package ch.ethz.matsim.projects.astra_2018_002.shared_av.pricing;

import ch.ethz.matsim.projects.astra_2018_002.shared_av.pricing.AVPricingListener.PriceType;

public class OnlineSharedAVTravelCost implements SharedAVTravelCost {
	private final AVPricingListener pricingListener;
	private final boolean isJoint;

	private double fixedPoolingPricePerKm;
	private double fixedTaxiPricePerKm;

	public OnlineSharedAVTravelCost(AVPricingListener pricingListener, boolean isJoint, double fixedPoolingPricePerKm,
			double fixedTaxiPricePerKm) {
		this.pricingListener = pricingListener;
		this.isJoint = isJoint;

		this.fixedPoolingPricePerKm = fixedPoolingPricePerKm;
		this.fixedTaxiPricePerKm = fixedTaxiPricePerKm;
	}

	@Override
	public double getTravelCost(double distance_km, boolean isPooled) {
		if (isJoint) {
			return pricingListener.getPricePerKm(PriceType.JOINT) * distance_km;
		} else if (isPooled) {
			if (Double.isNaN(fixedPoolingPricePerKm)) {
				return pricingListener.getPricePerKm(PriceType.POOLED) * distance_km;
			} else {
				return fixedPoolingPricePerKm * distance_km;
			}
		} else {
			if (Double.isNaN(fixedTaxiPricePerKm)) {
				return pricingListener.getPricePerKm(PriceType.TAXI) * distance_km;
			} else {
				return fixedTaxiPricePerKm * distance_km;
			}
		}
	}
}

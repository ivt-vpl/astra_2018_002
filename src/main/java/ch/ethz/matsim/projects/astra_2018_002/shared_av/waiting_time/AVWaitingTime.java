package ch.ethz.matsim.projects.astra_2018_002.shared_av.waiting_time;

import org.matsim.api.core.v01.network.Link;
import org.matsim.facilities.Facility;

public interface AVWaitingTime {
	double getWaitingTime(Facility<?> facility, double time, boolean isPooled);

	double getWaitingTime(Link link, double time, boolean isPooled);
}

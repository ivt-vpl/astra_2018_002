package ch.ethz.matsim.projects.astra_2018_002.mode_choice.base;

public class TripVariablesPt {
	public double accessEgressTime_min = 0.0;
	public double inVehicleTime_min = 0.0;
	public double waitingTime_min = 0.0; // Includes transfer walk acc. to Basil Schmid

	public int lineSwitches = 0;
	public double travelCost_CHF = 0.0;

	public double headway_min = 0.0;
	public double occupancy = 0.0;

	public enum MainMode {
		Rail, Tram, Bus
	}

	public MainMode mainMode = MainMode.Bus;

	public double delay_min = 0.0;
	public double travelDistance_km = 0.0;

	public double crowflyDistance_km = 0.0;
	public double inVehicleDistance_km = 0.0;

	public double originHomeDistance_km;
	public double destinationHomeDistance_km;
	
	public double railShare = 0.0;
}

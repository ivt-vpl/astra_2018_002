package ch.ethz.matsim.projects.astra_2018_002.analysis.flow;

public class AnalysisVehicle {
	public boolean isTruck = false;
	public boolean isShared = false;
	public boolean isPooled = false;
	public int automationLevel = 0;
}

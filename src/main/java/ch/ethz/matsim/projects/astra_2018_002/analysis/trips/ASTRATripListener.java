package ch.ethz.matsim.projects.astra_2018_002.analysis.trips;

import java.util.Collection;

import org.matsim.api.core.v01.events.ActivityEndEvent;
import org.matsim.api.core.v01.events.ActivityStartEvent;
import org.matsim.api.core.v01.events.LinkEnterEvent;
import org.matsim.api.core.v01.events.PersonDepartureEvent;
import org.matsim.api.core.v01.events.PersonEntersVehicleEvent;
import org.matsim.api.core.v01.events.PersonLeavesVehicleEvent;
import org.matsim.api.core.v01.network.Network;
import org.matsim.core.api.experimental.events.TeleportationArrivalEvent;
import org.matsim.core.router.MainModeIdentifier;
import org.matsim.core.router.StageActivityTypes;

import ch.ethz.matsim.baseline_scenario.analysis.trips.listeners.TripListener;
import ch.ethz.matsim.baseline_scenario.analysis.trips.utils.HomeActivityTypes;

public class ASTRATripListener extends TripListener {
	public ASTRATripListener(Network network, StageActivityTypes stageActivityTypes,
			HomeActivityTypes homeActivityTypes, MainModeIdentifier mainModeIdentifier,
			Collection<String> networkRouteModes) {
		super(network, stageActivityTypes, homeActivityTypes, mainModeIdentifier, networkRouteModes);
	}

	@Override
	public void handleEvent(ActivityEndEvent event) {
		if (event.getPersonId().toString().startsWith("av_") || event.getPersonId().toString().startsWith("prav_"))
			return;
		super.handleEvent(event);
	}

	@Override
	public void handleEvent(PersonDepartureEvent event) {
		if (event.getPersonId().toString().startsWith("av_") || event.getPersonId().toString().startsWith("prav_"))
			return;
		super.handleEvent(event);
	}

	@Override
	public void handleEvent(ActivityStartEvent event) {
		if (event.getPersonId().toString().startsWith("av_") || event.getPersonId().toString().startsWith("prav_"))
			return;
		super.handleEvent(event);
	}

	@Override
	public void handleEvent(PersonEntersVehicleEvent event) {
		if (event.getPersonId().toString().startsWith("av_") || event.getPersonId().toString().startsWith("prav_"))
			return;
		super.handleEvent(event);
	}

	@Override
	public void handleEvent(PersonLeavesVehicleEvent event) {
		if (event.getPersonId().toString().startsWith("av_") || event.getPersonId().toString().startsWith("prav_"))
			return;
		super.handleEvent(event);
	}

	@Override
	public void handleEvent(LinkEnterEvent event) {
		super.handleEvent(event);
	}

	@Override
	public void handleEvent(TeleportationArrivalEvent event) {
		if (event.getPersonId().toString().startsWith("av_") || event.getPersonId().toString().startsWith("prav_"))
			return;
		super.handleEvent(event);
	}
}

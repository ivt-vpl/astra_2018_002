package ch.ethz.matsim.projects.astra_2018_002.shared_av.pricing;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.matsim.core.controler.OutputDirectoryHierarchy;
import org.matsim.core.controler.events.IterationEndsEvent;
import org.matsim.core.controler.listener.IterationEndsListener;

import ch.ethz.matsim.av_cost_calculator.CostCalculatorExecutor;
import ch.ethz.matsim.av_cost_calculator.run.ParameterBuilder;
import ch.ethz.matsim.av_cost_calculator.run.PricingAnalysisHandler;
import ch.ethz.matsim.projects.astra_2018_002.shared_av.SharedAVModule;

public class AVPricingListener implements IterationEndsListener {
	private final PricingAnalysisHandler taxiHandler;
	private final PricingAnalysisHandler pooledHandler;

	private final CostCalculatorExecutor executor;
	private final BufferedWriter writer;

	private final double scenarioScale;
	private final int horizon;

	private List<Double> taxiHistory = new LinkedList<>();
	private List<Double> pooledHistory = new LinkedList<>();
	private List<Double> jointHistory = new LinkedList<>();

	private double activeTaxiPrice = 0.0;
	private double activePooledPrice = 0.0;
	private double activeJointPrice = 0.0;

	private final String taxiVehicleType = SharedAVModule.TAXI_CC_TYPE;
	private final String pooledVehicleType = SharedAVModule.POOLED_CC_TYPE;

	public AVPricingListener(double scenarioScale, int horizon, PricingAnalysisHandler taxiHandler,
			PricingAnalysisHandler pooledHandler, CostCalculatorExecutor executor, OutputDirectoryHierarchy output,
			double initialPrice) throws IOException {
		this.scenarioScale = scenarioScale;
		this.horizon = horizon;

		this.taxiHandler = taxiHandler;
		this.pooledHandler = pooledHandler;

		this.executor = executor;

		writer = new BufferedWriter(
				new OutputStreamWriter(new FileOutputStream(output.getOutputFilename("prices.csv"))));
		writer.write(
				"iteration;active_taxi_price;computed_taxi_price;active_pooled_price;computed_pooled_price;active_joint_price;computed_joint_price\n");
		writer.flush();

		for (int i = 0; i < horizon; i++) {
			taxiHistory.add(initialPrice);
			pooledHistory.add(initialPrice);
			jointHistory.add(initialPrice);
		}
	}

	@Override
	public void notifyIterationEnds(IterationEndsEvent event) {
		double totalTime = 24.0 * 3600.0;

		// Calculate taxi price
		Map<String, String> taxiParameters = new ParameterBuilder(scenarioScale, totalTime, taxiVehicleType)
				.build(taxiHandler);
		double taxiPrice = executor.computePricePerPassengerKm(taxiParameters);
		double taxiDistance = taxiHandler.getTotalPassengerDistance();
		taxiHandler.resetHandler();

		// Update taxi history
		if (!Double.isNaN(taxiPrice)) {
			taxiHistory.remove(0);
			taxiHistory.add(taxiPrice);
		}

		activeTaxiPrice = taxiHistory.stream().mapToDouble(d -> d).sum() / (double) horizon;

		// Calculate pooled price
		Map<String, String> pooledParameters = new ParameterBuilder(scenarioScale, totalTime, pooledVehicleType)
				.build(pooledHandler);
		double pooledPrice = executor.computePricePerPassengerKm(pooledParameters);
		double pooledDistance = pooledHandler.getTotalPassengerDistance();
		pooledHandler.resetHandler();

		// Update pooled history
		if (!Double.isNaN(pooledPrice)) {
			pooledHistory.remove(0);
			pooledHistory.add(pooledPrice);
		}

		activePooledPrice = pooledHistory.stream().mapToDouble(d -> d).sum() / (double) horizon;

		// Calculate joint price
		double jointPrice = Double.NaN;

		if (Double.isNaN(taxiPrice)) {
			jointPrice = pooledPrice;
		} else if (Double.isNaN(pooledPrice)) {
			jointPrice = taxiPrice;
		} else {
			double totalDistance = taxiDistance + pooledDistance;
			jointPrice = (taxiPrice * taxiDistance + pooledPrice * pooledDistance) / totalDistance;
		}

		// Update joint history
		if (!Double.isNaN(jointPrice)) {
			jointHistory.remove(0);
			jointHistory.add(jointPrice);
		}

		activeJointPrice = jointHistory.stream().mapToDouble(d -> d).sum() / (double) horizon;

		// Write out result

		try {
			writer.write(String.format("%d;%f;%f;%f;%f;%f;%f\n", event.getIteration(), activeTaxiPrice, taxiPrice,
					activePooledPrice, pooledPrice, activeJointPrice, jointPrice));
			writer.flush();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public enum PriceType {
		TAXI, POOLED, JOINT
	}

	public double getPricePerKm(PriceType priceType) {
		switch (priceType) {
		case JOINT:
			return activeJointPrice;
		case POOLED:
			return activePooledPrice;
		case TAXI:
			return activeTaxiPrice;
		default:
			throw new IllegalStateException();
		}
	}
}

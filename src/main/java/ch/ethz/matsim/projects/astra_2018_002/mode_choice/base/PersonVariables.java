package ch.ethz.matsim.projects.astra_2018_002.mode_choice.base;

import org.matsim.api.core.v01.Coord;

public class PersonVariables {
	public double age_over18a = 0;
	public double age_10a = 0;
	public double income_1000CHF = 0.0;
	public boolean isMale = false;
	public boolean carAlwaysAvailable = false;
	public Coord homeLocation = new Coord(0.0, 0.0);

	public boolean hasHalbtaxSubscription = false;
	public boolean hasRegionalSubscription = false;
	public boolean hasGeneralSubscription = false;

	public int regionIndex = 0;

	public enum MunicipalityType {
		Urban, SubUrban, Rural
	}

	public MunicipalityType municipalityType = MunicipalityType.Rural;
}

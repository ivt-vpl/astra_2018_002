package ch.ethz.matsim.projects.astra_2018_002.shared_av;

import org.matsim.core.config.ReflectiveConfigGroup;

public class SharedAVConfigGroup extends ReflectiveConfigGroup {
	public final static String GROUP_NAME = "shared_avs";

	private final static String USE_SHARED_AVS = "useSharedAVs";
	private final static String USE_COST_CALCULATOR = "useCostCalculator";

	private final static String TAXI_FLEET_SIZE = "taxiFleetSize";
	private final static String POOLED_FLEET_SIZE = "pooledFleetSize";

	private final static String MINIMUM_TRIP_DISTANCE_KM = "minimumTripDistance_km";
	private final static String POOLING_THRESHOLD_MIN = "poolingThreshold_min";

	private final static String SERVICE_AREA_ATTRIBUTE = "serviceAreaAttribute";
	private final static String ACTIVE_SERVICE_AREA = "activeServiceArea";
	private final static String SERVICE_AREA_SHAPEFILE = "serviceAreaShapefile";

	private final static String WAITING_TIME_ZONE_SHAPEFILE = "waitingTimeZoneShapefile";
	private final static String WAITING_TIME_ZONE_ATTRIBUTE = "waitingTimeZoneAttribute";

	private final static String COST_CALCULATOR_SCALING_FACTOR = "costCalculatorScalingFactor";
	private final static String COST_CALCULATOR_HORIZON = "costCalculatorHorizon";
	private final static String COST_CALCULATOR_INITIAL_PRICE = "costCalculatorInitialPrice";
	private final static String DISTRIBUTE_COSTS = "distributeCosts";

	private final static String WAITING_TIME_ESTIMATOR_START_TIME = "waitingTimeEstimatorStartTime";
	private final static String WAITING_TIME_ESTIMATOR_END_TIME = "waitingTimeEstimatorEndTime";
	private final static String WAITING_TIME_ESTIMATOR_INTERVAL = "waitingTimeEstimatorInterval";
	private final static String WAITING_TIME_ESTIMATOR_HORIZON = "waitingTimeEstimatorHorizon";
	private final static String WAITING_TIME_ESTIMATOR_FALLBACK_VALUE = "waitingTimeEstimatorFallbackValue";

	private final static String FIXED_POOLING_PRICE_PER_KM = "fixedPoolingPricePerKm";
	private final static String FIXED_TAXI_PRICE_PER_KM = "fixedTaxiPricePerKm";

	private boolean useSharedAVs = false;
	private boolean useCostCalculator = false;

	private int taxiFleetSize = 100;
	private int pooledFleetSize = 0;

	private double minimumTripDistance_km = 0.0;
	private double poolingThreshold_min = 4.0;

	private String serviceAreaAttribute = "SAREA";
	private String activeServiceArea = "Active service area not set";
	private String serviceAreaShapefile = "service_areas.shp";

	private String waitingTimeZoneAttribute = "WZONE";
	private String waitingTimeZoneShapefile = "waiting_time_zones.shp";

	private double costCalculatorScalingFactor = 1.0;
	private int costCalculatorHorizon = 10;
	private double costCalculatorInitialPrice = 0.4;
	private boolean distributeCosts = false;

	private double waitingTimeEstimatorStartTime = 6.0 * 3600.0;
	private double waitingTimeEstimatorEndTime = 22.0 * 3600.0;
	private double waitingTimeEstimatorInterval = 900.0;
	private int waitingTimeEstimatorHorizon = 10;
	private double waitingTimeEstimatorFallbackValue = 300.0;

	private double fixedPoolingPricePerKm = Double.NaN;
	private double fixedTaxiPricePerKm = Double.NaN;

	public SharedAVConfigGroup() {
		super(GROUP_NAME);
	}

	@StringGetter(USE_SHARED_AVS)
	public boolean getUseSharedAVs() {
		return useSharedAVs;
	}

	@StringSetter(USE_SHARED_AVS)
	public void setUseSharedAVs(boolean useSharedAVs) {
		this.useSharedAVs = useSharedAVs;
	}

	@StringGetter(USE_COST_CALCULATOR)
	public boolean getUseCostCalculator() {
		return useCostCalculator;
	}

	@StringSetter(USE_COST_CALCULATOR)
	public void setUseCostCalculator(boolean useCostCalculator) {
		this.useCostCalculator = useCostCalculator;
	}

	@StringGetter(MINIMUM_TRIP_DISTANCE_KM)
	public double getMinimumTripDistance_km() {
		return minimumTripDistance_km;
	}

	@StringSetter(MINIMUM_TRIP_DISTANCE_KM)
	public void setMinimumTripDistance(double minimumTripDistance) {
		this.minimumTripDistance_km = minimumTripDistance;
	}

	@StringGetter(POOLING_THRESHOLD_MIN)
	public double getPoolingThreshold_min() {
		return poolingThreshold_min;
	}

	@StringSetter(POOLING_THRESHOLD_MIN)
	public void setPoolingThreshold_min(double poolingThreshold_min) {
		this.poolingThreshold_min = poolingThreshold_min;
	}

	@StringGetter(TAXI_FLEET_SIZE)
	public int getTaxiFleetSize() {
		return taxiFleetSize;
	}

	@StringSetter(TAXI_FLEET_SIZE)
	public void setTaxiFleetSize(int taxiFleetSize) {
		this.taxiFleetSize = taxiFleetSize;
	}

	@StringGetter(POOLED_FLEET_SIZE)
	public int getPooledFleetSize() {
		return pooledFleetSize;
	}

	@StringSetter(POOLED_FLEET_SIZE)
	public void setPooledFleetSize(int pooledFleetSize) {
		this.pooledFleetSize = pooledFleetSize;
	}

	@StringGetter(SERVICE_AREA_ATTRIBUTE)
	public String getServiceAreaAttribute() {
		return serviceAreaAttribute;
	}

	@StringSetter(SERVICE_AREA_ATTRIBUTE)
	public void setServiceAreaAttribute(String serviceAreaAttribute) {
		this.serviceAreaAttribute = serviceAreaAttribute;
	}

	@StringGetter(ACTIVE_SERVICE_AREA)
	public String getActiveServiceArea() {
		return activeServiceArea;
	}

	@StringSetter(ACTIVE_SERVICE_AREA)
	public void setActiveServiceArea(String activeServiceArea) {
		this.activeServiceArea = activeServiceArea;
	}

	@StringGetter(SERVICE_AREA_SHAPEFILE)
	public String getServiceAreaShapefile() {
		return serviceAreaShapefile;
	}

	@StringSetter(SERVICE_AREA_SHAPEFILE)
	public void setServiceAreaShapefile(String serviceAreaShapefile) {
		this.serviceAreaShapefile = serviceAreaShapefile;
	}

	@StringGetter(WAITING_TIME_ZONE_SHAPEFILE)
	public String getWaitingTimeZoneShapefile() {
		return waitingTimeZoneShapefile;
	}

	@StringSetter(WAITING_TIME_ZONE_SHAPEFILE)
	public void setWaitingTimeZoneShapefile(String waitingTimeZoneShapefile) {
		this.waitingTimeZoneShapefile = waitingTimeZoneShapefile;
	}

	@StringGetter(WAITING_TIME_ZONE_ATTRIBUTE)
	public String getWaitingTimeZoneAttribute() {
		return waitingTimeZoneAttribute;
	}

	@StringSetter(WAITING_TIME_ZONE_ATTRIBUTE)
	public void setWaitingTimeZoneAttribute(String waitingTimeZoneAttribute) {
		this.waitingTimeZoneAttribute = waitingTimeZoneAttribute;
	}

	@StringGetter(COST_CALCULATOR_SCALING_FACTOR)
	public double getCostCalculatorScalingFactor() {
		return costCalculatorScalingFactor;
	}

	@StringSetter(COST_CALCULATOR_SCALING_FACTOR)
	public void setCostCalculatorScalingFactor(double costCalculatorScalingFactor) {
		this.costCalculatorScalingFactor = costCalculatorScalingFactor;
	}

	@StringGetter(COST_CALCULATOR_HORIZON)
	public int getCostCalculatorHorzion() {
		return costCalculatorHorizon;
	}

	@StringSetter(COST_CALCULATOR_HORIZON)
	public void setCostCalculatorHorzion(int costCalculatorHorizon) {
		this.costCalculatorHorizon = costCalculatorHorizon;
	}

	@StringGetter(COST_CALCULATOR_INITIAL_PRICE)
	public double getCostCalculatorInitialPrice() {
		return costCalculatorInitialPrice;
	}

	@StringSetter(COST_CALCULATOR_INITIAL_PRICE)
	public void setCostCalculatorInitialPrice(double costCalculatorInitialPrice) {
		this.costCalculatorInitialPrice = costCalculatorInitialPrice;
	}

	@StringGetter(DISTRIBUTE_COSTS)
	public boolean getDistributeCosts() {
		return distributeCosts;
	}

	@StringSetter(DISTRIBUTE_COSTS)
	public void setDistributeCosts(boolean distributeCosts) {
		this.distributeCosts = distributeCosts;
	}

	@StringGetter(WAITING_TIME_ESTIMATOR_START_TIME)
	public double getWaitingTimeEstimatorStartTime() {
		return waitingTimeEstimatorStartTime;
	}

	@StringSetter(WAITING_TIME_ESTIMATOR_START_TIME)
	public void setWaitingTimeEstimatorStartTime(double waitingTimeEstimatorStartTime) {
		this.waitingTimeEstimatorStartTime = waitingTimeEstimatorStartTime;
	}

	@StringGetter(WAITING_TIME_ESTIMATOR_END_TIME)
	public double getWaitingTimeEstimatorEndTime() {
		return waitingTimeEstimatorEndTime;
	}

	@StringSetter(WAITING_TIME_ESTIMATOR_END_TIME)
	public void setWaitingTimeEstimatorEndTime(double waitingTimeEstimatorEndTime) {
		this.waitingTimeEstimatorEndTime = waitingTimeEstimatorEndTime;
	}

	@StringGetter(WAITING_TIME_ESTIMATOR_INTERVAL)
	public double getWaitingTimeEstimatorInterval() {
		return waitingTimeEstimatorInterval;
	}

	@StringSetter(WAITING_TIME_ESTIMATOR_INTERVAL)
	public void setWaitingTimeEstimatorInterval(double waitingTimeEstimatorInterval) {
		this.waitingTimeEstimatorInterval = waitingTimeEstimatorInterval;
	}

	@StringGetter(WAITING_TIME_ESTIMATOR_HORIZON)
	public int getWaitingTimeEstimatorHorizon() {
		return waitingTimeEstimatorHorizon;
	}

	@StringSetter(WAITING_TIME_ESTIMATOR_HORIZON)
	public void getWaitingTimeEstimatorHorizon(int waitingTimeEstimatorHorizon) {
		this.waitingTimeEstimatorHorizon = waitingTimeEstimatorHorizon;
	}

	@StringGetter(WAITING_TIME_ESTIMATOR_FALLBACK_VALUE)
	public double getWaitingTimeEstimatorFallbackValue() {
		return waitingTimeEstimatorFallbackValue;
	}

	@StringSetter(WAITING_TIME_ESTIMATOR_FALLBACK_VALUE)
	public void setWaitingTimeEstimatorFallbackValue(double waitingTimeEstimatorFallbackValue) {
		this.waitingTimeEstimatorFallbackValue = waitingTimeEstimatorFallbackValue;
	}

	@StringGetter(FIXED_POOLING_PRICE_PER_KM)
	public double getFixedPoolingPricePerKm() {
		return fixedPoolingPricePerKm;
	}

	@StringSetter(FIXED_POOLING_PRICE_PER_KM)
	public void setFixedPoolingPricePerKm(double fixedPoolingPricePerKm) {
		this.fixedPoolingPricePerKm = fixedPoolingPricePerKm;
	}

	@StringGetter(FIXED_TAXI_PRICE_PER_KM)
	public double getFixedTaxiPricePerKm() {
		return fixedTaxiPricePerKm;
	}

	@StringSetter(FIXED_TAXI_PRICE_PER_KM)
	public void setFixedTaxiPricePerKm(double fixedTaxiPricePerKm) {
		this.fixedTaxiPricePerKm = fixedTaxiPricePerKm;
	}
}

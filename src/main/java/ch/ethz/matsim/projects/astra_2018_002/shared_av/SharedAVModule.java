package ch.ethz.matsim.projects.astra_2018_002.shared_av;

import java.net.URL;
import java.util.Collections;
import java.util.List;

import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.Scenario;
import org.matsim.api.core.v01.TransportMode;
import org.matsim.api.core.v01.network.Link;
import org.matsim.api.core.v01.network.Network;
import org.matsim.api.core.v01.population.PopulationFactory;
import org.matsim.core.config.ConfigGroup;
import org.matsim.core.controler.OutputDirectoryHierarchy;
import org.matsim.core.network.NetworkUtils;
import org.matsim.core.network.algorithms.NetworkCleaner;
import org.matsim.core.network.algorithms.TransportModeNetworkFilter;
import org.matsim.core.router.DijkstraFactory;
import org.matsim.core.router.RoutingModule;
import org.matsim.core.router.costcalculators.OnlyTimeDependentTravelDisutility;
import org.matsim.core.router.util.LeastCostPathCalculator;
import org.matsim.core.router.util.TravelTime;
import org.matsim.core.utils.collections.QuadTree;
import org.matsim.vehicles.VehicleType;

import com.google.inject.Key;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import com.google.inject.name.Names;

import ch.ethz.matsim.av.config.AVConfig;
import ch.ethz.matsim.av.config.AVDispatcherConfig;
import ch.ethz.matsim.av.config.AVGeneratorConfig;
import ch.ethz.matsim.av.config.AVOperatorConfig;
import ch.ethz.matsim.av.framework.AVModule;
import ch.ethz.matsim.av.framework.AVUtils;
import ch.ethz.matsim.av.routing.AVRouteFactory;
import ch.ethz.matsim.discrete_mode_choice.modules.AbstractDiscreteModeChoiceExtension;
import ch.ethz.matsim.projects.astra_2018_002.flow_efficiency.FlowEfficiencyConfigurator;
import ch.ethz.matsim.projects.astra_2018_002.shared_av.analysis.WaitingTimeListener;
import ch.ethz.matsim.projects.astra_2018_002.shared_av.routing.ASTRAAVRoutingModule;
import ch.ethz.matsim.projects.astra_2018_002.shared_av.service_area.ServiceArea;
import ch.ethz.matsim.projects.astra_2018_002.shared_av.service_area.ServiceAreaConstraint;
import ch.ethz.matsim.projects.astra_2018_002.shared_av.service_area.ServiceAreaGenerator;
import ch.ethz.matsim.projects.astra_2018_002.shared_av.service_area.ServiceAreaReader;
import ch.ethz.matsim.projects.astra_2018_002.shared_av.waiting_time.AVWaitingTime;
import ch.ethz.matsim.projects.astra_2018_002.shared_av.waiting_time.zonal.WaitingTimeZone;
import ch.ethz.matsim.projects.astra_2018_002.shared_av.waiting_time.zonal.WaitingTimeZoneReader;
import ch.ethz.matsim.projects.astra_2018_002.shared_av.waiting_time.zonal.ZonalWaitingTime;

public class SharedAVModule extends AbstractDiscreteModeChoiceExtension {
	public static final String CONSTRAINT_NAME = "ASTRA";
	public static final String GENERATOR_NAME = "ASTRA";

	public static final String AV_TAXI = "av_taxi";
	public static final String AV_POOL = "av_pool";

	public static final String WAITING_TIME_GROUP_ATTRIBUTE = "waitingTimeGroup";

	public static final long POOLED_NUMBER_OF_SEATS = 4;
	
	public static final String TAXI_CC_TYPE = "Solo";
	public static final String POOLED_CC_TYPE = "Midsize";

	@Override
	protected void installExtension() {
		bindTripConstraintFactory(CONSTRAINT_NAME).to(ServiceAreaConstraint.Factory.class);
		AVUtils.registerGeneratorFactory(binder(), GENERATOR_NAME, ServiceAreaGenerator.Factory.class);

		addControlerListenerBinding().to(AVNetworkWriter.class);

		bind(Key.get(TravelTime.class, Names.named(AV_TAXI)))
				.to(Key.get(TravelTime.class, Names.named(TransportMode.car)));
		bind(Key.get(TravelTime.class, Names.named(AV_POOL)))
				.to(Key.get(TravelTime.class, Names.named(TransportMode.car)));

		bind(AVWaitingTime.class).to(ZonalWaitingTime.class);
		addEventHandlerBinding().to(ZonalWaitingTime.class);
		addControlerListenerBinding().to(ZonalWaitingTime.class);

		addControlerListenerBinding().to(WaitingTimeListener.class);
	}
	
	@Provides
	@Singleton
	@Named("service_area")
	public QuadTree<Link> provideServiceAreaQuadTree(ServiceArea serviceArea, @Named(AVModule.AV_MODE) Network network) {
		double[] bounds = NetworkUtils.getBoundingBox(network.getNodes().values());
		QuadTree<Link> index = new QuadTree<>(bounds[0], bounds[1], bounds[2], bounds[3]);
		
		for (Id<Link> linkId : serviceArea.getServedLinkIds()) {
			Link link = network.getLinks().get(linkId);
			
			if (link != null) {
				index.put(link.getCoord().getX(), link.getCoord().getY(), link);
			}
		}
		
		return index;
	}

	@Provides
	@Named(AV_TAXI)
	public RoutingModule provideAvTaxiRoutingModule(PopulationFactory populationFactory, AVRouteFactory routeFactory,
			@Named(TransportMode.walk) RoutingModule walkRoutingModule, @Named(AVModule.AV_MODE) Network network,
			AVWaitingTime waitingTime, @Named(AV_TAXI) TravelTime travelTime, @Named("service_area") QuadTree<Link> serviceAreaIndex) {
		LeastCostPathCalculator router = new DijkstraFactory().createPathCalculator(network,
				new OnlyTimeDependentTravelDisutility(travelTime), travelTime);
		return new ASTRAAVRoutingModule(AV_TAXI, populationFactory, routeFactory, walkRoutingModule, router, network,
				travelTime, waitingTime, false, serviceAreaIndex);
	}

	@Provides
	@Named(AV_POOL)
	public RoutingModule provideAvPoolRoutingModule(PopulationFactory populationFactory, AVRouteFactory routeFactory,
			@Named(TransportMode.walk) RoutingModule walkRoutingModule, @Named(AVModule.AV_MODE) Network network,
			AVWaitingTime waitingTime, @Named(AV_POOL) TravelTime travelTime, @Named("service_area") QuadTree<Link> serviceAreaIndex) {
		LeastCostPathCalculator router = new DijkstraFactory().createPathCalculator(network,
				new OnlyTimeDependentTravelDisutility(travelTime), travelTime);
		return new ASTRAAVRoutingModule(AV_POOL, populationFactory, routeFactory, walkRoutingModule, router, network,
				travelTime, waitingTime, true, serviceAreaIndex);
	}

	@Provides
	@Singleton
	public ServiceAreaConstraint.Factory provideOperatingAreaConstraintFactory(ServiceArea registry,
			SharedAVConfigGroup savConfig) {
		return new ServiceAreaConstraint.Factory(registry, savConfig.getMinimumTripDistance_km());
	}

	@Provides
	@Singleton
	public ServiceArea provideServiceArea(Network network, SharedAVConfigGroup astraConfig) {
		ServiceAreaReader reader = new ServiceAreaReader(astraConfig.getServiceAreaAttribute(), network);
		URL url = ConfigGroup.getInputFileURL(getConfig().getContext(), astraConfig.getServiceAreaShapefile());
		return reader.read(astraConfig.getActiveServiceArea(), url);
	}

	@Provides
	@Singleton
	public AVConfig provideAVConfig(SharedAVConfigGroup astraConfig) {
		AVConfig avConfig = new AVConfig();

		AVOperatorConfig taxiConfig = avConfig.createOperatorConfig(SharedAVModule.AV_TAXI);

		AVGeneratorConfig taxiGeneratorConfig = taxiConfig.createGeneratorConfig(SharedAVModule.GENERATOR_NAME);
		taxiGeneratorConfig.setNumberOfVehicles(astraConfig.getTaxiFleetSize());

		taxiConfig.createDispatcherConfig("SingleHeuristic");
		taxiConfig.createPriceStructureConfig();

		AVOperatorConfig poolConfig = avConfig.createOperatorConfig(SharedAVModule.AV_POOL);

		AVGeneratorConfig poolGeneratorConfig = poolConfig.createGeneratorConfig(SharedAVModule.GENERATOR_NAME);
		poolGeneratorConfig.setNumberOfVehicles(astraConfig.getPooledFleetSize());

		AVDispatcherConfig poolDispatcherConfig = poolConfig.createDispatcherConfig("MultiOD");
		poolConfig.createPriceStructureConfig();

		double poolingThreshold_min = astraConfig.getPoolingThreshold_min();
		poolDispatcherConfig.addParam("maximumTimeRadius", String.valueOf(60.0 * poolingThreshold_min));

		return avConfig;
	}

	@Provides
	@Singleton
	public List<WaitingTimeZone> provideWaitingTimeZones(@Named(AVModule.AV_MODE) Network network,
			SharedAVConfigGroup astraConfig) {
		WaitingTimeZoneReader reader = new WaitingTimeZoneReader(astraConfig.getWaitingTimeZoneAttribute(),
				astraConfig.getServiceAreaAttribute(), network);
		URL url = ConfigGroup.getInputFileURL(getConfig().getContext(), astraConfig.getWaitingTimeZoneShapefile());
		return reader.read(astraConfig.getActiveServiceArea(), url);
	}

	@Provides
	@Singleton
	public ZonalWaitingTime provideAvWaitingTime(Network network, List<WaitingTimeZone> waitingTimeZones,
			SharedAVConfigGroup sharedConfig) {
		double estimationStartTime = sharedConfig.getWaitingTimeEstimatorStartTime();
		double estimationEndTime = sharedConfig.getWaitingTimeEstimatorEndTime();
		double estimationInterval = sharedConfig.getWaitingTimeEstimatorInterval();
		int horizon = sharedConfig.getWaitingTimeEstimatorHorizon();
		double defaultWaitingTime = sharedConfig.getWaitingTimeEstimatorFallbackValue();

		return new ZonalWaitingTime(estimationStartTime, estimationEndTime, estimationInterval, horizon,
				defaultWaitingTime, network, waitingTimeZones);
	}

	@Provides
	@Singleton
	public WaitingTimeListener provideWaitingTimeListener(AVWaitingTime waitingTime,
			List<WaitingTimeZone> waitingTimeZones, OutputDirectoryHierarchy outputHierarchy) {
		return new WaitingTimeListener(waitingTime, waitingTimeZones, outputHierarchy);
	}

	@Provides
	@Singleton
	@Named(AVModule.AV_MODE)
	public VehicleType provideVehicleType(Scenario scenario) {
		return scenario.getVehicles().getVehicleTypes().get(FlowEfficiencyConfigurator.SHARED_AV_VEHICLE_TYPE_ID);
	}

	@Provides
	@Singleton
	@Named(AVModule.AV_MODE)
	public Network provideAVNetwork(Network fullNetwork) {
		Network network = NetworkUtils.createNetwork();
		new TransportModeNetworkFilter(fullNetwork).filter(network, Collections.singleton(AVModule.AV_MODE));
		new NetworkCleaner().run(network);
		return network;
	}
}

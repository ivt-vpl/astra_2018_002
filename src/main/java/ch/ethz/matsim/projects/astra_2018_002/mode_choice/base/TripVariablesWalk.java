package ch.ethz.matsim.projects.astra_2018_002.mode_choice.base;

public class TripVariablesWalk {
	public double travelTime_min = 0.0;
	public double travelDistance_km = 0.0;
	public double delay_min = 0.0;
}

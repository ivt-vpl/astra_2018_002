package ch.ethz.matsim.projects.astra_2018_002.flow_efficiency;

import java.io.File;
import java.io.IOException;

import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.Scenario;
import org.matsim.api.core.v01.population.Person;
import org.matsim.core.config.Config;
import org.matsim.core.config.groups.QSimConfigGroup.VehiclesSource;
import org.matsim.vehicles.Vehicle;
import org.matsim.vehicles.VehicleCapacity;
import org.matsim.vehicles.VehicleType;
import org.matsim.vehicles.VehicleUtils;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import ch.ethz.matsim.baseline_scenario.config.CommandLine;
import ch.ethz.matsim.baseline_scenario.config.CommandLine.ConfigurationException;
import ch.ethz.matsim.projects.astra_2018_002.private_av.PrivateAVConfigGroup;
import ch.ethz.matsim.projects.astra_2018_002.shared_av.SharedAVConfigGroup;

public class FlowEfficiencyConfigurator {
	
	static final String FLOW_EFFICIENCY_PATH_CONFIG_KEY = "flow-efficiency-path";
	
	static final public Id<VehicleType> CONVENTIONAL_VEHICLE_TYPE_ID = Id.create("car", VehicleType.class);
	static final public Id<VehicleType> PRIVATE_AV_VEHICLE_TYPE_ID = Id.create("private_av", VehicleType.class);
	static final public Id<VehicleType> SHARED_AV_VEHICLE_TYPE_ID = Id.create("shared_av", VehicleType.class);
	static final public Id<VehicleType> PRAV3_ID = Id.create("prav3", VehicleType.class);
	static final public Id<VehicleType> PRAV4_ID = Id.create("prav4", VehicleType.class);
	static final public Id<VehicleType> PRAV5_ID = Id.create("prav5", VehicleType.class);
	static final public Id<VehicleType> PASSENGER_VEHICLE_TYPE_ID = Id.create("car_passenger", VehicleType.class);
	static final public Id<VehicleType> FREIGHT_TRUCK_ID = Id.create("truck", VehicleType.class);
	static final public Id<VehicleType> FREIGHT_TRUCK_AV_ID = Id.create("truckAv", VehicleType.class);

	static public void configure(Config config, CommandLine cmd)
			throws JsonParseException, JsonMappingException, IOException, ConfigurationException {
		// Load vehicles by vehicle type
		config.qsim().setVehiclesSource(VehiclesSource.modeVehicleTypesFromVehiclesData);

		PrivateAVConfigGroup privateConfig = (PrivateAVConfigGroup) config.getModules()
				.get(PrivateAVConfigGroup.GROUP_NAME);
		SharedAVConfigGroup sharedConfig = (SharedAVConfigGroup) config.getModules()
				.get(SharedAVConfigGroup.GROUP_NAME);

		if (privateConfig.getUsePrivateAVs() || sharedConfig.getUseSharedAVs()) {
			FlowEfficiencyDefinition definition = new FlowEfficiencyDefinition();
			
			
			if (cmd.hasOption(FLOW_EFFICIENCY_PATH_CONFIG_KEY)) {
				definition = FlowEfficiencyDefinition.fromFile(new File(cmd.getOptionStrict(FLOW_EFFICIENCY_PATH_CONFIG_KEY)));
			}

			// With the AvFlowEfficiencyCalculator we can distinguish between HLS
			// and other roads in combination with vehicle types

			FlowEfficiencyCalculator calculator = new AvFlowEfficiencyCalculator(definition);
			FlowEfficiencyCalculator.INSTANCE.set(calculator);
		} else {
			// Set the calculator instance (not the best design, but works)
			FlowEfficiencyCalculator calculator = new StaticFlowEfficiencyCalculator();
			FlowEfficiencyCalculator.INSTANCE.set(calculator);
		}
	}

	static public void defineVehicleTypes(Scenario scenario, int year) {
		VehicleCapacity defaultCapacity = scenario.getVehicles().getFactory().createVehicleCapacity();
		defaultCapacity.setSeats(4);

		VehicleType carVehicleType = VehicleUtils.getFactory() //
				.createVehicleType(CONVENTIONAL_VEHICLE_TYPE_ID);
		carVehicleType.setCapacity(defaultCapacity);
		scenario.getVehicles().addVehicleType(carVehicleType);

		VehicleType passengerVehicleType = VehicleUtils.getFactory() //
				.createVehicleType(PASSENGER_VEHICLE_TYPE_ID);
		passengerVehicleType.setCapacity(defaultCapacity);
		scenario.getVehicles().addVehicleType(passengerVehicleType);

		VehicleType privateAvVehicleType = VehicleUtils.getFactory() //
				.createVehicleType(PRIVATE_AV_VEHICLE_TYPE_ID);
		privateAvVehicleType.setCapacity(defaultCapacity);
		scenario.getVehicles().addVehicleType(privateAvVehicleType);

		VehicleType sharedAvVehicleType = VehicleUtils.getFactory() //
				.createVehicleType(SHARED_AV_VEHICLE_TYPE_ID);
		sharedAvVehicleType.setCapacity(defaultCapacity);
		scenario.getVehicles().addVehicleType(sharedAvVehicleType);

		VehicleType prav3VehicleType = VehicleUtils.getFactory() //
				.createVehicleType(PRAV3_ID);
		prav3VehicleType.setCapacity(defaultCapacity);
		scenario.getVehicles().addVehicleType(prav3VehicleType);

		VehicleType prav4VehicleType = VehicleUtils.getFactory() //
				.createVehicleType(PRAV4_ID);
		prav4VehicleType.setCapacity(defaultCapacity);
		scenario.getVehicles().addVehicleType(prav4VehicleType);

		VehicleType prav5VehicleType = VehicleUtils.getFactory() //
				.createVehicleType(PRAV5_ID);
		prav5VehicleType.setCapacity(defaultCapacity);
		scenario.getVehicles().addVehicleType(prav5VehicleType);

		VehicleType truckVehicleType = VehicleUtils.getFactory() //
				.createVehicleType(FREIGHT_TRUCK_ID);
		truckVehicleType.setCapacity(defaultCapacity);
		// truckVehicleType.setMaximumVelocity(22.22);
		scenario.getVehicles().addVehicleType(truckVehicleType);

		VehicleType truckAvVehicleType = VehicleUtils.getFactory() //
				.createVehicleType(FREIGHT_TRUCK_AV_ID);
		truckAvVehicleType.setCapacity(defaultCapacity);
		// truckAvVehicleType.setMaximumVelocity(22.22);
		scenario.getVehicles().addVehicleType(truckAvVehicleType);

		for (Person person : scenario.getPopulation().getPersons().values()) {
			Id<Vehicle> vehicleId = Id.createVehicleId(person.getId().toString() + "_car_passenger");
			Vehicle vehicle = scenario.getVehicles().getFactory().createVehicle(vehicleId, passengerVehicleType);
			scenario.getVehicles().addVehicle(vehicle);
		}
	}
}

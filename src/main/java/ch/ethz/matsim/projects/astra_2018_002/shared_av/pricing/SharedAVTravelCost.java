package ch.ethz.matsim.projects.astra_2018_002.shared_av.pricing;

public interface SharedAVTravelCost {
	public double getTravelCost(double distance_km, boolean isPooled);
}

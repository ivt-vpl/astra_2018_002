package ch.ethz.matsim.projects.astra_2018_002.mode_choice;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.matsim.api.core.v01.TransportMode;
import org.matsim.api.core.v01.population.Person;

import ch.ethz.matsim.discrete_mode_choice.model.DiscreteModeChoiceTrip;
import ch.ethz.matsim.discrete_mode_choice.model.mode_availability.CarModeAvailability;
import ch.ethz.matsim.projects.astra_2018_002.private_av.AssignPrivatAVs;

public class SwissModeAvailability extends CarModeAvailability {
	public SwissModeAvailability(Collection<String> modes) {
		super(modes);
	}

	@Override
	public Collection<String> getAvailableModes(Person person, List<DiscreteModeChoiceTrip> trips) {
		Boolean isFreight = (Boolean) person.getAttributes().getAttribute("isFreight");

		if (isFreight != null && isFreight) {
			Boolean freightAv = (Boolean) person.getAttributes().getAttribute("freight_av");

			if (freightAv != null && freightAv) {
				return Collections.singleton("truckAv");
			} else {
				return Collections.singleton("truck");
			}
		}

		boolean bikeAvailability = !"FOR_NONE".equals((String) person.getAttributes().getAttribute("bikeAvailability"));

		Set<String> modes = new HashSet<>();

		if (!bikeAvailability) {
			modes.addAll(super.getAvailableModes(person, trips).stream().filter(m -> !TransportMode.bike.equals(m))
					.collect(Collectors.toSet()));
		} else {
			modes.addAll(super.getAvailableModes(person, trips));
		}

		String pravAvailability = (String) person.getAttributes()
				.getAttribute(AssignPrivatAVs.AV_AVAILABILITY_ATTRIBUTE);

		if (pravAvailability != null && !pravAvailability.equals("noPrav")) {
			modes.add(pravAvailability);
		}

		Boolean outside = (Boolean) person.getAttributes().getAttribute("outside");

		if (outside != null && outside) {
			modes.add("outside");
		}

		Boolean isCarPassenger = (Boolean) person.getAttributes().getAttribute("isCarPassenger");

		if (isCarPassenger != null && isCarPassenger) {
			modes.add("car_passenger");
		}

		return modes;
	}
}

package ch.ethz.matsim.projects.astra_2018_002.analysis.trips;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;

import org.matsim.api.core.v01.network.Network;
import org.matsim.core.network.NetworkUtils;
import org.matsim.core.network.io.MatsimNetworkReader;
import org.matsim.core.router.MainModeIdentifier;
import org.matsim.core.router.MainModeIdentifierImpl;
import org.matsim.core.router.StageActivityTypes;
import org.matsim.core.router.StageActivityTypesImpl;
import org.matsim.pt.PtConstants;

import ch.ethz.matsim.baseline_scenario.analysis.trips.CSVTripWriter;
import ch.ethz.matsim.baseline_scenario.analysis.trips.TripItem;
import ch.ethz.matsim.baseline_scenario.analysis.trips.listeners.TripListener;
import ch.ethz.matsim.baseline_scenario.analysis.trips.readers.EventsTripReader;
import ch.ethz.matsim.baseline_scenario.analysis.trips.utils.BaselineHomeActivityTypes;
import ch.ethz.matsim.baseline_scenario.analysis.trips.utils.HomeActivityTypes;
import ch.ethz.matsim.baseline_scenario.config.CommandLine;
import ch.ethz.matsim.baseline_scenario.config.CommandLine.ConfigurationException;
import ch.ethz.matsim.projects.astra_2018_002.shared_av.routing.ASTRAAVRoutingModule;

public class ConvertTripsFromEvents {
	static public void main(String[] args) throws IOException, ConfigurationException {
		CommandLine cmd = new CommandLine.Builder(args) //
				.requireOptions("events-path", "network-path", "output-path") //
				.allowOptions("network-modes", "stage-activity-types") //
				.build();

		Network network = NetworkUtils.createNetwork();
		new MatsimNetworkReader(network).readFile(cmd.getOptionStrict("network-path"));

		StageActivityTypes stageActivityTypes = new StageActivityTypesImpl(
				Arrays.asList(PtConstants.TRANSIT_ACTIVITY_TYPE, ASTRAAVRoutingModule.AV_INTERACTION_ACTIVITY_TYPE));

		if (cmd.hasOption("stage-activity-types")) {
			Collection<String> raw = Arrays.asList(cmd.getOptionStrict("stage-activity-types").split(","));
			stageActivityTypes = new StageActivityTypesImpl(raw);
		}

		Collection<String> networkModes = Arrays.asList("car", "prav3", "prav4", "prav5", "av", "truck", "truckAv");

		if (cmd.hasOption("network-modes")) {
			networkModes = Arrays.asList(cmd.getOptionStrict("network-modes").split(","));
		}

		HomeActivityTypes homeActivityTypes = new BaselineHomeActivityTypes();
		MainModeIdentifier mainModeIdentifier = new MainModeIdentifierImpl();

		TripListener tripListener = new ASTRATripListener(network, stageActivityTypes, homeActivityTypes,
				mainModeIdentifier, networkModes);
		Collection<TripItem> trips = new EventsTripReader(tripListener).readTrips(cmd.getOptionStrict("events-path"));

		new CSVTripWriter(trips).write(cmd.getOptionStrict("output-path"));
	}
}

package ch.ethz.matsim.projects.astra_2018_002;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import org.matsim.api.core.v01.Scenario;
import org.matsim.api.core.v01.network.Link;
import org.matsim.core.config.Config;
import org.matsim.core.config.groups.ControlerConfigGroup.RoutingAlgorithmType;

public class ASTRAConfigurator {
	static public void configure(Config config) {
		config.controler().setRoutingAlgorithmType(RoutingAlgorithmType.FastAStarLandmarks);

		Collection<String> mainModes = new ArrayList<>(config.qsim().getMainModes());
		mainModes.addAll(Arrays.asList("truck", "truckAv"));
		config.qsim().setMainModes(mainModes);
		
		Set<String> networkModes = new HashSet<>(config.plansCalcRoute().getNetworkModes());
		networkModes.addAll(Arrays.asList("truck", "truckAv"));
		config.plansCalcRoute().setNetworkModes(networkModes);
		
		Set<String> analyzedModes = new HashSet<>(
				Arrays.asList(config.travelTimeCalculator().getAnalyzedModes().split(",")).stream()
						.map(String::trim).collect(Collectors.toSet()));
		analyzedModes.addAll(Arrays.asList("truck", "truckAv"));
		config.travelTimeCalculator().setAnalyzedModes(String.join(",", analyzedModes));
		config.travelTimeCalculator().setSeparateModes(false);
	}

	static public void updateNetwork(Scenario scenario, int year) {
		for (Link link : scenario.getNetwork().getLinks().values()) {
			if (link.getAllowedModes().contains("car")) {
				Set<String> allowedModes = new HashSet<>(link.getAllowedModes());
				allowedModes.addAll(Arrays.asList("truck", "car_passenger", "truckAv"));
				link.setAllowedModes(allowedModes);
			}
		}

		if (year >= 2030) {
			for (Link link : scenario.getNetwork().getLinks().values()) {
				if (link.getAllowedModes().contains("prav3")) {
					return;
				}
			}

			throw new IllegalStateException("Cannot find 'prav3' in network modes. Are you using the wrong network?");
		}
	}
}

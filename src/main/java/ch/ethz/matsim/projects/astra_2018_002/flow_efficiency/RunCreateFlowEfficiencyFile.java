package ch.ethz.matsim.projects.astra_2018_002.flow_efficiency;

import java.io.File;
import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

public class RunCreateFlowEfficiencyFile {
	static public void main(String[] args) throws JsonGenerationException, JsonMappingException, IOException {
		File path = new File(args[0]);
		
		ObjectMapper mapper = new ObjectMapper();
		mapper.enable(SerializationFeature.INDENT_OUTPUT);
		mapper.writeValue(path, new FlowEfficiencyDefinition());
	}
}

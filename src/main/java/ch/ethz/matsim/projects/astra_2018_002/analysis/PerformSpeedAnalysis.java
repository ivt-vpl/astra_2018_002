package ch.ethz.matsim.projects.astra_2018_002.analysis;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.events.LinkEnterEvent;
import org.matsim.api.core.v01.events.LinkLeaveEvent;
import org.matsim.api.core.v01.events.VehicleEntersTrafficEvent;
import org.matsim.api.core.v01.events.VehicleLeavesTrafficEvent;
import org.matsim.api.core.v01.events.handler.LinkEnterEventHandler;
import org.matsim.api.core.v01.events.handler.LinkLeaveEventHandler;
import org.matsim.api.core.v01.events.handler.VehicleEntersTrafficEventHandler;
import org.matsim.api.core.v01.events.handler.VehicleLeavesTrafficEventHandler;
import org.matsim.api.core.v01.network.Link;
import org.matsim.api.core.v01.network.Network;
import org.matsim.core.api.experimental.events.EventsManager;
import org.matsim.core.events.EventsUtils;
import org.matsim.core.events.MatsimEventsReader;
import org.matsim.core.network.NetworkUtils;
import org.matsim.core.network.io.MatsimNetworkReader;
import org.matsim.vehicles.Vehicle;

import ch.ethz.matsim.baseline_scenario.config.CommandLine;
import ch.ethz.matsim.baseline_scenario.config.CommandLine.ConfigurationException;

public class PerformSpeedAnalysis implements LinkEnterEventHandler, LinkLeaveEventHandler,
		VehicleEntersTrafficEventHandler, VehicleLeavesTrafficEventHandler {
	private final Network network;
	private final double startTime;
	private final double interval;
	private final int numberOfBins;
	private final List<String> roadTypes;

	private final Map<String, List<List<Double>>> speeds = new HashMap<>();
	private final Map<Id<Vehicle>, Double> enterTimes = new HashMap<>();

	public PerformSpeedAnalysis(Network network, List<String> roadTypes, double startTime, double endTime,
			double interval) {
		this.network = network;
		this.startTime = startTime;
		this.interval = interval;
		this.roadTypes = roadTypes;

		this.numberOfBins = (int) Math.ceil((endTime - startTime) / interval);

		for (String roadType : roadTypes) {
			List<List<Double>> bin = new ArrayList<>(numberOfBins);

			for (int i = 0; i < numberOfBins; i++) {
				bin.add(new LinkedList<>());
			}

			speeds.put(roadType, bin);
		}
	}

	@Override
	public void handleEvent(LinkEnterEvent event) {
		Link link = network.getLinks().get(event.getLinkId());
		String roadType = (String) link.getAttributes().getAttribute("osm:way:highway");

		if (roadTypes.contains(roadType)) {
			enterTimes.put(event.getVehicleId(), event.getTime());
		}
	}

	@Override
	public void handleEvent(LinkLeaveEvent event) {
		Link link = network.getLinks().get(event.getLinkId());
		String roadType = (String) link.getAttributes().getAttribute("osm:way:highway");

		if (roadTypes.contains(roadType)) {
			Double enterTime = enterTimes.remove(event.getVehicleId());

			if (enterTime != null) {
				double traversalTime = event.getTime() - enterTime;
				double speed = link.getLength() / traversalTime;
				int index = Math.min(Math.max(0, (int) Math.floor((enterTime - startTime) / interval)),
						numberOfBins - 1);

				speeds.get(roadType).get(index).add(speed);
			}
		}
	}

	@Override
	public void handleEvent(VehicleLeavesTrafficEvent event) {
		enterTimes.remove(event.getVehicleId());
	}

	@Override
	public void handleEvent(VehicleEntersTrafficEvent event) {
		enterTimes.remove(event.getVehicleId());
	}

	static public void main(String[] args) throws ConfigurationException, IOException {
		CommandLine cmd = new CommandLine.Builder(args) //
				.requireOptions("network-path", "events-path", "output-path") //
				.build();

		String networkPath = cmd.getOptionStrict("network-path");
		String eventsPath = cmd.getOptionStrict("events-path");
		String outputPath = cmd.getOptionStrict("output-path");

		double startTime = 0.0 * 3600.0;
		double endTime = 30.0 * 3600.0;
		double interval = 3600.0;

		List<String> roadTypes = Arrays.asList("motorway", "trunk", "primary", "secondary", "tertiary", "residential");

		Network network = NetworkUtils.createNetwork();
		new MatsimNetworkReader(network).readFile(networkPath);

		PerformSpeedAnalysis analysis = new PerformSpeedAnalysis(network, roadTypes, startTime, endTime, interval);

		EventsManager eventsManager = EventsUtils.createEventsManager();
		eventsManager.addHandler(analysis);
		new MatsimEventsReader(eventsManager).readFile(eventsPath);

		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(new File(outputPath))));

		writer.write(
				String.join(";", new String[] { "road_type", "start_time", "end_time", "speed_mean", "speed_median" })
						+ "\n");

		for (String roadType : roadTypes) {
			List<List<Double>> roadTypeBin = analysis.speeds.get(roadType);

			for (int i = 0; i < analysis.numberOfBins; i++) {
				double binStartTime = startTime + i * interval;
				double binEndTime = startTime + (i + 1) * interval;

				DescriptiveStatistics stats = new DescriptiveStatistics();
				roadTypeBin.get(i).forEach(stats::addValue);

				double mean = stats.getMean();
				double median = stats.getPercentile(50.0);

				writer.write(String.join(";", new String[] { roadType, String.valueOf(binStartTime),
						String.valueOf(binEndTime), String.valueOf(mean), String.valueOf(median) }) + "\n");
			}
		}

		writer.close();
	}
}

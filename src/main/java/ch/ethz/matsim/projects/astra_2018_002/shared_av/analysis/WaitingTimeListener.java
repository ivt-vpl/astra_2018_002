package ch.ethz.matsim.projects.astra_2018_002.shared_av.analysis;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.List;
import java.util.Optional;

import org.matsim.api.core.v01.network.Link;
import org.matsim.core.controler.OutputDirectoryHierarchy;
import org.matsim.core.controler.events.IterationEndsEvent;
import org.matsim.core.controler.listener.IterationEndsListener;
import org.matsim.core.router.LinkWrapperFacility;
import org.matsim.core.utils.misc.Time;
import org.matsim.facilities.Facility;

import ch.ethz.matsim.projects.astra_2018_002.shared_av.waiting_time.AVWaitingTime;
import ch.ethz.matsim.projects.astra_2018_002.shared_av.waiting_time.zonal.WaitingTimeZone;
import ch.ethz.matsim.projects.astra_2018_002.shared_av.waiting_time.zonal.ZonalWaitingTime;

public class WaitingTimeListener implements IterationEndsListener {
	private final static String FILE_NAME = "waiting_times.csv";

	private final List<WaitingTimeZone> waitingTimeZones;
	private final AVWaitingTime waitingTime;
	private final OutputDirectoryHierarchy outputHierarchy;

	public WaitingTimeListener(AVWaitingTime waitingTime, List<WaitingTimeZone> waitingTimeZones,
			OutputDirectoryHierarchy outputHierarchy) {
		this.waitingTime = waitingTime;
		this.waitingTimeZones = waitingTimeZones;
		this.outputHierarchy = outputHierarchy;
	}

	@Override
	public void notifyIterationEnds(IterationEndsEvent event) {
		double interval = 3600.0;
		double startTime = 5.0 * 3600 + 1800.0;
		double endTime = 22.0 * 3600.0;

		double time = startTime;

		try {
			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(outputHierarchy.getIterationFilename(event.getIteration(), FILE_NAME))));

			writer.write(String.join(";", new String[] { "time", "zone", "taxi_estimate", "pool_estimate",
					"taxi_observations", "pool_observations" }) + "\n");
			writer.flush();

			while (time < endTime) {
				for (WaitingTimeZone zone : waitingTimeZones) {
					Optional<Link> testLink = zone.getRepresentativeLink();

					if (testLink.isPresent()) {
						Facility<?> facility = new LinkWrapperFacility(testLink.get());

						double taxiEstimate = waitingTime.getWaitingTime(facility, time, false);
						double poolEstimate = waitingTime.getWaitingTime(facility, time, true);

						int taxiObservations = ((ZonalWaitingTime) waitingTime).getNumberOfObservations(facility, time,
								false);
						int poolObservations = ((ZonalWaitingTime) waitingTime).getNumberOfObservations(facility, time,
								true);

						writer.write(String.join(";",
								new String[] { Time.writeTime(time), zone.getIdentifier(), String.valueOf(taxiEstimate),
										String.valueOf(poolEstimate), String.valueOf(taxiObservations),
										String.valueOf(poolObservations) })
								+ "\n");
					}
				}

				time += interval;
			}

			writer.close();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}

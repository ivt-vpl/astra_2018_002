package ch.ethz.matsim.projects.astra_2018_002.mode_choice.cost;

public class CostParameters {
	public double carCostPerKm_CHF = 0.26;
	public double privateAvCostPerKm_CHF = 0.26;
	
	public double pooledAvCostPerKm_CHF = 0.4;
	public double taxiAvCostPerKm_CHF = 0.4;

	public double ptCostPerKm_CHF = 0.6;
	public double ptCostMinimum_CHF = 2.7;

	public double ptRegionalRadius_km = 15.0;
	
	public double ptRailReduction = 0.2;
	public double ptNonRailReducation = 0.4;
}

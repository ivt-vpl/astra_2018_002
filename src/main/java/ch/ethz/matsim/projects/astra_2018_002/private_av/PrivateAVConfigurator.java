package ch.ethz.matsim.projects.astra_2018_002.private_av;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.Scenario;
import org.matsim.core.config.Config;
import org.matsim.core.config.groups.PlanCalcScoreConfigGroup.ModeParams;
import org.matsim.vehicles.VehicleType;

import ch.ethz.matsim.discrete_mode_choice.modules.config.DiscreteModeChoiceConfigGroup;

public class PrivateAVConfigurator {
	static final public Id<VehicleType> PRAV3_ID = Id.create("prav3", VehicleType.class);
	static final public Id<VehicleType> PRAV4_ID = Id.create("prav4", VehicleType.class);
	static final public Id<VehicleType> PRAV5_ID = Id.create("prav5", VehicleType.class);

	static final public String PRAV_3 = "prav3";
	static final public String PRAV_4 = "prav4";
	static final public String PRAV_5 = "prav5";

	static final public Collection<String> PRAV_MODES = Arrays.asList(PRAV_3, PRAV_4, PRAV_5);

	static public void configure(Config config, int year) {
		PrivateAVConfigGroup privateConfig = (PrivateAVConfigGroup) config.getModules()
				.get(PrivateAVConfigGroup.GROUP_NAME);

		if (privateConfig == null) {
			privateConfig = new PrivateAVConfigGroup();
			config.addModule(privateConfig);
		}

		if (privateConfig.getUsePrivateAVs()) {
			DiscreteModeChoiceConfigGroup dmcConfig = (DiscreteModeChoiceConfigGroup) config.getModules()
					.get(DiscreteModeChoiceConfigGroup.GROUP_NAME);

			Set<String> cachedModes = new HashSet<>(dmcConfig.getCachedModes());
			cachedModes.addAll(PRAV_MODES);
			dmcConfig.setCachedModes(cachedModes);

			Set<String> restrictedModes = new HashSet<>(
					dmcConfig.getVehicleTourConstraintConfig().getRestrictedModes());
			restrictedModes.addAll(PRAV_MODES);
			dmcConfig.getVehicleTourConstraintConfig().setRestrictedModes(restrictedModes);

			Set<String> mainModes = new HashSet<>(config.qsim().getMainModes());
			mainModes.addAll(PRAV_MODES);
			config.qsim().setMainModes(mainModes);

			Set<String> networkModes = new HashSet<>(config.plansCalcRoute().getNetworkModes());
			networkModes.addAll(PRAV_MODES);
			config.plansCalcRoute().setNetworkModes(networkModes);

			Set<String> analyzedModes = new HashSet<>(
					Arrays.asList(config.travelTimeCalculator().getAnalyzedModes().split(",")).stream()
							.map(String::trim).collect(Collectors.toSet()));
			analyzedModes.addAll(PRAV_MODES);
			config.travelTimeCalculator().setAnalyzedModes(String.join(",", analyzedModes));
			config.travelTimeCalculator().setSeparateModes(false);

			for (String mode : PRAV_MODES) {
				ModeParams params = new ModeParams(mode);
				config.planCalcScore().addModeParams(params);
			}
		}
	}

	static public void assignPrivateAVs(Scenario scenario, int year, String ASTRA_Scenario) {
		PrivateAVConfigGroup privateConfig = (PrivateAVConfigGroup) scenario.getConfig().getModules()
				.get(PrivateAVConfigGroup.GROUP_NAME);

		if (privateConfig.getAssignPrivateAVs()) {
			if (!privateConfig.getUsePrivateAVs()) {
				throw new IllegalStateException("No point of assigning private AVs if usePrivateAVs = false");
			}

			AssignPrivatAVs.main(scenario, year, ASTRA_Scenario);
		}
	}
}

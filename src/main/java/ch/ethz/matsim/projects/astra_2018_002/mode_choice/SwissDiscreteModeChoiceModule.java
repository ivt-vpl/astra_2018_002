package ch.ethz.matsim.projects.astra_2018_002.mode_choice;

import java.io.File;
import java.io.IOException;

import org.apache.log4j.Logger;
import org.matsim.api.core.v01.network.Network;
import org.matsim.core.router.TripRouter;
import org.matsim.core.router.util.TravelTime;
import org.matsim.facilities.ActivityFacilities;
import org.matsim.pt.transitSchedule.api.TransitSchedule;

import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

import ch.ethz.matsim.baseline_scenario.config.CommandLine;
import ch.ethz.matsim.baseline_scenario.config.CommandLine.ConfigurationException;
import ch.ethz.matsim.discrete_mode_choice.components.utils.home_finder.HomeFinder;
import ch.ethz.matsim.discrete_mode_choice.modules.AbstractDiscreteModeChoiceExtension;
import ch.ethz.matsim.discrete_mode_choice.modules.config.DiscreteModeChoiceConfigGroup;
import ch.ethz.matsim.discrete_mode_choice.modules.config.ModeAvailabilityConfigGroup;
import ch.ethz.matsim.projects.astra_2018_002.RunASTRA2018002;
import ch.ethz.matsim.projects.astra_2018_002.mode_choice.SwissUtilityParameters.UtilitySet;
import ch.ethz.matsim.projects.astra_2018_002.mode_choice.cost.CostModel;
import ch.ethz.matsim.projects.astra_2018_002.mode_choice.cost.CostParameters;
import ch.ethz.matsim.projects.astra_2018_002.mode_choice.outside.OutsideFilter;
import ch.ethz.matsim.projects.astra_2018_002.mode_choice.outside.OutsideTripConstraint;
import ch.ethz.matsim.projects.astra_2018_002.mode_choice.outside.SwissHomeFinder;
import ch.ethz.matsim.projects.astra_2018_002.mode_choice.outside.SwissTourFinder;
import ch.ethz.matsim.projects.astra_2018_002.shared_av.pricing.DefaultSharedAVTravelCost;
import ch.ethz.matsim.projects.astra_2018_002.shared_av.pricing.SharedAVTravelCost;

public class SwissDiscreteModeChoiceModule extends AbstractDiscreteModeChoiceExtension {
	private final static Logger logger = Logger.getLogger(SwissDiscreteModeChoiceModule.class);
	public final static String COMMAND_LINE_PREFIX = "utility";
	public static final String SWISS_ESTIMATOR_NAME = "Swiss";
	public static final String SWISS_MODE_AVAILABILITY_NAME = "SwissModeAvailability";
	public static final String SWISS_TOUR_FINDER_NAME = "SwissTourFinder";
	public static final String OUTSIDE_TRIP_CONSTRAINT_NAME = "OutsideTripConstraint";
	public static final String PASSENGER_CONSTRAINT_NAME = "PassengerConstraint";
	public static final String OUTSIDE_FILTER = "OutsideFilter";

	private final UtilitySet utilitySet;
	private final boolean useRouteChoice;
	private final boolean onlySignificant;

	private final CommandLine cmd;
	private final int year;

	public SwissDiscreteModeChoiceModule(UtilitySet utilitySet, boolean useRouteChoice, boolean onlySignificant,
			CommandLine cmd, int year) {
		this.utilitySet = utilitySet;
		this.useRouteChoice = useRouteChoice;
		this.onlySignificant = onlySignificant;
		this.cmd = cmd;
		this.year = year;
	}

	@Override
	protected void installExtension() {
		bindTripEstimator(SWISS_ESTIMATOR_NAME).to(SwissUtilityEstimator.class);
		bindModeAvailability(SWISS_MODE_AVAILABILITY_NAME).to(SwissModeAvailability.class);
		bindTourFinder(SWISS_TOUR_FINDER_NAME).to(SwissTourFinder.class);
		bindTripConstraintFactory(OUTSIDE_TRIP_CONSTRAINT_NAME).to(OutsideTripConstraint.Factory.class);
		bindTripConstraintFactory(PASSENGER_CONSTRAINT_NAME).to(PassengerConstraint.Factory.class);
		bindTourFilter(OUTSIDE_FILTER).to(OutsideFilter.class);
	}

	@Provides
	public SwissUtilityEstimator provideSwissUtilityEstimator(TripRouter tripRouter, Network network,
			ActivityFacilities facilities, CostModel costModel, TransitSchedule transitSchedule,
			SwissUtilityParameters parameters, @Named("car") TravelTime travelTime) {
		return new SwissUtilityEstimator(tripRouter, network, facilities, costModel, transitSchedule, parameters,
				travelTime);
	}

	@Provides
	@Singleton
	public SwissUtilityParameters provideSwissUtilityParameters() throws IOException, ConfigurationException {
		SwissUtilityParameters parameters = SwissUtilityParameters.build(utilitySet, useRouteChoice, onlySignificant);

		if (cmd.hasOption("utility-parameters")) {
			File path = new File(cmd.getOptionStrict("utility-parameters"));
			SwissUtilityParameters.adaptFromFile(parameters, path);
		}

		SwissUtilityParameters.adaptFromCommandLine(parameters, cmd);
		return parameters;
	}

	@Singleton
	@Provides
	public SharedAVTravelCost provideTravelCost(CostParameters costParameters) {
		return new DefaultSharedAVTravelCost(costParameters);
	}

	@Provides
	@Singleton
	public CostModel provideCostModel(SwissUtilityParameters parameters, SharedAVTravelCost sharedCost) {
		System.out.println("HEY THE MODULE PROVIDECOSTMODEL IS BEING CALLED!");
		// the next line has been changed on 07/11/2019 because I am trying to be able to set "usePtCostReduction" from the command line
		boolean usePtCostReduction = Boolean.parseBoolean(this.cmd.getOption("use-pt-cost-reduction").orElse("false"));
		System.out.println("usePtCostReduction is equal to " + usePtCostReduction);
		logger.info(String.format("usePtCostReduction = %s", usePtCostReduction));
		return new CostModel(parameters.costs, sharedCost, year, usePtCostReduction);
	}

	@Provides
	@Singleton
	public CostParameters provideCostParameters(SwissUtilityParameters parameters) {
		return parameters.costs;
	}

	@Provides
	public SwissModeAvailability provideSwissModeAvailability(DiscreteModeChoiceConfigGroup dmcConfig) {
		ModeAvailabilityConfigGroup maConfig = dmcConfig.getCarModeAvailabilityConfig();
		return new SwissModeAvailability(maConfig.getAvailableModes());
	}

	@Provides
	public SwissTourFinder provideSwissTourFinder() {
		return new SwissTourFinder();
	}

	@Provides
	@Singleton
	public OutsideTripConstraint.Factory provideOutsideConstraintFactory() {
		return new OutsideTripConstraint.Factory();
	}

	@Provides
	@Singleton
	@Named("tour")
	public HomeFinder provideHomeFinder() {
		return new SwissHomeFinder();
	}

	@Provides
	@Singleton
	public PassengerConstraint.Factory providePassengerConstraintFactory() {
		return new PassengerConstraint.Factory();
	}

	@Provides
	public OutsideFilter provideOutsideFilter() {
		return new OutsideFilter();
	}
}

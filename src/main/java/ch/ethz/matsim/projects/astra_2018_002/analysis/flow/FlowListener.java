package ch.ethz.matsim.projects.astra_2018_002.analysis.flow;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.events.LinkEnterEvent;
import org.matsim.api.core.v01.events.handler.LinkEnterEventHandler;
import org.matsim.api.core.v01.network.Link;
import org.matsim.vehicles.Vehicle;
import org.matsim.vehicles.Vehicles;

import ch.ethz.matsim.projects.astra_2018_002.flow_efficiency.FlowEfficiencyCalculator;
import ch.ethz.matsim.projects.astra_2018_002.flow_efficiency.FlowEfficiencyConfigurator;
import ch.ethz.matsim.projects.astra_2018_002.shared_av.SharedAVModule;

public class FlowListener implements LinkEnterEventHandler {
	private final double startTime;
	private final double interval;

	private final int numberOfLinks;
	private final int numberOfBins;

	private final CountItem[][] timeItems;
	private final CountItem[] totalItems;

	private final Map<Id<Link>, Integer> linkMap = new HashMap<>();
	private final List<Link> links;
	private final Set<Id<Link>> linkIds;

	private final FlowEfficiencyCalculator flowEfficiencyCalculator;

	private final Vehicle CONVENTIONAL;
	private final Vehicle PRAV3;
	private final Vehicle PRAV4;
	private final Vehicle PRAV5;
	private final Vehicle TRUCK;
	private final Vehicle TRUCK_AV;

	public FlowListener(Collection<Link> links, double startTime, double endTime, double interval,
			FlowEfficiencyCalculator flowEfficiencyCalculator, Vehicles vehicles) {
		this.startTime = startTime;
		this.interval = interval;

		this.numberOfBins = (int) Math.ceil((endTime - startTime) / interval);
		this.numberOfLinks = links.size();

		this.links = links.stream().collect(Collectors.toList());
		this.linkIds = links.stream().map(Link::getId).collect(Collectors.toSet());

		for (int index = 0; index < links.size(); index++) {
			linkMap.put(this.links.get(index).getId(), index);
		}

		this.totalItems = new CountItem[numberOfLinks];
		this.timeItems = new CountItem[numberOfLinks][numberOfBins];
		this.flowEfficiencyCalculator = flowEfficiencyCalculator;

		for (int linkIndex = 0; linkIndex < numberOfLinks; linkIndex++) {
			totalItems[linkIndex] = new CountItem();

			for (int timeIndex = 0; timeIndex < numberOfBins; timeIndex++) {
				timeItems[linkIndex][timeIndex] = new CountItem();
			}
		}

		this.CONVENTIONAL = vehicles.getFactory().createVehicle(Id.createVehicleId("conventional"),
				vehicles.getVehicleTypes().get(FlowEfficiencyConfigurator.CONVENTIONAL_VEHICLE_TYPE_ID));
		this.PRAV3 = vehicles.getFactory().createVehicle(Id.createVehicleId("prav3"),
				vehicles.getVehicleTypes().get(FlowEfficiencyConfigurator.PRAV3_ID));
		this.PRAV4 = vehicles.getFactory().createVehicle(Id.createVehicleId("prav4"),
				vehicles.getVehicleTypes().get(FlowEfficiencyConfigurator.PRAV4_ID));
		this.PRAV5 = vehicles.getFactory().createVehicle(Id.createVehicleId("prav5"),
				vehicles.getVehicleTypes().get(FlowEfficiencyConfigurator.PRAV5_ID));
		this.TRUCK = vehicles.getFactory().createVehicle(Id.createVehicleId("truck"),
				vehicles.getVehicleTypes().get(FlowEfficiencyConfigurator.FREIGHT_TRUCK_ID));
		this.TRUCK_AV = vehicles.getFactory().createVehicle(Id.createVehicleId("truckAv"),
				vehicles.getVehicleTypes().get(FlowEfficiencyConfigurator.FREIGHT_TRUCK_AV_ID));
	}

	public class CountItem {
		// By function
		public int privateVehicles;
		public int pooledVehicles;
		public int taxiVehicles;
		public int truckVehicles;

		// By automation
		public int conventionalVehicles;
		public int level5Vehicles;
		public int level4Vehicles;
		public int level3Vehicles;
		public int automatedTrucks;

		// PCU
		public double pcusVehicles;
		public double pcusTrucks;
		public double pcusAll;
	}

	private AnalysisVehicle getAnaysisVehicle(Id<Vehicle> vehicleId) {
		String rawVehicleId = vehicleId.toString();
		AnalysisVehicle vehicle = new AnalysisVehicle();

		if (rawVehicleId.contains("prav3")) {
			vehicle.automationLevel = 3;
		} else if (rawVehicleId.contains("prav4")) {
			vehicle.automationLevel = 4;
		} else if (rawVehicleId.contains("prav5")) {
			vehicle.automationLevel = 5;
		} else if (rawVehicleId.contains(SharedAVModule.AV_TAXI)) {
			vehicle.automationLevel = 5;
			vehicle.isShared = true;
		} else if (rawVehicleId.contains(SharedAVModule.AV_POOL)) {
			vehicle.automationLevel = 5;
			vehicle.isShared = true;
			vehicle.isPooled = true;
		} else if (rawVehicleId.contains("truckAv")) {
			vehicle.isTruck = true;
			vehicle.automationLevel = 5;
		} else if (rawVehicleId.contains("truck")) {
			vehicle.isTruck = true;
		}

		return vehicle;
	}

	private Vehicle getAutomationLevelVehicle(int automationLevel, boolean isTruck) {
		if (isTruck) {
			if (automationLevel == 5) {
				return TRUCK_AV;
			} else {
				return TRUCK;
			}
		} else {
			switch (automationLevel) {
			case 3:
				return PRAV3;
			case 4:
				return PRAV4;
			case 5:
				return PRAV5;
			default:
				return CONVENTIONAL;
			}
		}
	}

	private void modifyCountItem(CountItem item, AnalysisVehicle analysisVehicle, Vehicle vehicle, Link link) {
		double flowEfficiency = flowEfficiencyCalculator.calculateFlowEfficiency(vehicle, link);

		if (!analysisVehicle.isTruck) {
			switch (analysisVehicle.automationLevel) {
			case 3:
				item.level3Vehicles++;
				break;
			case 4:
				item.level4Vehicles++;
				break;
			case 5:
				item.level5Vehicles++;
				break;
			default:
				item.conventionalVehicles++;
				break;
			}

			if (analysisVehicle.isShared) {
				if (analysisVehicle.isPooled) {
					item.pooledVehicles++;
				} else {
					item.taxiVehicles++;
				}
			} else {
				item.privateVehicles++;
			}

			item.pcusVehicles += 1.0 / flowEfficiency;
		} else {
			item.truckVehicles++;

			if (analysisVehicle.automationLevel == 5) {
				item.automatedTrucks++;
			}

			item.pcusTrucks += 1.0 / flowEfficiency;
		}

		item.pcusAll += 1.0 / flowEfficiency;
	}

	@Override
	public void handleEvent(LinkEnterEvent event) {
		if (linkIds.contains(event.getLinkId())) {
			int linkIndex = linkMap.get(event.getLinkId());
			Link link = links.get(linkIndex);

			AnalysisVehicle analysisVehicle = getAnaysisVehicle(event.getVehicleId());
			Vehicle vehicle = getAutomationLevelVehicle(analysisVehicle.automationLevel, analysisVehicle.isTruck);

			CountItem totalItem = totalItems[linkIndex];
			modifyCountItem(totalItem, analysisVehicle, vehicle, link);

			int timeIndex = (int) Math.floor((event.getTime() - startTime) / interval);

			if (timeIndex > -1 && timeIndex < numberOfBins) {
				CountItem timeItem = timeItems[linkIndex][timeIndex];
				modifyCountItem(timeItem, analysisVehicle, vehicle, link);
			}
		}
	}

	public int getNumberOfLinks() {
		return numberOfLinks;
	}

	public int getNumberOfBins() {
		return numberOfBins;
	}

	public CountItem getTotalItem(Id<Link> linkId) {
		int linkIndex = linkMap.get(linkId);

		if (linkIndex == -1) {
			throw new IllegalStateException();
		}

		return totalItems[linkIndex];
	}

	public CountItem[] getTimeItems(Id<Link> linkId) {
		int linkIndex = linkMap.get(linkId);

		if (linkIndex == -1) {
			throw new IllegalStateException();
		}

		return timeItems[linkIndex];
	}
}

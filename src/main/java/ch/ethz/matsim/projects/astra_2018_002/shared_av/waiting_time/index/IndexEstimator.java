package ch.ethz.matsim.projects.astra_2018_002.shared_av.waiting_time.index;

public interface IndexEstimator {
	void record(int itemIndex, int timeIndex, double value);

	double estimate(int itemIndex, int timeIndex);
	
	int getNumberOfObservations(int itemIndex, int timeIndex);

	void finish();
}

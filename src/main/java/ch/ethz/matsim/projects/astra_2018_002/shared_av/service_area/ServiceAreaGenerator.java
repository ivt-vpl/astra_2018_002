package ch.ethz.matsim.projects.astra_2018_002.shared_av.service_area;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;

import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.network.Link;
import org.matsim.api.core.v01.network.Network;
import org.matsim.api.core.v01.population.Activity;
import org.matsim.api.core.v01.population.Person;
import org.matsim.api.core.v01.population.Plan;
import org.matsim.api.core.v01.population.PlanElement;
import org.matsim.api.core.v01.population.Population;
import org.matsim.contrib.dvrp.data.Vehicle;
import org.matsim.core.config.groups.GlobalConfigGroup;
import org.matsim.core.utils.misc.Counter;

import com.google.inject.Inject;
import com.google.inject.name.Named;

import ch.ethz.matsim.av.config.AVGeneratorConfig;
import ch.ethz.matsim.av.data.AVVehicle;
import ch.ethz.matsim.av.framework.AVModule;
import ch.ethz.matsim.av.generator.AVGenerator;
import ch.ethz.matsim.projects.astra_2018_002.shared_av.SharedAVConfigGroup;
import ch.ethz.matsim.projects.astra_2018_002.shared_av.SharedAVModule;

public class ServiceAreaGenerator implements AVGenerator {
	private final String prefix;
	private final List<Link> links;
	private final int numberOfVehicles;
	private final Random random;
	private final double[] cdf;

	private int numberOfGeneratedVehicles = 0;

	public ServiceAreaGenerator(String prefix, int numberOfVehicles, List<Link> links, double[] cdf, Random random) {
		this.links = links;
		this.numberOfVehicles = numberOfVehicles;
		this.prefix = prefix;
		this.cdf = cdf;
		this.random = random;
	}

	@Override
	public boolean hasNext() {
		return numberOfGeneratedVehicles < numberOfVehicles;
	}

	@Override
	public AVVehicle next() {
		numberOfGeneratedVehicles++;

		double r = random.nextDouble();
		int selector = 0;

		while (r > cdf[selector]) {
			selector++;
		}

		Link startLink = links.get(selector);
		Id<Vehicle> vehicleId = Id.create(prefix + "_" + String.valueOf(numberOfGeneratedVehicles), Vehicle.class);

		return new AVVehicle(vehicleId, startLink, (double) SharedAVModule.POOLED_NUMBER_OF_SEATS, 0.0,
				Double.POSITIVE_INFINITY);
	}

	public static class Factory implements AVGeneratorFactory {
		@Inject
		ServiceArea serviceArea;

		@Inject
		SharedAVConfigGroup astraConfig;

		@Inject
		Population population;

		@Inject
		GlobalConfigGroup globalConfig;

		@Inject
		@Named(AVModule.AV_MODE)
		Network network;

		@Override
		public AVGenerator createGenerator(AVGeneratorConfig generatorConfig) {
			String operator = generatorConfig.getParent().getId().toString();

			String prefix = "av_" + operator;
			int numberOfVehicles = (int) generatorConfig.getNumberOfVehicles();

			Random random = new Random(globalConfig.getRandomSeed());

			Set<Link> linkSet = new HashSet<>();

			for (Id<Link> linkId : serviceArea.getServedLinkIds()) {
				Link link = network.getLinks().get(linkId);

				if (link != null) {
					linkSet.add(link);
				}
			}

			List<Link> links = new ArrayList<>(linkSet);
			double[] cdf = calculateCDF(links);

			return new ServiceAreaGenerator(prefix, numberOfVehicles, links, cdf, random);
		}

		private double[] calculateCDF(List<Link> links) {
			List<Id<Link>> linkIdsList = new ArrayList<>(links.stream().map(Link::getId).collect(Collectors.toList()));
			Set<Id<Link>> linkIdsSet = new HashSet<>(linkIdsList);

			int[] density = new int[links.size()];

			Counter counter = new Counter("Building CDF for initial vehicle distribution... ");

			for (Person person : population.getPersons().values()) {
				Plan plan = person.getSelectedPlan();

				for (PlanElement element : plan.getPlanElements()) {
					if (element instanceof Activity) {
						Activity activity = (Activity) element;

						if (activity.getType().equals("home")) {
							Id<Link> linkId = activity.getLinkId();

							if (linkIdsSet.contains(linkId)) {
								density[linkIdsList.indexOf(linkId)]++;
							}

							break;
						}
					}
				}

				counter.incCounter();
			}

			double[] cdf = new double[links.size()];
			cdf[0] = density[0];

			for (int i = 1; i < links.size(); i++) {
				cdf[i] = cdf[i - 1] + density[i];
			}

			for (int i = 0; i < links.size(); i++) {
				cdf[i] /= cdf[links.size() - 1];
			}

			return cdf;
		}
	}
}

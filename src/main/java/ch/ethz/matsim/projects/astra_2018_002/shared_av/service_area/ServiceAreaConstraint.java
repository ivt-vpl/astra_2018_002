package ch.ethz.matsim.projects.astra_2018_002.shared_av.service_area;

import java.util.Collection;
import java.util.List;

import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.network.Link;
import org.matsim.api.core.v01.population.Person;
import org.matsim.core.utils.geometry.CoordUtils;

import ch.ethz.matsim.discrete_mode_choice.model.DiscreteModeChoiceTrip;
import ch.ethz.matsim.discrete_mode_choice.model.constraints.AbstractTripConstraint;
import ch.ethz.matsim.discrete_mode_choice.model.trip_based.TripConstraint;
import ch.ethz.matsim.discrete_mode_choice.model.trip_based.TripConstraintFactory;
import ch.ethz.matsim.projects.astra_2018_002.shared_av.SharedAVModule;

public class ServiceAreaConstraint extends AbstractTripConstraint {
	private final ServiceArea serviceArea;
	private final double minimumTripDistance_km;

	public ServiceAreaConstraint(ServiceArea serviceArea, double minimumTripDistance_km) {
		this.serviceArea = serviceArea;
		this.minimumTripDistance_km = minimumTripDistance_km;
	}

	@Override
	public boolean validateBeforeEstimation(DiscreteModeChoiceTrip trip, String mode, List<String> previousModes) {
		if (mode.equals(SharedAVModule.AV_POOL) || mode.equals(SharedAVModule.AV_TAXI)) {
			double distance_km = CoordUtils.calcEuclideanDistance(trip.getOriginActivity().getCoord(),
					trip.getDestinationActivity().getCoord()) * 1e-3;

			if (distance_km < minimumTripDistance_km) {
				return false;
			}

			Id<Link> originLinkId = trip.getOriginActivity().getLinkId();
			Id<Link> destinationLinkId = trip.getDestinationActivity().getLinkId();

			return serviceArea.areLinksCovered(originLinkId, destinationLinkId);
		}

		return true;
	}

	public static class Factory implements TripConstraintFactory {
		private final ServiceArea serviceArea;
		private final double minimumTripDistance_km;

		public Factory(ServiceArea serviceArea, double minimumTripDistance_km) {
			this.serviceArea = serviceArea;
			this.minimumTripDistance_km = minimumTripDistance_km;
		}

		@Override
		public TripConstraint createConstraint(Person person, List<DiscreteModeChoiceTrip> planTrips,
				Collection<String> availableModes) {
			return new ServiceAreaConstraint(serviceArea, minimumTripDistance_km);
		}
	}
}

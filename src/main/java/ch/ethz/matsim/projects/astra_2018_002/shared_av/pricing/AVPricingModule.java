package ch.ethz.matsim.projects.astra_2018_002.shared_av.pricing;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import org.matsim.api.core.v01.network.Network;
import org.matsim.core.controler.AbstractModule;
import org.matsim.core.controler.OutputDirectoryHierarchy;

import com.google.inject.Key;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import com.google.inject.name.Names;

import ch.ethz.matsim.av.framework.AVModule;
import ch.ethz.matsim.av_cost_calculator.CostCalculator;
import ch.ethz.matsim.av_cost_calculator.CostCalculatorExecutor;
import ch.ethz.matsim.av_cost_calculator.run.AVValidator;
import ch.ethz.matsim.av_cost_calculator.run.IdAVValidator;
import ch.ethz.matsim.av_cost_calculator.run.MultiOccupancyAnalysisHandler;
import ch.ethz.matsim.av_cost_calculator.run.PricingAnalysisHandler;
import ch.ethz.matsim.av_cost_calculator.run.SingleOccupancyAnalysisHandler;
import ch.ethz.matsim.projects.astra_2018_002.shared_av.SharedAVConfigGroup;
import ch.ethz.matsim.projects.astra_2018_002.shared_av.SharedAVModule;

public class AVPricingModule extends AbstractModule {
	private final double scenarioScale;
	private final int horizon;

	public AVPricingModule(double scenarioScale, int horizon) {
		this.scenarioScale = scenarioScale;
		this.horizon = horizon;
	}

	@Override
	public void install() {
		addEventHandlerBinding().to(Key.get(PricingAnalysisHandler.class, Names.named(SharedAVModule.AV_TAXI)));
		addEventHandlerBinding().to(Key.get(PricingAnalysisHandler.class, Names.named(SharedAVModule.AV_POOL)));

		addControlerListenerBinding().to(AVPricingListener.class);
	}

	@Provides
	@Singleton
	public AVPricingListener provideAVPricingListener(@Named(SharedAVModule.AV_TAXI) PricingAnalysisHandler taxiHandler,
			@Named(SharedAVModule.AV_POOL) PricingAnalysisHandler pooledHandler, CostCalculatorExecutor executor,
			OutputDirectoryHierarchy output, SharedAVConfigGroup savConfig) throws IOException {
		return new AVPricingListener(scenarioScale, horizon, taxiHandler, pooledHandler, executor, output,
				savConfig.getCostCalculatorInitialPrice());
	}

	@Provides
	@Singleton
	@Named(SharedAVModule.AV_TAXI)
	public PricingAnalysisHandler provideTaxiPricingAnalysisHandler(@Named(AVModule.AV_MODE) Network network) {
		double totalTime = 24.0 * 3600.0;
		AVValidator validator = new IdAVValidator(SharedAVModule.AV_TAXI);
		return new SingleOccupancyAnalysisHandler(network, validator, totalTime);
	}

	@Provides
	@Singleton
	@Named(SharedAVModule.AV_POOL)
	public PricingAnalysisHandler providePooledPricingAnalysisHandler(@Named(AVModule.AV_MODE) Network network) {
		double totalTime = 24.0 * 3600.0;
		AVValidator validator = new IdAVValidator(SharedAVModule.AV_POOL);
		return new MultiOccupancyAnalysisHandler(network, validator, totalTime, SharedAVModule.POOLED_NUMBER_OF_SEATS);
	}

	@Provides
	@Singleton
	public CostCalculatorExecutor provideCostCalculatorExecutor(OutputDirectoryHierarchy outputHierarchy) {
		URL sourceURL = CostCalculator.class.getClassLoader().getResource("ch/ethz/matsim/av_cost_calculator/");

		File workingDirectory = new File(outputHierarchy.getTempPath() + "/av_cost_calculator");
		workingDirectory.mkdirs();

		return new CostCalculatorExecutor(workingDirectory, sourceURL);
	}

	@Singleton
	@Provides
	public SharedAVTravelCost provideTravelCost(AVPricingListener pricingListener, SharedAVConfigGroup config) {
		return new OnlineSharedAVTravelCost(pricingListener, config.getDistributeCosts(),
				config.getFixedPoolingPricePerKm(), config.getFixedTaxiPricePerKm());
	}
}

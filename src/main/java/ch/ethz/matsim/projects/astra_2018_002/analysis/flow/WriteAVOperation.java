package ch.ethz.matsim.projects.astra_2018_002.analysis.flow;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Collections;

import org.matsim.api.core.v01.network.Link;
import org.matsim.api.core.v01.network.Network;
import org.matsim.core.network.NetworkUtils;
import org.matsim.core.network.algorithms.TransportModeNetworkFilter;
import org.matsim.core.network.io.MatsimNetworkReader;

public class WriteAVOperation {
	static public void main(String[] args) throws IOException {
		String networkPath = args[0];
		String outputPath = args[1];

		Network network = NetworkUtils.createNetwork();
		new MatsimNetworkReader(network).readFile(networkPath);

		Network roadNetwork = NetworkUtils.createNetwork();
		new TransportModeNetworkFilter(network).filter(roadNetwork, Collections.singleton("car"));

		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputPath)));
		writer.write("link_id;av_operation\n");
		writer.flush();

		for (Link link : roadNetwork.getLinks().values()) {
			Boolean rawAttribute = (Boolean) link.getAttributes().getAttribute("avOperation");
			
			writer.write(String.format("%s;%d\n", link.getId().toString(),
					(rawAttribute != null && rawAttribute) ? 1 : 0));
			
			writer.flush();
		}

		writer.close();
	}
}

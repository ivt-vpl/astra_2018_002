package ch.ethz.matsim.projects.astra_2018_002.shared_av.service_area;

import java.util.Collection;
import java.util.stream.Collectors;

import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.network.Link;

public class ServiceArea {
	private final Collection<Id<Link>> servedLinkIds;

	public ServiceArea(Collection<Link> servedLinks) {
		this.servedLinkIds = servedLinks.stream().map(Link::getId).collect(Collectors.toSet());
	}

	public boolean areLinksCovered(Id<Link> originId, Id<Link> destinationId) {
		return servedLinkIds.contains(originId) && servedLinkIds.contains(destinationId);
	}

	public Collection<Id<Link>> getServedLinkIds() {
		return servedLinkIds;
	}
}

package ch.ethz.matsim.projects.astra_2018_002.shared_av.waiting_time.zonal;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.geotools.data.DataStore;
import org.geotools.data.DataStoreFinder;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureIterator;
import org.geotools.data.simple.SimpleFeatureSource;
import org.matsim.api.core.v01.network.Link;
import org.matsim.api.core.v01.network.Network;
import org.opengis.feature.simple.SimpleFeature;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Point;

public class WaitingTimeZoneReader {
	private final static Logger logger = Logger.getLogger(WaitingTimeZoneReader.class);

	private final Network network;
	private final String serviceAreaAttribute;
	private final String identifierAttribute;

	private final static GeometryFactory geometryFactory = new GeometryFactory();

	public WaitingTimeZoneReader(String identifierAttribute, String serviceAreaAttribute, Network network) {
		this.network = network;
		this.serviceAreaAttribute = serviceAreaAttribute;
		this.identifierAttribute = identifierAttribute;
	}

	public List<WaitingTimeZone> read(String activeServiceArea, URL url) {
		Map<String, Geometry> shapes = new HashMap<>();

		try {
			DataStore dataStore = DataStoreFinder.getDataStore(Collections.singletonMap("url", url));

			SimpleFeatureSource featureSource = dataStore.getFeatureSource(dataStore.getTypeNames()[0]);
			SimpleFeatureCollection featureCollection = featureSource.getFeatures();
			SimpleFeatureIterator featureIterator = featureCollection.features();

			while (featureIterator.hasNext()) {
				SimpleFeature feature = featureIterator.next();

				String serviceArea = (String) feature.getAttribute(serviceAreaAttribute);
				String identifier = (String) feature.getAttribute(identifierAttribute);

				if (serviceArea.equals(activeServiceArea)) {
					shapes.put(identifier, (Geometry) feature.getDefaultGeometry());
				}
			}

			featureIterator.close();
			dataStore.dispose();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

		Map<String, Collection<Link>> links = new HashMap<>();
		shapes.keySet().forEach(k -> links.put(k, new LinkedList<>()));

		for (Link link : network.getLinks().values()) {
			Coordinate coordinate = new Coordinate(link.getCoord().getX(), link.getCoord().getY());
			Point point = geometryFactory.createPoint(coordinate);

			for (Map.Entry<String, Geometry> entry : shapes.entrySet()) {
				if (entry.getValue().contains(point)) {
					links.get(entry.getKey()).add(link);
				}
			}
		}

		List<WaitingTimeZone> zones = new ArrayList<>(links.size());

		for (Map.Entry<String, Collection<Link>> entry : links.entrySet()) {
			logger.info(
					String.format("Found %d links for waiting time zone %s", entry.getValue().size(), entry.getKey()));

			zones.add(new WaitingTimeZone(entry.getKey(), entry.getValue()));
		}

		return zones;
	}
}

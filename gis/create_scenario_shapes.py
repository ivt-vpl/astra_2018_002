import pandas as pd
import geopandas as gpd

df = gpd.read_file("scenarios/Gemeinden_fuer_Raumtypen.shp")
df.crs = {"init": "EPSG:2056"}
df = df[["geometry", "Gebietstyp"]]

scenarios = {
    "sa": ("S", "A"),
    "lu": ("LU",),
    "lg": ("LG",)
}

for scenario, types in scenarios.items():
    df.loc[df["Gebietstyp"].isin(types), "scenario"] = scenario

df = df.dissolve(by = "scenario").reset_index()[["scenario", "geometry"]]
df.to_file("scenarios/scenarios.shp")

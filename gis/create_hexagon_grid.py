import pandas as pd
import geopandas as gpd
import numpy as np
import shapely.geometry as geo
import sys

operating_areas_path = sys.argv[1]
output_path = sys.argv[2]
service_area_attribute_name = sys.argv[3]
waiting_time_zone_attribute_name = sys.argv[4]
hexagon_radius = int(sys.argv[5])

df_operating_areas = gpd.read_file(operating_areas_path)

corners = [
    np.array([np.cos(30.0 * np.pi / 180.0), np.sin(30.0 * np.pi / 180.0)]) * hexagon_radius,
    np.array([np.cos(90.0 * np.pi / 180.0), np.sin(90.0 * np.pi / 180.0)]) * hexagon_radius,
    np.array([np.cos(150.0 * np.pi / 180.0), np.sin(150.0 * np.pi / 180.0)]) * hexagon_radius,
    np.array([np.cos(210.0 * np.pi / 180.0), np.sin(210.0 * np.pi / 180.0)]) * hexagon_radius,
    np.array([np.cos(270.0 * np.pi / 180.0), np.sin(270.0 * np.pi / 180.0)]) * hexagon_radius,
    np.array([np.cos(330.0 * np.pi / 180.0), np.sin(330.0 * np.pi / 180.0)]) * hexagon_radius
]

offset_x = 2.0 * hexagon_radius * np.cos(30.0 * np.pi / 180.0)

t = 2.0 * hexagon_radius * np.cos(30.0 * np.pi / 180.0) / np.sqrt(3.0)
offset_y = hexagon_radius + 0.5 * t

geometry = []
counter = {}

for area_name, area_geometry in zip(df_operating_areas[service_area_attribute_name], df_operating_areas["geometry"]):
    min_x, min_y, max_x, max_y = area_geometry.bounds

    max_i = int(np.ceil((max_x - min_x) / offset_x)) + 1
    max_j = int(np.ceil((max_y - min_y) / offset_y)) + 1

    if not area_name in counter:
        counter[area_name] = 0

    for i in range(max_i):
        for j in range(max_j):
            centroid = np.array([
                min_x + offset_x * i + (0.5 * offset_x if j % 2 == 0 else 0.0),
                min_y + offset_y * j
            ])

            hexagon = geo.Polygon(corners + centroid)

            if hexagon.intersects(area_geometry):
                counter[area_name] += 1
                geometry.append((area_name, "%s_%d" % (area_name, counter[area_name]), hexagon))

df = pd.DataFrame.from_records(geometry, columns = [service_area_attribute_name, waiting_time_zone_attribute_name, "geometry"])
df = gpd.GeoDataFrame(df, crs = {"init": "EPSG:2056"})
df.to_file(output_path)

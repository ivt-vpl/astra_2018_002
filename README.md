# How to run a simulation

Assuming that the scenario is located in `/my/path/to/scenario`, the ASTRA 2018/002 can be run as follows:

```
java [...] --config-path /my/path/to/scenario/switzerland_config.xml
```

Specific config parameters can be set like e.g. `--config:qsim.flowCapacityFactor 0.1`.

# How to run a simulation with private AVs

Private AVs can be activated via the `private_av` config group:

```xml
<module name="private_avs">
  <param name="usePrivateAVs" value="true" />
</module>
```

Likewise, this option can be set via the command line:

```
java [...] --config:private_av.usePrivateAVs true
```

# How to create a network with all relevant modes

To create a network that includes prav3, prav4, prav5 (and car_passenger and truck), one needs to call the script `ch.ethz.matsim.projects.astra_2018_002.link_assignment.AVLinkAssignment`:

```
java [...] ch.ethz.matsim.projects.astra_2018_002.link_assignment.AVLinkAssignment --input-path switzerland_network.xml.gz --output-path switzerland_2040_network.xml.gz --cities-shapefile gis/cities/cities.shp
```

Such a network needs to be used in the simulations, otherwise an error will be thrown.

# How to run a simulation with shared AVs

Shared AVs can be activated via the `shared_av` config group:

```xml
<module name="shared_avs">
  <!-- Defines whather shared AVs are active or not -->
  <param name="useSharedAVs" value="true" />

  <!-- Defines the fleet sizes of both services -->
  <param name="taxiFleetSize" value="100" />
  <param name="pooledFleetSize" value="100" />

  <!-- Define a shape file for the operating area and zones for waiting time measurement therein -->
  <param name="serviceAreaShapefile" value="path/to/service_areas.shp" />
  <param name="waitingTimeZoneShapefile" value="path/to/waiting_time_zones.shp" />

  <!-- Defines the relevant attribute names of the shapes in these shape files -->
  <param name="serviceAreaAttribute" value="SAREA" />
  <param name="waitingTimeZoneAttribute" value="WZONE" />

  <!-- Defines which operating area is active -->
  <param name="activeServiceArea" value="true" />

  <!-- Configures the AV cost calculator -->
  <param name="useCostCalculator" value="false" />
  <param name="costCalculatorScalingFactor" value="0.1" />
  <param name="costCalculatorHorizon" value="10" />

  <!-- Configures AV waiting time measurement -->
  <param name="waitingTimeEstimatorStartTime" value="18000" />
  <param name="waitingTimeEstimatorEndTime" value="79200" />
  <param name="waitingTimeEstimatorInterval" value="900" />
  <param name="waitingTimeEstimatorHorizon" value="10" />
  <param name="waitingTimeEstimatorFallbackValue" value="300" />
</module>
```

To run such a simulation, two shape files must be provided. The first is a file containing all the service areas for the scenarios (probably there will be exactly one per scenario). The path to that file must be given as the `serviceAreaShapefile` option. The shapes for these service areas must contain an attribute that can be configured via the `serviceAreaAttribute`. Finally, via config one can choose which of these areas is used in the next run by setting `activeServiceArea` to one of the possible values of that attribute, i.e. one selects one service area from this service area file.

Second, a file containing a segmentation of space for waiting time calculation must be given as `waitingTimeZoneShapefile`. Waiting times for the AV services are measured and predicted, for instance, in a grid. If the whole service area is one of these "waiting time zones", all waiting times in the service area will be averaged when predicting waiting times. The format of the shape file is again that each shape should contain an attribute that corresponds to `serviceAreaAttribute`. Only those shapes that correspond to the `selectedServiceArea` will be considered for estimation. The number of shapes for each service area is arbitrary. In any case, each of these waiting time zones gets a unique identifier. The attribute for that identifier is specified in the `waitingTimeZoneAttribute` option.

For the waiting time zones we usually just want some grid-like structure that is covering the service area. For that there is a script in `gis/create_hexagon_grid.py`. It takes a number of command line arguments:

- A path to the shape file containing the actual service areas
- An output path for the new shape file with the waiting time zones
- The name of the attribute that distinguishes between service areas
- The new name of the attribute that distinguishes between waiting time zones
- The horizontal width of the hexagon grid

Example:

```sh
python3 create_hexagon_grid.py path/to/service_areas.shp path/to/waiting_time_zones.shp SAREA WZONE 1000
```

The config group allows to configure whether and how the cost calculator is used. If `useCostCalculator` is `false`, a fixed value from the scenario (mode choice) parameters, see below, is used. If it is activated prices are calculated on the fly dependent on the utilization of the fleet in the simulation. The first iteration starts with a price of 0 CHF/km. In all subsequent iterations the cost calculator is used to obtain a `calculated price`. This calculated price is then transformed into the `active price` through a moving average process. So after the first iteration the `active price` will be `(9 * 0 + calculated price) / 10)`. Naturally, in the first iteratons there will be a mismatch between the actual calculated price from the cost calculator (which will be high because of low utilization) and the active price, which will be close to zero. As iterations progress those two prices will converge to each other. The progression of both prices will be written out in `prices.csv` in the simulation output folder if the cost calculator is activated. The `costCalculatorHorizon` config option defines the length of the moving average filter (i.e. number of iterations).

Furthermore, the way waiting times are predicted can be adjusted. Waiting times are measured by zone as given in the respective shape file, but also by time bin. The size of the time bins is given as `waitingTimeEstimatorInterval` and is by default set to 15 minutes. All times before `waitingTimeEstimatorStartTime` are considered one single bin and all times after `waitingTimeEstimatorEndTime` are also considered one combined. This way fewer calculations need to be performed for those times where there will be little demand in any case. Again, a moving average filter is applied when measuring waiting times in zones. The length of that filter can be defined in `waitingTimeEstimatorHorizon`. Furthermore, there will be cases where not a single waiting time is observed in the simulation for a certain time bin and a certain zone. In those cases, the `waitingTimeEstimatorFallbackValue` will be assumed for that data point, by default is is set to 5 minutes. This value may need to be adjusted. Especially if zones or bins get very small, this value might be used quite frequently. In those cases it could make sense to increase the zones and/or increase the time bin sizes.

Waiting times in the system are tracked by zone and time bin in `ITERS/it.N/N.waiting_times.csv` so they can be easily analyzed and visualized.

# How to adjust mode choice parameters (e.g. VOTs)

All parameters for the mode choice model are defined in `SwissUtilityParameters`. Usually, for the runs, those parameters are set explicitly via input data. To do that, there are two ways. Specific parameters can be set via command line:

```
--utility:car.betaTravelTime -0.32
```

Multiple of those options can be given. They always start with `--utility:` and then give the name of an option in `SwissUtilityParameters`. Alternatively (or in combination, a file containing parameters can be given:

`--utility-parameters path/to/my/model.params`

The file is a simple text file with all the options:

```
car.betaTravelTime = -0.32
walk.betaTravelTime = -0.60
```

An arbitrary number of options can be given again. The precendence in the simulation is as follows: First, parameters are taken from the selected preset (which currently is the calibrated model for ASTRA 2018/002). Afterwards, parameters from the `utility-parameters` file are overwritten. Finally, parameters which are set directly from the command line are taken into account.

To summarize, to change the VOT of the shared modes, one can for instance apply the following to the simulation start script:

```
--utility.tav.betaTravelTime CUSTOM_VALUE --utility.pav.betaTravelTime CUSTOM_VALUE
```

For private AVs, multiple VOTs must be defined. Each corresponds to one automation level:

```
--utility.prav.betaTravelTimeLevel3 CUSTOM_VALUE
--utility.prav.betaTravelTimeLevel4 CUSTOM_VALUE
--utility.prav.betaTravelTimeLevel5 CUSTOM_VALUE
```

# How to adjust PCU values

The PCU values can be adjusted by providing a file with all the necessary values. It is a JSON file, for instance `flow-efficiency.json`. The default content looks as follows:

```json
{
  "pcu_car" : 1.0,
  "pcu_prav3_motorway" : 1.0,
  "pcu_prav3_trunk" : 1.0,
  "pcu_prav3_secondaryfast" : 1.0,
  "pcu_prav3_secondaryslow" : 1.0,
  "pcu_prav4_motorway" : 0.83,
  "pcu_prav4_trunk" : 0.77,
  "pcu_prav4_secondaryfast" : 0.83,
  "pcu_prav4_secondaryslow" : 0.8,
  "pcu_prav5_motorway" : 0.83,
  "pcu_prav5_trunk" : 0.77,
  "pcu_prav5_secondaryfast" : 0.83,
  "pcu_prav5_secondaryslow" : 0.8,
  "pcu_sharedAv_motorway" : 0.83,
  "pcu_sharedAv_trunk" : 0.77,
  "pcu_sharedAv_secondaryfast" : 0.83,
  "pcu_sharedAv_secondaryslow" : 0.8,
  "pcu_truck" : 4.0,
  "pcu_truckAv_motorway" : 3.08,
  "pcu_truckAv_trunk" : 3.08,
  "pcu_truckAv_secondaryfast" : 3.33,
  "pcu_truckAv_secondaryslow" : 3.2
}
```

These are also the default values that will be used if no specific file is provided. The file can be provided via the command line:

```
--flow-efficiency-file /path/to/my/flow-efficiency.json
```

**Attention!** During analysis, this file should also be provided to `RunFlowAnalysis` (with the same command line parameter). Otherwise, the flow analysis will assume the default values and results will be incorrect!

# How to set fixed prices for operators

By default, costs will be calculated online for the taxi and pooling operator. Only if the following values are set to anything else than `NaN`, they will be used as fixed numbers:

```
--config:shared_avs.fixedPoolingPricePerKm 0.4
--config:shared_avs.fixedTaxiPricePerKm 0.3
```

Note that the cost calculation will *still* run in the background, so we also get the price that the operator *would need* to ask in order to be profitable.

# How to create the partial scenarios

This repository only contains code to run the requested scenarios in the ASTRA 2018/002 project. For the sake of avoiding code duplication, the scenario cutting code is still located in the [baseline_scenario](https://github.com/matsim-eth/baseline_scenario) repository, specifically in the (synpop)[https://github.com/matsim-eth/baseline_scenario/tree/synpop] branch.

The repository can be transformed into a "fat jar" by calling:

```sh
mvn -Pstandalone package
```

The basis for creating a partial scenario is the full Switzerland scenario in arbitrary sample size. How to create such a full Switzerland scenario is explained in the corresponding (Synthetic populaton of Switzerland)[https://gitlab.ethz.ch/ivt-vpl/populations/ch-zh-synpop] repository. This will create a scenario including `switzerland_population.xml.gz`, `switzerland_network.xml.gz` and so forth.

The `baseline_scenario` code can then be used to cut this population and network to a specific diameter. The relevant run script is `ch.ethz.matsim.baseline_scenario.CutScenario`. It has the following parameters:

- *input-config-path*: Path to `switzerland_config.xml`
- *shapefile-path*: Path to a shape file that contains the scenario boundary
- *shapefile-attribute*: Attribute of the shapes contained in the shape file that defines the scenario id
- *shapefile-value*: Defines which value the given attribute should have (see further below)
- *prefix*: Prefix in the filenames of the generated scenario, e.g. `prefix_population.xml.gz`
- *output-path*: Output path (directory) of the new scenario. The directoy must exist.
- (optional) *population-path*: Path (relative to the *input-config-path*) to an alternative population file (this would be, for instance, the output population of a relaxed simulation run)

The concept is therefore as follows: One has to provide a shape file to the script. The shapes (polygons) in that file all have a specific attribute (e.g. `scenario_id`). Then the script will look up the one specific polygon in that file that has the given `shapefile-value` for that attribute, e.g. `zurich_30km` or similar. This way one shape file can contain multiple scenario diameters, which avoids having a shape file for each of the scenarios.

For ASTRA 2018/002, there is already a shape file in `gis/scenarios/scenarios.shp`. It is created from the initial `Gemeinden_fuer_Raumtypen.shp` using the script `gis/create_scenario_shapes.py`. To create a "ländlich ungerichtet" (lu) scenario, one can call, for instance:

```
java -Xmx100G -cp baseline_scenario/baseline_scenario-0.0.1-synpop-SNAPSHOT.jar ch.ethz.matsim.baseline_scenario.CutScenario --input-config-path switzerland_config.xml --shapefile-path /path/to/gis/scenarios/scenarios.shp --shapefile-attribute scenario --shapefile-value lu --prefix lu_ --output-path lu_scenario
```

# How to create a long-distance scenario

To create the long-distance scenario, the script `ch.ethz.matsim.projects.astra_2018_002.long_distance.MakeLongDistanceScenario` is used. It receives a number of parameters:

- *network-path*: Path to the MATSim network file (e.g. `switzerland_network.xml.gz`)
- *population-path*: Path to the MATSim populaton file (e.g. `switzerland_population.xml.gz`)
- *output-path*: Output path of the long-distance population (e.g. `long_distance_population.xml.gz`)
- *osm-types*: `osm:highway` types that are considered as "long distance roads* (optional, default: "motorway,trunk")
- *threads*: Number of threads to use (optional, by default maximum is used)
- *batch-size*: Batch size in parallel processing (optional, default is 100)

The script will do the following:

1. Route every trip of every person using freespeed travel times
2. Find all persons that touch a link with the given `osm-types` at any point during their daily plan
3. Remove all remaining persons (those not interacting with "long distance roads")
4. Write out the reduced population

For a 0.1% sample that script removed around 1/3 of the agents. Remaining steps to verify the process are: https://gitlab.ethz.ch/ivt-vpl/astra_2018_002/milestones/3

# How to produce a flow analysis shape file

The flow analysis shape file can be created using the `ch.ethz.matsim.projects.astra_2018_002.analysis.flow.RunFlowAnalysis` script. It takes a number of command line arguents:

- `network-path`: Path to the MATSim network file
- `events-path`: Path to the MATSim events file to analyze
- `output-path`: Output path for the resulting shape file
- `osm-types` *(optional)*: A comma-separated list of OSM road types of the links that should be included
- `link-ids` *(optional)*: A comma-separated list of link ids that should be included

To produce a shape file to compare with ASTRA (highway) counts, one can for instance select:

```
java [...] ch.ethz.matsim.projects.astra_2018_002.analysis.flow.RunFlowAnalysis [...] --osm-types motorway,trunk,primary,secondary
```

For the capacity analysis in ASTRA 2018/002 it will be sufficient to obtain informaton for a hand full of links, e.g.:

```
java [...] ch.ethz.matsim.projects.astra_2018_002.analysis.flow.RunFlowAnalysis [...] --link-ids LINK_ID1,LINK_ID2,LINK_ID1,LINK_ID3
```

# How to do the flow comparison

In `flow_comparison/` there are two notebooks that allow for comparison of the scenario counts with ASTRA count stations. Additionally, the comparison data is included:

- `Jahresergebnisse-2017.xlsx` comes from the ASTRA website and shows road counts for 2017
- `messstellenverzeichnis.xlsx` contains all ASTRA count stations with their ID and name

The notebooks are the following:

- `Road count.ipynb` reads the `miessstellenverzeichnis.xlsx` and creates a GeoJSON file `astra_count_stations.geojson`. It contains all count stations in EPSG:2056 format including their name and reference count for private and freight traffic.
- `Matching.ipynb` takes an output shape file from the flow analysis above and the generates ASTRA count station file. It matches count stations to (analyzed) links by first creating a 200m buffer around each count station. Then, all links are searched for that intersect with this count station buffer. Finally, the link with the vehicle count that is closest to the reference value is chosen. Another GeoJSON file `match.geojson` is written out that compares the reference and simulation counts for each station. To verify that the script does not "cheat" in the way it matches stations, also a `comparison.geojson` is written out, which shows that count stations and their assigned MATSim links. While for a small number of stations one can see that the matching is not perfect, it is hard to find those cases, because most of them are matched nicely.

# How to perform comparison of calibration data

The `calibration/` folder contains a notebook that can be used to compare the
create Switzerland scenario (or partial scenarios) with microcensus reference
data. The reference data comes from the `data.microcensus.csv` stage of the
scenario pipeline. For comparison, a "trips file" from the MATSim simulation must
be created. This is done as follows:

```sh
java -Xmx120G -cp [...]/astra_2018_002-1.0.0.jar ch.ethz.matsim.projects.astra_2018_002.analysis.trips.ConvertTripsFromEvents --network-path /path/to/switzerland_network.xml.gz --events-path /path/to/output_events.xml.gz --output-path /path/to/new/trips.csv
```

The script will create a `trips.csv` file which then can be used to do all kind of
trip-based analyses or comparison to microcensus.

# How to perform analysis on shared AV services

There are two scripts, one produces trace information for all the shared AVs and the other one produces additional information about the AV trips. For instance, the waiting is given there explicitly while in the simple trips analysis (see above) it is included in the overall travel time.

Generate trace information:

```sh
java -Xmx120G -cp [...]/astra_2018_002-1.0.0.jar ch.ethz.matsim.projects.astra_2018_002.shared_av.analysis.traces.ConvertTracesFromEvents --network-path /path/to/switzerland_network.xml.gz --events-path /path/to/output_events.xml.gz --output-path /path/to/new/traces.csv
```

Generate service information:

```sh
java -Xmx120G -cp [...]/astra_2018_002-1.0.0.jar ch.ethz.matsim.projects.astra_2018_002.shared_av.analysis.service.ConvertAVServicesFromEvents --network-path /path/to/switzerland_network.xml.gz --events-path /path/to/output_events.xml.gz --output-path /path/to/new/traces.csv
```
# How to perform scenario analysis

On Euler, there are a couple of scripts now in `/cluster/work/ivt_vpl/astra1802`.
The process from start to end for doing a scenario analysis is as follows:

## 1. Copying scenario pipeline output

`01_synch_base_scenarios.sh`: This is a dummy script that is only on Euler to
make the process complete. It should actually be adjusted (user name etc. in the
file) and be called on nama/ifalik/pikelot, where the scenarios are created. On
ifalik/nama/pikelot, there should be a directory containing the different scenarios,
e.g.:

- `/nas/shoerl/my_scenarios`
  - `ch_2050_10pct`
  - `ch_2030_10pct`
  - `ch_2030_1pct`

*Note the naming scheme!* The directories have the prefix `ch_`, then comes the
year and then comes the sample size (as `100pct`, `25pct`, `10pct`, `1pct`, `1pm`,
or whatever we need). They contain the `switzerland_*.xml.gz` files.

The script (or rather just the single command in `01_synch_base_sceanrios.sh`) will
then make sure that whatever is in `my_scenarios` will be synched to `astra1802/scenarios`
on Euler. So one important folder in the `astra1802` is scenarios, because it should
contain all the basic scenarios, also with the naming scheme mentioned above.

You don't need the `01_synch_base_sceanriossh` to copy the scenarios, one could
also just use `scp`, but this is one option. At the end the desired sceanrios
should somehow end up in `astra1802/scenarios` with the correct naming scheme.

## 2. Updating the code

Sometimes, we update the code (master on github). In those cases, the code also
needs to be updated on Euler. It is contained in `astra1802/code`. However, one
rarely needs to touch that directory, because it gets auto-generated by the
`02_update_code.sh`. So in order to get the latest changes, build the master
branch etc., one just needs to call `sh 02_update_code.sh` and everything will
be prepared.

## 3. Running a baseline scenario

The script `03_run_baseline_scenario.sh` will run a baesline scenarios. This means
it will take a scenario (in terms of sample size and year) and it will run it
without any automated vehicles, flow efficiency changes etc. This will give a
simple baseline for all other simulations. The scripts needs a number of command
line arguments in this order:

- Year (e.g. `2020`, `2050`)
- Sample name (e.g. `10pct`, `25pct`)
- Sample value (e.g. `0.1`, `0.25`)

The first two arguments must refer to an existing scenario in the `scenarios/`
directory. So if `2020` and `10pct` is requested, there must be a folder
`ch_2020_10pct` in `scenarios/`.

An example call would then be:

```sh
sh 03_run_baseline_scenario.sh 2020 10pct 0.1
```

The "sample value", the last argument, must correspond to the "sample name" before,
but just given as a numeric value.

The script will then automatically submit a baseline simulation to Euler, which
will run for a while. The output will be written to `astra1802/output/bl_{year}_{sample_name}`.
So for the example above new output would appear in `astra1802/output/bl_2020_10pct`. This
folder especially contains the usual `output_plans.xml.gz` which then can be used as
the input to subsequent simulations.

## 4. Run a "private AV baseline"

The same can be done for all Switzerland, but with private AVs activated. This
is done automatically using the script `04_run_prav.sh`. It gets the same arguments
as the `03_run_baseline.sh` plus another argument saying `A` or `B` for the respective ASTRA scenario.
An output folder called like `output/prav_2020A_10pct` will be created.


## 5. Cutting the partial scenarios

To cut the partial scenarios the script `05_cut_scenario.sh` can be used. It receives
four arguments:

- Baseline prefix (`bl` or `prav`)
- Year
- Sample name (`10pct` / `25pct` / ...)
- ASTRA Scenario (`A` / `B`)

The first argument defines which "baseline output" is used for cutting the population.
This can either be the "true" baseline without any automation (derived from the
output of step 3), or it can be the "private av baseline" derived from step 4. So in
order to cut the partial scenarios for 2020, one would probably base it on output
from `03`, while for the future scenarios it should be based on `04`, where AVs
are already available.

If, for instance `2050`, `10pct` and `B` is requested, the script will put new scenarios
in the `scenarios/` folder. In this case it would be:

- `scenarios/lu_2050B_10pct`
- `scenarios/lg_2050B_10pct`
- `scenarios/sa_2050B_10pct`
- `scenarios/lod_2050B_10pct` (Long distance)

Those scenarios can the be usde later on. Again, the script automatically subtmits
the jobs to Euler and then all one needs to do is to wait for them to finish.

## 6. Creating waiting time zones

To create a necessary files for waiting time zones, the script `06_create_waiting_time_zones.sh`
can be used. It will simply call the necessary steps to create the shape files in
`scenarios/gis/waiting_time_zones.shp`.

## 7. Running a shared scenario

To run a shared scenario (on top of the cut partial scenarios), one can use the
script `07_run_shared.sh`. The input arguments are the following:

- Scenario (`lu`, `lg`, `sa`, `lod`)
- Year
- Sample name (`10pct`, `25pct`, ...)
- Sample value (`0.1`, `0.25`, ...)
- ASTRA Scenario (`A` / `B`vi)
- Taxi fleet size
- Pooled fleet size

The job will be submitted to Euler and the output will be written to e.g.
`output/lu_2050_10pct_shared_200_300` where `200` and `300` would be the taxi
and pooled fleet sizes.

## 8. Analysis

After doing all of this everything for analysis should already be present! All of
the run scripts `03`, `04`, `07` automatically call all the analysis tools that
analyze the output events files. The analysis data can be found in `astra1802/analysis`.
The files names there correspond to the scenarios, e.g.

- `bl_2020_10pct.trips.csv`, `bl_2020_10pct.flow.shp`, ...
- `prav_2050_10pct.trips.csv`, `bl_2020_10pct.flow.shp`
- `lu_2050_10pct_shared_200_300.traces.csv`, `lu_2050_10pct_shared_200_300.prices.csv`, ...

An example of how to analyse these files can be found in this repository in
`scenario_analysis/Scenario Analysis.ipynb`.

## Aggregate analysis

There are some scripts to perform aggregate analysis in `analysis/`. They can
be called directly on Euler through the `08_simulation_analysis.sh` script. It
will find all successfully run simulations in `astra18/analysis/` and aggregate
trips, services and traces files to a `*.aggregated.p` file in the same folder.

Finally, when those scripts have been run, `09_aggregate_analysis.sh` can be run,
which takes all `*.aggregated.p` files and creates a combined analysis file in
`analysis/`. It is written out in CSV and Parquet format:

    astra1802/analysis/aggregated.parquet
    astra1802/analysis/aggregated.csv

import pandas as pd
import glob, sys, pickle, re

# Expect a path to a folder containing *.aggregated.p files
data_path = sys.argv[1]

# Find all simulations
data = []
columns = set()

print(columns)

names = []

for path in glob.glob("%s/*.aggregated.p" % data_path):
    name = path.replace("%s/" % data_path, "").replace(".aggregated.p", "")
    names.append(name)

# Filter out random seed duplicates
for name in names[:]:
    m1 = re.match(r'^(.+)_([0-9]000|10000)_2$', name)
    m2 = re.match(r'^(.+)_([0-9]000|10000)$', name)

    short_name = None

    if m1:
        short_name = m1.group(1)
    elif m2:
        short_name = m2.group(1)

    if not short_name is None:
        if short_name in names:
            names.remove(name)
            print("Filtered out ", name)

for name in names:
    path = "%s/%s.aggregated.p" % (data_path, name)
    with open(path, "rb") as f:
        simulation_data = pickle.load(f)

        simulation_data["input_name"] = name
        if not "run_name" in simulation_data: simulation_data["run_name"] = None

        columns |= simulation_data.keys()
        data.append(simulation_data)

# Save dataframe
list_columns = list(columns)

list_columns.remove("case")
list_columns.remove("year")
list_columns.remove("scenario")
list_columns.remove("run_name")

list_columns = ["case", "year", "scenario", "run_name"] + sorted(list(list_columns))

df = pd.DataFrame.from_records(data, columns = list_columns)
df.to_parquet("%s/aggregated.parquet" % data_path)
df.to_csv("%s/aggregated.csv" % data_path, sep = ";", index = None)

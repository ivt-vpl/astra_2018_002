import sys, pickle, re, os
import pandas as pd
import numpy as np
import json

# Some constants
MODES = [
    "walk", "bike", "pt", "car", "prav3", "prav4", "prav5", "av",
    "truck", "truckAv", "car_passenger"
]

DISTANCES = [500, 1000, 2000, 5000, 10000]

# Expect a path to a trips file
trips_path = sys.argv[1]
print("trips_path =", trips_path)
phase1_path = sys.argv[2]
print("phase1_path =", phase1_path)
# We save all aggregated data in one dictionary
data = {}

# Derive some additional paths
# for Linux operating systems
name = trips_path.split("/")[-1].replace(".trips.csv", "")
print("name =", name)
base_path = "/".join(trips_path.split("/")[:-1])
print("base_path =", base_path)

# paths for linux operating systems
prices_path = "%s/%s.prices.csv" % (base_path, name)
services_path = "%s/%s.services.csv" % (base_path, name)
traces_path = "%s/%s.traces.csv" % (base_path, name)
trips_path = "%s/%s.trips.csv" % (base_path, name)
pt_distance_path = "%s/%s.pt_distance.json" % (base_path, name)
scenario_info_path = "%s/%s.scenario.json" % (base_path, name)
output_path = "%s/%s.aggregated.p" % (base_path, name)

# Find out what kind of trips file we have
# TODO: ADJUST  NAMING PATTERNS FOR PHASE 2
PATTERN_SHARED = r"(^[a-z]{2,3})_([0-9]{4})([ABCUVX])_25pct_shared_([0-9]+)_([0-9]+)"
PATTERN_PRAV = r"prav_([0-9]{4})([ABCUVX])_25pct$"
PATTERN_BASELINE = r"(^[a-z]{2,3})_([0-9]{4})_25pct$"
PATTERN_SHARED_BASELINE = r"(^[a-z]{2,3})_([0-9]{4})([ABCUVX])_25pct$"

PATTERN_SHARED_PH2 = r"(^[a-z]{2,3})_([0-9]{4})([ABCDEUVXWY])_25pct_(.+)_shared_([0-9]+)_([0-9]+)"
PATTERN_PRAV_PH2 = r"prav_([0-9]{4})([ABCDEUVXWY])_25pct_(.+)"
PATTERN_BASELINE_PH2 = r"(^[a-z]{2,3})_([0-9]{4})_25pct_(.+)"
# The pattern for shared baseline phase 2 also matches shared AND phase 2 shared! Solved by if else starting line 65
PATTERN_SHARED_BASELINE_PH2 = r"(^[a-z]{2,3})_([0-9]{4})([ABCDEUVXWY])_25pct_(.+)"

m_shared = re.search(PATTERN_SHARED, name)
print("m_shared =", m_shared)
m_baseline = re.search(PATTERN_BASELINE, name)
print("m_baseline", m_baseline)
m_prav = re.search(PATTERN_PRAV, name)
print("m_prav =", m_prav)
m_shared_baseline = re.search(PATTERN_SHARED_BASELINE, name)
print("m_shared_baseline =", m_shared_baseline)

m_shared_ph2 = re.search(PATTERN_SHARED_PH2, name)
print("m_shared_ph2 =", m_shared_ph2)
m_baseline_ph2 = re.search(PATTERN_BASELINE_PH2, name)
print("m_baseline_ph2 =", m_baseline_ph2)
m_prav_ph2 = re.search(PATTERN_PRAV_PH2, name)
print("m_prav_ph2 =", m_prav_ph2)
string_find_shared = "_shared"
if string_find_shared not in name:
    m_shared_baseline_ph2 = re.search(PATTERN_SHARED_BASELINE_PH2, name)
    print("m_shared_baseline_ph2 =", m_shared_baseline_ph2)
else:
    m_shared_baseline_ph2 = None
    print("m_shared_baseline_ph2 =", m_shared_baseline_ph2)

# Break apart name and put that information into the data frame. Establish reference trips for modal shifts.
# Phase 1
if m_shared: # Taxi / Shared run
    data["case"] = m_shared.group(1)
    data["year"] = int(m_shared.group(2))
    data["scenario"] = m_shared.group(3)
    data["taxi_fleet_size"] = int(m_shared.group(4))
    data["pooled_fleet_size"] = int(m_shared.group(5))
    # Caution! If your modal split shifts seem implausible or strange, check how reference_trips_path is being used!
    # Caution! It also assumes that if you are analysing ph1 data, your reference trips are also ph1 and in the same folder!
    # Note: I changed the reference trip path so that the reginal shared scenarios would reference regional prav scenarios
    # instead of the swiss wide prav scenarios.
    # reference_trips_path = "%s/prav_%d%s_25pct.trips.csv" % (base_path, data["year"], data["scenario"])
    if data["scenario"] == "A":
        reference_trips_path = "%s/%s_%dU_25pct.trips.csv" % (base_path, data["case"], data["year"])
        print("reference_trips_path =", reference_trips_path)
    elif data["scenario"] == "C":
        reference_trips_path = "%s/%s_%dV_25pct.trips.csv" % (base_path, data["case"], data["year"])
        print("reference_trips_path =", reference_trips_path)
    else:
        raise RuntimeError("A scenario other than A or C was used. Phase 1 shared Simulations only use Scenarios A and C.")


elif m_baseline: # Baseline run
    data["case"] = m_baseline.group(1)
    data["year"] = int(m_baseline.group(2))
    data["scenario"] = "X"
    data["taxi_fleet_size"] = 0
    data["pooled_fleet_size"] = 0
    reference_trips_path = None
    print("reference_trips_path =", reference_trips_path)

    if m_baseline.group(1) == "sa":
        exit(0)

elif m_prav: # Prav run
    data["case"] = "prav"
    data["year"] = int(m_prav.group(1))
    data["scenario"] = m_prav.group(2)
    data["taxi_fleet_size"] = 0
    data["pooled_fleet_size"] = 0
    # Caution! If your modal split shifts seem implausible or strange, check how reference_trips_path is being used!
    # Caution! It also assumes that if you are analysing ph1 data, your reference trips are also ph1 and in the same folder!
    reference_trips_path = "%s/bl_%d_25pct.trips.csv" % (base_path, data["year"])
    print("reference_trips_path =", reference_trips_path)

elif m_shared_baseline: # Shared baseline run
    data["case"] = m_shared_baseline.group(1)
    data["year"] = int(m_shared_baseline.group(2))
    data["scenario"] = m_shared_baseline.group(3)
    data["taxi_fleet_size"] = 0
    data["pooled_fleet_size"] = 0

    if data["scenario"] == "U" or data["scenario"] == "V":
        # Caution! If your modal split shifts seem implausible or strange, check how reference_trips_path is being used!
        # Caution! It also assumes that if you are analysing ph1 data, your reference trips are also ph1 and in the same folder!
        reference_trips_path = "%s/%s_%dX_25pct.trips.csv" % (base_path, data["case"], data["year"])
        print("reference_trips_path =", reference_trips_path)
    else:
        reference_trips_path = None
        print("reference_trips_path =", reference_trips_path)

# Phase 2
elif m_shared_ph2: # Taxi / Shared run
    data["case"] = m_shared_ph2.group(1)
    data["year"] = int(m_shared_ph2.group(2))
    data["scenario"] = m_shared_ph2.group(3)
    data["run_name"] = m_shared_ph2.group(4)
    data["taxi_fleet_size"] = int(m_shared_ph2.group(5))
    data["pooled_fleet_size"] = int(m_shared_ph2.group(6))
    # Caution! If your modal split shifts seem implausible or strange, check how reference_trips_path is being used!
    # TODO Caution! This does not account for taxi scenarios run solely using phase 1 prav scenarios! It will only work with ph2!
    # Note: I changed the reference trip path so that the reginal shared scenarios would reference regional prav scenarios
    # instead of the swiss wide prav scenarios.
    # reference_trips_path = "%s/prav_%d%s_25pct.trips.csv" % (base_path, data["year"], data["scenario"])
    if data["scenario"] == "A":
        reference_trips_path = "%s/%s_%dU_25pct_%s.trips.csv" % (base_path, data["case"], data["year"], data["run_name"])
        print("reference_trips_path =", reference_trips_path)
    elif data["scenario"] == "C":
        reference_trips_path = "%s/%s_%dV_25pct_%s.trips.csv" % (base_path, data["case"], data["year"], data["run_name"])
        print("reference_trips_path =", reference_trips_path)
    elif data["scenario"] == "D":
        reference_trips_path = "%s/%s_%dW_25pct_%s.trips.csv" % (base_path, data["case"], data["year"], data["run_name"])
        print("reference_trips_path =", reference_trips_path)
    elif data["scenario"] == "E":
        reference_trips_path = "%s/%s_%dY_25pct_%s.trips.csv" % (base_path, data["case"], data["year"], data["run_name"])
        print("reference_trips_path =", reference_trips_path)
    else:
        raise RuntimeError("A scenario other than A, C, D, or E was used. If you added a new scenario, this code must be updated, too.")

elif m_baseline_ph2: # Baseline run
    data["case"] = m_baseline_ph2.group(1)
    data["year"] = int(m_baseline_ph2.group(2))
    data["scenario"] = "X"
    data["run_name"] = m_baseline_ph2.group(3)
    data["taxi_fleet_size"] = 0
    data["pooled_fleet_size"] = 0
    reference_trips_path = None
    print("reference_trips_path =", reference_trips_path)

    if m_baseline_ph2.group(1) == "sa":
        exit(0)

elif m_prav_ph2: # Prav run
    data["case"] = "prav"
    data["year"] = int(m_prav_ph2.group(1))
    data["scenario"] = m_prav_ph2.group(2)
    data["run_name"] = m_prav_ph2.group(3)
    data["taxi_fleet_size"] = 0
    data["pooled_fleet_size"] = 0
    # Caution! the following reference trip path assumes that a baseline scenario with the same run_name was created!
    # In other words, it assumes that a new baseline scenario was run for phase 2!
    # If this is not the case, than the if statement starting line 247ish should take care of it and point to phase 1.
    reference_trips_path = "%s/bl_%d_25pct_%s.trips.csv" % (base_path, data["year"], data["run_name"])
    print("reference_trips_path =", reference_trips_path)

elif m_shared_baseline_ph2: # Shared baseline run
    data["case"] = m_shared_baseline_ph2.group(1)
    data["year"] = int(m_shared_baseline_ph2.group(2))
    data["scenario"] = m_shared_baseline_ph2.group(3)
    data["run_name"] = m_shared_baseline_ph2.group(4)
    data["taxi_fleet_size"] = 0
    data["pooled_fleet_size"] = 0

    if data["scenario"] == "U" or data["scenario"] == "V" or data["scenario"] == "W" or data["scenario"] == "Y":
        # Caution! If your modal split shifts seem implausible or strange, check how reference_trips_path is being used!
        # Also check the scenario name - shared BASELINE scenarios should NOT be named A, B, C, D, or E!
        # Caution! the following reference trip path assumes that a baseline scenario with the same run_name was created!
        # In other words, it assumes that a new baseline scenario was run for phase 2!
        # If there was no new baseline scenario run for phase 2, an if else loop later on should catch it and redirect to phase 1.
        reference_trips_path = "%s/%s_%dX_25pct_%s.trips.csv" % (base_path, data["case"], data["year"], data["run_name"])
        print("reference_trips_path =", reference_trips_path)
    else:
        reference_trips_path = None
        print("reference_trips_path =", reference_trips_path)
# if no matching files are found
else:
    raise RuntimeError("Cannot recognize type of trips file")

# Read prices
if os.path.exists(prices_path):
    df_prices = pd.read_csv(prices_path, sep = ";")
    data["taxi_price"] = df_prices["active_taxi_price"].values[-1]
    data["pooled_price"] = df_prices["active_pooled_price"].values[-1]

# Read AV services
if os.path.exists(services_path):
    df_services = pd.read_csv(services_path, sep = ";")
    data["median_waiting_time"] = df_services["waiting_time"].median()
    data["mean_waiting_time"] = df_services["waiting_time"].mean()
    data["q90_waiting_time"] = df_services["waiting_time"].quantile(0.9)
    data["q10_waiting_time"] = df_services["waiting_time"].quantile(0.1)

# Read AV traces
if os.path.exists(traces_path):
    df_traces = pd.read_csv(traces_path, sep = ";")
    data["av_customer_distance"] = df_traces[df_traces["occupancy"] > 0]["distance"].sum() * 1e-3
    data["av_empty_distance"] = df_traces[df_traces["occupancy"] == 0]["distance"].sum() * 1e-3
    data["av_customer_trips"] = np.count_nonzero(df_traces["occupancy"] > 0)
    data["av_empty_trips"] = np.count_nonzero(df_traces["occupancy"] == 0)

# Read PT distance
if os.path.exists(pt_distance_path):
    with open(pt_distance_path) as f:
        distance_data = json.load(f)

        data["pt_bus_distance"] = distance_data["busDistance"] * 1e-3
        data["pt_tram_distance"] = distance_data["tramDistance"] * 1e-3
        data["pt_rail_distance"] = distance_data["railDistance"] * 1e-3

# Read scenario info
if os.path.exists(scenario_info_path):
    with open(scenario_info_path) as f:
        distance_data = json.load(f)

        data["scenario_number_of_persons"] = distance_data["numberOfPersons"]
        data["scenario_number_of_persons_over_18"] = distance_data["numberOfPersonsOver18"]
        data["scenario_average_age"] = distance_data["averageAge"]
        data["scenario_car_availability"] = distance_data["carAvailability"]

# Analyze trips
df_trips = pd.read_csv(trips_path, sep = ";")
df_trips["distance_class"] = np.digitize(df_trips["crowfly_distance"], np.array(DISTANCES) * 1e-3)

df_trips = df_trips[df_trips["crowfly_distance"] > 0]
df_trips = df_trips[df_trips["preceedingPurpose"] != "outside"]
df_trips = df_trips[df_trips["followingPurpose"] != "outside"]

for m in MODES:
    f_mode = df_trips["mode"] == m
    data["trips_%s" % m] = np.count_nonzero(f_mode)
    data["distance_%s" % m] = df_trips[f_mode]["network_distance"].sum()
    data["travel_time_%s" % m] = df_trips[f_mode]["travel_time"].sum()

    for k in range(len(DISTANCES)):
        f_mode_distance = f_mode & (df_trips["distance_class"] == k)
        data["trips_%s_%d" % (m, DISTANCES[k])] = np.count_nonzero(f_mode_distance)

PASSENGER_MODES = ["car", "prav3", "prav4", "prav5", "av", "walk", "bike", "pt", "car_passenger"]
ROAD_MODES = ["car", "prav3", "prav4", "prav5", "av"]

df_trips["speed"] = df_trips["network_distance"] / (df_trips["travel_time"] / 3600)
f_valid = np.isfinite(df_trips["speed"])

for m in PASSENGER_MODES:
    f = (df_trips["mode"] == m) & f_valid
    data["mean_speed_%s" % m] = df_trips[f]["speed"].mean()

f = df_trips["mode"].isin(ROAD_MODES) & f_valid
data["mean_speed_road"] = df_trips[f]["speed"].mean()

f = df_trips["mode"].isin(PASSENGER_MODES) & f_valid
data["mean_speed_all"] = df_trips[f]["speed"].mean()

f = df_trips["mode"].isin(["walk", "bike"]) & f_valid
data["mean_speed_light"] = df_trips[f]["speed"].mean()

# Compare to reference trips (modal shifts)
if reference_trips_path is not None:
    # if reference_trips_path does not exist, it means the correct reference is a the phase 1 baseline scenario.
    # or the phase 2 naming convention was not followed and the baseline scenario has a different run name!
    # the following if else statements only deal with the first case, not the incorrect naming convention problem!
    if os.path.exists(reference_trips_path):
        reference_trips_path = reference_trips_path
        print("reference_trips_path =", reference_trips_path)
    else:
        if data["case"] == "lg" or data["case"] == "lu" or data["case"] == "sa":
            reference_trips_path = "%s/%s_%dX_25pct.trips.csv" % (phase1_path, data["case"], data["year"])
            print("reference_trips_path =", reference_trips_path)
        else:
            reference_trips_path = "%s/bl_%d_25pct.trips.csv" % (phase1_path, data["year"])
            print("reference_trips_path =", reference_trips_path)
    df_reference = pd.read_csv(reference_trips_path, sep = ";")
    df_reference = df_reference[df_reference["person_id"].isin(df_trips["person_id"].unique())]

    merge_slots = ["person_id", "person_trip_id", "mode"]
    df_merged = pd.merge(df_trips[merge_slots], df_reference[merge_slots], on = [
        "person_id", "person_trip_id"
    ], suffixes = ["_before", "_after"]).groupby(["mode_before", "mode_after"]).size().reset_index(name = "count")

    for m1 in MODES:
        for m2 in MODES:
            f = (df_merged["mode_before"] == m1) & (df_merged["mode_after"] == m2)

            if np.count_nonzero(f) > 0:
                data["shift_%s_%s" % (m1, m2)] = df_merged[f]["count"].values[-1]
            else:
                data["shift_%s_%s" % (m1, m2)] = 0.0

with open(output_path, "wb+") as f:
    pickle.dump(data, f)

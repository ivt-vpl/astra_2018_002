import matplotlib.pyplot as plt
import matplotlib.ticker as tck
import pandas as pd
import numpy as np
import palettable
import itertools
import geopandas as gpd
import subprocess as sp
import shutil
import shapely.geometry as geo
import sys

DISTANCES = (500, 1000, 2000, 5000, 10000)
DISTANCE_LABELS = ["500m", "1km", "2km", "5km", "10km"]

AREAS = ["ch", "sa", "lu", "lg"]

PASSENGER_MODES = ["car", "prav", "tav", "car_passenger", "pt", "bike", "walk"]
VEHICLE_MODES = ["car", "prav", "tav_customer", "tav_empty", "pt", "truck", "truck_av"]
ROAD_MODES = ["car", "prav", "tav"]
REFERENCE_MODES = ["car", "car_passenger", "pt", "bike", "walk"]

FLOW_CATEGORIES = ["all", "av", "motorway", "trunk", "primary", "14ms"]

def data(df, df_reference, scaling_factor):
    df = df.reset_index()

    # Rename everything, so we have:
    # - Areas: ch, sa, lu, lg
    # - Scenarios: baseline, pravA, pravB, tavA, tavB
    df = df[df["scenario"] != "B"]
    df.loc[df["case"] == "bl", "area"] = "ch"
    df.loc[df["scenario"] == "X", "scenario"] = "baseline"

    df.loc[(df["case"] == "prav") & (df["scenario"] == "A"), "scenario"] = "pravA"
    df.loc[(df["case"] == "prav") & (df["scenario"] == "C"), "scenario"] = "pravB"
    df.loc[(df["case"] == "prav") & (df["scenario"] == "D"), "scenario"] = "pravD"
    df.loc[(df["case"] == "prav") & (df["scenario"] == "E"), "scenario"] = "pravE"
    df.loc[df["case"] == "prav", "area"] = "ch"

    df.loc[df["scenario"] == "U", "scenario"] = "pravA"
    df.loc[df["scenario"] == "V", "scenario"] = "pravB"
    df.loc[df["scenario"] == "W", "scenario"] = "pravD"
    df.loc[df["scenario"] == "Y", "scenario"] = "pravE"
    df.loc[df["case"].isin(["sa", "lu", "lg"]), "area"] = df["case"]

    df.loc[df["scenario"] == "A", "scenario"] = "tavA"
    df.loc[df["scenario"] == "C", "scenario"] = "tavB"
    df.loc[df["scenario"] == "D", "scenario"] = "tavD"
    df.loc[df["scenario"] == "E", "scenario"] = "tavE"

    # Duplicate rows to have 2020 for prav and tav
    df_baseline = df[(df["scenario"] == "baseline") & (df["year"] == 2020)] # & (df["area"] != "ch")]
    df_duplicate = []

    for scenario in ["pravA", "pravB", "pravD", "pravE", "tavA", "tavB", "tavD", "tavE"]:
        df_scenario = df_baseline.copy()
        df_scenario["scenario"] = scenario
        df_duplicate.append(df_scenario)

    df = pd.concat([df] + df_duplicate)
    assert(not np.any(df["area"].isna()))

    # Add reference
    if not df_reference is None:
        df_reference = df_reference.copy()
        df_reference["area"] = df_reference["case"]
        df = pd.concat([df, df_reference], sort = True)

    # Define proper index
    df = df.drop(columns = "case").set_index(["area", "scenario", "year"], drop = True).sort_index()

    # PASSENGER DISTANCES
    for mode in ["car", "pt", "bike", "walk", "car_passenger"]:
        df["person_distance:%s" % mode] = df["distance_%s" % mode] / scaling_factor
        df["travel_time:%s" % mode] = df["travel_time_%s" % mode] / scaling_factor
        df["mean_speed:%s" % mode] = df["mean_speed_%s" % mode]

    df["person_distance:tav"] = df["distance_av"] / scaling_factor
    df["travel_time:tav"] = df["travel_time_av"] / scaling_factor
    df["mean_speed:tav"] = df["mean_speed_av"]

    df["person_distance:prav"] = (df["distance_prav3"] + df["distance_prav4"] + df["distance_prav5"]) / scaling_factor
    df["travel_time:prav"] = (df["travel_time_prav3"] + df["travel_time_prav4"] + df["travel_time_prav5"]) / scaling_factor
    df["mean_speed:prav"] = df["mean_speed_prav5"] # TODO: Maybe could be split up, but currently we only use prav5

    df["mean_speed:road"] = df["mean_speed_road"]
    df["mean_speed:all"] = df["mean_speed_all"]
    df["mean_speed:light"] = df["mean_speed_light"]

    # VEHICLE DISTANCES
    df["vehicle_distance:car"] = df["distance_car"] / scaling_factor
    df["vehicle_distance:prav"] = (df["distance_prav3"] + df["distance_prav4"] + df["distance_prav5"]) / scaling_factor
    df["vehicle_distance:truck"] = df["distance_truck"] / scaling_factor
    df["vehicle_distance:truck_av"] = df["distance_truckAv"] / scaling_factor
    df["vehicle_distance:pt"] = df["pt_bus_distance"] # Only bus! No Scaling!
    df["vehicle_distance:tav_customer"] = df["av_customer_distance"] / scaling_factor
    df["vehicle_distance:tav_empty"] = df["av_empty_distance"] / scaling_factor

    # Make sure we always report the same vehicle distance (there can be variations due to cutting)
    df["vehicle_distance:pt"] = df.groupby(by = "area").first()["vehicle_distance:pt"].reindex(df.index, level = "area")

    # TRIPS
    for mode in ["car", "pt", "bike", "walk", "car_passenger"]:
        df["trips:%s" % mode] = df["trips_%s" % mode] / scaling_factor

    df["trips:truck"] = df["trips_truck"] / scaling_factor
    df["trips:tav"] = df["trips_av"] / scaling_factor
    df["trips:truck_av"] = df["trips_truckAv"] / scaling_factor
    df["trips:prav"] = (df["trips_prav3"] + df["trips_prav4"] + df["trips_prav5"]) / scaling_factor

    # TRIPS BY DISTANCE
    for distance in DISTANCES:
        for mode in ["car", "pt", "bike", "walk", "car_passenger"]:
            df["trips_by_distance:%s:%d" % (mode, distance)] = df["trips_%s_%d" % (mode, distance)] / scaling_factor

        df["trips_by_distance:tav:%d" % distance] = df["trips_av_%d" % distance] / scaling_factor

        df["trips_by_distance:prav:%d" % distance] = sum([
            df["trips_prav3_%d" % distance],
            df["trips_prav4_%d" % distance],
            df["trips_prav5_%d" % distance]
        ]) / scaling_factor

    # AV SERVICE
    df["av:waiting_time:q10"] = df["q10_waiting_time"]
    df["av:waiting_time:q90"] = df["q90_waiting_time"]
    df["av:waiting_time:mean"] = df["mean_waiting_time"]
    df["av:waiting_time:median"] = df["median_waiting_time"]
    df["av:fleet_size"] = df["taxi_fleet_size"] / scaling_factor
    df["av:price"] = df["taxi_price"]
    df["av:trips"] = df["av_customer_trips"]
    df["av:empty_share"] = df["av_empty_distance"] / (df["av_empty_distance"] + df["av_customer_distance"])
    df["av:request_to_vehicle_ratio"] = df["av:trips"] / df["av:fleet_size"]
    df["av:unscaled_fleet_size"] = df["taxi_fleet_size"]

    df = df.sort_values(by = "av:trips", na_position = "first")
    df["av:is_maximum_demand"] = ~df.index.duplicated(keep = "last")

    # SHIFT
    for source_mode in ["car", "pt", "bike", "walk", "car_passenger", "av"]:
        for target_mode in ["car", "pt", "bike", "walk", "car_passenger", "av"]:
            df["shift:%s>%s" % (source_mode, target_mode)] = df["shift_%s_%s" % (target_mode, source_mode)] / scaling_factor

    for mode in ["car", "pt", "bike", "walk", "car_passenger", "av"]:
        df["shift:prav>%s" % mode] = (df["shift_%s_prav3" % mode] + df["shift_%s_prav4" % mode] + df["shift_%s_prav5" % mode]) / scaling_factor
        df["shift:%s>prav" % mode] = (df["shift_prav3_%s" % mode] + df["shift_prav4_%s" % mode] + df["shift_prav5_%s" % mode]) / scaling_factor

    df["shift:prav>prav"] = sum([
        df["shift_prav%d_prav%d" % (i,j)]
        for i, j in zip([3, 4, 5], [3, 4, 5])
    ]) / scaling_factor

    df.columns = [c.replace("shift:av>", "shift:tav>").replace(">av", ">tav") for c in df.columns]

    # SCENARIO
    df["scenario:number_of_persons"] = df.groupby(by = df.index.names).first()["scenario_number_of_persons"].reindex(df.index) / scaling_factor
    df["scenario:number_of_persons_over_18"] = df.groupby(by = df.index.names).first()["scenario_number_of_persons_over_18"].reindex(df.index) / scaling_factor
    df["scenario:average_age"] = df.groupby(by = df.index.names).first()["scenario_average_age"].reindex(df.index)
    df["scenario:car_availability"] = df["scenario_car_availability"]

    # Consolidate columns
    df = df[[
        c for c in df.columns
        if c.startswith("vehicle_distance:") or c.startswith("person_distance:") or c.startswith("trips:")
        or c.startswith("travel_time:") or c.startswith("trips_by_distance:") or c.startswith("shift:") or c.startswith("mean_speed:")
        or c.startswith("av:") or c.startswith("scenario:") or c.startswith("person_speed:")
    ] + ["run_name"]]

    df["run_name"] = df["run_name"].fillna("")

    return df.sort_index()

def general_path_provider(suffix, flow_path, area, scenario, year, run_name, fleet_size = None):
    postfix = ""

    if fleet_size == 0:
        fleet_size = None

    if area == "ch":
        if scenario == "baseline" or year == 2020:
            prefix = "bl"
            scenario = ""
        elif scenario in ("pravA", "pravB", "pravD", "pravE" ):
            prefix = "prav"
            scenario = dict(pravA = "A", pravB = "C", pravD = "D", pravE = "E")[scenario]
    else:
        prefix = area

        if scenario == "baseline" or year == 2020:
            scenario = "X"
        elif scenario in ("pravA", "pravB", "pravD", "pravE") or fleet_size is None:
            scenario = dict(pravA = "U", pravB = "V", pravD = "W", pravE = "Y", tavA = "U", tavB = "V", tavD = "W", tavE = "Y")[scenario]
        elif scenario in ("tavA", "tavB", "tavD", "tavE"):
            scenario = dict(tavA = "A", tavB = "C", tavD = "D", tavE = "E")[scenario]

        if not fleet_size is None:
            postfix = "_shared_%d_0" % fleet_size

    run_name = "" if (run_name is None or run_name == "") else "_%s" % run_name

    print((flow_path, prefix, year, scenario, run_name, postfix, suffix))
    print("%s/%s_%d%s_25pct%s%s.%s" % (flow_path, prefix, year, scenario, run_name, postfix, suffix))
    return "%s/%s_%d%s_25pct%s%s.%s" % (flow_path, prefix, year, scenario, run_name, postfix, suffix)

def flow_path_provider(flow_path, area, scenario, run_name, year, fleet_size = None):
    return general_path_provider("flow.shp", flow_path, area, scenario, year, run_name, fleet_size)

def speed_path_provider(flow_path, area, scenario, run_name, year, fleet_size = None):
    return general_path_provider("speeds.csv", flow_path, area, scenario, year, run_name, fleet_size)

def mode_share_by_year(df):
    df = df[["trips:%s" % m for m in PASSENGER_MODES]].copy()
    df["trips:total"] = sum([df["trips:%s" % m] for m in PASSENGER_MODES])

    for mode in PASSENGER_MODES:
        df["share:%s" % mode] = df["trips:%s" % mode] / df["trips:total"]

    df["trips:light"] = df["trips:bike"] + df["trips:walk"]
    df["share:light"] = df["share:bike"] + df["share:walk"]

    return df

def mode_share_by_distance(df):
    df = df[["trips_by_distance:%s:%d" % (m, d) for m, d in itertools.product(PASSENGER_MODES, DISTANCES)]].copy()

    df = df.reset_index().melt(["area", "scenario", "year"])
    df["mode"] = df["variable"].str.split(":").apply(lambda x: x[1])
    df["distance_class"] = df["variable"].str.split(":").apply(lambda x: x[2]).astype(np.int)
    df["trips"] = df["value"].fillna(0.0)

    df = df = df.set_index(["area", "scenario", "year", "distance_class", "mode"])[["trips"]]
    df = df.pivot_table(index = ["area", "scenario", "year", "distance_class"], columns = ["mode"], values = "trips")
    df = df.rename(columns = { m: "trips:%s" % m for m in PASSENGER_MODES })

    df["trips:total"] = sum([df["trips:%s" % m] for m in PASSENGER_MODES])

    for mode in PASSENGER_MODES:
        df["share:%s" % mode] = df["trips:%s" % mode] / df["trips:total"]

    df["share:light"] = df["share:bike"] + df["share:walk"]

    return df.sort_index()

def mean_distance_travel_time_and_speed_by_year(df):
    df = df[
        ["trips:%s" % m for m in PASSENGER_MODES] +
        ["travel_time:%s" % m for m in PASSENGER_MODES] +
        ["person_distance:%s" % m for m in PASSENGER_MODES] +
        ["mean_speed:%s" % m for m in PASSENGER_MODES] +
        ["mean_speed:road", "mean_speed:all", "mean_speed:light"]
    ].copy()

    df["travel_time:all"] = 0.0
    df["travel_time:road"] = 0.0
    df["trips:all"] = 0.0
    df["trips:road"] = 0.0
    df["distance:all"] = 0.0
    df["distance:road"] = 0.0

    for mode in PASSENGER_MODES:
        df["travel_time:all"] += df["travel_time:%s" % mode]
        df["trips:all"] += df["trips:%s" % mode]
        df["distance:all"] += df["person_distance:%s" % mode]

        if mode in ROAD_MODES:
            df["travel_time:road"] += df["travel_time:%s" % mode]
            df["trips:road"] += df["trips:%s" % mode]
            df["distance:road"] += df["person_distance:%s" % mode]

    df["travel_time:light"] = df["travel_time:walk"] + df["travel_time:bike"]
    df["distance:light"] = df["person_distance:walk"] + df["person_distance:bike"]
    df["trips:light"] = df["trips:walk"] + df["trips:bike"]

    for mode in PASSENGER_MODES:
        df["travel_time:%s" % mode] = df["travel_time:%s" % mode] / df["trips:%s" % mode]
        df["distance:%s" % mode] = df["person_distance:%s" % mode] / df["trips:%s" % mode]
        df["speed:%s" % mode] = df["distance:%s" % mode] / df["travel_time:%s" % mode]

    df["travel_time:all"] /= df["trips:all"]
    df["distance:all"] /= df["trips:all"]
    df["speed:all"] = df["distance:all"] / df["travel_time:all"]

    df["travel_time:road"] /= df["trips:road"]
    df["distance:road"] /= df["trips:road"]
    df["speed:road"] = df["distance:road"] / df["travel_time:road"]

    df["travel_time:light"] /= df["trips:light"]
    df["distance:light"] /= df["trips:light"]
    df["speed:light"] = df["distance:light"] / df["travel_time:light"]

    return df

def distance_by_year(df, prefix):
    modes = PASSENGER_MODES if prefix == "person" else VEHICLE_MODES

    df = df[["%s_distance:%s" % (prefix, m) for m in modes]].copy()
    df["%s_distance:total" % prefix] = sum([df["%s_distance:%s" % (prefix, m)] for m in modes])

    for mode in modes:
        df["%s_distance_share:%s" % (prefix, mode)] = df["%s_distance:%s" % (prefix, mode)] / df["%s_distance:total" % prefix]

    if prefix == "person":
        df["%s_distance:light" % prefix] = df["%s_distance:bike" % prefix] + df["%s_distance:walk" % prefix]
        df["%s_distance_share:light" % prefix] = df["%s_distance_share:bike" % prefix] + df["%s_distance_share:walk" % prefix]

    return df

def modal_shift_by_year(df, target_mode = None, source_mode = None):
    if target_mode is None and source_mode is not None:
        template = "shift:%s>%%s" % source_mode
        inverse_template = "shift:%%s>%s" % source_mode
    elif source_mode is None and target_mode is not None:
        template = "shift:%%s>%s" % target_mode
        inverse_template = "shift:%s>%%s" % target_mode
    else:
        raise RuntimeError("Either target or source mode must be given")

    columns = [template % m for m in PASSENGER_MODES] + [inverse_template % m for m in PASSENGER_MODES]
    df = df[columns].copy()

    for mode in PASSENGER_MODES:
        df["shift:%s" % mode] = 0
        df["shift:%s" % mode] = df[template % mode] - df[inverse_template % mode]

    df["shift:light"] = df["shift:bike"] + df["shift:walk"]

    return df

def scenario_info(df):
    df = df[~df.index.duplicated(keep = "first")]
    df = df[[c for c in df.columns if c.startswith("scenario:")]]
    return df

def reference(persons_path, trips_path, shapes_path, scaling_factor):
    df_persons = pd.read_csv(persons_path, sep = ";")
    df_persons = df_persons[["person_id", "person_weight"]]

    df_reference = pd.read_csv(trips_path, sep = ";")
    df_reference = pd.merge(df_reference, df_persons, on = "person_id")

    df_reference["network_distance"] *= 1e-3
    df_reference["crowfly_distance"] *= 1e-3

    df_reference = df_reference[df_reference["crowfly_distance"] > 0] # why? Why elimated these trips? Could they be recreational?

    scaling = scaling_factor * 8156286 / df_persons["person_weight"].sum()

    df_shapes = gpd.read_file(shapes_path)
    df_shapes.crs = {"init": "EPSG:2056"}
    df_shapes["scenario"] = df_shapes["scenario"].astype("category")

    df_od = pd.DataFrame(df_reference[["person_id", "trip_id", "origin_x", "destination_x", "origin_y", "destination_y"]], copy = True)
    df_od["origin_geometry"] = [geo.Point(*xy) for xy in zip(df_od["origin_x"], df_od["origin_y"])]
    df_od["destination_geometry"] = [geo.Point(*xy) for xy in zip(df_od["destination_x"], df_od["destination_y"])]
    df_od = gpd.GeoDataFrame(df_od, crs = {"init": "EPSG:2056"})

    df_od = df_od.set_geometry("origin_geometry")
    df_od = gpd.sjoin(df_od, df_shapes.rename({"scenario": "origin_scenario"}, axis = 1), op = "within")
    del df_od["index_right"]

    df_od = df_od.set_geometry("destination_geometry")
    df_od = gpd.sjoin(df_od, df_shapes.rename({"scenario": "destination_scenario"}, axis = 1), op = "within")
    del df_od["index_right"]

    df_od = df_od[["person_id", "trip_id", "origin_scenario", "destination_scenario"]]
    df_reference = pd.merge(df_reference, df_od, how = "left", on = ["person_id", "trip_id"])

    df_reference["case"] = ""

    for case in ("sa", "lu", "lg"):
        df_reference.loc[
            (df_reference["origin_scenario"] == case) & (df_reference["destination_scenario"] == case)
        , "case"] = case

    df_reference["distance_class"] = np.digitize(df_reference["crowfly_distance"], np.array(DISTANCES) * 1e-3)
    records = []

    for case in ("ch", "sa", "lu", "lg"):
        df_case = df_reference

        if not case == "ch":
            df_case = df_case[df_case["case"] == case]

        record = {}

        for mode in REFERENCE_MODES:
            f_mode = df_case["mode"] == mode

            record["distance_%s" % mode] = np.sum(
                df_case[f_mode]["network_distance"] * df_case[f_mode]["person_weight"]
            ) * scaling

            record["trips_%s" % mode] = np.sum(
                df_case[f_mode]["person_weight"]
            ) * scaling

            for k in range(len(DISTANCES)):
                record["trips_%s_%d" % (mode, DISTANCES[k])] = df_case[
                    f_mode & (df_case["distance_class"] == k)
                ]["person_weight"].sum() * scaling

        record["case"] = case
        record["scenario"] = "reference"
        record["year"] = 2020
        records.append(record)

    return pd.DataFrame.from_records(records)

def single_flow(flow_path, scaling_factor, period, area, scenario, year, fleet_size = None, df = None, run_name = None):
    if run_name is None and df is not None:
        df = df[df["av:is_maximum_demand"]]
        df = df[df.index.get_level_values("area") == area]
        df = df[df.index.get_level_values("scenario") == scenario]
        df = df[df.index.get_level_values("year") == year]
        assert(len(df) == 1)
        run_name = df["run_name"].values[0]

    if fleet_size is None and df is not None:
        df = df[df["av:is_maximum_demand"]]
        df = df[df.index.get_level_values("area") == area]
        df = df[df.index.get_level_values("scenario") == scenario]
        df = df[df.index.get_level_values("year") == year]
        assert(len(df) == 1)
        fleet_size = df["av:unscaled_fleet_size"].values[0]
        run_name = df["run_name"].values[0]

    path = flow_path_provider(flow_path, area = area, scenario = scenario, run_name = run_name, year = year, fleet_size = fleet_size)
    df = gpd.read_file(path)

    period_time = dict(am = 1.0, pm = 1.0, day = 24.0)

    df = df.rename(columns = {
        "capacity": "link:capacity",
        "freespeed": "link:freespeed",
        "osm_type": "link:osm"
    })

    df["link:length"] = df["geometry"].length

    df["count:conventional:car"] = df["%s_conv" % period] / scaling_factor
    df["count:conventional:truck"] = df["%s_truck" % period] / scaling_factor
    df["count:automated:car"] = df["%s_pass3" % period] / scaling_factor + df["%s_pass4" % period] / scaling_factor + df["%s_pass5" % period] / scaling_factor
    df["count:automated:truck"] = df["%s_truck5" % period] / scaling_factor
    df["count:automated:taxi"] = df["%s_taxi" % period] / scaling_factor

    # Fix truck calculation
    df["count:conventional:truck"] -= df["count:automated:truck"]

    df["count:vehicles"] = sum([
        df["count:conventional:car"], df["count:automated:car"],
        df["count:conventional:truck"], df["count:automated:truck"],
        df["count:automated:taxi"]
    ])

    df["count:pcus"] = df["%s_pcut" % period] / scaling_factor + df["%s_pcup" % period] / scaling_factor
    df["link:utilization"] = df["count:pcus"] / (df["link:capacity"] * period_time[period])

    if not "av_op" in df:
        df["link:av"] = False
    else:
        df["link:av"] = df["av_op"] == 1

    df["area"] = area
    df["scenario"] = scenario
    df["year"] = year
    df["fleet_size"] = fleet_size

    return df[[
        "link_id", "link:osm", "link:freespeed", "link:av", "link:capacity", "link:length",
        "count:conventional:car", "count:automated:car",
        "count:conventional:truck", "count:automated:truck",
        "count:automated:taxi",
        "count:vehicles", "count:pcus", "geometry", "link:utilization",
        "area", "scenario", "year", "fleet_size"
    ]].set_index("link_id")

def multiple_flows(df, flow_path, scaling_factor, period, cases):
    df = df[df["av:is_maximum_demand"]]
    df = df.reindex(pd.MultiIndex.from_tuples(cases, names = ("area", "scenario", "year")))
    df_result = []

    for index, series in df.iterrows():
        area, scenario, year = index
        fleet_size = series["av:unscaled_fleet_size"]
        run_name = series["run_name"]

        df_partial = single_flow(flow_path, scaling_factor, period, area, scenario, year, fleet_size, run_name = run_name)
        df_result.append(df_partial)

    return pd.concat(df_result)

def flow_by_category(df):
    df = df.set_index(["area", "scenario", "year"])
    df_result = []

    for group_index, df_group in df.groupby(df.index, as_index = True):
        df_group = _flow_by_category(df_group)

        for name, value in zip(df.index.names, group_index):
            df_group[name] = value

        df_group = df_group.set_index(df.index.names + ["category"])
        df_result.append(df_group)

    df_result = pd.concat(df_result)
    return df_result

def _flow_by_category(df):
    df = df.copy()

    df["category:all"] = np.ones((len(df),), dtype = np.bool)
    df["category:av"] = df["link:av"]
    df["category:motorway"] = df["link:osm"] == "motorway"
    df["category:trunk"] = df["link:osm"] == "trunk"
    df["category:primary"] = df["link:osm"] == "primary"
    df["category:14ms"] = df["link:freespeed"] <= 14

    df["weight"] = df["link:length"]
    df.loc[df["link:utilization"] == 0, "weight"] = 0.0

    records = []

    for category in FLOW_CATEGORIES:
        df_category = df[df["category:%s" % category]].copy()
        df_category["weight"] /= df_category["weight"].sum()

        utilization_mean = np.sum(df_category["link:utilization"] * df_category["weight"])

        f = df_category["link:utilization"] > 0.9
        utilization_90 = df_category[f]["weight"].sum() / np.maximum(1e-12, df_category["weight"].sum())

        record = {
            "category": category,
            "length": df_category["link:length"].sum(),
            "utilization:mean": utilization_mean,
            "utilization:90": utilization_90,
            "share:conventional_car": df_category["count:conventional:car"].sum() / np.maximum(1e-12, df_category["count:vehicles"].sum()),
            "share:automated_car": df_category["count:automated:car"].sum() / np.maximum(1e-12, df_category["count:vehicles"].sum()),
            "share:conventional_truck": df_category["count:conventional:truck"].sum() / np.maximum(1e-12, df_category["count:vehicles"].sum()),
            "share:automated_truck": df_category["count:automated:truck"].sum() / np.maximum(1e-12, df_category["count:vehicles"].sum()),
            "share:automated_taxi": df_category["count:automated:taxi"].sum() / np.maximum(1e-12, df_category["count:vehicles"].sum()),
        }

        records.append(record)

    df = pd.DataFrame.from_records(records)
    df["share:conventional"] = df["share:conventional_car"] + df["share:conventional_truck"]
    df["share:automated"] = df["share:automated_car"] + df["share:automated_truck"] + df["share:automated_taxi"]
    df["share:personal"] = df["share:conventional_car"] + df["share:automated_car"] + df["share:automated_taxi"]
    df["share:freight"] = df["share:conventional_truck"] + df["share:automated_truck"]

    return df

def speeds_by_year(speeds_path, area, scenario, year, fleet_size = None, df = None, run_name = None):
    if run_name is None and df is not None:
        df = df[df["av:is_maximum_demand"]]
        df = df[df.index.get_level_values("area") == area]
        df = df[df.index.get_level_values("scenario") == scenario]
        df = df[df.index.get_level_values("year") == year]
        assert(len(df) == 1)
        run_name = df["run_name"].values[0]

    if fleet_size is None and df is not None:
        df = df[df["av:is_maximum_demand"]]
        df = df[df.index.get_level_values("area") == area]
        df = df[df.index.get_level_values("scenario") == scenario]
        df = df[df.index.get_level_values("year") == year]
        assert(len(df) == 1)
        fleet_size = df["av:unscaled_fleet_size"].values[0]
        run_name = df["run_name"].values[0]

    path = speed_path_provider(speeds_path, area = area, scenario = scenario, run_name = run_name, year = year, fleet_size = fleet_size)
    df = pd.read_csv(path, sep = ";")

    df["time"] = (df["end_time"] + df["start_time"]) * 0.5

    df["area"] = area
    df["scenario"] = scenario
    df["run_name"] = run_name
    df["year"] = year
    df["speed"] = df["speed_median"]

    return df.set_index(["area", "scenario", "run_name", "year"])[["road_type", "time", "speed"]]

def multiple_speeds_by_year(df, speeds_path, cases):
    df = df[df["av:is_maximum_demand"]]
    df = df.reindex(pd.MultiIndex.from_tuples(cases, names = ("area", "scenario", "year")))
    df_result = []

    for index, series in df.iterrows():
        area, scenario, year = index
        fleet_size = series["av:unscaled_fleet_size"]
        run_name = series["run_name"]

        df_partial = speeds_by_year(speeds_path, area, scenario, year, fleet_size, run_name = run_name)
        df_result.append(df_partial)

    return pd.concat(df_result)

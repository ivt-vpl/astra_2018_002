import prepare

for area in ("sa",):
    for scenario in ("baseline", "pravA", "pravB", "tavA", "tavB"):
        for year in (2020, 2030, 2040, 2050):
            suffix = ""
            flow_path = ""

            path = prepare.general_path_provider(suffix, flow_path, area, scenario, year, fleet_size = None)
            print(area, scenario, year, path)

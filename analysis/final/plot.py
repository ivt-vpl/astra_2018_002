import matplotlib.pyplot as plt
import matplotlib.ticker as tck
import pandas as pd
import numpy as np
import palettable
import itertools
import geopandas as gpd
import subprocess as sp
import shutil
import shapely.geometry as geo
import sys

if len(sys.argv) == 1:
    INPUT_PATH = "/mnt/hgfs/ASTRA_2018_002_AFs_Schweiz/Output_From_MATSim/from_euler/data_from_euler/clarissa_astra1802/analysis/complete_sensitivies_copies_for_testing/"
    REPOSITORY_PATH = "/home/clivings/PycharmProjects/astra_2018_002"
    REFERENCE_PATH = "/mnt/hgfs/ASTRA_2018_002_AFs_Schweiz/Output_From_MATSim/from_euler/reference"
    OUTPUT_PATH = "/home/clivings/Documents/plotting_test"
    FILE_SUFFIX = "png"
else:
    INPUT_PATH = sys.argv[1]
    REPOSITORY_PATH = sys.argv[2]
    REFERENCE_PATH = sys.argv[3]
    OUTPUT_PATH = sys.argv[4]
    FILE_SUFFIX = sys.argv[5]

TEX_TO_IMAGE = False

INITIAL_MODES = ["car", "carpassenger", "pt", "walk", "bike", "prav", "truck", "truckAv", "av"]
SCALING_FACTOR = 4

DISTANCES = (500, 1000, 2000, 5000, 10000)
DISTANCE_LABELS = ["500m", "1km", "2km", "5km", "10km"]

TEMP_PATH = "%s/temp" % OUTPUT_PATH

YEARS = (2020, 2050)
AV_YEARS = (2050)

SCENARIOS = ("D", "E")
CASES = ("prav", "sa", "lu", "lg")

FLOW_PERIOD = "am"
SHAPES_PATH = "%s/gis/scenarios/scenarios.shp" % REPOSITORY_PATH

ROAD_COUNT_STATIONS_PATH = "%s/flow_analysis/messstellenverzeichnis.xlsx" % REPOSITORY_PATH
ROAD_COUNTS_PATH = "%s/flow_analysis/Jahresergebnisse-2017.xlsx" % REPOSITORY_PATH

REFERENCE_MODES = ["car", "pt", "bike", "walk", "car_passenger"]

FIGSIZE = (6.0, 3.0)

DICTIONARY = {
    "scenario_A": "Szenario A",
    "scenario_C": "Szenario B",
    "scenario_U": "Szenario A (nur AF-MIV)",
    "scenario_V": "Szenario B (nur AF-MIV)",
    "scenario_D": "Szenario A (100% AF)",
    "scenario_E": "Szenario B (100% AF)",
    "scenario_W": "Szenario A (100% AF)(nur AF-MIV)",
    "scenario_Y": "Szenario B (100% AF)(nur AF-MIV)",
    "scenario_X": "Grundzustand",
    "car": "MIV",
    "carpassenger": "Mitfahrer",
    "pt": "ÖV",
    "light": "LV",
    "prav": "AF-MIV",
    "truck": "GV",
    "truckAv": "AF-GV",
    "av": "TAF",
    "av_customer": "TAF",
    "av_empty": "TAFL",
    "passenger_distance": "Personendistanz",
    "vehicle_distance": "Fahrzeugdistanz",
    "bike": "Fahrrad",
    "walk": "Zu Fuss",
    "all": "Gesamt",
    "road": "Strasse",
    "road_category_all": "Alle",
    "road_category_av": "Automatisiert",
    "road_category_motorway": "Autobahn",
    "road_category_trunk": "Autostrasse",
    "road_category_primary": "Hauptstrasse",
    "road_category_14ms": "14 m/s"
}

COLORS = palettable.colorbrewer.qualitative.Set2_6.mpl_colors

cmap = palettable.colorbrewer.qualitative.Paired_11.mpl_colors
MODE_COLORS = {
    "car": cmap[0],
    "carpassenger": cmap[3],
    "car_passenger": cmap[3],
    "pt": cmap[2],
    "light": cmap[4],
    "prav": cmap[1],
    "truck": cmap[8],
    "truckAv": cmap[9],
    "av": cmap[6],
    "av_customer": cmap[6],
    "av_empty": cmap[7],
    "bike": cmap[4],
    "walk": cmap[5],
    "all": "k",
    "road": "k"
}

SCENARIO_COLORS = {
    "A": cmap[0],
    "C": cmap[2],
    "D": cmap[0],
    "E": cmap[2]
}


def get_modes(case, purpose):
    if purpose in ("passenger", "shift"):
        modes = ["car", "prav", "carpassenger", "pt", "light"]

        if not case in ("bl", "prav", "ref"):
            modes.insert(modes.index("prav") + 1, "av")

        if purpose == "shift":
            modes.remove("carpassenger")

        return modes
    elif purpose == "vehicle":
        modes = ["car", "prav", "pt", "truck", "truckAv"]

        if not case in ("bl", "prav", "ref"):
            modes.insert(modes.index("prav") + 1, "av")

        return modes
    else:
        raise RuntimeError()


def get_flow_path(df, case, scenario, run_name, year):
    if case == "bl" and run_name is not "":
        name = "%s_%d_25pct_%s" % (case, year, run_name)
    elif case == "bl" and run_name is "":
        name = "%s_%d_25pct" % (case, year)
    elif case == "prav" and run_name is not "":
        name = "%s_%d%s_25pct_%s" % (case, year, scenario, run_name)
    elif case == "prav" and run_name is "":
        name = "%s_%d%s_25pct" % (case, year, scenario)
    elif run_name is not "":
        fleet_size = get_fleet_size(df, case, scenario, run_name, year)
        name = "%s_%d%s_25pct_%s_shared_%d_0" % (case, year, scenario, run_name, fleet_size)
    else:
        fleet_size = get_fleet_size(df, case, scenario, run_name, year)
        name = "%s_%d%s_25pct_shared_%d_0" % (case, year, scenario, fleet_size)

    return "%s/%s.flow.shp" % (INPUT_PATH, name)


def prepare_data(df):
    df_reference = get_reference()
    df = pd.concat([df, df_reference], sort=True)

    df.columns = [c.replace("car_passenger", "carpassenger") for c in df.columns]

    # df["pt_vehicle_distance"] = sum([
    #    df["pt_tram_distance"], df["pt_rail_distance"], df["pt_bus_distance"]
    # ])
    df["pt_vehicle_distance"] = df["pt_bus_distance"]
    df["pt_vehicle_distance"] /= SCALING_FACTOR

    for case in ("sa", "lg", "lu"):
        f = df["case"] == case
        distance = df[f]["pt_vehicle_distance"].dropna().values[0]
        df.loc[f, "pt_vehicle_distance"] = distance

    distance = df[df["case"] == "bl"]["pt_vehicle_distance"].dropna().values[0]
    df.loc[df["case"] == "bl", "pt_vehicle_distance"] = distance
    df.loc[df["case"] == "prav", "pt_vehicle_distance"] = distance

    df["trips_prav"] = df["trips_prav3"] + df["trips_prav4"] + df["trips_prav5"]
    df["distance_prav"] = df["distance_prav3"] + df["distance_prav4"] + df["distance_prav5"]
    df["travel_time_prav"] = df["travel_time_prav3"] + df["travel_time_prav4"] + df["travel_time_prav5"]

    df["trips_light"] = df["trips_walk"] + df["trips_bike"]
    df["distance_light"] = df["distance_walk"] + df["distance_bike"]
    df["travel_time_light"] = df["travel_time_walk"] + df["travel_time_bike"]

    for d in DISTANCES:
        df["trips_prav_%d" % d] = df["trips_prav3_%d" % d] + df["trips_prav4_%d" % d] + df["trips_prav5_%d" % d]
        df["trips_light_%d" % d] = df["trips_walk_%d" % d] + df["trips_bike_%d" % d]

    for mode in INITIAL_MODES:
        if not "prav" in mode:
            df["shift_%s_prav" % mode] = df["shift_%s_prav3" % mode] + df["shift_%s_prav4" % mode] + df[
                "shift_%s_prav5" % mode]
            df["shift_prav_%s" % mode] = df["shift_prav3_%s" % mode] + df["shift_prav4_%s" % mode] + df[
                "shift_prav5_%s" % mode]

        if not mode in ("walk", "bike"):
            df["shift_%s_light" % mode] = df["shift_%s_walk" % mode] + df["shift_%s_bike" % mode]
            df["shift_light_%s" % mode] = df["shift_walk_%s" % mode] + df["shift_bike_%s" % mode]

    df["shift_prav_prav"] = 0

    for source_mode in ("prav3", "prav4", "prav5"):
        for target_mode in ("prav3", "prav4", "prav5"):
            df["shift_prav_prav"] += df["shift_%s_%s" % (source_mode, target_mode)]

    df["shift_light_light"] = 0

    for source_mode in ("walk", "bike"):
        for target_mode in ("walk", "bike"):
            df["shift_light_light"] += df["shift_%s_%s" % (source_mode, target_mode)]

    duplicates = []

    for case in ("sa", "lg", "lu"):
        df_baseline = df[(df["scenario"] == "X") & (df["year"] == 2020) & (df["case"] == case)]

        for scenario in ["A", "C", "D", "E", "U", "V", "W", "Y"]:
            df_duplicate = pd.DataFrame(df_baseline, copy=True)
            df_duplicate["scenario"] = scenario
            duplicates.append(df_duplicate)

    df_baseline = df[(df["scenario"] == "X") & (df["year"] == 2020) & (df["case"] == "bl")]

    for scenario in SCENARIOS:
        df_duplicate = pd.DataFrame(df_baseline, copy=True)
        df_duplicate["case"] = "prav"
        df_duplicate["scenario"] = scenario
        duplicates.append(df_duplicate)

    df = pd.concat([df] + duplicates)

    return df


def get_fleet_size(df, case, scenario, year):
    return df[
        (df["case"] == case) & (df["scenario"] == scenario) & (df["year"] == year)
        ].sort_values(by="trips_av")["taxi_fleet_size"].values[-1]


def get_case_filter(df, case):
    if case in ("bl", "prav") or case.startswith("ref"):
        return df["case"] == case
    else:
        f = np.zeros((len(df),), dtype=np.bool)

        for scenario in SCENARIOS:
            for year in YEARS:
                f_local = (df["scenario"] == scenario) & (df["case"] == case) & (df["year"] == year)
                f_local &= df["taxi_fleet_size"] == get_fleet_size(df, case, scenario, year)
                f |= f_local

        return f


def get_scenario_filter(df, case, scenario):
    if case in ("sa", "lu", "lg") and scenario in ("X", "U", "V", "W", "Y"):
        return (df["scenario"] == scenario) & (df["case"] == case)

    return get_case_filter(df, case) & (df["scenario"] == scenario)


def prepare_total_mode_share_by_year(df, case, scenario, modes=None):
    df = df[get_scenario_filter(df, case, scenario)]

    if modes is None:
        modes = get_modes(case, "passenger")

    df = pd.DataFrame(df[
                          ["year"] + ["trips_%s" % m for m in modes]
                          ].set_index("year").reindex(YEARS).fillna(0), copy=True).reset_index()

    df = df.melt(["year"])
    df["mode"] = df["variable"].apply(lambda x: x.split("_")[1])
    df["trips"] = df["value"]

    df_sum = df[["year", "trips"]].groupby("year").sum().reset_index()
    df_sum["total"] = df_sum["trips"]

    df = pd.merge(df, df_sum[["year", "total"]], on="year")
    df["share"] = df["trips"] / df["total"]

    return df[["year", "mode", "trips", "share"]]


def prepare_mean_distance_and_travel_time_by_year(df, case, scenario, modes=None):
    df = df[get_scenario_filter(df, case, scenario)]

    if modes is None:
        modes = get_modes(case, "passenger")

    df = pd.DataFrame(df[
                          ["year"] + ["trips_%s" % m for m in modes] + ["travel_time_%s" % m for m in modes] + [
                              "distance_%s" % m for m in modes]
                          ].set_index("year").reindex(YEARS).fillna(0), copy=True).reset_index()

    df["travel_time_all"] = 0
    df["distance_all"] = 0
    df["trips_all"] = 0

    df["travel_time_road"] = 0
    df["distance_road"] = 0
    df["trips_road"] = 0

    for mode in modes:
        df["travel_time_all"] += df["travel_time_%s" % mode]
        df["distance_all"] += df["distance_%s" % mode]
        df["trips_all"] += df["trips_%s" % mode]

        if mode in ("car", "prav", "av"):
            df["travel_time_road"] += df["travel_time_%s" % mode]
            df["distance_road"] += df["distance_%s" % mode]
            df["trips_road"] += df["trips_%s" % mode]

    for mode in list(modes) + ["all", "road"]:
        df["travel_time__%s" % mode] = df["travel_time_%s" % mode] / df["trips_%s" % mode]
        df["distance__%s" % mode] = df["distance_%s" % mode] / df["trips_%s" % mode]

    df = df[
        ["year"] + ["distance__%s" % m for m in modes + ["all", "road"]] + ["travel_time__%s" % m for m in
                                                                            modes + ["all", "road"]]
        ]

    df = df.melt(["year"])
    df["mode"] = df["variable"].apply(lambda x: x.split("__")[1])
    df["variable"] = df["variable"].apply(lambda x: x.split("__")[0])

    df = df.set_index(["year", "mode"]).pivot(columns="variable")
    df.columns = ["distance", "travel_time"]
    df = df.reset_index()

    return df


def prepare_mode_share_by_distance(df, case, scenario, year, modes=None):
    df = df[get_scenario_filter(df, case, scenario)]
    df = df[df["year"] == year]

    if modes is None:
        modes = get_modes(case, "passenger")

    product = itertools.product(modes, DISTANCES)

    df = pd.DataFrame(df[[
        "trips_%s_%d" % p for p in product
    ]], copy=True)

    df = df.melt()
    df["mode"] = df["variable"].apply(lambda x: x.split("_")[1])
    df["distance"] = df["variable"].apply(lambda x: x.split("_")[2]).astype(np.int)
    df["trips"] = df["value"]

    df_sum = df[["distance", "trips"]].groupby("distance").sum().reset_index()
    df_sum["total"] = df_sum["trips"]

    df = pd.merge(df, df_sum[["distance", "total"]], on=["distance"])
    df["share"] = df["trips"] / df["total"]

    return df[["mode", "distance", "trips", "share"]]


def prepare_distance_by_year(df, case, scenario, type, modes=None):
    df = df[get_scenario_filter(df, case, scenario)]

    if modes is None:
        modes = get_modes(case, type)

    df = pd.DataFrame(df[
                          ["year"] + ["distance_%s" % m for m in modes] + ["av_customer_distance", "av_empty_distance",
                                                                           "pt_vehicle_distance"]
                          ].set_index("year").reindex(YEARS).fillna(0), copy=True).reset_index()
    df = df.rename(columns={"av_customer_distance": "distance_av_customer", "av_empty_distance": "distance_av_empty",
                            "pt_vehicle_distance": "distance_pt_vehicle"})

    df = df.melt(["year"])
    df["mode"] = df["variable"].apply(lambda x: x.replace("distance_", ""))
    df["distance"] = df["value"]

    if type == "vehicle":
        df = df[~(df["mode"] == "pt")]
        df.loc[df["mode"] == "pt_vehicle", "mode"] = "pt"
        df = df[~(df["mode"] == "av")]
    else:
        df = df[~(df["mode"] == "pt_vehicle")]
        df = df[~(df["mode"] == "av_customer")]
        df = df[~(df["mode"] == "av_empty")]

    df_sum = df[["year", "distance"]].groupby("year").sum().reset_index()
    df_sum["total"] = df_sum["distance"]

    df = pd.merge(df, df_sum[["year", "total"]], on="year")
    df["share"] = df["distance"] / df["total"]

    return df[["year", "mode", "distance", "share"]]


def prepare_modal_shift_by_year(df, case, scenario, target_mode):
    source_modes = get_modes(case, "shift")
    if target_mode in source_modes: source_modes.remove(target_mode)

    df = df[get_scenario_filter(df, case, scenario)]

    df = pd.DataFrame(df[
                          ["year"] + ["shift_%s_%s" % (m, target_mode) for m in source_modes] + [
                              "shift_%s_%s" % (target_mode, m) for m in source_modes]
                          ].set_index("year").reindex(YEARS).fillna(0), copy=True).reset_index()

    df = df.melt(["year"])
    df["source_mode"] = df["variable"].apply(lambda x: x.split("_")[2])
    df["target_mode"] = df["variable"].apply(lambda x: x.split("_")[1])

    df.loc[df["target_mode"] == target_mode, "trips"] = df["value"]
    df.loc[~(df["target_mode"] == target_mode), "trips"] = -df["value"]

    df.loc[df["target_mode"] == target_mode, "mode"] = df["source_mode"]
    df.loc[~(df["target_mode"] == target_mode), "mode"] = df["target_mode"]

    df = df[["year", "mode", "trips"]].groupby(["year", "mode"]).sum()
    df = df.reindex(pd.MultiIndex.from_tuples(itertools.product(YEARS, source_modes), names=["year", "mode"]))
    df = df.reset_index()

    df_sum = df[df["trips"] > 0][["year", "trips"]].groupby("year").sum().reset_index()
    df_sum["total_year"] = df_sum["trips"]

    df = pd.merge(df, df_sum[["year", "total_year"]], on="year")
    df["share_year"] = np.maximum(0, df["trips"]) / df["total_year"]

    return df[["year", "mode", "trips", "share_year"]]


def prepare_fleet_sizing(df, case, year):
    f = df["case"] == case
    f &= df["year"] == year
    df = pd.DataFrame(df[f], copy=True)

    df["trips"] = df["av_customer_trips"]
    df["empty_share"] = df["av_empty_distance"] / (df["av_empty_distance"] + df["av_customer_distance"])
    df["price"] = df["taxi_price"]
    df["waiting_time"] = df["median_waiting_time"]
    df["fleet_size"] = df["taxi_fleet_size"]

    return df[["scenario", "fleet_size", "trips", "empty_share", "price", "waiting_time"]]


def prepare_fleet_info(df, case):
    df = pd.DataFrame(df[get_case_filter(df, case)], copy=True)

    df["trips"] = df["av_customer_trips"]
    df["empty_share"] = df["av_empty_distance"] / (df["av_empty_distance"] + df["av_customer_distance"])
    df["price"] = df["taxi_price"]
    df["waiting_time"] = df["median_waiting_time"]
    df["fleet_size"] = df["taxi_fleet_size"]

    df = df[["scenario", "case", "year", "trips", "empty_share", "price", "waiting_time", "fleet_size"]]
    df = df.sort_values(by=["scenario", "year"])
    df = df[df["year"].isin(AV_YEARS)]

    return df


def prepare_flows(df, case, scenario, run_name, year, flow_period=None):
    df = gpd.read_file(get_flow_path(df, case, scenario, run_name, year))

    if flow_period is None:
        flow_period = FLOW_PERIOD

    df["length"] = df["geometry"].length

    df["count_conventional_car"] = df["%s_conv" % flow_period] * SCALING_FACTOR
    df["count_automated_car"] = df["%s_pass3" % flow_period] * SCALING_FACTOR + df[
        "%s_pass4" % flow_period] * SCALING_FACTOR + df["%s_pass5" % flow_period] * SCALING_FACTOR
    df["count_conventional_truck"] = df["%s_truck" % flow_period] * SCALING_FACTOR
    df["count_automated_truck"] = df["%s_truck5" % flow_period] * SCALING_FACTOR
    df["count_automated_taxi"] = df["%s_taxi" % flow_period] * SCALING_FACTOR

    df["count_vehicles"] = sum([
        df["count_conventional_car"], df["count_automated_car"],
        df["count_conventional_truck"], df["count_automated_truck"],
        df["count_automated_taxi"]
    ])

    df["pcu"] = df["%s_pcut" % flow_period] * SCALING_FACTOR + df["%s_pcup" % flow_period] * SCALING_FACTOR
    df["utilization"] = df["pcu"] / df["capacity"]

    if not "av_op" in df:
        df["av_allowed"] = False
    else:
        df["av_allowed"] = df["av_op"] == 1

    return df[[
        "link_id", "osm_type", "freespeed", "av_allowed", "capacity", "length",
        "count_conventional_car", "count_automated_car",
        "count_conventional_truck", "count_automated_truck",
        "count_automated_taxi",
        "count_vehicles", "pcu", "geometry", "utilization"
    ]]


def prepare_partial_flow_table(df, case, scenario, year):
    df = prepare_flows(df, case, scenario, year, flow_period="am")

    categories = ["all", "av", "motorway", "trunk", "primary", "14ms"]

    df["is_all"] = np.ones((len(df),), dtype=np.bool)
    df["is_av"] = df["av_allowed"]
    df["is_motorway"] = df["osm_type"] == "motorway"
    df["is_trunk"] = df["osm_type"] == "trunk"
    df["is_primary"] = df["osm_type"] == "primary"
    df["is_14ms"] = df["freespeed"] <= 14

    records = []

    for category in categories:
        df_category = pd.DataFrame(df[df["is_%s" % category]], copy=True)

        df_category["weight"] = df_category["length"]
        df_category.loc[df_category["utilization"] == 0, "weight"] = 0
        df_category["weight"] /= df_category["weight"].sum()

        utilization_mean = np.sum(df_category["utilization"] * df_category["weight"])

        f = df_category["utilization"] > 0.9
        utilization_90 = df_category[f]["weight"].sum() / np.maximum(1e-12, df_category["weight"].sum())

        records.append({
            "category": category,
            "length": df_category["length"].sum(),
            "utilization_mean": utilization_mean,
            "utilization_90": utilization_90,
            "share_conventional_car": df_category["count_conventional_car"].sum() / np.maximum(1e-12, df_category[
                "count_vehicles"].sum()),
            "share_automated_car": df_category["count_automated_car"].sum() / np.maximum(1e-12, df_category[
                "count_vehicles"].sum()),
            "share_conventional_truck": df_category["count_conventional_truck"].sum() / np.maximum(1e-12, df_category[
                "count_vehicles"].sum()),
            "share_automated_truck": df_category["count_automated_truck"].sum() / np.maximum(1e-12, df_category[
                "count_vehicles"].sum()),
            "share_automated_taxi": df_category["count_automated_taxi"].sum() / np.maximum(1e-12, df_category[
                "count_vehicles"].sum()),
        })

    df = pd.DataFrame.from_records(records)
    df["case"] = case
    df["scenario"] = scenario
    df["year"] = year

    df["share_conventional"] = df["share_conventional_car"] + df["share_conventional_truck"]
    df["share_automated"] = df["share_automated_car"] + df["share_automated_truck"] + df["share_automated_taxi"]
    df["share_passenger"] = df["share_conventional_car"] + df["share_automated_car"] + df["share_automated_taxi"]
    df["share_freight"] = df["share_conventional_truck"] + df["share_automated_truck"]

    return df


def plot_total_mode_share_by_year(df, case, baseline):
    figure = plt.figure()
    axes = []

    maximum = 0
    modes = get_modes(case, "passenger")

    if baseline:
        if case == "bl":
            cases = ["bl", "prav", "prav"]
            scenarios = ["X", "A", "C", "D", "E"]
        else:
            scenarios = ["X", "U", "V", "W", "Y"]
            cases = [case] * len(scenarios)

        if "av" in modes:
            modes.remove("av")
    else:
        scenarios = SCENARIOS[:]
        cases = [case] * len(scenarios)

    for scenario_index, (scenario, scenario_case) in enumerate(zip(scenarios, cases)):
        df_scenario = prepare_total_mode_share_by_year(df, scenario_case, scenario, modes=modes)
        df_scenario = df_scenario.sort_values(by="year")

        axes.append(figure.add_subplot(1, len(scenarios), scenario_index + 1))

        for mode_index, mode in enumerate(modes):
            values = df_scenario[df_scenario["mode"] == mode]["trips"].values * SCALING_FACTOR * 1e-6
            maximum = max(maximum, max(values))

            plt.bar(
                np.arange(len(YEARS)) * len(modes) + mode_index * 0.8, values,
                edgecolor="white", linewidth=0.5, width=0.8, align="edge",
                label=DICTIONARY[mode], color=MODE_COLORS[mode]
            )

        plt.gca().xaxis.set_major_locator(tck.FixedLocator(
            np.arange(len(YEARS)) * len(modes) + len(modes) * 0.5 * 0.8
        ))

        plt.gca().xaxis.set_major_formatter(tck.FixedFormatter(
            list(map(str, YEARS))
        ))

        plt.grid()
        plt.gca().set_axisbelow(True)
        plt.gca().xaxis.grid(alpha=0.0)

        plt.xlabel("Jahr")

        if scenario_index == 0:
            plt.ylabel("Reisen [Mio]")

        plt.title(DICTIONARY["scenario_%s" % scenario])

    for axis in axes:
        axis.set_ylim([0, maximum * 1.05])

    plt.tight_layout()
    figure.subplots_adjust(bottom=0.3)
    legend = axes[0].legend(ncol=4, loc="upper left", bbox_to_anchor=(0.0, -0.25))

    plt.savefig("%s/%s_total_mode_share%s.%s" % (OUTPUT_PATH, case, "_baseline" if baseline else "", FILE_SUFFIX))
    plt.close()


def plot_distance_by_year(df, case, type, baseline):
    figure = plt.figure()
    axes = []

    maximum = 0
    base_modes = get_modes(case, type)

    if baseline:
        if case == "bl":
            scenarios = ["X", "A", "C", "D", "E"]
            cases = ["bl", "prav", "prav"]
        else:
            scenarios = ["X", "U", "V", "W", "Y"]
            cases = [case] * len(scenarios)

        if "av" in base_modes:
            base_modes.remove("av")
    else:
        scenarios = SCENARIOS[:]
        cases = [case] * len(scenarios)

    modes = base_modes[:]

    if type == "vehicle" and "av" in modes:
        modes.remove("av")
        modes.append("av_customer")
        modes.append("av_empty")

    for scenario_index, (scenario, scenario_case) in enumerate(zip(scenarios, cases)):
        df_scenario = prepare_distance_by_year(df, scenario_case, scenario, type, modes=base_modes)
        df_scenario = df_scenario.sort_values(by="year")

        axes.append(figure.add_subplot(1, len(scenarios), scenario_index + 1))
        offset = np.zeros((len(YEARS),))

        for mode_index, mode in enumerate(modes):
            values = df_scenario[df_scenario["mode"] == mode]["distance"].values * SCALING_FACTOR * 1e-3

            plt.bar(
                np.arange(len(YEARS)), values,
                bottom=offset,
                edgecolor="white", linewidth=0.5, width=0.8, align="edge",
                label=DICTIONARY[mode], color=MODE_COLORS[mode]
            )

            offset += values

        plt.gca().xaxis.set_major_locator(tck.FixedLocator(
            np.arange(len(YEARS)) + 0.5 * 0.8
        ))

        plt.gca().xaxis.set_major_formatter(tck.FixedFormatter(
            list(map(str, YEARS))
        ))

        plt.grid()
        plt.gca().set_axisbelow(True)
        plt.gca().xaxis.grid(alpha=0.0)

        plt.xlabel("Jahr")

        if scenario_index == 0:
            plt.ylabel("%s [1000 km]" % DICTIONARY["%s_distance" % type])

        plt.title(DICTIONARY["scenario_%s" % scenario])
        maximum = max(maximum, max(offset))

    for axis in axes:
        axis.set_ylim([0, maximum * 1.05])

    plt.tight_layout()
    figure.subplots_adjust(bottom=0.3)
    legend = axes[0].legend(ncol=4, loc="upper left", bbox_to_anchor=(0.0, -0.25))

    plt.savefig("%s/%s_%s_distance%s.%s" % (OUTPUT_PATH, case, type, "_baseline" if baseline else "", FILE_SUFFIX))
    plt.close()


def plot_modal_shift_by_year(df, case):
    for shift_case in ("av", "pt"):
        figure = plt.figure()
        axes = []

        if shift_case == "pt":
            target_mode = "pt"
        elif case == "prav":
            target_mode = "prav"
        else:
            target_mode = "av"

        modes = get_modes(case, "shift")
        modes.remove(target_mode)
        maximum = 0
        minimum = 0

        for scenario_index, scenario in enumerate(SCENARIOS):
            df_scenario = prepare_modal_shift_by_year(df, case, scenario, target_mode)
            df_scenario = df_scenario.sort_values(by="year")

            axes.append(figure.add_subplot(1, 2, scenario_index + 1))

            for mode_index, mode in enumerate(modes):
                values = df_scenario[df_scenario["mode"] == mode]["trips"].values * SCALING_FACTOR * 1e-3
                maximum = max(maximum, max(values))
                minimum = min(minimum, min(values))

                plt.bar(
                    np.arange(len(AV_YEARS)) * len(modes) + mode_index * 0.8, values,
                    edgecolor="white", linewidth=0.5, width=0.8, align="edge",
                    label=DICTIONARY[mode], color=MODE_COLORS[mode]
                )

            plt.gca().xaxis.set_major_locator(tck.FixedLocator(
                np.arange(len(AV_YEARS)) * len(modes) + len(modes) * 0.5 * 0.8
            ))

            plt.gca().xaxis.set_major_formatter(tck.FixedFormatter(
                list(map(str, AV_YEARS))
            ))

            plt.grid()
            plt.gca().set_axisbelow(True)
            plt.gca().xaxis.grid(alpha=0.0)

            plt.xlabel("Jahr")

            if scenario_index == 0:
                plt.ylabel("Geänderte Reisen zu %s [x1000]" % DICTIONARY[target_mode])
                plt.legend(loc="best", ncol=2)

            plt.title(DICTIONARY["scenario_%s" % scenario])

        for axis in axes:
            axis.set_ylim([minimum * 1.05, maximum * 1.05])

        plt.tight_layout()
        plt.savefig("%s/%s_%s_mode_shift.%s" % (OUTPUT_PATH, case, shift_case, FILE_SUFFIX))
        plt.close()


def plot_fleet_sizing(df, case):
    for year in AV_YEARS:
        df_year = prepare_fleet_sizing(df, case, year)
        df_year = df_year.sort_values(by=["scenario", "fleet_size"])

        figsize = (FIGSIZE[0], FIGSIZE[1] * 1.4)
        plt.figure(figsize=figsize)

        centers = [
            df_year[df_year["scenario"] == scenario].sort_values(by="trips")["fleet_size"].values[-1]
            for scenario in SCENARIOS
        ]

        plt.subplot(2, 2, 1)  # Number of trips

        for scenario_index, scenario in enumerate(SCENARIOS):
            df_scenario = df_year[df_year["scenario"] == scenario]

            plt.plot(
                df_scenario["fleet_size"].values * SCALING_FACTOR * 1e-3,
                df_scenario["trips"].values * SCALING_FACTOR * 1e-3,
                linewidth=1.0, marker=".", markersize=5, color=COLORS[scenario_index]
            )

        plt.xlabel("Flottengrösse [1000 Fhz]")
        plt.ylabel("Reisen [x1000]")
        plt.grid()

        plt.subplot(2, 2, 2)  # Empty distance

        for scenario_index, scenario in enumerate(SCENARIOS):
            df_scenario = df_year[df_year["scenario"] == scenario]

            plt.plot(
                df_scenario["fleet_size"].values * SCALING_FACTOR * 1e-3,
                df_scenario["empty_share"].values * 1e2,
                linewidth=1.0, marker=".", markersize=5, color=COLORS[scenario_index]
            )

        plt.xlabel("Flottengrösse [1000 Fhz]")
        plt.ylabel("Leerdistanz [%]")
        plt.grid()

        plt.subplot(2, 2, 3)  # Waiting time

        for scenario_index, scenario in enumerate(SCENARIOS):
            df_scenario = df_year[df_year["scenario"] == scenario]

            plt.plot(
                df_scenario["fleet_size"].values * SCALING_FACTOR * 1e-3,
                df_scenario["waiting_time"].values / 60,
                linewidth=1.0, marker=".", markersize=5, color=COLORS[scenario_index],
                label=DICTIONARY["scenario_%s" % scenario]
            )

        plt.ylim([0, 15])

        plt.xlabel("Flottengrösse [1000 Fhz]")
        plt.ylabel("Median Wartezeit [min]")
        plt.legend(loc="best")
        plt.grid()

        plt.subplot(2, 2, 4)  # Price

        for scenario_index, scenario in enumerate(SCENARIOS):
            df_scenario = df_year[df_year["scenario"] == scenario]

            plt.plot(
                df_scenario["fleet_size"].values * SCALING_FACTOR * 1e-3,
                df_scenario["price"].values,
                linewidth=1.0, marker=".", markersize=5, color=COLORS[scenario_index]
            )

        plt.ylim([0, 1])

        plt.xlabel("Flottengrösse [1000 Fhz]")
        plt.ylabel("Preis [CHF/km]")
        plt.grid()

        plt.tight_layout()
        plt.savefig("%s/%s_%d_fleet_sizing.%s" % (OUTPUT_PATH, case, year, FILE_SUFFIX))
        plt.close()


def plot_mode_share_by_distance(df, case):
    figure = plt.figure(figsize=(FIGSIZE[0], FIGSIZE[1] * 2.5))
    axes = []

    maximum = 0
    modes = get_modes(case, "passenger")

    for year_index, year in enumerate(YEARS):
        for scenario_index, scenario in enumerate(SCENARIOS):
            df_scenario = prepare_mode_share_by_distance(df, case, scenario, year)
            df_scenario = df_scenario.sort_values(by="distance")

            offset = np.zeros((len(DISTANCES),))
            axes.append(plt.subplot(
                len(YEARS), len(SCENARIOS), year_index * len(SCENARIOS) + scenario_index + 1
            ))

            for mode_index, mode in enumerate(modes):
                values = df_scenario[
                    df_scenario["mode"] == mode
                    ]["share"].values  # * SCALING_FACTOR * 1e-3

                plt.bar(
                    np.arange(len(DISTANCES)), values,
                    bottom=offset,
                    edgecolor="white", linewidth=0.5, width=0.8, align="edge",
                    label=DICTIONARY[mode], color=MODE_COLORS[mode]
                )

                offset += values

            if year_index == 3:
                plt.gca().xaxis.set_major_locator(tck.FixedLocator(
                    np.arange(len(DISTANCES)) + 0.5 * 0.8
                ))

                plt.gca().xaxis.set_major_formatter(tck.FixedFormatter(
                    DISTANCE_LABELS
                ))

                plt.xlabel("Distanzklasse")
            else:
                plt.gca().xaxis.set_major_formatter(tck.FuncFormatter(
                    lambda x, p: ""
                ))

            plt.grid()
            plt.gca().set_axisbelow(True)
            plt.gca().xaxis.grid(alpha=0.0)

            if scenario_index == 1 and year_index == 0:
                pass  # plt.legend(loc = "lower right", ncol = 2)

            if scenario_index == 1:
                plt.gca().yaxis.set_major_formatter(tck.FuncFormatter(
                    lambda x, p: ""
                ))
            else:
                plt.gca().yaxis.set_major_formatter(tck.FuncFormatter(
                    lambda x, p: "%d" % (100 * x,)
                ))
                plt.ylabel("Reisen [%]")

            plt.title("%s / %d" % (DICTIONARY["scenario_%s" % scenario], year))
            maximum = max(maximum, max(offset))

    for axis in axes:
        axis.set_ylim([0, maximum * 1.05])

    plt.tight_layout()
    figure.subplots_adjust(bottom=0.12)
    legend = axes[6].legend(ncol=4, loc="upper left", bbox_to_anchor=(0.0, -0.27))

    plt.savefig("%s/%s_mode_share_by_distance.%s" % (OUTPUT_PATH, case, FILE_SUFFIX))
    plt.close()


def make_fleet_info(df):
    df = pd.concat([prepare_fleet_info(df, case) for case in ("sa", "lg", "lu")])
    output = []

    if TEX_TO_IMAGE:
        output.append("\\documentclass{article}")
        output.append("\\usepackage[utf8]{inputenc}")
        output.append("\\usepackage[margin=0.5in]{geometry}")
        output.append("\\begin{document}")
        output.append("\\pagenumbering{gobble}")
        output.append("\\begin{table}")

    output.append("\\begin{tabular}{ll|llll|llll}")

    output.append(
        "       &          & Szenario A    &             &        &           & Szenario B    &             &        &           \\\\")
    output.append(
        "       &          & Flottengrösse & Leerdistanz  & Preis  & Wartezeit & Flottengrösse & Leerdistanz& Preis & Wartezeit \\\\")
    output.append("Gebiet & Jahr     & [Fhz] & [\\%] & [CHF/km]  & [min] & [Fhz] & [\\%] & [CHF/km]  & [min] \\\\")
    output.append("\\hline")

    for case_index, case in enumerate(("sa", "lu", "lg")):
        for year_index, year in enumerate(AV_YEARS):
            row_output = []

            row_output.append(case if year_index == 0 else "")
            row_output.append(str(year))

            for scenario in SCENARIOS:
                f = (df["case"] == case) & (df["year"] == year) & (df["scenario"] == scenario)

                value_fleet_size = "%d" % (df[f]["fleet_size"].values[0] * SCALING_FACTOR,)
                value_empty_share = "%.2f" % (df[f]["empty_share"].values[0] * 100,)
                value_price = "%.2f" % df[f]["price"].values[0]
                value_waiting_time = "%.2f" % (df[f]["waiting_time"].values[0] / 60)

                row_output.append(value_fleet_size)
                row_output.append(value_empty_share)
                row_output.append(value_price)
                row_output.append(value_waiting_time)

            output.append(" & ".join(row_output) + "\\\\")

            if year_index == 2 and case_index != 2:
                output.append("\\hline")

    output.append("\\end{tabular}")

    if TEX_TO_IMAGE:
        output.append("\\end{table}")
        output.append("\\end{document}")

    output = "\n".join(output)

    if TEX_TO_IMAGE:
        with open("%s/table.tex" % TEMP_PATH, "w+") as f:
            f.write(output)

        sp.check_call(["pdflatex", "table.tex"], cwd=TEMP_PATH)
        sp.check_call(["pdfcrop", "table.pdf", "cropped.pdf"], cwd=TEMP_PATH)
        sp.check_call(
            ["convert", "-density", "300", "-background", "white", "-alpha", "remove", "-alpha", "off", "cropped.pdf",
             "cropped.png"], cwd=TEMP_PATH)

        shutil.copy("%s/cropped.png" % TEMP_PATH, "%s/fleet_info_table.%s" % (OUTPUT_PATH, FILE_SUFFIX))
    else:
        with open("%s/fleet_info_table.tex" % OUTPUT_PATH, "w+") as f:
            f.write(output)


def make_flow_table(df, case):
    df = pd.concat([
        prepare_partial_flow_table(df, case, scenario, year)
        for scenario, year in itertools.product(SCENARIOS, AV_YEARS)
    ]).sort_values(by=["case", "year"])

    output = []

    if TEX_TO_IMAGE:
        output.append("\\documentclass{article}")
        output.append("\\usepackage[utf8]{inputenc}")
        output.append("\\usepackage[margin=0.5in]{geometry}")
        output.append("\\begin{document}")
        output.append("\\pagenumbering{gobble}")
        output.append("\\begin{table}")

    output.append("\\begin{tabular}{lll|lllllll|llll}")

    output.append(" & ".join(
        ["         ", "        ", "    ", "Auslastung", "Stau"] + ["MIV", "A-MIV", "GV", "A-GV", "TAV", "Konventionell",
                                                                   "Automatisiert", "Personenverkehr",
                                                                   "Güterverkehr"]) + "\\\\")
    output.append(" & ".join(
        ["Kategorie", "Szenario", "Jahr", "[\\%]       ", "[\\%] "] + ["[\\%]", "[\\%]  ", "[\\%]", "[\\%]", "[\\%]",
                                                                       "[\\%]", "[\\%]", "[\\%]", "[\\%]"]) + "\\\\")
    output.append("\\hline")

    for category_index, category in enumerate(("all", "av", "motorway", "trunk", "primary", "14ms")):
        for scenario_index, scenario in enumerate(SCENARIOS):
            for year_index, year in enumerate(AV_YEARS):
                row_output = []

                row_output.append(
                    DICTIONARY["road_category_%s" % category] if scenario_index == 0 and year_index == 0 else "")
                row_output.append(DICTIONARY["scenario_%s" % scenario] if year_index == 0 else "")
                row_output.append(str(year))

                f = (df["category"] == category) & (df["year"] == year) & (df["scenario"] == scenario)

                # value_length = "%.2f" % (df[f]["length"] * 1e-6,)
                value_utilization_mean = "%.2f" % (df[f]["utilization_mean"].values[0] * 100,)
                value_utilization_90 = "%.2f" % (df[f]["utilization_90"].values[0] * 100,)
                value_conventional_car_share = "%.2f" % (df[f]["share_conventional_car"].values[0] * 100,)
                value_automated_car_share = "%.2f" % (df[f]["share_automated_car"].values[0] * 100,)
                value_conventional_truck_share = "%.2f" % (df[f]["share_conventional_truck"].values[0] * 100,)
                value_automated_truck_share = "%.2f" % (df[f]["share_automated_truck"].values[0] * 100,)
                value_automated_taxi_share = "%.2f" % (df[f]["share_automated_taxi"].values[0] * 100,)

                value_conventional_share = "%.2f" % (df[f]["share_conventional"].values[0] * 100,)
                value_automated_share = "%.2f" % (df[f]["share_automated"].values[0] * 100,)
                value_passanger_share = "%.2f" % (df[f]["share_passenger"].values[0] * 100,)
                value_freight_share = "%.2f" % (df[f]["share_freight"].values[0] * 100,)

                # row_output.append(value_length)
                row_output.append(value_utilization_mean)
                row_output.append(value_utilization_90)
                row_output.append(value_conventional_car_share)
                row_output.append(value_automated_car_share)
                row_output.append(value_conventional_truck_share)
                row_output.append(value_automated_truck_share)
                row_output.append(value_automated_taxi_share)

                row_output.append(value_conventional_share)
                row_output.append(value_automated_share)
                row_output.append(value_passanger_share)
                row_output.append(value_freight_share)

                output.append(" & ".join(row_output) + "\\\\")

        if category_index < 5:
            output.append("\\hline")

    output.append("\\end{tabular}")

    if TEX_TO_IMAGE:
        output.append("\\end{table}")
        output.append("\\end{document}")

    output = "\n".join(output)

    if TEX_TO_IMAGE:
        with open("temp/table.tex", "w+") as f:
            f.write(output)

        sp.check_call(["pdflatex", "table.tex"], cwd="temp")
        sp.check_call(["pdfcrop", "table.pdf", "cropped.pdf"], cwd="temp")
        sp.check_call(
            ["convert", "-density", "300", "-background", "white", "-alpha", "remove", "-alpha", "off", "cropped.pdf",
             "cropped.png"], cwd="temp")

        shutil.copy("temp/cropped.png", "%s/%s_flow_table_table.%s" % (OUTPUT_PATH, case, FILE_SUFFIX))
    else:
        with open("%s/%s_flow_table_table.tex" % (OUTPUT_PATH, case), "w+") as f:
            f.write(output)


def plot_flow_comparison(df):
    df_baseline = prepare_flows(df, "bl", "X", 2020, flow_period="day")
    df_baseline = df_baseline[df_baseline["osm_type"] == "motorway"]

    for what in ["pcu", "count_vehicles"]:
        figure = plt.figure(figsize=(FIGSIZE[0], FIGSIZE[1] * 2.5))
        axes = []

        maximum = 0

        for year_index, year in enumerate(AV_YEARS):
            for scenario_index, scenario in enumerate(SCENARIOS):
                print(what, year, scenario)

                axes.append(figure.add_subplot(
                    len(AV_YEARS), len(SCENARIOS), len(SCENARIOS) * year_index + scenario_index + 1
                ))

                df_population = prepare_flows(df, "bl", scenario, year, flow_period="day")
                df_prav = prepare_flows(df, "prav", scenario, year, flow_period="day")

                df_population = pd.merge(df_population, df_baseline, on="link_id", suffixes=["_future", "_reference"])
                df_prav = pd.merge(df_prav, df_baseline, on="link_id", suffixes=["_future", "_reference"])

                maximum = max([
                    df_population[what + "_reference"].max() * 1e-3,
                    df_population[what + "_future"].max() * 1e-3,
                    df_prav[what + "_reference"].max() * 1e-3,
                    df_prav[what + "_future"].max() * 1e-3
                ])

                plt.plot(
                    df_population[what + "_reference"].values * 1e-3,
                    df_population[what + "_future"].values * 1e-3,
                    color=COLORS[0], marker=".", markersize=0.5,
                    linestyle="none", rasterized=True
                )

                plt.plot(
                    df_prav[what + "_reference"].values * 1e-3,
                    df_prav[what + "_future"].values * 1e-3,
                    color=COLORS[1], marker=".", markersize=0.5,
                    linestyle="none", rasterized=True
                )

                plt.plot([0, 1e6], [0, 1e6], "k--")
                plt.grid()

                if year_index == 0 and scenario_index == 0:
                    plt.plot([np.nan] * 2, [np.nan] * 2, color=COLORS[0], label="Bevölkerung")
                    plt.plot([np.nan] * 2, [np.nan] * 2, color=COLORS[1], label="Private AF")
                    plt.legend(loc="upper left")

                unit = "PCU" if what == "pcu" else "Fhz"

                plt.xlabel("Grundzustand 2020 [1000 %s]" % unit)
                plt.ylabel("Zukunftszustand %d [1000 %s]" % (year, unit))

        for axis in axes:
            axis.set_xlim([0, maximum * 1.05])
            axis.set_ylim([0, maximum * 1.05])

        plt.tight_layout()
        plt.savefig("%s/flow_comparison_%s.%s" % (OUTPUT_PATH, what, FILE_SUFFIX))
        plt.close()


def plot_utilization_scenario(df_base, case, year, scenario):
    df_shapes = gpd.read_file(SHAPES_PATH)
    df = prepare_flows(df_base, case, scenario, year, flow_period="am")

    minx, miny, maxx, maxy = df.total_bounds
    width, height = maxx - minx, maxy - miny
    ratio = width / height
    figsize = (FIGSIZE[0], FIGSIZE[0] / ratio)

    colors = palettable.colorbrewer.sequential.OrRd_5.mpl_colors
    cmap = palettable.colorbrewer.sequential.OrRd_5.mpl_colormap

    plt.figure(figsize=figsize)

    df.plot(
        ax=plt.gca(),
        column="utilization", cmap=cmap, linewidth=1.0,
        vmin=0.0, vmax=1.0
    )

    df_shapes.plot(
        ax=plt.gca(), color=(0.0, 0.0, 0.0, 0.0),
        linestyle="--", edgecolor="k", linewidth=0.5
    )

    for i in range(5):
        lower = i / 5
        upper = (i + 1) / 5

        plt.plot(
            [np.nan] * 2, [np.nan] * 2, color=colors[i],
            label="%.0f - %.0f" % (lower * 100, upper * 100)
        )

    plt.gca().xaxis.set_visible(False)
    plt.gca().yaxis.set_visible(False)
    plt.gca().margins(0)
    plt.gca().tick_params(left=False, labelleft=False, bottom=False, labelbottom=False)
    plt.gca().axis("off")

    plt.legend(loc="lower right", title="Auslastung [%]")

    if case in df_shapes["scenario"].values:
        minx, miny, maxx, maxy = df_shapes[df_shapes["scenario"] == case].total_bounds
        plt.xlim([minx, maxx])
        plt.ylim([miny, maxy])

    plt.tight_layout()
    plt.savefig("%s/utilization_%s_%d_%s.%s" % (OUTPUT_PATH, case, year, scenario, FILE_SUFFIX))


def plot_utilization(df_base):
    df_shapes = gpd.read_file(SHAPES_PATH)

    for case in ("prav", "sa", "lg", "lu"):
        for year in AV_YEARS:
            for scenario in SCENARIOS:
                plot_utilization_scenario(df_base, case, year, scenario)


def plot_pcu_change_scenario(df_base, reference_case, reference_scenario, reference_year, comparison_case,
                             comparison_scenario, comparison_year):
    df_shapes = gpd.read_file(SHAPES_PATH)

    df_reference = prepare_flows(df_base, reference_case, reference_scenario, reference_year, flow_period="am")
    df_comparison = prepare_flows(df_base, comparison_case, comparison_scenario, comparison_year, flow_period="am")

    df = pd.merge(df_comparison, df_reference, on="link_id", suffixes=["_comparison", "_reference"])
    df = gpd.GeoDataFrame(df[df["pcu_reference"] > 0])

    df["change"] = (df["pcu_comparison"] - df["pcu_reference"]) / df["pcu_reference"]
    df["geometry"] = df["geometry_comparison"]

    maximum_change = max([df["change"].max(), -df["change"].min()])
    maximum_change = 1

    minx, miny, maxx, maxy = df_comparison.total_bounds
    width, height = maxx - minx, maxy - miny
    ratio = width / height
    figsize = (FIGSIZE[0], FIGSIZE[0] / ratio)

    colors = palettable.colorbrewer.sequential.OrRd_5.mpl_colors
    cmap = palettable.colorbrewer.sequential.OrRd_5.mpl_colormap

    plt.figure(figsize=figsize)

    df.plot(
        ax=plt.gca(),
        column="change", cmap=cmap, linewidth=1.0,
        vmin=0, vmax=maximum_change
    )

    df_shapes.plot(
        ax=plt.gca(), color=(0.0, 0.0, 0.0, 0.0),
        linestyle="--", edgecolor="k", linewidth=0.5
    )

    for i in range(5):
        lower = i / 5
        upper = (i + 1) / 5

        plt.plot(
            [np.nan] * 2, [np.nan] * 2, color=colors[i],
            label="% 1.0f%% - % 1.0f%%" % (lower * 100, upper * 100)
        )

    plt.gca().xaxis.set_visible(False)
    plt.gca().yaxis.set_visible(False)
    plt.gca().margins(0)
    plt.gca().tick_params(left=False, labelleft=False, bottom=False, labelbottom=False)
    plt.gca().axis("off")

    plt.legend(loc="lower right", title="Zusätzliche PCUs")

    if comparison_case in df_shapes["scenario"].values:
        minx, miny, maxx, maxy = df_shapes[df_shapes["scenario"] == case].total_bounds
        plt.xlim([minx, maxx])
        plt.ylim([miny, maxy])

    plt.title("Baseline 2020 vs. %s %d %s" % (comparison_case, comparison_year, comparison_scenario))

    plt.tight_layout()
    plt.savefig("%s/utilization_change_%s_%d_%s_to_%s_%d_%s.%s" % (
    OUTPUT_PATH, reference_case, reference_year, reference_scenario, comparison_case, comparison_year,
    comparison_scenario, FILE_SUFFIX))


def get_reference():
    df_persons = pd.read_csv("%s/ch_persons.csv" % REFERENCE_PATH, sep=";")
    df_persons = df_persons[["person_id", "person_weight"]]

    df_reference = pd.read_csv("%s/ch_trips.csv" % REFERENCE_PATH, sep=";")
    df_reference = pd.merge(df_reference, df_persons, on="person_id")

    df_reference["network_distance"] *= 1e-3
    df_reference["crowfly_distance"] *= 1e-3

    df_reference = df_reference[df_reference["crowfly_distance"] > 0]

    scaling = 8e6 / df_persons["person_weight"].sum()

    df_shapes = gpd.read_file(SHAPES_PATH)
    df_shapes.crs = {"init": "EPSG:2056"}
    df_shapes["scenario"] = df_shapes["scenario"].astype("category")

    df_od = pd.DataFrame(
        df_reference[["person_id", "trip_id", "origin_x", "destination_x", "origin_y", "destination_y"]], copy=True)
    df_od["origin_geometry"] = [geo.Point(*xy) for xy in zip(df_od["origin_x"], df_od["origin_y"])]
    df_od["destination_geometry"] = [geo.Point(*xy) for xy in zip(df_od["destination_x"], df_od["destination_y"])]
    df_od = gpd.GeoDataFrame(df_od, crs={"init": "EPSG:2056"})

    df_od = df_od.set_geometry("origin_geometry")
    df_od = gpd.sjoin(df_od, df_shapes.rename({"scenario": "origin_scenario"}, axis=1), op="within")
    del df_od["index_right"]

    df_od = df_od.set_geometry("destination_geometry")
    df_od = gpd.sjoin(df_od, df_shapes.rename({"scenario": "destination_scenario"}, axis=1), op="within")
    del df_od["index_right"]

    df_od = df_od[["person_id", "trip_id", "origin_scenario", "destination_scenario"]]
    df_reference = pd.merge(df_reference, df_od, how="left", on=["person_id", "trip_id"])

    df_reference["case"] = ""

    for case in ("sa", "lu", "lg"):
        df_reference.loc[
            (df_reference["origin_scenario"] == case) & (df_reference["destination_scenario"] == case)
            , "case"] = case

    df_reference["distance_class"] = np.digitize(df_reference["crowfly_distance"], np.array(DISTANCES) * 1e-3)
    records = []

    for case in ("ch", "sa", "lu", "lg"):
        df_case = df_reference

        if not case == "ch":
            df_case = df_case[df_case["case"] == case]

        record = {}

        for mode in REFERENCE_MODES:
            f_mode = df_case["mode"] == mode

            record["distance_%s" % mode] = np.sum(
                df_case[f_mode]["network_distance"] * df_case[f_mode]["person_weight"]
            ) * scaling

            record["trips_%s" % mode] = np.sum(
                df_case[f_mode]["person_weight"]
            ) * scaling

            for k in range(len(DISTANCES)):
                record["trips_%s_%d" % (mode, DISTANCES[k])] = df_case[
                                                                   f_mode & (df_case["distance_class"] == k)
                                                                   ]["person_weight"].sum() * scaling

        record["case"] = "ref_" + case
        record["scenario"] = "X"
        record["year"] = 2020
        records.append(record)

    return pd.DataFrame.from_records(records)


def plot_reference_mode_share(df, baseline_case, reference_case):
    figure = plt.figure()
    modes = ("car", "carpassenger", "pt", "bike", "walk")

    df_baseline = prepare_total_mode_share_by_year(df, baseline_case, "X", modes=modes)
    df_reference = prepare_total_mode_share_by_year(df, reference_case, "X", modes=modes)

    df_baseline = df_baseline[df_baseline["year"] == 2020]
    df_reference = df_reference[df_reference["year"] == 2020]

    plt.subplot(1, 2, 1)

    for mode_index, mode in enumerate(modes):
        baseline_values = df_baseline[df_baseline["mode"] == mode]["trips"].values * SCALING_FACTOR * 1e-6
        reference_values = df_reference[df_reference["mode"] == mode]["trips"].values * 1e-6

        plt.bar(
            2 * mode_index, baseline_values,
            edgecolor="white", linewidth=0.5, width=0.8, align="edge",
            color=MODE_COLORS[mode]  # , label = DICTIONARY[mode]
        )

        plt.bar(
            2 * mode_index + 0.8, reference_values,
            linewidth=0.5, width=0.8, align="edge",
            edgecolor=MODE_COLORS[mode], color="white", hatch="//"
        )

    plt.gca().xaxis.set_major_locator(tck.FixedLocator(
        np.arange(len(modes)) * 2 + 1 * 0.8
    ))

    plt.gca().xaxis.set_major_formatter(tck.FixedFormatter(
        [DICTIONARY[m] for m in modes]
    ))

    plt.grid()
    plt.gca().set_axisbelow(True)
    plt.gca().xaxis.grid(alpha=0.0)

    plt.ylabel("Anzahl Reisen [Mio]")

    plt.bar([0], [np.nan], color=MODE_COLORS[modes[0]], label="Simulation")
    plt.bar([0], [np.nan], edgecolor=MODE_COLORS[modes[0]], color="white", label="Referenz", hatch="//")
    plt.legend(loc="best")

    plt.subplot(1, 2, 2)

    df_baseline = prepare_mode_share_by_distance(df, baseline_case, "X", 2020, modes=modes)
    df_reference = prepare_mode_share_by_distance(df, reference_case, "X", 2020, modes=modes)

    offset_baseline = np.zeros((len(DISTANCES),))
    offset_reference = np.zeros((len(DISTANCES),))

    for mode_index, mode in enumerate(modes):
        baseline_values = df_baseline[
            df_baseline["mode"] == mode
            ]["share"].values

        reference_values = df_reference[
            df_reference["mode"] == mode
            ]["share"].values

        plt.plot(
            DISTANCES, baseline_values,
            label=DICTIONARY[mode],
            color=MODE_COLORS[mode],
            marker=".", linestyle="-", markersize=3, linewidth=1
        )

        plt.plot(
            DISTANCES, reference_values,
            color=MODE_COLORS[mode],
            marker=".", linestyle="--", markersize=3, linewidth=1
        )

    plt.gca().xaxis.set_major_locator(tck.FixedLocator(np.arange(10) * 1e3 * 2 + 1000))
    plt.gca().xaxis.set_major_formatter(tck.FuncFormatter(lambda x, p: int(x * 1e-3)))
    plt.xlabel("Luftliniendistanz [km]")

    plt.gca().yaxis.set_major_locator(tck.FixedLocator(np.arange(11) * 0.1))
    plt.gca().yaxis.set_major_formatter(tck.FuncFormatter(lambda x, p: "%d%%" % (x * 100,)))
    plt.ylabel("Anteil Reisen")

    plt.grid()
    plt.gca().set_axisbelow(True)
    # plt.gca().xaxis.grid(alpha = 0.0)
    plt.legend(loc="best", ncol=2)

    plt.tight_layout()
    plt.savefig("%s/%s_reference_mode_share.%s" % (OUTPUT_PATH, baseline_case, FILE_SUFFIX))
    plt.close()


def plot_highway_count_comparison(df):
    import counts
    import scipy.optimize as opt

    df_reference = counts.read_reference_counts(ROAD_COUNT_STATIONS_PATH, ROAD_COUNTS_PATH)
    df_flow = prepare_flows(df, "bl", "X", "", 2020, flow_period="day")

    correction_factors = {
        "car": 0.914,
        "truck": 1.04,
        # "truck": 0.914,
        "both": 0.916
    }

    for what in ("car", "truck", "both"):
        if what == "car":
            print(what)
            df_reference["reference"] = df_reference["personal_count"]
            df_flow["simulation"] = df_flow["count_conventional_car"]
        elif what == "truck":
            print(what)
            df_reference["reference"] = df_reference["freight_count"]
            df_flow["simulation"] = df_flow["count_conventional_truck"]
        elif what == "both":
            print(what)
            df_reference["reference"] = df_reference["personal_count"] + df_reference["freight_count"]
            df_flow["simulation"] = df_flow["count_conventional_car"] + df_flow["count_conventional_truck"]
        else:
            raise RuntimeError()

        # This code can be used to calibrate the matching
        # if what == "truck":
        #    counts.fit(df_flow[df_flow["simulation"] > 0], df_reference[df_reference["reference"] > 0], 2.0, 0.6, 0.7, samples = 11)

        # continue

        # We need to add a factor of two, because ASTRA counts both directions
        df_match = counts.match(df_flow[df_flow["simulation"] > 0], df_reference[df_reference["reference"] > 0],
                                2.0 * correction_factors[what])[0]
        df_match["value"] = df_match["delta"] / df_match["reference"]

        df_match["is_too_high"] = df_match["value"] > 0.25
        df_match["is_too_low"] = df_match["value"] < -0.25
        df_match["is_good"] = ~(df_match["is_too_high"] | df_match["is_too_low"])

        plt.figure()

        plt.subplot(1, 2, 1)
        cmap = palettable.colorbrewer.diverging.RdBu_3.mpl_colors

        df_flow.plot(ax=plt.gca())#, color=(0.8, 0.8, 0.8), linewidth=0.5, zorder=-100)
        df_match[df_match["is_too_high"]].plot(ax=plt.gca(), color="red", marker="o", edgecolor="none", markersize=5)
        df_match[df_match["is_too_low"]].plot(ax=plt.gca(), color="blue", marker="o", edgecolor="none", markersize=5)
        df_match[df_match["is_good"]].plot(ax=plt.gca(), color="k", marker="o", edgecolor="none", markersize=5)

        plt.gca().xaxis.set_visible(False)
        plt.gca().yaxis.set_visible(False)
        plt.gca().margins(0)
        plt.gca().tick_params(left=False, labelleft=False, bottom=False, labelbottom=False)
        plt.gca().axis("off")

        plt.subplot(1, 2, 2)

        f = df_match["is_too_high"]
        plt.plot(df_match[f]["reference"].values, df_match[f]["simulation"].values, color=cmap[2], marker=".",
                 markersize=3, linestyle="none")
        f = df_match["is_too_low"]
        plt.plot(df_match[f]["reference"].values, df_match[f]["simulation"].values, color=cmap[0], marker=".",
                 markersize=3, linestyle="none")
        f = df_match["is_good"]
        plt.plot(df_match[f]["reference"].values, df_match[f]["simulation"].values, color="k", marker=".", markersize=3,
                 linestyle="none")

        maximum = max([df_match["reference"].max(), df_match["simulation"].max()]) * 1.05
        plt.plot([0, maximum], [0, maximum], 'k--', linewidth=1.0)

        plt.xlim([0, maximum])
        plt.ylim([0, maximum])

        plt.gca().xaxis.set_major_formatter(tck.FuncFormatter(lambda x, p: "%d" % (x * 1e-3,)))
        plt.gca().yaxis.set_major_formatter(tck.FuncFormatter(lambda x, p: "%d" % (x * 1e-3,)))

        plt.xlabel("Referenz [1000 Fhz]")
        plt.ylabel("Simulation [1000 Fhz]")

        plt.plot([np.nan] * 2, [np.nan] * 2, color=cmap[2], label="-20%")
        plt.plot([np.nan] * 2, [np.nan] * 2, color=cmap[0], label="+20%")
        plt.legend(loc="lower right")
        plt.grid()

        plt.tight_layout()
        plt.savefig("%s/count_matching_%s.%s" % (OUTPUT_PATH, what, FILE_SUFFIX))


def plot_mean_distance_and_travel_time_by_year(df, case):
    modes = get_modes(case, "passenger") + ["all", "road"]

    for what in ("travel_time", "distance"):
        figure = plt.figure()
        axes = []

        maximum = -np.inf
        minimum = np.inf

        for scenario_index, scenario in enumerate(SCENARIOS):
            df_scenario = prepare_mean_distance_and_travel_time_by_year(df, case, scenario)
            df_scenario = df_scenario.sort_values(by="year")

            axes.append(figure.add_subplot(1, 2, scenario_index + 1))

            scaling = 60 if what == "travel_time" else 1

            for mode_index, mode in enumerate(modes):
                values = df_scenario[df_scenario["mode"] == mode][what].values / scaling

                maximum = max(maximum, max(values))
                minimum = min(minimum, min(values))

                plt.plot(
                    np.arange(len(YEARS)), values,
                    color=MODE_COLORS[mode], linewidth=1,
                    linestyle=":" if mode == "road" else "-", marker=".", markersize=5,
                    label=DICTIONARY[mode]
                )

            plt.gca().xaxis.set_major_locator(tck.FixedLocator(
                np.arange(len(YEARS))
            ))

            plt.gca().xaxis.set_major_formatter(tck.FixedFormatter(
                list(map(str, YEARS))
            ))

            plt.grid()
            plt.gca().set_axisbelow(True)
            plt.gca().xaxis.grid(alpha=0.0)

            plt.xlabel("Jahr")

            if scenario_index == 0:
                plt.ylabel("Reisezeit pro Weg [min]" if what == "travel_time" else "Reisedistanz pro Weg [km]")
                # plt.legend(loc = "best", ncol = 2)

            plt.title(DICTIONARY["scenario_%s" % scenario])

        for axis in axes:
            axis.set_ylim([minimum * 0.95, maximum * 1.05])

        plt.tight_layout()
        figure.subplots_adjust(bottom=0.3)
        legend = axes[0].legend(ncol=4, loc="upper left", bbox_to_anchor=(0.0, -0.25))

        plt.savefig("%s/%s_mean_%s.%s" % (OUTPUT_PATH, case, what, FILE_SUFFIX))
        plt.close()


def prepare_scenario_info(df, case):
    if case == "bl":
        df = df[
            ((df["case"] == "bl") & (df["scenario"] == "X")) | (
                        (df["case"] == "prav") & (df["scenario"].isin(["A", "C", "D", "E"])))
            ]
    else:
        df = df[df["scenario"].isin(["X", "U", "V", "W", "Y"])]
        df = df[df["case"] == case]

    return df[[
        "case", "scenario", "year", "scenario_number_of_persons",
        "scenario_number_of_persons_over_18", "scenario_car_availability",
        "scenario_average_age"
    ]].rename(columns={
        "scenario_number_of_persons": "number_of_persons",
        "scenario_number_of_persons_over_18": "number_of_persons_over_18",
        "scenario_car_availability": "car_avaiability",
        "scenario_average_age": "average_age"
    }).sort_values(by=["scenario", "year"])


def make_scenario_info(df, case):
    df = prepare_scenario_info(df, case)
    df.loc[df["scenario"] == "U", "scenario"] = "A"
    df.loc[df["scenario"] == "V", "scenario"] = "C"
    df.loc[df["scenario"] == "W", "scenario"] = "D"
    df.loc[df["scenario"] == "Y", "scenario"] = "E"

    output = []

    output.append("\\begin{tabular}{ll|lll}")

    output.append(" & ".join(["    ", "        ", "Personen", "Durchschnittsalter", "PW Verfügbarkeit"]) + "\\\\")
    output.append(" & ".join(["Jahr", "Szenario", "        ", "[a]               ", "[\\%]           "]) + "\\\\")
    output.append("\\hline")

    for year_index, year in enumerate(YEARS):
        year_scenarios = ["X", "D", "E"]

        for scenario_index, scenario in enumerate(year_scenarios):
            row_output = []

            row_output.append(str(year) if scenario_index == 0 else "")
            row_output.append(DICTIONARY["scenario_%s" % scenario])

            f = (df["year"] == year) & (df["scenario"] == "X")
            print(f)
            value_number_of_persons = "%d" % (df[f]["number_of_persons"].values[0] * SCALING_FACTOR,)
            value_average_age = "%.2f" % (df[f]["average_age"].values[0],)

            f = (df["year"] == year) & (df["scenario"] == scenario)
            value_car_availability = "%.2f" % (df[f]["car_avaiability"].values[0] * 100,)

            row_output.append(value_number_of_persons if scenario_index == 0 else "")
            row_output.append(value_average_age if scenario_index == 0 else "")
            row_output.append(value_car_availability)

            output.append(" & ".join(row_output) + "\\\\")

        if year_index < 3:
            output.append("\\hline")

    output.append("\\end{tabular}")
    output = "\n".join(output)

    with open("%s/%s_region_info_table.tex" % (OUTPUT_PATH, case), "w+") as f:
        f.write(output)


# General plotting setup
font_size = 8
dpi = 300
figsize = FIGSIZE

plt.rc("font", family="serif", size=font_size)
plt.rc("figure", dpi=dpi, figsize=figsize)
plt.rc("legend", fontsize=font_size, loc="best", fancybox=False)
plt.rc("grid", linewidth=0.5)
plt.rc("patch", linewidth=0.5)

# Start plotting

df = pd.read_parquet("%s/aggregated.parquet" % INPUT_PATH)
df = prepare_data(df)

for case in ["bl", "sa", "lu", "lg"]:
    make_scenario_info(df, case)

for case in ["bl", "sa", "lu", "lg"]:
    plot_total_mode_share_by_year(df, case, True)
    plot_distance_by_year(df, case, "passenger", True)
    plot_distance_by_year(df, case, "vehicle", True)

for case in ["sa", "lu", "lg"]:
    plot_total_mode_share_by_year(df, case, False)
    plot_distance_by_year(df, case, "passenger", False)
    plot_distance_by_year(df, case, "vehicle", False)

plot_reference_mode_share(df, "bl", "ref_ch")
plot_reference_mode_share(df, "sa", "ref_sa")
plot_reference_mode_share(df, "lg", "ref_lg")
plot_reference_mode_share(df, "lu", "ref_lu")

plot_highway_count_comparison(df)

for case in CASES:
    plot_modal_shift_by_year(df, case)
    plot_mode_share_by_distance(df, case)
    make_flow_table(df, case)
    plot_mean_distance_and_travel_time_by_year(df, case)

    if not case == "prav":
        plot_fleet_sizing(df, case)

make_fleet_info(df)
plot_flow_comparison(df)
plot_utilization(df)

for year in AV_YEARS:
    plot_pcu_change_scenario(df, "bl", "X", 2020, "prav", "A", 2050)
    plot_pcu_change_scenario(df, "bl", "X", 2020, "prav", "C", 2050)

#

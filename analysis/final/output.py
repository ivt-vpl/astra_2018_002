import prepare, counts
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as tck
import palettable
import geopandas as gpd

SCENARIO_INDEX = ["baseline", "pravA", "pravB", "tavA", "tavB"]
YEAR_INDEX = [2020, 2030, 2040, 2050]

DICTIONARY = {
    "area": "Gebiet",
    "scenario": "Szenario",
    "year": "Jahr",

    "baseline": "Referenzszenario",
    "pravA": "A (nur A-MIV)",
    "pravB": "B (nur A-MIV)",
    "pravD": "A (AF 100%)(nur A-MIV)",
    "pravE": "B (AF 100%)(nur A-MIV)",
    "tavA": "A (mit AT)",
    "tavB": "B (mit AT)",
    "tavD": "A (AF 100%)(mit AT)",
    "tavE": "B (AF 100%)(mit AT)",


    "scenario:number_of_persons": "Einwohner",
    "scenario:average_age": "Durchschn. Alter",
    "scenario:car_availability": "PW Verfügbarkeit",

    "trips_M": "Wege [Mio]",
    "trips_1000": "Wege [x1000]",

    "car": "MIV",
    "pt": "ÖV",
    "bike": "Fahrrad",
    "walk": "Zu Fuss",
    "light": "LV",
    "prav": "A-MIV",
    "tav": "AT",
    "truck": "GV",
    "truck_av": "A-GV",
    "car_passenger": "Mitfahrer",
    "tav_customer": "AT",
    "tav_empty": "AT-L",
    "road": "Strasse",
    "all": "Gesamt",

    "person_distance": "Verkehrsleistung [Mio Pkm]",
    "vehicle_distance": "Fahrleistung (Strasse) [Mio Fzg-km]",

    "shift_to_prav": "Geänderte Wege zu A-MIV [1000]",
    "shift_to_tav": "Geänderte Wege zu AT [1000]",
    "shift_to_pt": "Geänderte Wege zu ÖV [1000]",

    "distance_class": "Distanzklasse",
    "trips_pct": "Wege [%]",

    "travel_time_per_trip_min": "Reisezeit pro Weg [min]",
    "travel_distance_per_trip_km": "Reisedistanz pro Weg [km]",
    "travel_speed_per_trip_km_h": "Durchschn. Reisegeschw. [km/h]",

    "fleet_size_1k_veh": "Flottengrösse [1000 Fhz]",
    "trips_1k": "Wege [x1000]",
    "empty_distance_pct": "Leerdistanz [%]",
    "median_waiting_time_min": "Median Wartezeit [min]",
    "price_chf_km": "Preis [CHF/km]",

    "simulation": "Simulation",
    "reference": "Referenz",

    "av:fleet_size": "Flottengrösse",
    "av:trips": "Wege",
    "av:waiting_time:median": "Median Wartezeit",
    "av:price": "Preis",
    "av:empty_share": "Leerdistanz",
    "av:request_to_vehicle_ratio": "Weg/Fahrzeug Verhältnis",

    "[veh]": "[Fzg]",

    "reference_1000vehs": "Referenz [1000 Fzg]",
    "simulation_1000vehs": "Simulation [1000 Fzg]",

    "category": "Strassenkategorie",
    "category:all": "Alle",
    "category:av": "Automatisiert",
    "category:motorway": "Autobahn",
    "category:trunk": "Autostrasse",
    "category:primary": "Hauptstrasse",
    "category:14ms": "< 14 m/s",

    "utilization:mean": "Auslastung",
    "utilization:90": "Stauw.",

    "share:conventional_car": "MIV",
    "share:automated_car": "A-MIV",
    "share:conventional_truck": "GV",
    "share:automated_truck": "A-GV",
    "share:automated_taxi": "AT",
    "share:conventional": "Konventionell",
    "share:automated": "Automatisiert",
    "share:personal": "Personenverkehr",
    "share:freight": "Güterverkehr",

    "base_case": "Basisszenario",
    "future_case": "Zukunftszenario",
    "unit_pcus": "PCU",
    "unit_vehs": "Fzg",

    "change_pcus": "Δ PCUs",
    "change_vehicles": "Δ Fzg",

    "time_of_day": "Uhrzeit",
    "speed_km_h": "Geschwindigkeit [km/h]",

    "road_type:motorway": "Autobahn",
    "road_type:trunk": "Autostrasse",
    "road_type:primary": "Hauptstrasse",
    "road_type:secondary": "Nebenstrasse (sekundär)",
    "road_type:tertiary": "Nebenstrasse (tertiär)",
    "road_type:residential": "Wohnstrasse"
}

DISTANCE_LABELS = ["500m", "1km", "2km", "5km", "10km"]

SORTER = {
    "area": ["ch", "sa", "lu", "lg"],
    "scenario": ["baseline", "pravA", "pravB", "pravD", "pravE", "tavA", "tavB", "tavD", "tavE"],
    "year": [2020, 2030, 2040, 2050]
}

cmap = palettable.colorbrewer.qualitative.Paired_12.mpl_colors
cmap = palettable.cubehelix.Cubehelix.make(
    start = 1,
    n = 10,
    min_light = 0.4,
    max_light = 0.9,
    rotation = 5.0,
    min_sat = 1.0, max_sat = 1.0
).mpl_colors

MODE_COLORS = {
    "car": cmap[0],
    "car_passenger": cmap[2],
    "prav": cmap[1],

    "pt": cmap[3],

    "light": cmap[4],
    "walk": cmap[4],
    "bike": cmap[5],

    "tav": cmap[6],
    "tav_customer": cmap[6],
    "tav_empty": cmap[7],

    "truck": cmap[8],
    "truck_av": cmap[9],

    "all": "k",
    "road": "k"
}

SCENARIO_COLORS = {
    "tavA": cmap[1],
    "tavB": cmap[7],
    "tavD": cmap[1],
    "tavE": cmap[7]
}

cmap = palettable.colorbrewer.qualitative.Set2_6.mpl_colors

FLOW_COLORS = {
    "population": cmap[0],
    "prav": cmap[1]
}

ROAD_COUNT_COLORS = palettable.colorbrewer.diverging.RdBu_3.mpl_colors

cmap = palettable.cartocolors.qualitative.Bold_4.mpl_colors
YEAR_COLORS = {
    2020: cmap[0],
    2030: cmap[1],
    2040: cmap[2],
    2050: cmap[3]
}

def lexicographical_sort(df, columns, default = "remove"):
    rank_columns = []

    for column, values in columns.items():
        if column in df:
            rank_column = "%s__rank" % column
            rank_columns.append(rank_column)

            df[rank_column] = len(values) + 1

            for value_index, value in enumerate(values):
                df.loc[df[column] == value, rank_column] = value_index

    return df.sort_values(by = rank_columns).drop(columns = rank_columns)

def translate(df):
    df = df.rename(columns = DICTIONARY, index = DICTIONARY)
    df.index = df.index.rename([DICTIONARY[name] if name in DICTIONARY else name for name in df.index.names])
    return df

def units(df, units):
    df.columns = pd.MultiIndex.from_tuples([pair for pair in zip(df.columns, units)])
    return df

def tabulate_scenario_info(df, area, scenarios, path):
    df = prepare.scenario_info(df).reset_index()
    df = df[df["scenario"].isin(scenarios)]

    df = df[df["area"] == area].drop(columns = ["scenario:number_of_persons_over_18", "area"])
    df["scenario:number_of_persons"] *= 1e-3
    df["scenario:car_availability"] *= 1e2

    df["scenario:average_age"] = df["scenario:average_age"].round(2)
    df["scenario:number_of_persons"] = df["scenario:number_of_persons"].round(2)
    df["scenario:car_availability"] = df["scenario:car_availability"].round(2)

    df = lexicographical_sort(df, SORTER).set_index(["scenario", "year"])
    df = translate(df)
    df = units(df, ("[1000]", "[a]", "[%]"))

    df.to_latex(path)
    df.to_csv("%s.csv" % path)

def filter_index(df, index, value):
    df = df.copy()
    df = df[df.index.get_level_values(index) == value]
    df.index = df.index.droplevel(index)
    return df

def initialize(figure_size = (6.0, 3.0), font_size = 8, dpi = 300):
    font_size = 8
    dpi = 300

    plt.rc("font", family = "sans", size = font_size)
    plt.rc("figure", dpi = dpi, figsize = figure_size)
    plt.rc("legend", fontsize = font_size, loc = "best", fancybox = False)
    plt.rc("grid", linewidth = 0.5)
    plt.rc("patch", linewidth = 0.5)

def plot_mode_share_by_year(df, area, scenarios, modes, path):
    df = df[df["av:is_maximum_demand"]]
    df = prepare.mode_share_by_year(df)
    df_area = filter_index(df, "area", area)

    figure = plt.figure()
    axes = []
    maximum = 0

    for scenario_index, scenario in enumerate(scenarios):
        df_scenario = filter_index(df_area, "scenario", scenario)
        axes.append(figure.add_subplot(1, len(scenarios), scenario_index + 1))

        for mode_index, mode in enumerate(modes):
            values = df_scenario["trips:%s" % mode].values * 1e-6
            maximum = max(maximum, max(values))

            plt.bar(
                np.arange(len(df_scenario)) * len(modes) + mode_index * 0.8, values,
                edgecolor = "white", linewidth = 0.5, width = 0.8, align = "edge",
                label = DICTIONARY[mode], color = MODE_COLORS[mode]
            )

        plt.gca().xaxis.set_major_locator(tck.FixedLocator(
            np.arange(len(df_scenario)) * len(modes) + len(modes) * 0.5 * 0.8
        ))

        plt.gca().xaxis.set_major_formatter(tck.FixedFormatter(
            list(map(str, df_scenario.index))
        ))

        plt.grid()
        plt.gca().set_axisbelow(True)
        plt.gca().xaxis.grid(alpha = 0.0)

        plt.xlabel(DICTIONARY["year"])

        if scenario_index == 0:
            plt.ylabel(DICTIONARY["trips_M"])
        else:
            plt.gca().yaxis.set_major_formatter(tck.FuncFormatter(lambda x,p: ""))

        plt.title(DICTIONARY[scenario])

    for axis in axes:
        axis.set_ylim([0, maximum * 1.05])

    plt.tight_layout()
    figure.subplots_adjust(bottom = 0.3)
    legend = axes[0].legend(ncol = 6, loc = "upper left", bbox_to_anchor = (0.0, -0.25))

    plt.savefig(path)
    plt.close()

def tabulate_mode_share_by_year(df, area, scenarios, modes, path):
    df = df[df["av:is_maximum_demand"]]
    df = prepare.mode_share_by_year(df)
    df = filter_index(df, "area", area)
    df = df[df.index.get_level_values("scenario").isin(scenarios)]

    for mode in modes:
        df[mode] = (df["share:%s" % mode] * 100).round(2)

    df = df[list(modes)]

    df = df.reset_index()
    df = lexicographical_sort(df, SORTER).set_index(["scenario", "year"])
    df = translate(df)
    df = units(df, ["[%]"] * len(df.columns))

    df.to_latex(path)
    df.to_csv("%s.csv" % path)

def plot_distance_by_year(df, area, scenarios, modes, prefix, path):
    df = df[df["av:is_maximum_demand"]]
    df = prepare.distance_by_year(df, prefix)
    df_area = filter_index(df, "area", area)

    figure = plt.figure()
    axes = []
    maximum = 0

    for scenario_index, scenario in enumerate(scenarios):
        df_scenario = filter_index(df_area, "scenario", scenario)

        axes.append(figure.add_subplot(1, len(scenarios), scenario_index + 1))
        offset = np.zeros((len(df_scenario),))

        for mode_index, mode in enumerate(modes):
            values = df_scenario["%s_distance:%s" % (prefix, mode)].values * 1e-6

            plt.bar(
                np.arange(len(df_scenario)), values,
                bottom = offset,
                edgecolor = "white", linewidth = 0.5, width = 0.8, align = "edge",
                label = DICTIONARY[mode], color = MODE_COLORS[mode]
            )

            offset += values

        plt.gca().xaxis.set_major_locator(tck.FixedLocator(
            np.arange(len(df_scenario)) + 0.5 * 0.8
        ))

        plt.gca().xaxis.set_major_formatter(tck.FixedFormatter(
            list(map(str, df_scenario.index))
        ))

        plt.grid()
        plt.gca().set_axisbelow(True)
        plt.gca().xaxis.grid(alpha = 0.0)

        plt.xlabel(DICTIONARY["year"])

        if scenario_index == 0:
            plt.ylabel(DICTIONARY["%s_distance" % prefix])
        else:
            plt.gca().yaxis.set_major_formatter(tck.FuncFormatter(lambda x,p: ""))

        plt.title(DICTIONARY[scenario])
        maximum = max(maximum, max(offset))

    for axis in axes:
        axis.set_ylim([0, maximum * 1.05])

    plt.tight_layout()
    figure.subplots_adjust(bottom = 0.3)
    legend = axes[0].legend(ncol = 6, loc = "upper left", bbox_to_anchor = (0.0, -0.25))

    plt.savefig(path)
    plt.close()

def tabulate_distance_by_year(df, area, scenarios, modes, prefix, path):
    df = df[df["av:is_maximum_demand"]]
    df = prepare.distance_by_year(df, prefix)
    df = filter_index(df, "area", area)
    df = df[df.index.get_level_values("scenario").isin(scenarios)]

    for mode in modes:
        df[mode] = (df["%s_distance:%s" % (prefix, mode)] * 1e-6).round(2)

    df = df[list(modes)]

    df = df.reset_index()
    df = lexicographical_sort(df, SORTER).set_index(["scenario", "year"])
    df = translate(df)
    df = units(df, ["[Mio km]"] * len(df.columns))

    df.to_latex(path)
    df.to_csv("%s.csv" % path)

def plot_modal_shift_by_year(df, area, scenarios, source_modes, target_mode, path):
    df = df[df["av:is_maximum_demand"]]
    df = df[df.index.get_level_values("year") > 2020]
    df = prepare.modal_shift_by_year(df, target_mode = target_mode)
    df_area = filter_index(df, "area", area)

    figure = plt.figure()
    axes = []

    maximum = 0
    minimum = 0

    for scenario_index, scenario in enumerate(scenarios):
        df_scenario = filter_index(df_area, "scenario", scenario)
        axes.append(figure.add_subplot(1, len(scenarios), scenario_index + 1))

        for mode_index, mode in enumerate(source_modes):
            values = df_scenario["shift:%s" % mode].values * 1e-3
            maximum = max(maximum, max(values))
            minimum = min(minimum, min(values))

            plt.bar(
                np.arange(len(df_scenario)) * len(source_modes) + mode_index * 0.8, values,
                edgecolor = "white", linewidth = 0.5, width = 0.8, align = "edge",
                label = DICTIONARY[mode], color = MODE_COLORS[mode]
            )

        plt.gca().xaxis.set_major_locator(tck.FixedLocator(
            np.arange(len(df_scenario)) * len(source_modes) + len(source_modes) * 0.5 * 0.8
        ))

        plt.gca().xaxis.set_major_formatter(tck.FixedFormatter(
            list(map(str, df_scenario.index))
        ))

        plt.grid()
        plt.gca().set_axisbelow(True)
        plt.gca().xaxis.grid(alpha = 0.0)

        plt.xlabel(DICTIONARY["year"])

        if scenario_index == 0:
            plt.ylabel(DICTIONARY["shift_to_%s" % target_mode])
            plt.legend(loc = "best", ncol = 2)
        else:
            plt.gca().yaxis.set_major_formatter(tck.FuncFormatter(lambda x,p: ""))

        plt.title(DICTIONARY[scenario])

    for axis in axes:
        axis.set_ylim([minimum * 1.05, maximum * 1.05])

    plt.tight_layout()
    figure.subplots_adjust(bottom = 0.3)
    legend = axes[0].legend(ncol = 6, loc = "upper left", bbox_to_anchor = (0.0, -0.25))

    plt.savefig(path)
    plt.close()

def plot_mode_share_by_distance(df, area, scenarios, modes, path):
    df = df[df["av:is_maximum_demand"]]
    df = prepare.mode_share_by_distance(df)
    df_area = filter_index(df, "area", area)

    figure = plt.figure(figsize = (6.0, 3.0 * 2.5))
    axes = []

    for year_index, year in enumerate(YEAR_INDEX):
        for scenario_index, scenario in enumerate(scenarios):
            df_scenario = filter_index(df_area, "scenario", scenario)
            df_scenario = filter_index(df_scenario, "year", year)

            offset = np.zeros((len(prepare.DISTANCES),))
            axes.append(plt.subplot(
                len(YEAR_INDEX), len(scenarios), year_index * len(scenarios) + scenario_index + 1
            ))

            for mode_index, mode in enumerate(modes):
                values = df_scenario["share:%s" % mode].values

                plt.bar(
                    np.arange(len(prepare.DISTANCES)), values,
                    bottom = offset,
                    edgecolor = "white", linewidth = 0.5, width = 0.8, align = "edge",
                    label = DICTIONARY[mode], color = MODE_COLORS[mode]
                )

                offset += values

            if year_index == len(YEAR_INDEX) - 1:
                plt.gca().xaxis.set_major_locator(tck.FixedLocator(
                    np.arange(len(prepare.DISTANCES)) + 0.5 * 0.8
                ))

                plt.gca().xaxis.set_major_formatter(tck.FixedFormatter(
                    DISTANCE_LABELS
                ))

                plt.xlabel(DICTIONARY["distance_class"])
            else:
                plt.gca().xaxis.set_major_formatter(tck.FuncFormatter(
                    lambda x, p: ""
                ))

            plt.grid()
            plt.gca().set_axisbelow(True)
            plt.gca().xaxis.grid(alpha = 0.0)
            plt.ylim([0, 1])

            if scenario_index == 1 and year_index == 0:
                pass #plt.legend(loc = "lower right", ncol = 2)

            if scenario_index > 0:
                plt.gca().yaxis.set_major_formatter(tck.FuncFormatter(
                    lambda x, p: ""
                ))
            else:
                plt.gca().yaxis.set_major_formatter(tck.FuncFormatter(
                    lambda x, p: "%d" % (100 * x,)
                ))
                plt.ylabel(DICTIONARY["trips_pct"])

            plt.title("%s / %d" % (DICTIONARY[scenario], year))

    plt.tight_layout()
    figure.subplots_adjust(bottom = 0.12)
    legend = axes[len(scenarios) * (len(YEAR_INDEX) - 1)].legend(ncol = 8, loc = "upper left", bbox_to_anchor = (0.0, -0.3))

    plt.savefig(path)
    plt.close()

def plot_mean_distance_travel_time_and_speed_by_year(df, area, scenarios, modes, slot, path):
    df = df[df["av:is_maximum_demand"]]
    df = prepare.mean_distance_travel_time_and_speed_by_year(df)
    df_area = filter_index(df, "area", area)

    figure = plt.figure()
    axes = []

    maximum = -np.inf
    minimum = np.inf

    for scenario_index, scenario in enumerate(scenarios):
        df_scenario = filter_index(df_area, "scenario", scenario)
        axes.append(figure.add_subplot(1, len(scenarios), scenario_index + 1))

        scaling = dict(travel_time = 60.0, distance = 1.0, speed = 1.0 / 3600.0, mean_speed = 1.0)[slot]

        for mode_index, mode in enumerate(modes):
            values = df_scenario["%s:%s" % (slot, mode)].values / scaling

            non_nan_values = values[~np.isnan(values)]
            if len(non_nan_values) > 0:
                maximum = max(maximum, np.max(non_nan_values))
                minimum = min(minimum, np.min(non_nan_values))

            markersize = 5
            if "av" in mode: markersize = 8

            plt.plot(
                np.arange(len(df_scenario)), values,
                color = MODE_COLORS[mode], linewidth = 1,
                linestyle = ":" if mode == "road" else "-", marker = ".", markersize = markersize,
                label = DICTIONARY[mode]
            )

        plt.gca().xaxis.set_major_locator(tck.FixedLocator(
            np.arange(len(df_scenario))
        ))

        plt.gca().xaxis.set_major_formatter(tck.FixedFormatter(
            list(map(str, df_scenario.index))
        ))

        plt.grid()
        plt.gca().set_axisbelow(True)
        plt.gca().xaxis.grid(alpha = 0.0)

        plt.xlabel(DICTIONARY["year"])

        if scenario_index == 0:
            label = dict(travel_time = "travel_time_per_trip_min", distance = "travel_distance_per_trip_km", speed = "travel_speed_per_trip_km_h", mean_speed = "travel_speed_per_trip_km_h")[slot]
            plt.ylabel(DICTIONARY[label])
        else:
            plt.gca().yaxis.set_major_formatter(tck.FuncFormatter(lambda x, p: ""))

        plt.title(DICTIONARY[scenario])

    for axis in axes:
        axis.set_ylim([minimum * 0.95, maximum * 1.05])

    plt.tight_layout()
    figure.subplots_adjust(bottom = 0.3)
    legend = axes[0].legend(ncol = 4, loc = "upper left", bbox_to_anchor = (0.0, -0.25))

    plt.savefig(path)
    plt.close()

def plot_fleet_sizing(df, area, year, scenarios, path):
    df_area = filter_index(df, "area", area)

    df_year = filter_index(df_area, "year", year)
    df_year = df_year.sort_values(by = ["scenario", "av:fleet_size"])

    figsize = (6.0, 3.0 * 1.4)
    plt.figure(figsize = figsize)

    plt.subplot(2, 2, 1) # Number of trips

    for scenario_index, scenario in enumerate(scenarios):
        df_scenario = df_year[df_year.index.get_level_values("scenario") == scenario]

        plt.plot(
            df_scenario["av:fleet_size"].values * 1e-3,
            df_scenario["trips:tav"].values * 1e-3,
            linewidth = 1.0, marker = ".", markersize = 5, color = SCENARIO_COLORS[scenario]
        )

    plt.xlabel(DICTIONARY["fleet_size_1k_veh"])
    plt.ylabel(DICTIONARY["trips_1k"])
    plt.grid()

    plt.subplot(2, 2, 2) # Empty distance

    for scenario_index, scenario in enumerate(scenarios):
        df_scenario = df_year[df_year.index.get_level_values("scenario") == scenario]

        plt.plot(
            df_scenario["av:fleet_size"].values * 1e-3,
            df_scenario["av:empty_share"].values * 1e2,
            linewidth = 1.0, marker = ".", markersize = 5, color = SCENARIO_COLORS[scenario]
        )

    plt.xlabel(DICTIONARY["fleet_size_1k_veh"])
    plt.ylabel(DICTIONARY["empty_distance_pct"])
    plt.grid()

    plt.subplot(2, 2, 3) # Waiting time

    for scenario_index, scenario in enumerate(scenarios):
        df_scenario = df_year[df_year.index.get_level_values("scenario") == scenario]

        plt.plot(
            df_scenario["av:fleet_size"].values * 1e-3,
            df_scenario["av:waiting_time:median"].values / 60,
            linewidth = 1.0, marker = ".", markersize = 5, color = SCENARIO_COLORS[scenario],
            label = DICTIONARY[scenario]
        )

    plt.ylim([0, 15])

    plt.xlabel(DICTIONARY["fleet_size_1k_veh"])
    plt.ylabel(DICTIONARY["median_waiting_time_min"])
    plt.legend(loc = "best")
    plt.grid()

    plt.subplot(2, 2, 4) # Price

    for scenario_index, scenario in enumerate(scenarios):
        df_scenario = df_year[df_year.index.get_level_values("scenario") == scenario]

        plt.plot(
            df_scenario["av:fleet_size"].values * 1e-3,
            df_scenario["av:price"].values,
            linewidth = 1.0, marker = ".", markersize = 5, color = SCENARIO_COLORS[scenario]
        )

    plt.ylim([0, 1])

    plt.xlabel(DICTIONARY["fleet_size_1k_veh"])
    plt.ylabel(DICTIONARY["price_chf_km"])
    plt.grid()

    plt.tight_layout()
    plt.savefig(path)
    plt.close()

def get_trips_scaling(df, modes):
    maximum = np.max(df[["trips:%s" % mode for mode in modes]].values)

    if maximum > 1e6:
        return 1e-6, "trips_M"
    else:
        return 1e-3, "trips_1000"

def plot_reference_mode_share(df, area, modes, path):
    df = df[df["av:is_maximum_demand"]]

    df_share = prepare.mode_share_by_year(df)
    df_share = filter_index(df_share, "area", area)
    df_share = filter_index(df_share, "year", 2020)
    df_baseline = df_share[df_share.index == "baseline"]
    df_reference = df_share[df_share.index == "reference"]

    figure = plt.figure()

    plt.subplot(1, 2, 1)

    scaling_factor, scaling_label = get_trips_scaling(df_baseline, modes)

    for mode_index, mode in enumerate(modes):
        baseline_values = df_baseline["trips:%s" % mode].values * scaling_factor
        reference_values = df_reference["trips:%s" % mode].values * scaling_factor

        plt.bar(
            2 * mode_index, baseline_values,
            edgecolor = "white", linewidth = 0.5, width = 0.8, align = "edge",
            color = MODE_COLORS[mode] #, label = DICTIONARY[mode]
        )

        plt.bar(
            2 * mode_index + 0.8, reference_values,
            linewidth = 0.5, width = 0.8, align = "edge",
            edgecolor = MODE_COLORS[mode], color = "white", hatch = "//"
        )

    plt.gca().xaxis.set_major_locator(tck.FixedLocator(
        np.arange(len(modes)) * 2 + 1 * 0.8
    ))

    plt.gca().xaxis.set_major_formatter(tck.FixedFormatter(
        [DICTIONARY[m] for m in modes]
    ))

    plt.grid()
    plt.gca().set_axisbelow(True)
    plt.gca().xaxis.grid(alpha = 0.0)

    plt.ylabel(DICTIONARY[scaling_label])

    plt.bar([0], [np.nan], color = MODE_COLORS[modes[0]], label = DICTIONARY["simulation"])
    plt.bar([0], [np.nan], edgecolor = MODE_COLORS[modes[0]], color = "white", label = DICTIONARY["reference"], hatch = "//")
    plt.legend(loc = "best")

    plt.subplot(1, 2, 2)

    df_share = prepare.mode_share_by_distance(df)
    df_share = filter_index(df_share, "area", area)
    df_share = filter_index(df_share, "year", 2020)

    df_baseline = filter_index(df_share, "scenario", "baseline")
    df_reference = filter_index(df_share, "scenario", "reference")

    for mode_index, mode in enumerate(modes):
        values_baseline = df_baseline["trips:%s" % mode].values * scaling_factor
        values_reference = df_reference["trips:%s" % mode].values * scaling_factor

        plt.plot(np.arange(len(prepare.DISTANCES)), values_baseline,
            color = MODE_COLORS[mode],
            linewidth = 1.0, marker = ".", markersize = 5, linestyle = "-")

        plt.plot(np.arange(len(prepare.DISTANCES)), values_reference,
            color = MODE_COLORS[mode],
            linewidth = 1.0, marker = ".", markersize = 5, linestyle = "--")

    plt.gca().xaxis.set_major_locator(tck.FixedLocator(
        np.arange(len(prepare.DISTANCES))
    ))

    plt.gca().xaxis.set_major_formatter(tck.FixedFormatter(
        DISTANCE_LABELS
    ))

    plt.xlabel(DICTIONARY["distance_class"])

    plt.grid()
    plt.gca().set_axisbelow(True)
    plt.gca().xaxis.grid(alpha = 0.0)

    plt.ylabel(DICTIONARY[scaling_label])

    plt.plot([np.nan], [np.nan], color = "k", linestyle = "-", label = DICTIONARY["simulation"])
    plt.plot([np.nan], [np.nan], color = "k", linestyle = "--", label = DICTIONARY["reference"])
    plt.legend(loc = "best")

    plt.tight_layout()
    plt.savefig(path)
    plt.close()

def tabulate_fleet_by_year(df, area, scenarios, years, path):
    df = df[df["av:is_maximum_demand"]]

    df = df[df.index.get_level_values("year").isin(years)]
    df = df[df.index.get_level_values("scenario").isin(scenarios)]

    df = filter_index(df, "area", area)

    df = df[[
        "av:fleet_size", "av:trips", "av:waiting_time:median", "av:price", "av:empty_share", "av:request_to_vehicle_ratio"
    ]].copy()

    df["av:fleet_size"] = df["av:fleet_size"].astype(np.int)

    df["av:waiting_time:median"] /= 60
    df["av:waiting_time:median"] = df["av:waiting_time:median"].round(2)

    df["av:empty_share"] *= 100
    df["av:empty_share"] = df["av:empty_share"].round(2)

    df["av:price"] = df["av:price"].round(2)
    df["av:request_to_vehicle_ratio"] = df["av:request_to_vehicle_ratio"].round(2)

    df = lexicographical_sort(df.reset_index(), SORTER).set_index(["scenario", "year"])
    df = translate(df)
    df = units(df, (DICTIONARY["[veh]"], "[1]", "[min]", "[CHF/km]", "[%]", "[1]"))

    df.to_latex(path)
    df.to_csv("%s.csv" % path)

def plot_reference_count_comparison(df_reference, df_flow, correction_factor, what, path):
    if what == "person":
        df_reference["reference"] = df_reference["personal_count"]
        df_flow["simulation"] = df_flow["count:conventional:car"]
    elif what == "freight":
        df_reference["reference"] = df_reference["freight_count"]
        df_flow["simulation"] = df_flow["count:conventional:truck"]
    elif what == "all":
        df_reference["reference"] = df_reference["personal_count"] + df_reference["freight_count"]
        df_flow["simulation"] = df_flow["count:conventional:car"] + df_flow["count:conventional:truck"]
    else:
        raise RuntimeError()

    # We need to add a factor of two, because ASTRA counts both directions
    df_match = counts.match(df_flow[df_flow["simulation"] > 0], df_reference[df_reference["reference"] > 0], 2.0 * correction_factor)[0]
    df_match["value"] = df_match["delta"] / df_match["reference"]

    df_match["is_too_high"] = df_match["value"] > 0.25
    df_match["is_too_low"] = df_match["value"] < -0.25
    df_match["is_good"] = ~(df_match["is_too_high"] | df_match["is_too_low"])

    plt.figure()

    plt.subplot(1, 2, 1)
    cmap = ROAD_COUNT_COLORS

    df_flow.plot(ax = plt.gca(), color = (0.8, 0.8, 0.8), linewidth = 0.5, zorder = -100)
    df_match[df_match["is_too_high"]].plot(ax = plt.gca(), color = cmap[2], marker = "o", edgecolor = "none", markersize = 5)
    df_match[df_match["is_too_low"]].plot(ax = plt.gca(), color = cmap[0], marker = "o", edgecolor = "none", markersize = 5)
    df_match[df_match["is_good"]].plot(ax = plt.gca(), color = "k", marker = "o", edgecolor = "none", markersize = 5)

    plt.gca().xaxis.set_visible(False)
    plt.gca().yaxis.set_visible(False)
    plt.gca().margins(0)
    plt.gca().tick_params(left=False, labelleft=False, bottom=False, labelbottom=False)
    plt.gca().axis("off")

    plt.subplot(1, 2, 2)

    f = df_match["is_too_high"]
    plt.plot(df_match[f]["reference"].values, df_match[f]["simulation"].values, color = cmap[2], marker = ".", markersize = 3, linestyle = "none")
    f = df_match["is_too_low"]
    plt.plot(df_match[f]["reference"].values, df_match[f]["simulation"].values, color = cmap[0], marker = ".", markersize = 3, linestyle = "none")
    f = df_match["is_good"]
    plt.plot(df_match[f]["reference"].values, df_match[f]["simulation"].values, color = "k", marker = ".", markersize = 3, linestyle = "none")

    maximum = max([df_match["reference"].max(), df_match["simulation"].max()]) * 1.05
    plt.plot([0, maximum], [0, maximum], 'k--', linewidth = 1.0)

    plt.xlim([0, maximum])
    plt.ylim([0, maximum])

    plt.gca().xaxis.set_major_formatter(tck.FuncFormatter(lambda x, p: "%d" % (x * 1e-3,)))
    plt.gca().yaxis.set_major_formatter(tck.FuncFormatter(lambda x, p: "%d" % (x * 1e-3,)))

    plt.xlabel(DICTIONARY["reference_1000vehs"])
    plt.ylabel(DICTIONARY["simulation_1000vehs"])

    plt.plot([np.nan] * 2, [np.nan] * 2, color = cmap[0], label = "-20%")
    plt.plot([np.nan] * 2, [np.nan] * 2, color = cmap[2], label = "+20%")
    plt.legend(loc = "lower right")
    plt.grid()

    plt.tight_layout()
    plt.savefig(path)

def tabulate_flow(df_flow, path):
    if not "{part}" in path:
        raise RuntimeError("Need {part} placeholder in path")

    for part in ["basic", "summary"]:
        df = prepare.flow_by_category(df_flow)
        df.index = df.index.droplevel("area")

        if part == "basic":
            df = df[[
                "utilization:90",
                "share:conventional_car", "share:automated_car",
                "share:conventional_truck", "share:automated_truck"
            ]]

            table_units = ["[% km]"] + ["[% Fhz]"] * 4
        elif part == "summary":
            df = df[[
                "share:conventional", "share:automated",
                "share:personal", "share:freight"
            ]]

            table_units = ["[% Fhz]"] * 4
        else:
            raise RuntimeError("Invalid part")

        for column in df.columns:
            df[column] = (df[column] * 100).round(2)

        df = df.reset_index()
        df["category"] = df["category"].apply(lambda x: "category:%s" % x)

        df = lexicographical_sort(df, SORTER).set_index(["scenario", "year", "category"])
        df = translate(df)
        df = units(df, table_units)

        df.to_latex(path.replace("{part}", part))
        df.to_csv("%s.csv" % path.replace("{part}", part))

def plot_flow_comparison(df_flow, slot, years, scenarios, path):
    df_flow = df_flow.copy()
    df_flow = df_flow[df_flow["link:osm"] == "motorway"]

    df_baseline = df_flow[df_flow["year"] == 2020]

    figure = plt.figure(figsize = (6.0, 3.0 * 2.5))
    axes = []

    maximum = 0
    column = "count:%s" % slot

    for year_index, year in enumerate(years):
        for scenario_index, scenario in enumerate(scenarios):
            axes.append(figure.add_subplot(
                len(years), len(scenarios), len(scenarios) * year_index + scenario_index + 1
            ))

            df_population = df_flow[(df_flow["year"] == year) & (df_flow["scenario"] == "baseline")]
            df_prav = df_flow[(df_flow["year"] == year) & (df_flow["scenario"] == scenario)]

            df_population = pd.merge(df_population, df_baseline, on = "link_id", suffixes = ["_future", "_reference"])
            df_prav = pd.merge(df_prav, df_baseline, on = "link_id", suffixes = ["_future", "_reference"])

            maximum = max([
                df_population[column + "_reference"].max() * 1e-3,
                df_population[column + "_future"].max() * 1e-3,
                df_prav[column + "_reference"].max() * 1e-3,
                df_prav[column + "_future"].max() * 1e-3
            ])

            plt.plot(
                df_population[column + "_reference"].values * 1e-3,
                df_population[column + "_future"].values * 1e-3,
                color = FLOW_COLORS["population"], marker = ".", markersize = 0.5,
                linestyle = "none", rasterized = True
            )

            plt.plot(
                df_prav[column + "_reference"].values * 1e-3,
                df_prav[column + "_future"].values * 1e-3,
                color = FLOW_COLORS["prav"], marker = ".", markersize = 0.5,
                linestyle = "none", rasterized = True
            )

            plt.plot([0, 1e6], [0, 1e6], "k--")
            plt.grid()

            if year_index == 0 and scenario_index == 0:
                plt.plot([np.nan] * 2, [np.nan] * 2, color = FLOW_COLORS["population"], label = "Bevölkerung")
                plt.plot([np.nan] * 2, [np.nan] * 2, color = FLOW_COLORS["prav"], label = "Private AVs")
                plt.legend(loc = "upper left")

            unit = DICTIONARY["unit_pcus" if slot == "pcus" else "unit_vehs"]

            plt.xlabel("%s 2020 [1000 %s]" % (DICTIONARY["base_case"], unit))
            plt.ylabel("%s %d [1000 %s]" % (DICTIONARY["future_case"], year, unit))

    for axis in axes:
        axis.set_xlim([0, maximum * 1.05])
        axis.set_ylim([0, maximum * 1.05])

    plt.tight_layout()
    plt.savefig(path)
    plt.close()

def plot_flow_map(df_baseline, df_future, area, shapes_path, path, slot = "pcus", legend = True):
    df_shapes = gpd.read_file(shapes_path)

    minx, miny, maxx, maxy = df_baseline.total_bounds
    width, height = maxx - minx, maxy - miny
    ratio = width / height
    figsize = (6.0, 3.0 * ratio)

    colors = palettable.colorbrewer.diverging.RdBu_5.mpl_colors[::-1]
    cmap = palettable.colorbrewer.diverging.RdBu_5.mpl_colormap.reversed()

    df_baseline = df_baseline[df_baseline["link:osm"].isin(["motorway", "trunk", "primary"])]
    df_future = df_future[df_future["link:osm"].isin(["motorway", "trunk", "primary"])]

    df_baseline = df_baseline[["count:pcus", "count:vehicles", "geometry"]]
    df_future = df_future[["count:pcus", "count:vehicles"]]

    df = pd.merge(df_baseline, df_future, on = "link_id", suffixes = ["_baseline", "_future"])
    df["absolute_difference"] = df["count:%s_future" % slot] - df["count:%s_baseline" % slot]
    df["relative_difference"] = df["absolute_difference"] / df["count:%s_baseline" % slot]

    plt.figure(figsize = figsize, dpi = 300)

    minimum_value = -0.25
    maximum_value = 0.25
    number_of_intervals = 5

    df.plot(
        ax = plt.gca(),
        column = "relative_difference", cmap = cmap, linewidth = 2.0,
        vmin = minimum_value, vmax = maximum_value
    )

    df.plot(
        ax = plt.gca(), linewidth = 0.25, color = (0.5, 0.5, 0.5)
    )

    df_shapes.plot(
        ax = plt.gca(), color = (0.0, 0.0, 0.0, 0.0),
        edgecolor = "k", linewidth = 1.0
    )

    for i in range(number_of_intervals):
        i = number_of_intervals - i - 1
        interval = (maximum_value - minimum_value) / number_of_intervals
        lower = minimum_value + i * interval
        upper = minimum_value + (i + 1) * interval

        plt.plot(
            [np.nan] * 2, [np.nan] * 2, color = colors[i],
            label = "%+.0f%% — %+.0f%%" % (lower * 100, upper * 100)
        )

    plt.gca().xaxis.set_visible(False)
    plt.gca().yaxis.set_visible(False)
    plt.gca().margins(0)
    plt.gca().tick_params(left=False, labelleft=False, bottom=False, labelbottom=False)
    plt.gca().axis("off")

    if legend:
        plt.legend(loc = "lower right", title = DICTIONARY["change_%s" % slot])

    if area in df_shapes["scenario"].values:
        minx, miny, maxx, maxy = df_shapes[df_shapes["scenario"] == area].total_bounds
        plt.xlim([minx, maxx])
        plt.ylim([miny, maxy])

    plt.tight_layout()
    plt.savefig(path)
    plt.close()

def plot_speeds_by_year(speeds_path, df, area, scenarios, path):
    cases = []
    years = YEAR_INDEX

    for scenario in scenarios:
        for year in years:
            cases.append((area, scenario, year))

    df = prepare.multiple_speeds_by_year(df, speeds_path, cases)

    figure = plt.figure()
    axes = []

    maximum = -np.inf
    minimum = np.inf

    for scenario_index, scenario in enumerate(scenarios):
        df_scenario = filter_index(df, "scenario", scenario)
        axes.append(figure.add_subplot(1, len(scenarios), scenario_index + 1))

        for year_index, year in enumerate(years):
            df_year = filter_index(df_scenario, "year", year)

            for road_type_index, road_type in enumerate(["motorway", "trunk", "primary", "secondary", "tertiary", "residential"]):
                df_road_type = df_year[df_year["road_type"] == road_type]

                times = df_road_type["time"].values
                values = df_road_type["speed"].values * 3.6

                maximum = max(maximum, max(values))
                minimum = min(minimum, min(values))

                plt.plot(times, values, color = YEAR_COLORS[year], linewidth = 1.0)

                if year_index == 0 and scenario_index == 0:
                    plt.text(5.0 * 3600 + (year_index % 2) * 3600, np.mean(values), str(road_type_index + 1), fontsize = 6)

        plt.gca().xaxis.set_major_locator(tck.FixedLocator(
            5 * 3600 + np.arange(24) * 3600 * 5
        ))

        plt.gca().xaxis.set_major_formatter(tck.FuncFormatter(
            lambda x, p: "%02d:00" % (x / 3600,)
        ))

        plt.grid()

        plt.xlabel(DICTIONARY["time_of_day"])
        plt.xlim([3600 * 4.5, 24.5 * 3600])

        plt.title(DICTIONARY[scenario])
        plt.ylim([10, None])

        if scenario_index == 0:
            plt.ylabel(DICTIONARY["speed_km_h"])

        if scenario_index == len(scenarios) - 1:
            for year in years:
                plt.plot([np.nan], [np.nan], color = YEAR_COLORS[year], label = str(year))

            #plt.legend(ncol = 2, fontsize = 6, loc = (0.0, -1.0))

    plt.tight_layout()

    figure.subplots_adjust(bottom = 0.3)
    axes[-1].legend(ncol = 2, loc = "upper left", bbox_to_anchor = (0.0, -0.2))

    names = "\n".join([
        "%d: %s" % (road_type_index + 1, DICTIONARY["road_type:%s" % road_type])
        for road_type_index, road_type in enumerate(["motorway", "trunk", "primary", "secondary", "tertiary", "residential"])
    ])

    axes[0].text(20.0 * 3600.0, -5.0, names, fontsize = 6, verticalalignment = "top")

    #figure.subplots_adjust(bottom = 0.3)
    #legend = axes[0].legend(ncol = 4, loc = "upper left", bbox_to_anchor = (0.0, -0.25))

    plt.savefig(path)
    plt.close()

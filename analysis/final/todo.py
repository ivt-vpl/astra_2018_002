
def plot_pcu_change_scenario(df_base, reference_case, reference_scenario, reference_year, comparison_case, comparison_scenario, comparison_year):
    df_shapes = gpd.read_file(SHAPES_PATH)

    df_reference = prepare_flows(df_base, reference_case, reference_scenario, reference_year, flow_period = "am")
    df_comparison = prepare_flows(df_base, comparison_case, comparison_scenario, comparison_year, flow_period = "am")

    df = pd.merge(df_comparison, df_reference, on = "link_id", suffixes = ["_comparison", "_reference"])
    df = gpd.GeoDataFrame(df[df["pcu_reference"] > 0])

    df["change"] = (df["pcu_comparison"] - df["pcu_reference"]) / df["pcu_reference"]
    df["geometry"] = df["geometry_comparison"]

    maximum_change = max([df["change"].max(), -df["change"].min()])
    maximum_change = 1

    minx, miny, maxx, maxy = df_comparison.total_bounds
    width, height = maxx - minx, maxy - miny
    ratio = width / height
    figsize = (FIGSIZE[0], FIGSIZE[0] / ratio)

    colors = palettable.colorbrewer.sequential.OrRd_5.mpl_colors
    cmap = palettable.colorbrewer.sequential.OrRd_5.mpl_colormap

    plt.figure(figsize = figsize)

    df.plot(
        ax = plt.gca(),
        column = "change", cmap = cmap, linewidth = 1.0,
        vmin = 0, vmax = maximum_change
    )

    df_shapes.plot(
        ax = plt.gca(), color = (0.0, 0.0, 0.0, 0.0),
        linestyle = "--", edgecolor = "k", linewidth = 0.5
    )

    for i in range(5):
        lower = i / 5
        upper = (i + 1) / 5

        plt.plot(
            [np.nan] * 2, [np.nan] * 2, color = colors[i],
            label = "% 1.0f%% - % 1.0f%%" % (lower * 100, upper * 100)
        )

    plt.gca().xaxis.set_visible(False)
    plt.gca().yaxis.set_visible(False)
    plt.gca().margins(0)
    plt.gca().tick_params(left=False, labelleft=False, bottom=False, labelbottom=False)
    plt.gca().axis("off")

    plt.legend(loc = "lower right", title = "Zusätzliche PCUs")

    if comparison_case in df_shapes["scenario"].values:
        minx, miny, maxx, maxy = df_shapes[df_shapes["scenario"] == case].total_bounds
        plt.xlim([minx, maxx])
        plt.ylim([miny, maxy])

    plt.title("Baseline 2020 vs. %s %d %s" % (comparison_case, comparison_year, comparison_scenario))

    plt.tight_layout()
    plt.savefig("%s/utilization_change_%s_%d_%s_to_%s_%d_%s.%s" % (OUTPUT_PATH, reference_case, reference_year, reference_scenario, comparison_case, comparison_year, comparison_scenario, FILE_SUFFIX))

for year in AV_YEARS:
    plot_pcu_change_scenario(df, "bl", "X", 2020, "prav", "A", 2050)
    plot_pcu_change_scenario(df, "bl", "X", 2020, "prav", "C", 2050)










#

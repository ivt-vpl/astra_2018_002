import pandas as pd
import shapely.geometry as geo
import geopandas as gpd
import numpy as np

def read_reference_counts(stations_path, counts_path):
    df_locations = pd.read_excel(stations_path, skiprows = 6).rename({
        "Zählstellen-Bezeichnung": "name",
        "Nr": "id", "Koordinate Ost": "x", "Koordinate Nord": "y"
    }, axis = 1)[["id", "name", "x", "y"]].set_index("id")

    df_locations["geometry"] = [geo.Point(*p) for p in zip(df_locations["x"], df_locations["y"])]
    df_locations = gpd.GeoDataFrame(df_locations)
    df_locations.crs = {"init": "EPSG:21781"}
    df_locations = df_locations.to_crs({"init": "epsg:2056"})

    df_locations = df_locations[["name", "geometry"]]

    df_counts = pd.read_excel(
        counts_path,
        skiprows = 7, sheet_name = "Klassendaten", usecols = [0, 5, 23],
        names = ["id", "slot", "count"]
    )

    df_counts["id"] = df_counts["id"].fillna(method = "pad").astype(np.int)
    df_counts = df_counts[df_counts["slot"].str.startswith("DWV")]

    df_counts["freight"] = (df_counts["slot"] == "DWV SGF") | (df_counts["slot"] == "DWV SV")
    df_counts = df_counts.drop(columns = ["slot"])
    df_counts = df_counts.groupby(["id", "freight"]).sum().reset_index()

    df_total = df_counts[~df_counts["freight"]][["id", "count"]].rename({"count": "total_count"}, axis = 1)
    df_freight = df_counts[df_counts["freight"]][["id", "count"]].rename({"count": "freight_count"}, axis = 1)

    df_counts = pd.merge(df_total, df_freight, how = "left", on = "id")
    assert(len(df_counts) == len(df_counts["id"].unique()))

    df_counts["personal_count"] = df_counts["total_count"] - df_counts["freight_count"]
    df_counts = df_counts[df_counts["total_count"] > 0]

    df_counts = df_counts.set_index("id")

    df = pd.concat([df_locations, df_counts], axis = 1).dropna()
    return df.reset_index()

def match(df_flow, df_count, factor, matching_radius = 100):
    if not "simulation" in df_flow: raise RuntimeError()
    if not "reference" in df_count: raise RuntimeError()

    df_match = df_count.copy()
    df_match["geometry"] = df_match["geometry"].buffer(matching_radius)
    df_match = gpd.sjoin(df_match, df_flow, op = "intersects", how = "left").drop(columns = ["index_right"])

    df_match["simulation"] *= factor
    df_match["delta"] = df_match["simulation"] - df_match["reference"]
    df_match["abs_delta"] = np.abs(df_match["delta"])
    df_match["geometry"] = df_match["geometry"].centroid

    df_match = df_match.sort_values(by = ["id", "abs_delta"])
    df_match = df_match.drop_duplicates("id")

    score = df_match["abs_delta"].sum()
    return df_match, score

def fit(df_flow, df_count, base_factor, lower, upper, samples = 5, matching_radius = 100):
    import matplotlib.pyplot as plt

    candidates = np.linspace(lower, upper, samples)
    scores = [match(df_flow, df_count, base_factor * c, matching_radius)[1] for c in candidates]

    plt.figure()
    plt.plot(candidates, scores, 'x-')
    plt.show()

if __name__ == "__main__":
    stations_path = "../../flow_analysis/messstellenverzeichnis.xlsx"
    counts_path = "../../flow_analysis/Jahresergebnisse-2017.xlsx"

    df = read_reference_counts(stations_path, counts_path)

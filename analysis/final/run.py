import prepare, output, counts
import pandas as pd
import sys

if len(sys.argv) > 1:
    analysis_path = sys.argv[1]
    reference_path = sys.argv[2]
    repository_path = sys.argv[3]
    output_path = sys.argv[4]
else:
    # the path to the directory that contains the aggregate.p file that you wish to create plots from
    # Caution: only from one series of simulations! Because otherwise it probably won't handle having multiple "pravA"s, etc, correctly!
    analysis_path = "C:\\Users\\clivings\\Documents\\ASTRA_2018_002_AFs_Schweiz\\Output_From_MATSim\\from_euler\\clarissa_astra1802\\analysis\\complete_sensitivies_copies_for_testing"
    # the path to the directory that contains two csv files with person and trip information direct from the Microzsenus, reformated to match the MATSim output
    reference_path = "C:\\Users\\clivings\\Documents\\ASTRA_2018_002_AFs_Schweiz\\Output_From_MATSim\\from_euler\\reference"
    repository_path = "C:\\Users\\clivings\\git\\astra_2018_002"
    output_path = "C:\\Users\\clivings\\Documents\\ASTRA_2018_002_AFs_Schweiz\\Output_From_MATSim\\from_euler\\data_from_euler\clarissa_astra1802\\analysis\\complete_sensitivies_copies_for_testing\\plots"

data_path = "%s/aggregated.parquet" % analysis_path
flow_path = analysis_path

scaling_factor = 0.25

reference_modes = ("car", "car_passenger", "pt", "bike", "walk")

baseline_person_modes = ("car", "prav", "car_passenger", "pt", "light")
baseline_vehicle_modes = ("car", "prav", "pt", "truck", "truck_av")
baseline_mean_modes = ("car", "prav", "pt", "light", "road", "all")
baseline_scenarios = ("baseline", "pravA", "pravB")

tav_person_modes = ("car", "prav", "tav", "car_passenger", "pt", "light")
tav_vehicle_modes = ("car", "prav", "tav_customer", "tav_empty", "pt", "truck", "truck_av")
tav_mean_modes = ("car", "prav", "tav", "pt", "light", "road", "all")
tav_scenarios = ("baseline", "tavA", "tavB")

fleet_sizing_years = (2030, 2040, 2050)
fleet_sizing_scenarios = ("tavA", "tavB")

reference_persons_path = "%s/ch_persons.csv" % reference_path
reference_trips_path = "%s/ch_trips.csv" % reference_path
shapes_path = "%s/gis/scenarios/scenarios.shp" % repository_path

road_count_stations_path = "%s/flow_analysis/messstellenverzeichnis.xlsx" % repository_path
road_counts_path = "%s/flow_analysis/Jahresergebnisse-2017.xlsx" % repository_path
road_count_correction_factors = {
    "person": 0.914,
    "freight": 1.04,
    "all": 0.916
}

output.initialize()

df = pd.read_parquet(data_path)
df_reference = prepare.reference(reference_persons_path, reference_trips_path, shapes_path, scaling_factor)
df = prepare.data(df, df_reference, scaling_factor)
df.to_csv("%s/data.csv" % output_path, sep = ";")

for area in prepare.AREAS:
    for year in fleet_sizing_years:
        output.plot_fleet_sizing(df, area, year, fleet_sizing_scenarios, "%s/%s_fleet_sizing_%d.png" % (output_path, area, year))

    output.plot_reference_mode_share(df, area, reference_modes, "%s/%s_reference_comparison.png" % (output_path, area))

    output.plot_mean_distance_travel_time_and_speed_by_year(df, area, baseline_scenarios, baseline_mean_modes, "distance", "%s/%s_mean_distance_by_year_prav.png" % (output_path, area))
    output.plot_mean_distance_travel_time_and_speed_by_year(df, area, baseline_scenarios, baseline_mean_modes, "travel_time", "%s/%s_mean_travel_time_by_year_prav.png" % (output_path, area))
    output.plot_mean_distance_travel_time_and_speed_by_year(df, area, baseline_scenarios, baseline_mean_modes, "mean_speed", "%s/%s_mean_speed_by_year_prav.png" % (output_path, area))

    output.plot_mode_share_by_distance(df, area, baseline_scenarios, baseline_person_modes, "%s/%s_mode_share_by_distance_prav.png" % (output_path, area))

    output.plot_modal_shift_by_year(df, area, ("pravA", "pravB"), ("car", "pt", "light"), "prav", "%s/%s_shift_to_prav.png" % (output_path, area))
    output.plot_modal_shift_by_year(df, area, ("pravA", "pravB"), ("car", "prav", "light"), "pt", "%s/%s_shift_to_pt_with_prav.png" % (output_path, area))

    output.plot_distance_by_year(df, area, baseline_scenarios, baseline_person_modes, "person", "%s/%s_person_distance_by_year_prav.png" % (output_path, area))
    output.plot_distance_by_year(df, area, baseline_scenarios, baseline_vehicle_modes, "vehicle", "%s/%s_vehicle_distance_by_year_prav.png" % (output_path, area))
    output.tabulate_distance_by_year(df, area, baseline_scenarios, baseline_person_modes, "person", "%s/%s_person_distance_by_year_prav.tex" % (output_path, area))
    output.tabulate_distance_by_year(df, area, baseline_scenarios, baseline_vehicle_modes, "vehicle", "%s/%s_vehicle_distance_by_year_prav.tex" % (output_path, area))

    output.plot_mode_share_by_year(df, area, baseline_scenarios, baseline_person_modes, "%s/%s_trips_by_year_prav.png" % (output_path, area))
    output.tabulate_mode_share_by_year(df, area, baseline_scenarios, baseline_person_modes, "%s/%s_trips_by_year_prav.tex" % (output_path, area))

    output.tabulate_scenario_info(df, area, baseline_scenarios, "%s/%s_area_info.tex" % (output_path, area))

    output.plot_speeds_by_year(flow_path, df, area, baseline_scenarios, "%s/%s_speeds_by_year_prav.png" % (output_path, area))

    for timeslot in ("day", "am", "pm"):
        df_baseline_flow = prepare.single_flow(flow_path, scaling_factor, timeslot, area, "baseline", 2020)

        for year in [2030, 2040, 2050]:
            for slot in ["pcus", "vehicles"]:
                df_flow = prepare.single_flow(flow_path, scaling_factor, timeslot, area, "baseline", year)
                output.plot_flow_map(df_baseline_flow, df_flow, area, shapes_path, "%s/%s_%d_map_%s_population_%s.png" % (output_path, area, year, slot, timeslot), slot, legend = True)

                df_flow = prepare.single_flow(flow_path, scaling_factor, timeslot, area, "pravA", year, df = df)
                output.plot_flow_map(df_baseline_flow, df_flow, area, shapes_path, "%s/%s_%d_map_%s_pravA_%s.png" % (output_path, area, year, slot, timeslot), slot, legend = True)

                df_flow = prepare.single_flow(flow_path, scaling_factor, timeslot, area, "pravB", year, df = df)
                output.plot_flow_map(df_baseline_flow, df_flow, area, shapes_path, "%s/%s_%d_map_%s_pravB_%s.png" % (output_path, area, year, slot, timeslot), slot, legend = True)

    if not area == "ch":
        output.tabulate_fleet_by_year(df, area, fleet_sizing_scenarios, fleet_sizing_years, "%s/%s_fleet_by_year.tex" % (output_path, area))

        output.plot_mean_distance_travel_time_and_speed_by_year(df, area, tav_scenarios,tav_mean_modes, "distance", "%s/%s_mean_distance_by_year_tav.png" % (output_path, area))
        output.plot_mean_distance_travel_time_and_speed_by_year(df, area, tav_scenarios,tav_mean_modes, "travel_time", "%s/%s_mean_travel_time_by_year_tav.png" % (output_path, area))
        output.plot_mean_distance_travel_time_and_speed_by_year(df, area, tav_scenarios,tav_mean_modes, "mean_speed", "%s/%s_mean_speed_by_year_tav.png" % (output_path, area))

        output.plot_mode_share_by_distance(df, area, tav_scenarios, tav_person_modes, "%s/%s_mode_share_by_distance_tav.png" % (output_path, area))

        output.plot_modal_shift_by_year(df, area, ("tavA", "tavB"), ("car", "prav", "pt", "light"), "tav", "%s/%s_shift_to_tav.png" % (output_path, area))
        output.plot_modal_shift_by_year(df, area, ("tavA", "tavB"), ("car", "prav", "tav", "light"), "pt", "%s/%s_shift_to_pt_with_tav.png" % (output_path, area))

        output.plot_distance_by_year(df, area, tav_scenarios, tav_person_modes, "person", "%s/%s_person_distance_by_year_tav.png" % (output_path, area))
        output.plot_distance_by_year(df, area, tav_scenarios, tav_vehicle_modes, "vehicle", "%s/%s_vehicle_distance_by_year_tav.png" % (output_path, area))
        output.tabulate_distance_by_year(df, area, tav_scenarios, tav_person_modes, "person", "%s/%s_person_distance_by_year_tav.tex" % (output_path, area))
        output.tabulate_distance_by_year(df, area, tav_scenarios, tav_vehicle_modes, "vehicle", "%s/%s_vehicle_distance_by_year_tav.tex" % (output_path, area))

        output.plot_mode_share_by_year(df, area, tav_scenarios, tav_person_modes, "%s/%s_trips_by_year_tav.png" % (output_path, area))
        output.tabulate_mode_share_by_year(df, area, tav_scenarios, tav_person_modes, "%s/%s_trips_by_year_tav.tex" % (output_path, area))

        output.plot_speeds_by_year(flow_path, df, area, tav_scenarios, "%s/%s_speeds_by_year_tav.png" % (output_path, area))

        for timeslot in ("day", "am", "pm"):
            df_baseline_flow = prepare.single_flow(flow_path, scaling_factor, timeslot, area, "baseline", 2020)

            output.tabulate_flow(prepare.multiple_flows(df, flow_path, scaling_factor, timeslot, [
                (area, "tavA", 2030),
                (area, "tavB", 2030),
                (area, "tavA", 2040),
                (area, "tavB", 2040),
                (area, "tavA", 2050),
                (area, "tavB", 2050),
            ]), "%s/%s_flow_by_category_{part}_%s.tex" % (output_path, area, timeslot))

            for year in [2030, 2040, 2050]:
                for slot in ["pcus", "vehicles"]:
                    df_flow = prepare.single_flow(flow_path, scaling_factor, timeslot, area, "tavA", year, df = df)
                    output.plot_flow_map(df_baseline_flow, df_flow, area, shapes_path, "%s/%s_%d_map_%s_tavA_%s.png" % (output_path, area, year, slot, timeslot), slot, legend = True)

                    df_flow = prepare.single_flow(flow_path, scaling_factor, timeslot, area, "tavB", year, df = df)
                    output.plot_flow_map(df_baseline_flow, df_flow, area, shapes_path, "%s/%s_%d_map_%s_tavB_%s.png" % (output_path, area, year, slot, timeslot), slot, legend = True)

for timeslot in ("day", "am", "pm"):
    output.tabulate_flow(prepare.multiple_flows(df, flow_path, scaling_factor, timeslot, [
        ("ch", "baseline", 2020),
        ("ch", "pravA", 2030),
        ("ch", "pravB", 2030),
        ("ch", "pravA", 2040),
        ("ch", "pravB", 2040),
        ("ch", "pravA", 2050),
        ("ch", "pravB", 2050),
    ]), "%s/ch_flow_by_category_{part}_%s.tex" % (output_path, timeslot))

df_reference = counts.read_reference_counts(road_count_stations_path, road_counts_path)
df_simulation = prepare.single_flow(flow_path, scaling_factor, "day", "ch", "baseline", 2020)

output.plot_reference_count_comparison(df_reference, df_simulation, road_count_correction_factors["person"], "person", "%s/ch_reference_count_comparion_person.png" % output_path)
output.plot_reference_count_comparison(df_reference, df_simulation, road_count_correction_factors["freight"], "freight", "%s/ch_reference_count_comparion_freight.png" % output_path)
output.plot_reference_count_comparison(df_reference, df_simulation, road_count_correction_factors["all"], "all", "%s/ch_reference_count_comparion_all.png" % output_path)

df_flow = prepare.multiple_flows(df, flow_path, scaling_factor, "day", [
    ("ch", "baseline", 2020),
    ("ch", "baseline", 2030),
    ("ch", "pravA", 2030),
    ("ch", "pravB", 2030),
    ("ch", "baseline", 2040),
    ("ch", "pravA", 2040),
    ("ch", "pravB", 2040),
    ("ch", "baseline", 2050),
    ("ch", "pravA", 2050),
    ("ch", "pravB", 2050),
])

output.plot_flow_comparison(df_flow, "pcus", [2030, 2040, 2050], ["pravA", "pravB"], "%s/ch_flow_comparison_pcus.png" % output_path)
output.plot_flow_comparison(df_flow, "vehicles", [2030, 2040, 2050], ["pravA", "pravB"], "%s/ch_flow_comparison_vehicles.png" % output_path)

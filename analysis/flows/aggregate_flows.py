import pandas as pd
import geopandas as gpd
import sys, os

pattern = sys.argv[1]
output_name = sys.argv[2]

df_aggregated = []

for sample in [1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000, 9000, 10000]:
    path = pattern % sample

    if os.path.exists(path):
        print("Loading", path)
        df_partial = gpd.read_file(pattern % sample)
        df_aggregated.append(df_partial)
    else:
        print("Does not exist:", path)

df_aggregated = pd.concat(df_aggregated).drop(columns = ["geometry"])
columns = [c for c in df_partial.columns if c.startswith("am_") or c.startswith("pm_") or c.startswith("day_")]
df_aggregated = df_aggregated[["link_id"] + columns]

if output_name.endswith("shp"):
    df_aggregated = df_aggregated.groupby("link_id").mean().reset_index()
else:
    df_aggregated = df_aggregated.groupby("link_id").agg(["mean", "std"]).reset_index()
    df_aggregated.columns = ['_'.join(c) if not c[0] == "link_id" else "link_id" for c in df_aggregated.columns.values]

df_aggregated = pd.merge(df_partial[["link_id", "capacity", "freespeed", "lanes", "osm_type", "av_op", "geometry"]], df_aggregated, on = "link_id")
df_aggregated.to_file(output_name)

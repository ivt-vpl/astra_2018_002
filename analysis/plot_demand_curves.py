import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

df = pd.read_parquet("astra.parquet")
#df = df[4 * df["taxi_fleet_size"] <= 20000]

for case in ["lu", "lg", "sa"]:
    plt.figure(dpi = 120, figsize = (6,4))

    for scenario, linestyle in zip(["A", "C"], ["-", "--"]):
        for year, color in zip([2030, 2040, 2050], ["C0", "C1", "C2"]):
            f = (df["scenario"] == scenario) & (df["year"] == year) & (df["case"] == case)

            fleet_size = df[f]["taxi_fleet_size"].values * 4
            values = df[f]["trips_av"].values * 4
            sorter = np.argsort(fleet_size)

            plt.plot(fleet_size[sorter], values[sorter], 'x', color = color, linestyle = linestyle)

    for year, color in zip([2030, 2040, 2050], ["C0", "C1", "C2"]):
        plt.plot([np.nan, np.nan], [np.nan, np.nan], color = color, label = "Year %d" % year)

    for scenario, linestyle in zip(["A", "C"], ["-", "--"]):
        plt.plot([np.nan, np.nan], [np.nan, np.nan], linestyle = linestyle, label = "Scenario %s" % scenario, color = "k")

    plt.grid()
    plt.legend(loc = "best")
    plt.title("Demand curve: %s" % case)
    plt.savefig("plots/demand_curve_%s.png" % case)







    plt.figure(dpi = 120, figsize = (6,4))

    for scenario, linestyle in zip(["A", "C"], ["-", "--"]):
        for year, color in zip([2030, 2040, 2050], ["C0", "C1", "C2"]):
            f = (df["scenario"] == scenario) & (df["year"] == year) & (df["case"] == case)

            fleet_size = df[f]["taxi_fleet_size"].values * 4
            values = df[f]["taxi_price"].values
            sorter = np.argsort(fleet_size)

            plt.plot(fleet_size[sorter], values[sorter], 'x', color = color, linestyle = linestyle)

    for year, color in zip([2030, 2040, 2050], ["C0", "C1", "C2"]):
        plt.plot([np.nan, np.nan], [np.nan, np.nan], color = color, label = "Year %d" % year)

    for scenario, linestyle in zip(["A", "C"], ["-", "--"]):
        plt.plot([np.nan, np.nan], [np.nan, np.nan], linestyle = linestyle, label = "Scenario %s" % scenario, color = "k")

    plt.grid()
    plt.ylim([0.0, 1.0])
    plt.legend(loc = "best")
    plt.title("Price curve: %s" % case)
    plt.savefig("plots/price_curve_%s.png" % case)







    plt.figure(dpi = 120, figsize = (6,4))

    for scenario, linestyle in zip(["A", "C"], ["-", "--"]):
        for year, color in zip([2030, 2040, 2050], ["C0", "C1", "C2"]):
            f = (df["scenario"] == scenario) & (df["year"] == year) & (df["case"] == case)

            fleet_size = df[f]["taxi_fleet_size"].values * 4
            mean_values = df[f]["mean_waiting_time"].values
            #median_values = df[f]["q90_waiting_time"].values
            sorter = np.argsort(fleet_size)

            plt.plot(fleet_size[sorter], mean_values[sorter], 'x', color = color, linestyle = linestyle)
            #plt.plot(fleet_size[sorter], median_values[sorter], 'x', color = color, linestyle = "--")

    for year, color in zip([2030, 2040, 2050], ["C0", "C1", "C2"]):
        plt.plot([np.nan, np.nan], [np.nan, np.nan], color = color, label = "Year %d" % year)

    for scenario, linestyle in zip(["A", "C"], ["-", "--"]):
        plt.plot([np.nan, np.nan], [np.nan, np.nan], linestyle = linestyle, label = "Scenario %s" % scenario, color = "k")

    plt.grid()
    plt.legend(loc = "best")
    plt.title("Waiting time curve: %s" % case)
    plt.savefig("plots/waiting_time_curve_%s.png" % case)







    plt.figure(dpi = 120, figsize = (6,4))

    for scenario, linestyle in zip(["A", "C"], ["-", "--"]):
        for year, color in zip([2030, 2040, 2050], ["C0", "C1", "C2"]):
            f = (df["scenario"] == scenario) & (df["year"] == year) & (df["case"] == case)

            fleet_size = df[f]["taxi_fleet_size"].values * 4
            sorter = np.argsort(fleet_size)

            total_distance = df[f]["av_customer_distance"].values + df[f]["av_empty_distance"].values
            values = df[f]["av_empty_distance"].values / total_distance

            plt.plot(fleet_size[sorter], values[sorter], 'x', color = color, linestyle = linestyle)

    for year, color in zip([2030, 2040, 2050], ["C0", "C1", "C2"]):
        plt.plot([np.nan, np.nan], [np.nan, np.nan], color = color, label = "Year %d" % year)

    for scenario, linestyle in zip(["A", "C"], ["-", "--"]):
        plt.plot([np.nan, np.nan], [np.nan, np.nan], linestyle = linestyle, label = "Scenario %s" % scenario, color = "k")

    plt.grid()
    plt.legend(loc = "best")
    plt.title("Emtpy distance share: %s" % case)
    plt.savefig("plots/empty_distance_curve_%s.png" % case)







    plt.figure(dpi = 120, figsize = (6,4))

    maximum_fleet_sizes = {}
    maximum_shares = {}

    for scenario, linestyle in zip(["A", "C"], ["-", "--"]):
        for year, color in zip([2030, 2040, 2050], ["C0", "C1", "C2"]):
            f = (df["scenario"] == scenario) & (df["year"] == year) & (df["case"] == case)

            fleet_size = df[f]["taxi_fleet_size"].values * 4
            sorter = np.argsort(fleet_size)

            total_distance = 0
            for mode in ["car", "pt", "bike", "walk", "av"]:
                total_distance += df[f]["distance_%s" % mode].values

            av_distance = df[f]["distance_av"].values
            values = av_distance / total_distance

            plt.plot(fleet_size[sorter], values[sorter], 'x', color = color, linestyle = linestyle)

            if not scenario in maximum_fleet_sizes:
                maximum_fleet_sizes[scenario] = []
                maximum_shares[scenario] = []

            maximum_index = np.argmax(values)
            maximum_fleet_sizes[scenario].append(fleet_size[maximum_index])
            maximum_shares[scenario].append(values[maximum_index])

    for year, color in zip([2030, 2040, 2050], ["C0", "C1", "C2"]):
        plt.plot([np.nan, np.nan], [np.nan, np.nan], color = color, label = "Year %d" % year)

    for scenario, linestyle in zip(["A", "C"], ["-", "--"]):
        plt.plot([np.nan, np.nan], [np.nan, np.nan], linestyle = linestyle, label = "Scenario %s" % scenario, color = "k")

    for scenario, linestyle in zip(["A", "C"], ["-", "--"]):
        plt.plot(maximum_fleet_sizes[scenario], maximum_shares[scenario], color = "k", linestyle = linestyle)

    plt.grid()
    plt.legend(loc = "best")
    plt.title("Mode share: %s" % case)
    plt.savefig("plots/mode_share_curve_%s.png" % case)

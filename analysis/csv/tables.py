import pandas as pd
import numpy as np

KEYWORDS = {
    "car": "MIV",
    "car_passenger": "Mitfahrer",
    "pt": "ÖV",
    "light": "LV",
    "total": "Gesamt",
    "total_mit_ex_cp": "G. MIV",
    "total_mit_incl_cp": "G. MIV + Mf",
    "total_mit": "G. MIV",
    "prav": "A-MIV",
    "tav": "TAV",
    "truck": "GV",
    "truck_av": "A-GV",

    "baseline": "Grundzustand",
    "pravA": "A (nur AF-MIV)",
    "pravB": "B (nur AF-MIV)",

    "scenario": "Szenario",
    "year": "Jahr",
}

def make_table(df, table):
    area, modes, level = table["area"], table["modes"], table["level"]
    relative = table["relative"]
    additional_modes = table["additional_modes"] if "additional_modes" in table else []

    df = df[["area", "scenario", "year", "sensitivity"] + [
        "%s_distance:%s" % (level, mode) for mode in modes + additional_modes
    ]].copy()

    df.columns = [c.replace("person_distance:", "") for c in df.columns]
    df.columns = [c.replace("vehicle_distance:", "") for c in df.columns]

    df_table = []

    for row in table["rows"]:
        if row is None: continue

        primary, comparison = row
        primary_filter = df["area"] == area
        primary_filter &= df["scenario"] == primary[0]
        primary_filter &= df["year"] == primary[1]

        sensitivity = "phase1" if primary[2] is None else primary[2]
        primary_filter &= df["sensitivity"] == sensitivity

        primary_row = df[primary_filter].copy()

        if not len(primary_row) == 1:
            print(primary_row)
            print("Not exactly one row for ", area, primary)
            raise RuntimeError()

        if relative and comparison is None:
            total = np.sum([primary_row[mode].values[0] for mode in modes])
            for mode in modes + additional_modes: primary_row[mode] /= total

        if not comparison is None:
            comparison_filter = df["area"] == area
            comparison_filter &= df["scenario"] == comparison[0]
            comparison_filter &= df["year"] == comparison[1]

            sensitivity = "phase1" if comparison[2] is None else comparison[2]
            comparison_filter &= df["sensitivity"] == sensitivity

            comparison_row = df[comparison_filter].copy()

            if not len(comparison_row) == 1:
                print("Not exactly one row for ", area, comparison)
                raise RuntimeError()

            for mode in modes + additional_modes:
                primary_row[mode] -= comparison_row[mode].values[0]
                if relative: primary_row[mode] /= comparison_row[mode].values[0]

        primary_row = primary_row.replace([np.inf, -np.inf], np.nan)
        df_table.append(primary_row)

    df_table = pd.concat(df_table)
    df_table = df_table.drop(columns = ["area", "sensitivity"])

    # Figure out units and scaling
    unit = "[%]"

    if not relative:
        maximum = np.max(df_table[modes + additional_modes].max().values)

        #if maximum > 1e6:
        for mode in modes + additional_modes: df_table[mode] /= 1e6
        unit = "[Mio km]"
        #elif maximum > 1e3:
        #    for mode in modes + additional_modes: df_table[mode] /= 1e3
        #    unit = "[Tsd km]"
        #else:
        #    unit = "[km]"
    else:
        for mode in modes + additional_modes: df_table[mode] *= 1e2

    # Round numbers
    for mode in modes + additional_modes: df_table[mode] = np.round(df_table[mode], 2)

    # Remove columns that have only NaN (e.g. TAV in non-TAV scenarios)
    remaining_modes = modes + additional_modes

    for mode in modes + additional_modes:
        values = df_table[mode]

        if np.all(np.isnan(values)):
            df_table = df_table.drop(columns = [mode])
            remaining_modes.remove(mode)

    # Format the numbers as strings with +/- in relative case and "-" for NaN
    if relative and not comparison is None:
        for mode in remaining_modes:
            f_positive = ~df_table[mode].isna() & (df_table[mode] > 0.0)

            df_table[mode] = df_table[mode].astype(str)
            df_table.loc[f_positive, mode] = "+" + df_table.loc[f_positive, mode]
            df_table[mode] = df_table[mode].str.replace("NaN", "-")

    replace_name = lambda x: x if not x in KEYWORDS else KEYWORDS[x]

    # Add units to columns names
    df_table.columns = pd.MultiIndex.from_tuples([
        (replace_name(c), unit) if c in modes + additional_modes else (replace_name(c), "") for c in df_table.columns
    ])

    df_table = df_table.replace(KEYWORDS)

    return df_table

if __name__ == "__main__":
    # Read data sets

    sensitivities = {
        "phase1": "data/data.csv",
        "sensitivity1": "data/data1.csv",
        "sensitivity2": "data/data2.csv",
        "sensitivity4": "data/data4.csv",
        "sensitivity5": "data/data5.csv"
    }

    df = []

    for name, path in sensitivities.items():
        df_sensitivity = pd.read_csv(path, sep = ";")
        df_sensitivity["sensitivity"] = name
        df.append(df_sensitivity)

    df = pd.concat(df)

    # Filter ony maximum demand cases
    df = df[df["av:is_maximum_demand"]]

    # Filter for relevant columns
    df = df[["area", "scenario", "year", "sensitivity"] + [
        c for c in df.columns if ("person_distance:" in c or "vehicle_distance:" in c)
    ]]

    # Construct additonal aggregated modes
    df["person_distance:light"] = df["person_distance:bike"] + df["person_distance:walk"]
    df["vehicle_distance:tav"] = df["vehicle_distance:tav_customer"] + df["vehicle_distance:tav_empty"]

    df["person_distance:total"] = 0
    df["person_distance:total"] += df["person_distance:car"]
    df["person_distance:total"] += df["person_distance:car_passenger"]
    df["person_distance:total"] += df["person_distance:prav"]
    df["person_distance:total"] += df["person_distance:tav"]
    df["person_distance:total"] += df["person_distance:pt"]
    df["person_distance:total"] += df["person_distance:light"]

    df["vehicle_distance:total"] = 0
    df["vehicle_distance:total"] += df["vehicle_distance:car"]
    df["vehicle_distance:total"] += df["vehicle_distance:prav"]
    df["vehicle_distance:total"] += df["vehicle_distance:tav"]
    df["vehicle_distance:total"] += df["vehicle_distance:pt"]
    df["vehicle_distance:total"] += df["vehicle_distance:truck"]
    df["vehicle_distance:total"] += df["vehicle_distance:truck_av"]

    df["person_distance:total_mit_ex_cp"] = df["person_distance:car"] + df["person_distance:prav"]
    df["person_distance:total_mit_incl_cp"] = df["person_distance:car"] + df["person_distance:prav"] + df["person_distance:car_passenger"]
    df["vehicle_distance:total_mit"] = df["vehicle_distance:car"] + df["vehicle_distance:prav"]

    # Define tables
    tables = []

    level_modes = {
        "person": ["car", "prav", "tav", "car_passenger", "pt", "light"],
        "vehicle": ["car", "prav", "tav", "pt", "truck", "truck_av"]
    }

    level_additional_modes = {
        "person": ["total", "total_mit_ex_cp", "total_mit_incl_cp"],
        "vehicle": ["total", "total_mit"],
    }

    for level in ("person", "vehicle"):
        for area in ("ch", "sa", "lu", "lg"):
            for relative in (True, False):
                relative_name = "relative" if relative else "absolute"

                # Table that simply states the distances
                tables.append(("%s_%s_distance_%s" % (area, level, relative_name), make_table(df, {
                    "area": area, "level": level, "relative": relative,
                    "modes": level_modes[level], "additional_modes": level_additional_modes[level],
                    "rows": [
                        # Primary case                  # Reference case
                        [("baseline", 2020, None),      None],
                        [("baseline", 2030, None),      None],
                        [("baseline", 2040, None),      None],
                        [("baseline", 2050, None),      None],
                        [("pravA", 2020, None),         None],
                        [("pravA", 2030, None),         None],
                        [("pravA", 2040, None),         None],
                        [("pravA", 2050, None),         None],
                        [("pravB", 2020, None),         None],
                        [("pravB", 2030, None),         None],
                        [("pravB", 2040, None),         None],
                        [("pravB", 2050, None),         None],
                        [("tavA", 2020, None),         None] if area != "ch" else None,
                        [("tavA", 2030, None),         None] if area != "ch" else None,
                        [("tavA", 2040, None),         None] if area != "ch" else None,
                        [("tavA", 2050, None),         None] if area != "ch" else None,
                        [("tavB", 2020, None),         None] if area != "ch" else None,
                        [("tavB", 2030, None),         None] if area != "ch" else None,
                        [("tavB", 2040, None),         None] if area != "ch" else None,
                        [("tavB", 2050, None),         None] if area != "ch" else None,
                    ]
                })))

                # Table comparing the scenarios with their yearly baseline
                tables.append(("%s_%s_distance_comparison_20XX_vs_20XX_baseline_%s" % (area, level, relative_name), make_table(df, {
                    "area": area, "level": level, "relative": relative,
                    "modes": level_modes[level], "additional_modes": level_additional_modes[level],
                    "rows": [
                        # Primary case                  # Reference case
                        [("baseline", 2020, None),      ("baseline", 2020, None)],
                        [("baseline", 2030, None),      ("baseline", 2030, None)],
                        [("baseline", 2040, None),      ("baseline", 2040, None)],
                        [("baseline", 2050, None),      ("baseline", 2050, None)],
                        [("pravA", 2020, None),         ("baseline", 2020, None)],
                        [("pravA", 2030, None),         ("baseline", 2030, None)],
                        [("pravA", 2040, None),         ("baseline", 2040, None)],
                        [("pravA", 2050, None),         ("baseline", 2050, None)],
                        [("pravB", 2020, None),         ("baseline", 2020, None)],
                        [("pravB", 2030, None),         ("baseline", 2030, None)],
                        [("pravB", 2040, None),         ("baseline", 2040, None)],
                        [("pravB", 2050, None),         ("baseline", 2050, None)],
                        [("tavA", 2020, None),         ("baseline", 2020, None)] if area != "ch" else None,
                        [("tavA", 2030, None),         ("baseline", 2030, None)] if area != "ch" else None,
                        [("tavA", 2040, None),         ("baseline", 2040, None)] if area != "ch" else None,
                        [("tavA", 2050, None),         ("baseline", 2050, None)] if area != "ch" else None,
                        [("tavB", 2020, None),         ("baseline", 2020, None)] if area != "ch" else None,
                        [("tavB", 2030, None),         ("baseline", 2030, None)] if area != "ch" else None,
                        [("tavB", 2040, None),         ("baseline", 2040, None)] if area != "ch" else None,
                        [("tavB", 2050, None),         ("baseline", 2050, None)] if area != "ch" else None,
                    ]
                })))

                # Table comparing the scenarios with 2020 baseline
                tables.append(("%s_%s_distance_comparison_20XX_vs_2020_baseline_%s" % (area, level, relative_name), make_table(df, {
                    "area": area, "level": level, "relative": relative,
                    "modes": level_modes[level], "additional_modes": level_additional_modes[level],
                    "rows": [
                        # Primary case                  # Reference case
                        [("baseline", 2020, None),      ("baseline", 2020, None)],
                        [("baseline", 2030, None),      ("baseline", 2020, None)],
                        [("baseline", 2040, None),      ("baseline", 2020, None)],
                        [("baseline", 2050, None),      ("baseline", 2020, None)],
                        [("pravA", 2020, None),         ("baseline", 2020, None)],
                        [("pravA", 2030, None),         ("baseline", 2020, None)],
                        [("pravA", 2040, None),         ("baseline", 2020, None)],
                        [("pravA", 2050, None),         ("baseline", 2020, None)],
                        [("pravB", 2020, None),         ("baseline", 2020, None)],
                        [("pravB", 2030, None),         ("baseline", 2020, None)],
                        [("pravB", 2040, None),         ("baseline", 2020, None)],
                        [("pravB", 2050, None),         ("baseline", 2020, None)],
                        [("tavA", 2020, None),         ("baseline", 2020, None)] if area != "ch" else None,
                        [("tavA", 2030, None),         ("baseline", 2020, None)] if area != "ch" else None,
                        [("tavA", 2040, None),         ("baseline", 2020, None)] if area != "ch" else None,
                        [("tavA", 2050, None),         ("baseline", 2020, None)] if area != "ch" else None,
                        [("tavB", 2020, None),         ("baseline", 2020, None)] if area != "ch" else None,
                        [("tavB", 2030, None),         ("baseline", 2020, None)] if area != "ch" else None,
                        [("tavB", 2040, None),         ("baseline", 2020, None)] if area != "ch" else None,
                        [("tavB", 2050, None),         ("baseline", 2020, None)] if area != "ch" else None,
                    ]
                })))

                for year in (2020, 2030, 2040, 2050):
                    tables.append(("%s_%s_distance_comparison_%d_vs_%d_baseline_%s" % (area, level, year, year, relative_name), make_table(df, {
                        "area": area, "level": level, "relative": relative,
                        "modes": level_modes[level], "additional_modes": level_additional_modes[level],
                        "rows": [
                            # Primary case                  # Reference case
                            [("baseline", year, None),      ("baseline", year, None)],
                            [("pravA", year, None),         ("baseline", year, None)],
                            [("pravB", year, None),         ("baseline", year, None)],
                            [("tavA", year, None),         ("baseline", year, None)] if area != "ch" else None,
                            [("tavB", year, None),         ("baseline", year, None)] if area != "ch" else None,
                        ]
                    })))

                    tables.append(("%s_%s_distance_comparison_%d_vs_2020_baseline_%s" % (area, level, year, relative_name), make_table(df, {
                        "area": area, "level": level, "relative": relative,
                        "modes": level_modes[level], "additional_modes": level_additional_modes[level],
                        "rows": [
                            # Primary case                  # Reference case
                            [("baseline", year, None),      ("baseline", 2020, None)],
                            [("pravA", year, None),         ("baseline", 2020, None)],
                            [("pravB", year, None),         ("baseline", 2020, None)],
                            [("tavA", year, None),         ("baseline", 2020, None)] if area != "ch" else None,
                            [("tavB", year, None),         ("baseline", 2020, None)] if area != "ch" else None,
                        ]
                    })))

                # Table comparing prav and tav
                if area != "ch":
                    tables.append(("%s_%s_distance_comparison_tav_vs_prav_%s" % (area, level, relative_name), make_table(df, {
                        "area": area, "level": level, "relative": relative,
                        "modes": level_modes[level], "additional_modes": level_additional_modes[level],
                        "rows": [
                            # Primary case                  # Reference case
                            [("tavA", 2030, None),         ("pravA", 2030, None)],
                            [("tavA", 2040, None),         ("pravA", 2040, None)],
                            [("tavA", 2050, None),         ("pravA", 2050, None)],
                            [("tavB", 2030, None),         ("pravB", 2030, None)],
                            [("tavB", 2040, None),         ("pravB", 2040, None)],
                            [("tavB", 2050, None),         ("pravB", 2050, None)],
                        ]
                    })))

    # Sensitivities
    for level in ("person", "vehicle"):
        for area in ("ch", "sa", "lu", "lg"):
            for relative in (True, False):
                relative_name = "relative" if relative else "absolute"

                for sensitivity in sensitivities.keys():
                    if sensitivity != "phase1":
                        pravA_name = "pravA"
                        pravB_name = "pravB"
                        tavA_name = "tavA"
                        tavB_name = "tavB"

                        if sensitivity == "sensitivity5":
                            pravA_name = "pravD"
                            pravB_name = "pravE"
                            tavA_name = "tavD"
                            tavB_name = "tavE"

                        # Table that shows distances for the sensitivities
                        tables.append(("%s_%s_distance_%s_%s" % (area, level, sensitivity, relative_name), make_table(df, {
                            "area": area, "level": level, "relative": relative,
                            "modes": level_modes[level], "additional_modes": level_additional_modes[level],
                            "rows": [
                                # Primary case                  # Reference case
                                [("baseline", 2020, sensitivity),      None],
                                [("baseline", 2050, sensitivity),      None],
                                [(pravA_name, 2020, sensitivity),         None],
                                [(pravA_name, 2050, sensitivity),         None],
                                [(pravB_name, 2020, sensitivity),         None],
                                [(pravB_name, 2050, sensitivity),         None],
                                [(tavA_name, 2020, sensitivity),         None] if area != "ch" else None,
                                [(tavA_name, 2050, sensitivity),         None] if area != "ch" else None,
                                [(tavB_name, 2020, sensitivity),         None] if area != "ch" else None,
                                [(tavB_name, 2050, sensitivity),         None] if area != "ch" else None,
                            ]
                        })))

                        # Table that shows changes to phase 1
                        tables.append(("%s_%s_distance_comparison_%s_vs_phase1_%s" % (area, level, sensitivity, relative_name), make_table(df, {
                            "area": area, "level": level, "relative": relative,
                            "modes": level_modes[level], "additional_modes": level_additional_modes[level],
                            "rows": [
                                # Primary case                  # Reference case
                                [("baseline", 2020, sensitivity),      ("baseline", 2020, "phase1")],
                                [("baseline", 2050, sensitivity),      ("baseline", 2050, "phase1")],
                                [(pravA_name, 2020, sensitivity),         ("pravA", 2020, "phase1")],
                                [(pravA_name, 2050, sensitivity),         ("pravA", 2050, "phase1")],
                                [(pravB_name, 2020, sensitivity),         ("pravB", 2020, "phase1")],
                                [(pravB_name, 2050, sensitivity),         ("pravB", 2050, "phase1")],
                                [(tavA_name, 2020, sensitivity),         ("pravA", 2020, "phase1")] if area != "ch" else None,
                                [(tavA_name, 2050, sensitivity),         ("pravA", 2050, "phase1")] if area != "ch" else None,
                                [(tavB_name, 2020, sensitivity),         ("pravB", 2020, "phase1")] if area != "ch" else None,
                                [(tavB_name, 2050, sensitivity),         ("pravB", 2050, "phase1")] if area != "ch" else None,
                            ]
                        })))

    for (name, table) in tables:
        table.to_csv("tables/%s.csv" % name, index = None, encoding = "latin1")



















#

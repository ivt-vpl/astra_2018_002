import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

df = pd.read_parquet("astra.parquet")
years = [2020, 2030, 2040, 2050]

for scenario, linestyle in zip(["A", "B"], ["-", "--"]):
    for year, color in zip([2030, 2040, 2050], ["C0", "C1", "C2"]):
        f = (df["scenario"] == scenario) & (df["year"] == year) & (df["case"] == "lu")

        fleet_size = df[f]["taxi_fleet_size"].values * 4
        values = df[f]["trips_av"].values * 4
        sorter = np.argsort(fleet_size)

        plt.plot(fleet_size[sorter], values[sorter], 'x', color = color, linestyle = linestyle)

for year, color in zip([2030, 2040, 2050], ["C0", "C1", "C2"]):
    plt.plot([np.nan, np.nan], [np.nan, np.nan], color = color, label = "Year %d" % year)

for scenario, linestyle in zip(["A", "B"], ["-", "--"]):
    plt.plot([np.nan, np.nan], [np.nan, np.nan], linestyle = linestyle, label = "Scenario %s" % scenario, color = "k")

plt.grid()
plt.legend(loc = "best")
plt.show()


exit()

distances_car_baseline = [
    sum([
        df[(df["year"] == year) & (df["case"] == "prav") & (df["scenario"] == "A")]["trips_car"].sum(),
        df[(df["year"] == year) & (df["case"] == "prav") & (df["scenario"] == "A")]["trips_prav3"].sum(),
        df[(df["year"] == year) & (df["case"] == "prav") & (df["scenario"] == "A")]["trips_prav4"].sum(),
        df[(df["year"] == year) & (df["case"] == "prav") & (df["scenario"] == "A")]["trips_prav5"].sum()
    ])
    for year in years]

distances_pt_baseline = [
    df[(df["year"] == year) & (df["case"] == "prav") & (df["scenario"] == "A")]["trips_pt"].sum()
    for year in years]

distances_car_prav = [
    sum([
        df[(df["year"] == year) & (df["case"] == "prav") & (df["scenario"] == "B")]["trips_car"].sum(),
        df[(df["year"] == year) & (df["case"] == "prav") & (df["scenario"] == "B")]["trips_prav3"].sum(),
        df[(df["year"] == year) & (df["case"] == "prav") & (df["scenario"] == "B")]["trips_prav4"].sum(),
        df[(df["year"] == year) & (df["case"] == "prav") & (df["scenario"] == "B")]["trips_prav5"].sum()
    ])
    for year in years]

distances_pt_prav = [
    df[(df["year"] == year) & (df["case"] == "prav") & (df["scenario"] == "B")]["trips_pt"].sum()
    for year in years]

distances_car_prav[0] = distances_car_baseline[0]
distances_pt_prav[0] = distances_pt_baseline[0]

plt.plot(years, distances_car_baseline, 'x-', color = "C0")
plt.plot(years, distances_pt_baseline, 'x-', color = "C1")

plt.plot(years, distances_car_prav, 'x--', color = "C0")
plt.plot(years, distances_pt_prav, 'x--', color = "C1")

plt.ylabel("# Trips")

plt.plot([np.nan, np.nan], [np.nan, np.nan], color = "C0", label = "Private Vehicle")
plt.plot([np.nan, np.nan], [np.nan, np.nan], color = "C1", label = "Public Transport")
plt.plot([np.nan, np.nan], [np.nan, np.nan], linestyle = "-", color = "k", label = "A")
plt.plot([np.nan, np.nan], [np.nan, np.nan], linestyle = "--", color = "k", label = "B")

plt.legend()
plt.show()

exit()

distances_car_baseline = [
    sum([
        df[(df["year"] == year) & (df["case"] == "baseline")]["trips_car"].sum(),
        df[(df["year"] == year) & (df["case"] == "baseline")]["trips_prav3"].sum(),
        df[(df["year"] == year) & (df["case"] == "baseline")]["trips_prav4"].sum(),
        df[(df["year"] == year) & (df["case"] == "baseline")]["trips_prav5"].sum()
    ])
    for year in years]

distances_pt_baseline = [
    df[(df["year"] == year) & (df["case"] == "baseline")]["trips_pt"].sum()
    for year in years]

distances_car_prav = [
    sum([
        df[(df["year"] == year) & (df["case"] == "prav") & (df["scenario"] == "B")]["trips_car"].sum(),
        df[(df["year"] == year) & (df["case"] == "prav") & (df["scenario"] == "B")]["trips_prav3"].sum(),
        df[(df["year"] == year) & (df["case"] == "prav") & (df["scenario"] == "B")]["trips_prav4"].sum(),
        df[(df["year"] == year) & (df["case"] == "prav") & (df["scenario"] == "B")]["trips_prav5"].sum()
    ])
    for year in years]

distances_pt_prav = [
    df[(df["year"] == year) & (df["case"] == "prav") & (df["scenario"] == "B")]["trips_pt"].sum()
    for year in years]

distances_car_prav[0] = distances_car_baseline[0]
distances_pt_prav[0] = distances_pt_baseline[0]

plt.plot(years, distances_car_baseline, 'x-', color = "C0")
plt.plot(years, distances_pt_baseline, 'x-', color = "C1")

plt.plot(years, distances_car_prav, 'x--', color = "C0")
plt.plot(years, distances_pt_prav, 'x--', color = "C1")

plt.ylabel("# Trips")

plt.plot([np.nan, np.nan], [np.nan, np.nan], color = "C0", label = "Private Vehicle")
plt.plot([np.nan, np.nan], [np.nan, np.nan], color = "C1", label = "Public Transport")
plt.plot([np.nan, np.nan], [np.nan, np.nan], linestyle = "-", color = "k", label = "Baseline")
plt.plot([np.nan, np.nan], [np.nan, np.nan], linestyle = "--", color = "k", label = "+ Private AVs")

plt.legend()
plt.show()

exit()


for scenario, linestyle in zip(["A", "B"], ["-", "--"]):
    for year, color in zip([2030, 2040, 2050], ["C0", "C1", "C2"]):
        f = (df["scenario"] == scenario) & (df["year"] == year) & (df["case"] == "lu")

        fleet_size = df[f]["taxi_fleet_size"].values * 4
        values = df[f]["av_trips"].values * 4
        sorter = np.argsort(fleet_size)

        plt.plot(fleet_size[sorter], values[sorter], 'x', color = color, linestyle = linestyle)

for year, color in zip([2030, 2040, 2050], ["C0", "C1", "C2"]):
    plt.plot([np.nan, np.nan], [np.nan, np.nan], color = color, label = "Year %d" % year)

for scenario, linestyle in zip(["A", "B"], ["-", "--"]):
    plt.plot([np.nan, np.nan], [np.nan, np.nan], linestyle = linestyle, label = "Scenario %s" % scenario, color = "k")

plt.grid()
plt.legend(loc = "best")
plt.show()
